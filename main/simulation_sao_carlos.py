import os
import numpy as np
import networkx as nx
from comorbuss import community
from comorbuss import (
    decisions,
    quarantines,
    vaccinations,
    services,
    diagnostics,
    reinfection,
)
from comorbuss import tools, Analysis, settings, visualizer
from comorbuss.aux_classes import RandomDistibution
from comorbuss.modules import extra_symptoms

#######################################################################################################
###### Definition of parameters needed to simulate propagation of a infectious disease in a city ######
#######################################################################################################

parameters = dict()
##### Randomness
parameters["random_seed"] = 1  # Seed for random number generator

##### Scenario
parameters["free_hours"] = [7.0, 22.0]
# Daily time period when population is allowed outside their homes

parameters["services_close"] = False
# Closes an opens services (requires a decision mechanic, see Lockdown and Closing of Services Parameters)

parameters["social_isolation"] = False
# A percentage of the population stays at home every day

parameters["lockdown"] = False
# Simulate lockdowns (requires a decision mechanic, see Lockdown and Closing of Services Parameters)

parameters["reduce_inf_prob_time"] = False

parameters["store_time_series"] = True

##### Resolution for this run
parameters["number_of_particles"] = 2000  # Number of persons in the simulation
parameters["number_of_days"] = 60  # Number of days  to be considered in the simulation
parameters["start_date"] = "2020-03-01"

use_stop_filter = False
if use_stop_filter:
    parameters["stop_filter"] = ("diagnosed_cumulative", ">=", "150")

load_demographics_from_file = False
city_name = "sao carlos"
federation_unit = "SP"

##### Demographic data (real city's input data)
if load_demographics_from_file:
    tools.load_demographics(city_name, federation_unit, parameters)
else:
    parameters["city_name"] = "Sao_carlos"  # Name of the city simulated in this run
    parameters["population_ages"] = [
        # Number of persons on each age group, age groups are:
        13005,  # 0-4 yo
        13828,  # 5-9 yo
        16170,  # 10-14 yo
        17023,  # 15-19 yo
        19253,  # 20-24 yo
        20522,  # 25-29 yo
        19068,  # 30-34 yo
        17048,  # 35-39 yo
        15903,  # 40-44 yo
        15765,  # 45-49 yo
        14347,  # 50-54 yo
        11152,  # 55-59 yo
        8672,  # 60-64 yo
        6480,  # 65-69 yo
        5465,  # 70-74 yo
        3917,  # 75-79 yo
        2426,  # 80-84 yo
        1328,  # 85-89 yo
        480,  # 90-94 yo
        88,  # 95-99 yo
        12,  # 100+ yo
    ]
    parameters["persons_per_home"] = 3.0  # Mean number of persons per house

# Load a graph with a real population familiar structure and use it to generate the synthetic population
load_population_graph = False
if load_population_graph:
    parameters["population_graph"] = nx.read_gexf(
        os.path.join(settings.DATA_DIREC, "example_dociciliar_network.gexf")
    )
    parameters["population_graph_age_attribute"] = "age"

##### Parameters for services customization
nursery = {
    "name": "Nursery",
    "number": 40,
    "workers": [
        (services.WORKERS_STUDENTS, {"population_fraction": 0.5, "age_groups": [0]}),
        (services.WORKERS_TEACHERS, {"number": 30}),
    ],
    "net_type": settings.NET_AIRBORNE,
    "net_par": {},
}
parameters["services"] = [
    (services.MARKETS, dict(number=253)),
    (services.HOSPITALS, dict(number=16)),
    (services.SCHOOLS, dict(number=160)),
    (services.RESTAURANTS, dict(number=740)),
]
simulate_nursery = True
simulate_generic_services = True
if simulate_nursery:
    parameters["services"].append((services.SCHOOLS, nursery))
if simulate_generic_services:
    parameters["services"].append((services.GENERIC, {"number": 3700}))


simulate_transport = False
if simulate_transport:
    parameters["transport"] = {
        "time_in_transport": -1,
        "public_transport_fraction": 0.5,
        "transport_size": 20,
        "transport_size_std": 5,
        "mean_contacts": 2,
    }

##### Infection parameters
# Currently there isn't a parameter for the mortality rate. As the program still thinks that recovered mean immune and deceased people are technically immune, there is no big problem for now, although this need to be fixed in the close future
parameters["inf0_perc"] = 0.01  # Initial percentage of the population that is infected
# parameters['inf0_perc'] = [.99, 0.01, .0, .0]                           # Initial percentage of the population that each state [S, E, I, R]
# parameters['inf0_perc_symp'] = [.5, .25, .25]                           # Initial percentage of the infectious population in each symptom [asymp, symp, severe]

parameters["inf_probability"] = 0.016
# Chance of a infectious particle infect a susceptible one
# parameters["inf_probability"] = RandomDistibution(
#     mean=-4.5,
#     sigma=0.5,
#     distribution="lognormal",
# )
parameters["home_network"] = {
    "inf_prob_weight": 1.0,
}  # A float that is used multiplying inf_probability for contacts happened at home
parameters["environment_network"] = {
    "inf_prob_weight": 1.0,
}  # A float that is used multiplying inf_probability for contacts happened at the environment layer
parameters["inf_incubation"] = 4.6  # days
# Mean length of time for an infected person to become infectious
# parameters["inf_incubation"] = RandomDistibution(
#     mean=1.1577502124482724,
#     sigma=0.8582611386364607,
#     distribution="lognormal",
# )
parameters["inf_duration"] = 8  # days
# Mean length of time after infection for a person to recover
# parameters["inf_duration"] = RandomDistibution(
#     mean=2.0491292307716185,
#     sigma=0.24622067706923975,
#     distribution="lognormal",
# )
parameters["inf_severe_duration"] = 14  # days
# Mean length of time after infection for a severe symptomatic person to recover
# parameters["inf_severe_duration"] = RandomDistibution(
#     mean=2.6245752231222124,
#     sigma=0.17018875693209617,
#     distribution="lognormal",
# )
parameters["inf_activation_time"] = 2  # days
# Length of time before the infectious person becomes symptomatic
# parameters["inf_activation_time"] = RandomDistibution(
#     mean=0.4291212817151636,
#     sigma=0.5633211929032236,
#     distribution="lognormal",
# )
parameters["inf_prob_sympt"] = 0.4
# Chance of an infected person to becomes symptomatic
# parameters["inf_prob_sympt"] = [
#     0.5,
#     0.5,
#     0.55,
#     0.55,
#     0.6,
#     0.6,
#     0.65,
#     0.65,
#     0.7,
#     0.7,
#     0.75,
#     0.75,
#     0.8,
#     0.8,
#     0.85,
#     0.85,
#     0.9,
#     0.9,
#     0.9,
#     0.9,
#     0.9,
# ]
parameters["inf_severe_sympt_prob"] = 0.08
# Chance of an infected person to develop severe symptoms
# parameters["inf_severe_sympt_prob"] = [
#     0.00050,  # 0-4 yo
#     0.00050,  # 5-9 yo
#     0.00165,  # 10-14 yo
#     0.00165,  # 15-19 yo
#     0.00720,  # 20-24 yo
#     0.00720,  # 25-29 yo
#     0.02080,  # 30-34 yo
#     0.02080,  # 35-39 yo
#     0.03430,  # 40-44 yo
#     0.03430,  # 45-49 yo
#     0.07650,  # 50-54 yo
#     0.07650,  # 55-59 yo
#     0.13280,  # 60-64 yo
#     0.13280,  # 65-69 yo
#     0.20655,  # 70-74 yo
#     0.20655,  # 75-79 yo
#     0.24570,  # 80-84 yo
#     0.24570,  # 85-89 yo
#     0.24570,  # 90-94 yo
#     0.24570,  # 95-99 yo
#     0.24570,  # 100+ yo
# ]
parameters["inf_severe_death_prob"] = 0.012
# Chance of an infected person to die instead of recover
# parameters["inf_severe_death_prob"] = [
#     0.00002,  # 0-4 yo
#     0.00002,  # 5-9 yo
#     0.00002,  # 10-14 yo
#     0.00002,  # 15-19 yo
#     0.00010,  # 20-24 yo
#     0.00010,  # 25-29 yo
#     0.00032,  # 30-34 yo
#     0.00032,  # 35-39 yo
#     0.00098,  # 40-44 yo
#     0.00098,  # 45-49 yo
#     0.00265,  # 50-54 yo
#     0.00265,  # 55-59 yo
#     0.00766,  # 60-64 yo
#     0.00766,  # 65-69 yo
#     0.02439,  # 70-74 yo
#     0.02439,  # 75-79 yo
#     0.08292,  # 80-84 yo
#     0.08292,  # 85-89 yo
#     0.16190,  # 90-94 yo
#     0.16190,  # 95-99 yo
#     0.16190,  # 100+ yo
# ]

parameters["inf_susceptibility"] = [
    0.34,
    0.34,
    0.34,
    1.0,
    1.0,
    1.0,
    1.0,
    1.0,
    1.0,
    1.0,
    1.0,
    1.0,
    1.0,
    1.47,
    1.47,
    1.47,
    1.47,
    1.47,
    1.47,
    1.47,
    1.47,
]

##### Diagnostics
parameters["diagnostics"] = [(diagnostics.SEROLOGICAL, {"test_delay": 1.0})]

##### Vaccination
parameters["vaccinations"] = []
# Vaccinate all not diagnosed citizens
vaccinate_all = False
if vaccinate_all:
    parameters["vaccinations"] += [(vaccinations.DIAG_NO, {"doses_per_day": 5000})]
# Vaccinate only senior citizens
vaccinate_seniors = False
if vaccinate_seniors:
    parameters["vaccinations"] += [(vaccinations.SENIORS, {"doses_per_day": 5000})]
# Vaccinate only adult citizens
vaccinate_adults = False
if vaccinate_adults:
    parameters["vaccinations"] += [(vaccinations.ADULTS, {"doses_per_day": 5000})]
# Vaccinate only child citizens
vaccinate_children = False
if vaccinate_children:
    parameters["vaccinations"] += [(vaccinations.CHILDRENS, {"doses_per_day": 5000})]
# Vaccinate hospitals workers
vaccinate_hopitals_workers = False
if vaccinate_hopitals_workers:
    parameters["vaccinations"] += [
        (vaccinations.HOSPITALS_WORKERS, {"doses_per_day": 100})
    ]
# Vaccinate markets workers
vaccinate_markets_workers = False
if vaccinate_markets_workers:
    parameters["vaccinations"] += [
        (vaccinations.MAKETS_WORKERS, {"doses_per_day": 100})
    ]

# Avoid loading module without use
if parameters["vaccinations"] == []:
    del parameters["vaccinations"]

vaccination_trial = False
if vaccination_trial:
    parameters["vaccination_trial"] = {
        "include_family": True,
        "exclude_target": False,
        "placebo_ratio": 0.5,
        "target_number": 150,
        "is_trial": True,
        "vaccination": (
            vaccinations.HOSPITALS_WORKERS,
            {
                "doses_per_day": 10000,
            },
        ),
        "diagnostics": (
            diagnostics.PCR,
            {
                "name": "Trial_diag",
                "filter_particles": (
                    "vaccination_attr",
                    "Hospitals workers",
                    "mask_in_trial",
                ),
                "retest_delay": 7.0,
                "allow_retest": "negative",
            },
        ),
    }
    parameters["stop_filter"] = (
        "self.vaccination_trial.diagnosed_cumulative[self.clk.step]",
        "==",
        "20",
    )

# parameters["challenges_statistics"] = {"by_age": True, "by_work": True}

use_reinfection = False
if use_reinfection:
    parameters["reinfection"] = {
        "immunity": {
            "inf_susceptibility": vaccinations.linear_decay,
        },
        "immunity_parameters": {
            "initial_value": 1.0,
            "decay_factor": 60  # RandomDistibution(
            #     low=1 / 90, high=1 / 120, distribution="uniform"
            # ),
        },
    }

# This module is still in development and the default parameters are arbitrary values
use_extra_symptoms = False
if use_extra_symptoms:
    parameters["extra_symptoms"] = extra_symptoms.TEST_SYMPTOMS

##### Quarantines
parameters["quarantines"] = [quarantines.HOSPITALIZATION]
# Quarantine symptomatic citizens to their homes
quarantine_symptomatic = True
if quarantine_symptomatic:
    parameters["quarantines"] += [quarantines.SYMPTOMS]
# Quarantine diagnosed citizens to their homes
quarantine_diagnosed = False
parameters['quarantines'] = [(quarantines.DIAGNOSTICS, {"filter_out": quarantines.OUT_CURED})]
if quarantine_diagnosed:
    parameters["quarantines"] += [(quarantines.DIAGNOSTICS, {"delay": 1.0})]
# Quarantine traced citizens to their homes for 14 days
quarantine_traced = False
if quarantine_traced:
    parameters["quarantines"] += [(quarantines.TRACING, {"filter_out": ("days", 14.0)})]

##### Tracing Parameters
parameters["tracing_percent"] = 0.5
# Percentage of the population with tracing capability

hospitals_workers_on_hotels = False
if hospitals_workers_on_hotels:
    parameters["services"].append(services.HOTELS)
    parameters["quarantines"].append(quarantines.HOSPITAL_WORKERS_ON_HOTELS)

disease_workers_on_hotels = False
if disease_workers_on_hotels and not hospitals_workers_on_hotels:
    parameters["services"].append(services.HOTELS)
    parameters["quarantines"].append(quarantines.HOSPITAL_DISEASE_WORKERS_ON_HOTELS)

##### Encounters limiting
parameters["limit_encounters"] = False
# Enables a filter that limits the number of encounters per particle per hour
parameters["limit_encounters_number"] = 10
# Maximum number of encounters per particle per hour

# Isolation parameters might be overridden by lockdown
# Children are still isolated at home when probability is zero, unless percentage is negative
use_example_isol_pct_time_series = False
if use_example_isol_pct_time_series:
    parameters["isol_pct_time_series"] = np.loadtxt(
        os.path.join(
            settings.CONFIGS_DIREC, "isolation_percent_time_series_example.txt"
        )
    )
else:
    parameters["isol_pct_time_series"] = 0.3
    # Time series of percentage of people isolated at home
parameters["isol_stay_prob"] = 0.6
# Probability of a person that stayed at home at a given day remain at home next day. For lockdown it may be interesting to use a 100% value

##### Lockdown and Closing of Services Parameters
# Lockdown will increase the social isolation percentage to lockdown_adhere_percent during the lockdown
parameters["lockdown_adhere_percent"] = 0.7
# Percentage of the population that respect the lockdown
parameters["lockdown_decision_offset"] = 3  # days
# Offset days to take action on start or stop lockdown
parameters["decision"] = decisions.BY_DIAGNOSTICS
parameters["decision_par_lockdown"] = {
    "start_frac": 0.02,
    "stop_frac": 0.01,
}
parameters["decision_par_services"] = {
    "start_frac": 0.01,
    "stop_frac": 0.005,
}

# if use_day_series is True, then services and lockdown will use a day series
# for the duration of the series. After that, normal decision (selected above)
# will take place
parameters["use_day_series"] = False
# The day series can either be provided inside service configuration or through a file.
# In this last case, the key service_close_day_series_config must state the path
# to the file, the start and the end day of the time series to be used in the
# provided file, which must be in csv format (columns are service names, and
# rows are the day series value (0 for close, 1 for open))
parameters["service_close_day_series_config"] = [
    settings.SERVICE_CLOSE_DAYS_FILE,
    1,
    1 + parameters["number_of_days"],
]
parameters["lockdown_day_series"] = -1

parameters["reduce_workers"] = False
parameters["reduce_workers_series"] = np.ones((60, 2))
parameters["reduce_workers_series"][10:30, 0] = 0.5

parameters["reduce_visitors"] = False
parameters["reduce_visitors_series"] = np.ones((60, 2))
parameters["reduce_visitors_series"][10:30, 0] = 0.5

parameters["show_warning_messages"] = True

run_visualizer = False
if run_visualizer:
    parameters["visualizer"] = {
        "size": (1500, 1600),
        "elements": [
            visualizer.elements.AgentsStates(),
            visualizer.elements.AgentsSymptoms(),
            # visualizer.elements.Split(
            #     visualizer.elements.AgentsPlacement(
            #         mask="self.comm.srv['Hospitals'].workers_ids"
            #     ),
            #     visualizer.elements.AgentsSymptoms(
            #         mask="self.comm.srv['Hospitals'].workers_ids"
            #     ),
            #     visualizer.elements.AgentsDiagnostics(
            #         mask="self.comm.srv['Hospitals'].workers_ids"
            #     ),
            #     label="Hospital workers",
            # ),
        ],
    }

#######################################################################################################
################ Simulation of propagation of a infectious disease in a simulated city ################
if __name__ == "__main__":
    comm = community(**parameters)
    comm.simulate()

    if comm.simOk:
        Rt = comm.compute_R0t()

        city = comm.parameters["city_name"].replace(" ", "_").lower()
        ##### Case identification for output
        out_direc = os.path.join(settings.OUT_DIR, comm.scenario_id)
        if os.path.isdir(out_direc) == False:
            os.makedirs(out_direc)
        if type(parameters["inf_probability"]) is RandomDistibution:
            inf_prob = "dist"
        else:
            inf_prob = "{:.2e}".format(parameters["inf_probability"])
        case = os.path.join(
            out_direc,
            city
            + "_N-{:}_NI0-{:}_infP-{}".format(
                parameters["number_of_particles"], parameters["inf0_perc"], inf_prob
            ),
        )

        #######################################################################################################
        ############################ Visualization of the result of the simulation ############################
        #######################################################################################################

        analysis = Analysis.from_comm(comm, to_store=settings.TO_STORE_TRACING)
        analysis.plot_SEIR(filename=case + ".pdf", events=True)
        analysis.plot_SEIR_stack(filename=case + "_stack.pdf")
        analysis.plot_R(filename=case + "_R.pdf")
        analysis.plot_symptomatic_states(filename=case + "_infected.pdf", events=True)
        analysis.plot_symptoms(filename=case + "_symptoms.pdf")
        analysis.plot_inf_source_symptoms(
            filename=case + "_inf_source_symptoms.pdf", pie=True
        )
        analysis.plot_infection_placement(filename=case + "_inf_placement.pdf")
        analysis.plot_encounters(
            filename=case + "_encounters.pdf",
            plot_per_day=True,
            events=True,
            events_type=settings.MSG_EVENT,
        )
        analysis.plot_encounters(
            filename=case + "_encounters.pdf",
            plot_per_day=True,
            log_y=True,
            events=True,
            events_type=settings.MSG_EVENT,
        )
        analysis.plot_services_visitors(filename="_visitors.pdf", time_window="week")
        analysis.plot_quarantine(filename=case + "_quarantine.pdf")
        analysis.plot_diagnosed(
            filename=case + "_diagnosed.pdf",
            cumulative=True,
            events=True,
            events_type=settings.MSG_EVENT,
        )
        analysis.plot_homes_infection_hist(filename=case + "_homes_inf_hist.pdf")
        if use_reinfection:
            analysis.plot_data_line(["mean_infection_count"])
            analysis.plot_naive(filename=case + "_naive.pdf")
        # analysis.plot_inf_tree(filename=case+"_inf_tree.pdf",
        #                        title="Infection tree N={:}, N_0={:}, inf_p={:}, scenario={:}".format(parameters['number_of_particles'],
        #                        parameters['inf0_perc'], parameters['inf_probability'], comm.scenario_id), color='placement')
        try:
            analysis.plot_secondary_infections_hist(
                filename=case + "_source_inf_hist.pdf"
            )
        except ValueError:
            print("[Warning] Not enough data to plot infections count histogram.")
        if parameters.get("social_isolation", False):
            analysis.plot_isolation_percentage(
                filename=case + "_isolation.pdf",
                events=True,
                events_type=settings.MSG_EVENT,
            )
        if type(parameters["inf_probability"]) is RandomDistibution:
            analysis.plot_inf_probability(
                filename=case + "_inf_prob.pdf", remove_zeros=True
            )
        if "vaccinations" in parameters:
            analysis.plot_vaccinated_day(filename=case + "_vaccinated.pdf")
        if simulate_transport:
            analysis.plot_user_in_pubtransp(filename=case + "_transport.pdf")
        if "extra_symptoms" in parameters:
            analysis.plot_extra_symptoms(filename=case + "_extra_symptoms.pdf")
        # analysis.plot_contact_age_group(filename=case+"_contact_age_group.pdf")
        # analysis.plot_encounters_age_group(filename=case+"_encounters_age_group.pdf")
        analysis.close()

        #######################################################################################################
        ####################### Save the result of the simulation in a external txt file ######################
        #######################################################################################################

        T = np.arange(comm.Nsteps) * comm.dt

        ##### Aggregate results in a daily time series and output to .csv file
        [susceptible, exposed, infectious, recovered] = tools.aggregate_results(
            1.0,
            24,
            T,
            comm.pop.susceptible,
            comm.pop.exposed,
            comm.pop.infectious,
            comm.pop.recovered,
        )
        tools.save_csv(case + ".csv", susceptible, exposed, infectious, recovered)

        ##### Save parameters in a json file
        # tools.save_parameters_json(parameters, case+"_parameters.json")

        ##### Save full results and parameters in a .hdf5 file
        tools.save_hdf5([comm], case + ".hdf5", to_store=settings.TO_STORE)
        # To store tracing information on the hdf5 file uncomment the line below
        # tools.save_hdf5([comm], case + '.hdf5', to_store=TO_STORE_TRACING, to_store_srvc=[], skip_defaults=True, try_append=True)
        # To store tracing of infectious particles information on the hdf5 file uncomment the line below
        # tools.save_hdf5([comm], case + '.hdf5', to_store=TO_STORE_TRACING_INF, to_store_srvc=[], skip_defaults=True, try_append=True)
        print("[Progress] Results stored at", os.path.join(os.getcwd(), out_direc))

    # # examples of contact matrices post processing
    # enc_prob_matrix = tools.plot_enc_pct_matrix(comm.pop.tracing, comm.pop.placement, PLC_HOME, title='Prob. of having encounters at Home / time step')
    # enc_prob_matrix = tools.plot_enc_pct_matrix(comm.pop.tracing, comm.pop.placement, PLC_STRT, title='Prob. of having encounters at the Environment / time step')
    # enc_prob_matrix = tools.plot_enc_pct_matrix(comm.pop.tracing, comm.pop.placement, comm.srv['Markets'].placement, title='Prob. of having encounters at Markets / time step')
    # enc_prob_matrix = tools.plot_enc_pct_matrix(comm.pop.tracing, comm.pop.placement, comm.srv['Hospitals'].placement, title='Prob. of having encounters at Hospitals / time step')
    # enc_prob_matrix = tools.plot_enc_pct_matrix(comm.pop.tracing, comm.pop.placement, comm.srv['Schools'].placement, title='Prob. of having encounters at School / time step')
    # enc_prob_matrix = tools.plot_enc_pct_matrix(comm.pop.tracing, comm.pop.placement, comm.srv['Restaurants'].placement, title='Prob. of having encounters at Restaurants / time step')
