import setuptools, os
from comorbuss import __version__ as version

with open("../README.md", "r") as fh:
    long_description = fh.read()

configs_files = [os.path.join("data", f) for f in list(os.walk("data"))[0][2]]

with open("requirements.txt", "r") as fh:
    require = fh.read()
    require = require.split("\n")

setuptools.setup(
    name="COMORBUSS",  # Replace with your own username
    version=version,
    author="ggoedert",
    author_email="ggoedert@gmail.com",
    description="COMORBUSS is a detailed agent-based stochastic simulator for disease propagation driven by social iterations, providing clear evaluation of the effect of each community component on viral spread.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://comorbuss.org/",
    packages=setuptools.find_packages(),
    data_files=[("data", configs_files)],
    classifiers=[
        "Development Status :: 4 - Beta",
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
    install_requires=require,
)
