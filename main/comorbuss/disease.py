from . import settings as S
import numpy as np
from numpy.random import default_rng
from numba import njit
from .tools import init_distribution
from .aux_classes import coefficient_container, RandomDistibution, mem_clear


class disease(coefficient_container, mem_clear):
    """This class stores the attributes that characterize a disease and
        the methods for its initialization.2

    Attributes:
        given_parameters (dict):
        parameters (dict):
        initOk (np.ndarray):
        simOk (bool):
        random_seed (int):
        randgencom (Generator):
    """

    # List of disease parameters/attributes, all of them will be initialized
    # with a vector of Nparticles with constant values, a distribution or
    # values per age group.
    dist_parameters = [
        ("inf_probability", S.DIST_CONST),
        ("inf_incubation", S.DIST_EXP),
        ("inf_duration", S.DIST_EXP),
        ("inf_severe_duration", S.DIST_EXP),
        ("inf_activation_time", S.DIST_CONST),
        ("inf_prob_sympt", S.DIST_CONST),
        ("inf_severe_sympt_prob", S.DIST_CONST),
        ("inf_severe_death_prob", S.DIST_CONST),
        ("inf_susceptibility", S.DIST_CONST),
    ]

    def __init__(self, parameters, pop):
        self.comm = pop.comm
        self.pop = pop
        self.event_log = pop.event_log
        self.clk = pop.clk
        self.parameters = {}

        self.rng = default_rng(parameters["random_seed"])
        self.pid = np.arange(self.pop.Nparticles)  # Particle index

        for param, param_type in self.dist_parameters:
            values = init_distribution(
                parameters[param], param, param_type, self.pop, self.rng, self.event_log
            )
            self.parameters[param] = parameters[param]
            setattr(self, param, values)

        self.inf_severe_death_prob /= self.inf_prob_sympt

    def update_attributes(self, mask=()):
        """Updates disease attributes for a subset of particles determined by a mask.

        Args:
            mask (np.array, optional): An boolean mask of size Nparticles or list of ids. Defaults to ().
        """
        for param, param_type in self.dist_parameters:
            values = getattr(self, param)
            values[mask] = init_distribution(
                self.parameters[param],
                param,
                param_type,
                self.pop,
                self.rng,
                self.event_log,
            )[mask]
            setattr(self, param, values)

    def mask_to_recovered(self, states, symptoms):
        """Generate a mask of newly recovered or deceased particles.

        Args:
            states (np.ndarray): States array for the current step.
            symptoms (np.ndarray): Symptoms array for the current step.

        Returns:
            np.array: Mask array for the newly recovered particles
        """
        # Check activated and infectious particles if the infection duration time elapsed
        mask_sympt = (
            np.isin(symptoms, [S.SYMPT_YES, S.SYMPT_NO])
            & (states == S.STATE_I)
            & (self.clk.time_since(self.pop.time_activated) >= self.inf_duration)
        )
        mask_severe = (
            (symptoms == S.SYMPT_SEVERE)
            & (states == S.STATE_I)
            & (self.clk.time_since(self.pop.time_activated) >= self.inf_severe_duration)
        )
        # Check death probability for severe particles
        probab = self.rng.uniform(
            0.0, 1.0, size=self.pop.Nparticles
        )  # Associate a random probability to every particle
        mask_deceased = mask_severe & (probab < self.inf_severe_death_prob)
        mask_recovered = mask_sympt | (mask_severe & ~mask_deceased)
        return mask_recovered, mask_deceased

    def mask_to_infectious(self, states):
        """Generate a mask of new infectious particles.

        Args:
            states (np.ndarray): States array for the current step.

        Returns:
            np.array: Mask array for the newly infectious particles
        """
        # Select exposed particles if the incubation duration has elapsed
        mask_infectious = (states == S.STATE_E) & (
            self.clk.time_since(self.pop.time_exposed) >= self.inf_incubation
        )
        return mask_infectious

    def mask_to_symptoms(self, states, symptoms, time_infectious):
        """Generates symptoms masks for newly activated particles.

        Args:
            states (np.ndarray): States array for the current step.
            symptoms (np.ndarray): Symptoms array for the current step.
            time_inf (np.ndarray): Array of the time on which each particle became infectious.

        Returns:
            np.array, np.array, np.array: mask_symptomatic, mask_asymptomatic, mask_severe
        """
        # Check not activated particles infectious if activation time elapsed
        mask_activate = (
            (states == S.STATE_I)
            & (symptoms == S.SYMPT_NYET)
            & (self.clk.time_since(time_infectious) >= self.inf_activation_time)
        )
        # Test for symptoms or no symptoms
        probab = self.rng.uniform(0.0, 1.0, size=self.pop.Nparticles)
        test = probab < self.inf_prob_sympt
        mask_symptomatic = mask_activate & test
        mask_asymptomatic = mask_activate & ~test
        # Test symptomatic for severe symptoms
        probab = self.rng.uniform(0.0, 1.0, size=self.pop.Nparticles)
        mask_severe = mask_symptomatic & (probab < self.inf_severe_sympt_prob)
        return mask_symptomatic, mask_asymptomatic, mask_severe

    @staticmethod
    @njit
    def mask_new_infections(
        possibly_infected,
        mask_particles,
        nparticles,
        inf_probability,
        inf_susceptibility,
        plc_weight,
        probab,
    ):
        """Generate a mask newly infected particles using probabilistic mechanics

        Args:
            possibly_infected (np.ndarray): Encounters array of Infectious particles with Susceptible particles

        Returns:
            np.array: Mask array of successful infections (not treated to filter multiple infection sources)
        """
        weights_row = inf_susceptibility[mask_particles] * plc_weight
        inf_prob_column = inf_probability[mask_particles].reshape((nparticles, 1))
        inf_prob_matrix = weights_row * inf_prob_column
        mask_new_infections = possibly_infected & (
            probab < inf_prob_matrix
        )  # Find a mask of new infection (possibly infected whose associated probability was lower than the probability of been infected)
        return mask_new_infections
