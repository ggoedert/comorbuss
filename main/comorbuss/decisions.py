from . import settings as S
import numpy as np

#%% Decision functions

### DO NOT CREATE DOCSTRINGS ON THE FUNCTIONS BELLOW, USE SIMPLE COMMENTS!!!


def to_start_hospilalization(comm, param, *_):
    Nqrntd_at_hosp = np.count_nonzero(comm.get_hospitalized_mask())
    return (Nqrntd_at_hosp / comm.Nparticles) > param["start_frac"]


def to_stop_hospitalization(comm, param, *_):
    Nqrntd_at_hosp = np.count_nonzero(comm.get_hospitalized_mask())
    return (Nqrntd_at_hosp / comm.Nparticles) < param["stop_frac"]


def to_start_diagnostic(comm, param, *_):
    if hasattr(comm.pop, "diag_states"):
        mask_diag_pos = (
            comm.pop.diag_states == S.DIAG_YES
        )  # Get mask for positively diagnosed particles
        mask_infectious = comm.pop.states == S.STATE_I
        mask_diagnosed = mask_diag_pos & mask_infectious
        Ndiagnosed = np.count_nonzero(mask_diagnosed)
        return (Ndiagnosed / comm.Nparticles) > param["start_frac"]
    else:
        comm.event_log(
            "Tried to use a diagnostic decision function without diagnostics module loaded.",
            S.MSG_WRNG,
        )


def to_stop_diagnostic(comm, param, *_):
    if hasattr(comm.pop, "diag_states"):
        mask_diag_pos = (
            comm.pop.diag_states == S.DIAG_YES
        )  # Get mask for positively diagnosed particles
        mask_infectious = comm.pop.states == S.STATE_I
        mask_diagnosed = mask_diag_pos & mask_infectious
        Ndiagnosed = np.count_nonzero(mask_diagnosed)
        return (Ndiagnosed / comm.Nparticles) < param["stop_frac"]
    else:
        comm.event_log(
            "Tried to use a diagnostic decision function without diagnostics module loaded.",
            S.MSG_WRNG,
        )


def to_start_symptoms(comm, param, *_):
    mask_sympt = (comm.pop.symptoms == S.SYMPT_YES) | (
        comm.pop.symptoms == S.SYMPT_SEVERE
    )
    percent_symp = np.count_nonzero(mask_sympt)
    return (percent_symp / comm.Nparticles) > param["start_frac"]


def to_stop_symptoms(comm, param, *_):
    mask_sympt = (comm.pop.symptoms == S.SYMPT_YES) | (
        comm.pop.symptoms == S.SYMPT_SEVERE
    )
    percent_symp = np.count_nonzero(mask_sympt)
    return (percent_symp / comm.Nparticles) < param["start_frac"]


def to_start_infections(comm, param, *_):
    percent_infected = np.count_nonzero(comm.pop.states == S.STATE_I)
    return (percent_infected / comm.Nparticles) > param["start_frac"]


def to_stop_infections(comm, param, *_):
    percent_infected = np.count_nonzero(comm.pop.states == S.STATE_I)
    return (percent_infected / comm.Nparticles) < param["stop_frac"]


def to_start_fixed_period(comm, param, *_):
    return param["start_day"] == comm.clk.today


def to_stop_fixed_period(comm, param, *_):
    return param["stop_day"] == comm.clk.today


def to_close_services(comm, param, srvc):
    if comm.clk.today < len(srvc.day_series):
        if srvc.day_series[comm.clk.today] >= 0:
            return srvc.day_series[comm.clk.today] == 0
    if srvc.close:
        return srvc.to_close(comm, param)
    else:
        return False


def to_reopen_services(comm, param, srvc):
    if comm.clk.today < len(srvc.day_series):
        if srvc.day_series[comm.clk.today] >= 0:
            return srvc.day_series[comm.clk.today] != 0
    if srvc.close:
        return srvc.to_open(comm, param)
    else:
        return False


def to_start_lockdown_day_series(comm, param, *_):
    if comm.clk.today > comm.lockdown_ds_len:
        if comm.lockdown_ds[comm.clk.today] >= 0:
            return comm.lockdown_ds[comm.clk.today] != 0
    return comm.lockdown_to_start2(comm, param)


def to_finish_lockdown_day_series(comm, param, *_):
    if comm.clk.today > comm.lockdown_ds_len:
        if comm.lockdown_ds[comm.clk.today] >= 0:
            return comm.lockdown_ds[comm.clk.today] == 0
    return comm.lockdown_to_stop2(comm, param)


#%% Decision related constants
BY_HOSPITALIZATION = 1
"""Decisions are taken by the percentage of hospitalized particles.

Args:
    start_frac (float): Fraction of the population hospitalized to start intervention.
        Default: 0.06
    stop_frac (float): Fraction of the population hospitalized to stop intervention.
        Default: 0.03
"""
BY_DIAGNOSTICS = 2
"""Decisions are taken by the percentage of diagnosed particles.

Args:
    start_frac (float): Fraction of the population diagnosed to start intervention.
        Default: 0.02
    stop_frac (float): Fraction of the population diagnosed to stop intervention.
        Default: 0.01
"""
BY_SYMPTOMATICS = 3
"""Decisions are taken by the percentage of symptomatic particles.

Args:
    start_frac (float): Fraction of the population symptomatic to start intervention.
        Default: 0.06
    stop_frac (float): Fraction of the population symptomatic to stop intervention.
        Default: 0.03
"""
BY_INFECTIOUS = 4
"""Decisions are taken by the percentage of infectious particles..

Args:
    start_frac (float): Fraction of the population infectious to start intervention.
        Default: 0.04
    stop_frac (float): Fraction of the population infectious to stop intervention.
        Default: 0.02
"""
FIXED_PERIOD = 5
"""Decisions are taken by a fixed period.

Args:
    start_day (float): Day of the simulation to start intervention.
        Default: 15
    stop_day (float): Day of the simulation to stop intervention.
        Default: 25
"""

DECISON_FUNCTIONS = {
    BY_HOSPITALIZATION: {
        "start": to_start_hospilalization,
        "stop": to_stop_hospitalization,
    },
    BY_DIAGNOSTICS: {
        "start": to_start_diagnostic,
        "stop": to_stop_diagnostic,
    },
    BY_SYMPTOMATICS: {
        "start": to_start_symptoms,
        "stop": to_stop_symptoms,
    },
    BY_INFECTIOUS: {
        "start": to_start_infections,
        "stop": to_stop_infections,
    },
    FIXED_PERIOD: {
        "start": to_start_fixed_period,
        "stop": to_stop_fixed_period,
    },
}

DEF_PAR_LCKD = {
    BY_HOSPITALIZATION: {
        "start_frac": 0.06,
        "stop_frac": 0.03,
    },
    BY_DIAGNOSTICS: {
        "start_frac": 0.02,
        "stop_frac": 0.01,
    },
    BY_SYMPTOMATICS: {
        "start_frac": 0.06,
        "stop_frac": 0.03,
    },
    BY_INFECTIOUS: {
        "start_frac": 0.04,
        "stop_frac": 0.02,
    },
    FIXED_PERIOD: {
        "start_day": 15,
        "stop_day": 25,
    },
}

DEF_PAR_SRV_CLOSE = {
    BY_HOSPITALIZATION: {
        "start_frac": 0.03,
        "stop_frac": 0.015,
    },
    BY_DIAGNOSTICS: {
        "start_frac": 0.01,
        "stop_frac": 0.005,
    },
    BY_SYMPTOMATICS: {
        "start_frac": 0.04,
        "stop_frac": 0.02,
    },
    BY_INFECTIOUS: {
        "start_frac": 0.03,
        "stop_frac": 0.015,
    },
    FIXED_PERIOD: {
        "start_day": 10,
        "stop_day": 30,
    },
}

DECISIONS_NAMES = {
    BY_HOSPITALIZATION: "decision by hospitalization",
    BY_DIAGNOSTICS: "decision by disnosed particles",
    BY_SYMPTOMATICS: "decisions by symptomaic partilces",
    BY_INFECTIOUS: "decisions by infectious particles",
    FIXED_PERIOD: "decisions by fixed period",
}
