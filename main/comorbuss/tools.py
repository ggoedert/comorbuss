import h5dict
import os
import sys
import traceback
import json
import time
import numpy as np
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
from . import settings as S
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.lines import Line2D
import scipy.integrate as integrate
from packaging.version import parse as version_parse
from scipy.optimize import minimize
from multiprocessing import Pool
from scipy.sparse import csr_matrix
from scipy.sparse import save_npz
from scipy.sparse import load_npz
from typing import Any, Union
from .aux_classes import RandomDistibution

myfont = {"size": 8}
matplotlib.rc("font", **myfont)


def init_distribution(dist, name, param_type, pop, rng, event_log):
    """Initialize values of a disease attribute, passed parameter can be a constant
        value, a list of values per age goup, a RandomDistribution object or a list
        of RandomDistribution goups per age group.

    Args:
        dist (float or list): Given parameter for the attribute.
        name (str): Name of the attribute.
        param_type (int): Marker for the default behavior for the attribute:

            - `S.DIST_CONT`: the constant value will be assined for all particles,
            - `S.DIST_EXP`: the value will be used in a exponential distribution to assign
                values for particles.
        pop (population): Population object.
        rng (np.random.Generator): Random number generator.
        event_log (callable): Event_log function.

    Returns:
        np.array: Generated values.
    """
    # If array is a single value remove it from array
    if len(dist.shape) == 0:
        dist = dist[()]
    # If dist is only one RandomDistribution object sample and return values
    if type(dist) is RandomDistibution:
        return dist.sample(rng, size=pop.Nparticles)
    else:
        dist = np.array(dist)
        # If dist have the same length of ALL_AGES generate values for each age_group
        if dist.shape == np.array(S.ALL_AGES).shape:
            values = np.zeros(pop.Nparticles)
            for i in S.ALL_AGES:
                mask_age = pop.ages == i
                values[mask_age] = init_distribution(
                    dist[i], name, param_type, pop, rng, event_log
                )[mask_age]
            return values
        # If dist has other shape print warning and gets first single value
        if dist.shape != ():
            event_log(
                "A value ({}) for {} doesn't have the appropriate shape (one value or"
                " one value or each age group), proceeding with first value.".format(
                    dist, name
                ),
                S.MSG_WRNG,
            )
            while dist.shape != ():
                dist = dist[0]
                dist = np.array(dist)
        # If dist is a single value return it for the whole population
        if param_type == S.DIST_CONST:
            return dist * np.ones(pop.Nparticles)
        elif param_type == S.DIST_EXP:
            return rng.exponential(dist, pop.Nparticles)


def efficacy_calc(
    cases_vacc: int,
    not_cases_vacc: int,
    cases_not_vacc: int,
    not_cases_not_vacc: int,
    return_limits: bool = False,
) -> float:
    """Calculates efficacy of a vaccine from the number of cases and not cases.

    Args:
        cases_vacc (int): Cases in the vaccinated population.
        not_cases_vacc (int): Not cases in the vaccinated population.
        cases_not_vacc (int): Cases in the not vaccinated population.
        not_cases_not_vacc (int): Not cases in the not vaccinated population.
        return_limits (bool): Return also upper and lower limits of efficacy.

    Returns:
        float: [description]
    """
    rv = cases_vacc / (cases_vacc + not_cases_vacc)
    rnv = cases_not_vacc / (cases_not_vacc + not_cases_not_vacc)
    rr = rv / rnv
    efficacy = 1 - rr
    if return_limits:
        rr_lower = np.exp(
            np.log(rr)
            - 1.96 * np.sqrt(((1 - rv) / cases_vacc) + ((1 - rnv) / cases_not_vacc))
        )
        rr_upper = np.exp(
            np.log(rr)
            + 1.96 * np.sqrt(((1 - rv) / cases_vacc) + ((1 - rnv) / cases_not_vacc))
        )
        efficacy_limts = (1 - rr_lower, 1 - rr_upper)
        return efficacy, efficacy_limts
    else:
        return efficacy


def function(f):
    """Checks if f is callable, if true returns it, else raise TypeError.

    Args:
        f (callable): To be tested
    """
    if callable(f):
        return f
    else:
        raise TypeError("{} is not callable.".format(f))


def check_h5dict_version():
    from . import __version__ as version

    if version_parse(h5dict.__version__) < version_parse("0.2.2"):
        raise ImportError(
            "COMORBUSS {} requires h5dict>=0.2.2, {} is installed, please run `python -m pip install -U 'h5dict>=0.2.2'`".format(
                version, h5dict.__version__
            )
        )


def normalize(l):
    """Normalizes a list of floats.

    Args:
        list (list): List of floats.

    Returns:
        list: Normalized list.
    """
    norm_f = np.sum(l)
    new_l = []
    for i in l:
        new_l.append(i / norm_f)
    return np.array(new_l)


def check_dir(path, complete_path=None):
    """Checks if a directory exists, if not recursively creates it.

    Args:
        path (str): Path to directory.
        complete_path (str): Should not be set by user, original path to be printed if
            an error is encountered.

    Raises:
        OSError: Unable to create directory.
    """
    complete_path = complete_path or path
    if not os.path.isdir(path):
        base_d, tail_d = os.path.split(path)
        if tail_d == "":
            raise OSError("Unable to create '{}' directory.".format(complete_path))
        check_dir(base_d, complete_path)
        os.mkdir(path)


def recursive_copy(
    object: Union[dict, list, tuple, Any]
) -> Union[dict, list, tuple, Any]:
    """Makes a recursive copy of a nested dict or list object.

    Args:
        object (Union[dict, list]): Object to be copied.

    Returns:
        Union[dict, list]: Copy of the original object.
    """
    if type(object) is dict:
        return {key: recursive_copy(value) for key, value in object.items()}
    elif type(object) is list:
        return [recursive_copy(value) for value in object]
    elif type(object) is tuple:
        return tuple(recursive_copy(value) for value in object)
    else:
        try:
            return object.copy()
        except AttributeError:
            return object


def e_dist(a, b, metric="euclidean"):
    """Distance calculation for 1D, 2D and 3D points using einsum
    : a, b   - list, tuple, array in 1, 2 or 3D form
    : metric - euclidean ('e', 'eu'...), sqeuclidean ('s', 'sq'...),
    :-----------------------------------------------------------------------
    """
    a = np.asarray(a)
    b = np.atleast_2d(b)
    a_dim = a.ndim
    b_dim = b.ndim
    if a_dim == 1:
        a = a.reshape(1, 1, a.shape[0])
    if a_dim >= 2:
        a = a.reshape(np.prod(a.shape[:-1]), 1, a.shape[-1])
    if b_dim > 2:
        b = b.reshape(np.prod(b.shape[:-1]), b.shape[-1])
    diff = a - b
    dist_arr = np.einsum("ijk, ijk->ij", diff, diff)
    if metric[:1] == "e":
        dist_arr = np.sqrt(dist_arr)
    dist_arr = np.squeeze(dist_arr)
    return dist_arr


def in_list_of_dicts(list, item, key="name", return_dict=False):
    """Checks if an item is contained in a list of dicts.

    Args:
        list (list): List to search.
        item (any): Item to be searched.
        key (str, optional): Key to search for item. If None will search on all keys of
            the dictionaries. Defaults to 'name'.
        return_dict (bool): Returns dict instead of True. Defaults to False.

    Returns:
        bool: True if item is found
    """
    for dict in list:
        if key != None:
            keys = [key]
        else:
            keys = dict.keys()
        for key in keys:
            if dict[key] == item:
                if return_dict:
                    return dict
                else:
                    return True
    return False


def filter_parse(
    f, comm, allowed_filters=[], comm_str="self.comm", allow_function=True
):
    """Get a tuple of comparative and boolean operations and transform it in a expression that
        can be evaluated.

    Args:
        f (tuple): Tuple with the operations.
        comm (community): Community object of the simulation.
        allowed_filters (str): List with allowed custom filters ("days", "tracing", "workers, "service", "diagnostic" or "vaccination").

    Returns:
        str, bool: Expression that can be evaluated., True if parse successful.
    """
    # Check if filter is a function
    if allow_function and callable(f):
        return f, True
    out = ""
    ok = True
    # Check is filter is days
    if type(f) is tuple and len(f) == 2 and type(f[0]) is str:
        if f[0].lower() == "days":
            if "days" in allowed_filters:
                try:
                    return (
                        "self.clk.time_since(self.pop.time_quarantined)>={}".format(
                            comm.clk.days_to_time(float(f[1]))
                        ),
                        ok,
                    )
                except:
                    pass
            else:
                comm.event_log("Days can not be used in this filter.", S.MSG_WRNG)
                ok = False
    # Check if filter is tracing
    if type(f) is tuple and len(f) == 3 and type(f[0]) is str:
        if f[0].lower() == "tracing":
            if "tracing" in allowed_filters:
                parsed_str, ok = filter_parse(
                    f[1], comm, allowed_filters, comm_str=comm_str
                )
                tracing_lenght = comm.clk.days_to_time(float(f[2]))
                tracing_steps = comm.clk.time_to_steps(tracing_lenght)
                comm.pop.enable_tracing(buffer_length=tracing_steps + 2)
                try:
                    return (
                        "self.pop.trace_particles({}, {})".format(
                            parsed_str, tracing_lenght
                        ),
                        ok,
                    )
                except:
                    pass
            else:
                comm.event_log("Tracing can not be used in this filter.", S.MSG_WRNG)
                ok = False
    # Check if filter is workers
    if type(f) is tuple and len(f) == 3 and type(f[0]) is str:
        if f[0].lower() == "workers":
            if "workers" in allowed_filters:
                try:
                    for wrkr in comm.srv[f[1]].workers_parameters:
                        if wrkr["name"] == f[2]:
                            return wrkr["id"], ok
                except:
                    pass
            else:
                comm.event_log("Workers can not be used in this filter.", S.MSG_WRNG)
                ok = False
    # Check if filter is service
    if type(f) is tuple and len(f) == 2 and type(f[0]) is str:
        if f[0].lower() == "service":
            if "service" in allowed_filters:
                try:
                    return comm.srv[f[1]].id, ok
                except:
                    pass
            else:
                comm.event_log("Service can not be used in this filter.", S.MSG_WRNG)
                ok = False
    # Check if filter is a vaccination
    if type(f) is tuple and len(f) == 2 and type(f[0]) is str:
        if f[0].lower() == "vaccination":
            if "vaccination" in allowed_filters and "vaccinations" in comm.modules:
                try:
                    return comm.vaccinations[f[1]].id, ok
                except KeyError:
                    pass
            else:
                comm.event_log(
                    "Vaccination can not be used in this filter.", S.MSG_WRNG
                )
                ok = False
    # Check if filter is a vaccination_attribute
    if type(f) is tuple and len(f) == 3 and type(f[0]) is str:
        if f[0].lower() == "vaccination_attr":
            if "vaccination_attr" in allowed_filters and hasattr(comm, "vaccinations"):
                try:
                    eval("comm.vaccinations[{}].{}".format(f[1], f[2]))
                    return "{}.vaccinations[{}].{}".format(comm_str, f[1], f[2]), ok
                except KeyError:
                    pass
            else:
                comm.event_log(
                    "Vaccination can not be used in this filter.", S.MSG_WRNG
                )
                ok = False
    # Check if filter is a diagnostic
    if type(f) is tuple and len(f) == 2 and type(f[0]) is str:
        if f[0].lower() == "diagnostic":
            if "diagnostic" in allowed_filters:
                for diag in comm.diagnostics:
                    if diag.name.lower() == f[1].lower():
                        return diag.id, ok
    # Check if filter is a diagnostic
    if type(f) is tuple and len(f) == 2 and type(f[0]) is str:
        if f[0].lower() == "module":
            if "module" in allowed_filters:
                if hasattr(comm, f[1]):
                    return "{}.{}".format(comm_str, f[1]), ok
            else:
                comm.event_log("Diagnostic can not be used in this filter.", S.MSG_WRNG)
                ok = False
    # Check if filter is isin
    if type(f) is tuple and len(f) == 3 and type(f[1]) is str:
        if f[1].lower() == "isin":
            try:
                eval("comm.pop.{}".format(f[0]))
                return "np.isin({}.pop.{}, {})".format(comm_str, f[0], list(f[2])), ok
            except:
                try:
                    eval("comm.{}".format(f[0]))
                    return "np.isin({}.{}, {})".format(comm_str, f[0], list(f[2])), ok
                except:
                    pass
    # If neither treat as comparative and boolean operations
    for item in f:
        if type(item) is tuple or type(item) is list:
            parsed_str, ok = filter_parse(
                item, comm, allowed_filters, comm_str=comm_str
            )
            out += "({})".format(parsed_str)
        else:
            # Test if current element can be interpreted as an population attribute
            try:
                eval("comm.pop.{}".format(item))
                item = "{}.pop.{}".format(comm_str, item)
            except:
                # Test if current element can be interpreted as a community attribute
                try:
                    eval("comm.{}".format(item))
                    item = "{}.{}".format(comm_str, item)
                except:
                    pass
            out += str(item)
    return out, ok


##### Data analysis functions
def aggregate_results(dt_old, dt_new, T, susceptible, exposed, infectious, recovered):
    nt = int(dt_new / dt_old)
    Tnew = np.zeros(int(len(T) / nt) + 1)
    recovered_new = np.copy(Tnew)
    exposed_new = np.copy(Tnew)
    infectious_new = np.copy(Tnew)
    susceptible_new = np.copy(Tnew)
    Tnew[0] = T[0]
    recovered_new[0] = recovered[0]
    exposed_new[0] = exposed[0]
    infectious_new[0] = infectious[0]
    susceptible_new[0] = susceptible[0]
    for i in range(len(Tnew) - 1):
        Tnew[i] = T[i * nt] / dt_new
        recovered_new[i] = recovered[i * nt]
        exposed_new[i] = exposed[i * nt]
        infectious_new[i] = infectious[i * nt]
        susceptible_new[i] = susceptible[i * nt]
    Tnew[-1] = T[-1] / dt_new
    recovered_new[-1] = recovered[-1]
    exposed_new[-1] = exposed[-1]
    susceptible_new[-1] = susceptible[-1]
    infectious_new[-1] = infectious[-1]
    return susceptible_new, exposed_new, infectious_new, recovered_new


def check_time_series(tracing, time_series, Nparticles=0):
    if isinstance(time_series, list):
        if time_series == []:
            if Nparticles == 0:
                for key in tracing[1]:
                    Nparticles = np.maximum(Nparticles, np.max(tracing[1][key]))
                Nparticles += 1
            return np.zeros((len(tracing), Nparticles), dtype=bool)
    return time_series


def get_enc_pct_matrix(
    tracing, Nparticles, time_series=[], query_label=0, time_slice=slice(1, None)
):
    """This function returns a percentage, for each entry of the matrix. The
    entry (i, j) represents the percentage of the time steps for which the
    particle i encounters particle j, if the time series match the query_label"""
    time_series = check_time_series(tracing, time_series, Nparticles)
    rows = []
    cols = []
    slice_size = 0
    slice_index_array = np.arange(len(tracing))
    # loop over time steps
    for step, dict in zip(slice_index_array[time_slice], tracing[time_slice]):
        slice_size += 1
        for key in dict:
            key_values = dict[key]
            mask_add = time_series[step, key_values] == query_label
            mask_add = mask_add | (time_series[step, key] == query_label)
            for i, v in enumerate(mask_add):
                if v:
                    rows.append(key)
                    cols.append(key_values[i])
    data = np.full(len(rows), 1.0 / slice_size)
    return csr_matrix((data, (rows, cols)), shape=(Nparticles, Nparticles)), len(rows)


def get_encounters_matrix_age_group(matrix, ages, ageGroups):
    """This function returns a matrix mtx_ageG which concentrates the entries
    of the provided matrix according to the age groups in ages"""
    mtx_ageG = np.zeros((len(ageGroups), len(ageGroups)))
    coo_mtx = matrix.tocoo()
    for ind, row in enumerate(coo_mtx.row):
        col = coo_mtx.col[ind]
        mtx_ageG[ages[row], ages[col]] += coo_mtx.data[ind]
    return mtx_ageG


def get_encounters_array_age_group(
    tracing, ages, ageGroups=S.ALL_AGES, dt=1, slice_rng=slice(1, None)
):
    """This function returns an array whose entry i is the average number of
    encounters that a particle in the i-th age group has with another particle
    along a day"""
    Nparticles = len(ages)
    [enc_pct_matrix, _] = get_enc_pct_matrix(tracing, Nparticles, [], 0, slice_rng)
    arr_ageG = np.zeros(len(ageGroups))
    mtx_ageG = get_encounters_matrix_age_group(enc_pct_matrix, ages, ageGroups)
    ageG_num = np.zeros(len(ageGroups))
    for ageGroup in ageGroups:
        ageG_num[ageGroup] = np.count_nonzero(ages == ageGroup)
        if ageG_num[ageGroup] > 0:
            arr_ageG[ageGroup] = np.sum(mtx_ageG[ageGroup, :]) / (
                dt * ageG_num[ageGroup] / 24
            )
    for row in ageGroups:
        for col in ageGroups:
            if mtx_ageG[row, col] > 0:
                mtx_ageG[row, col] /= dt * len(ages) / 24
    return arr_ageG, mtx_ageG


def get_contact_matrix_per_day(
    tracing, dt, Nparticles, time_series=[], condition=False, days_slice=slice(0, None)
):
    time_series = check_time_series(tracing, time_series, Nparticles)
    Nparticles = time_series.shape[1]
    days = np.arange(np.ceil(len(tracing) / (24 / dt)), dtype=int)
    contact_matrix = csr_matrix((Nparticles, Nparticles))
    ndays = 0
    for day in days[days_slice]:
        ndays += 1
        steps = slice(int(day * (24 / dt)), int((day + 1) * (24 / dt)), 1)
        [day_matrix, _] = get_enc_pct_matrix(
            tracing,
            Nparticles,
            time_series=time_series,
            query_label=condition,
            time_slice=steps,
        )
        mask_day_matrix = day_matrix > 0
        contact_matrix[mask_day_matrix] = contact_matrix[mask_day_matrix] + 1
    return contact_matrix / ndays


def get_contact_array_age_group(tracing, ages, dt, days_slice=slice(0, None)):
    ageGroups = S.ALL_AGES
    Nparticles = len(ages)
    pid = np.arange(Nparticles)
    contact_matrix = get_contact_matrix_per_day(
        tracing, dt, Nparticles, days_slice=days_slice
    )
    contact_array = np.zeros(len(ageGroups))
    contact_matrix_AG = np.zeros((len(ageGroups), len(ageGroups)))
    for i, age in enumerate(ageGroups):
        p_in_group = pid[ages == age]
        contact_array[i] = np.mean(np.sum(contact_matrix[p_in_group], axis=1))
        for j, agej in enumerate(ageGroups):
            p_in_another_group = pid[ages == agej]
            contact_matrix_AG[i, j] = np.sum(
                np.sum(contact_matrix[p_in_group, :][:, p_in_another_group])
            ) / len(ages)
    return contact_array, contact_matrix_AG


def get_mean_encounter_time_array_age_group(tracing, ages, dt):
    return (
        get_encounters_array_age_group(tracing, ages, dt=dt)[1]
        / get_contact_array_age_group(tracing, ages, dt)[1]
    )


def save_contact_matrix(contact_matrix, filename):
    save_npz(filename, contact_matrix)


def load_contact_matrix(filename, size=(5, 4)):
    contact_matrix = load_npz(filename)
    coo_mtx = contact_matrix.tocoo()
    plt.figure(figsize=size)
    plt.title("Contact matrix")
    ax = plt.gca()
    ax.set_xlim(0, coo_mtx.shape[0])
    ax.set_ylim(0, coo_mtx.shape[1])
    sc = plt.scatter(coo_mtx.row, coo_mtx.col, c=coo_mtx.data, s=1)
    plt.colorbar(sc)
    plt.show()
    return contact_matrix


def get_service_infection_percent(
    Nhomes, serv_per_id, inf_placement, placement_label=S.PLC_HOME
):
    inf_servs = np.zeros(Nhomes)
    tot_servs = np.zeros(Nhomes)
    for ind, home in enumerate(serv_per_id):
        tot_servs[home] += inf_placement[ind] != S.NO_INFEC
        inf_servs[home] += inf_placement[ind] == placement_label
    inf_servs_percent = []
    for ind, inf_home in enumerate(inf_servs):
        if tot_servs[ind] > 0:
            inf_servs_percent.append(inf_home / tot_servs[ind])
    return inf_servs, tot_servs, inf_servs_percent


def get_source_inf_count(inf_source, states, get_zero_infecters=False):
    # getting indices of sources and count of these sources
    [source_inf_ind, source_inf_count] = np.unique(inf_source, return_counts=True)
    source_inf_ind = source_inf_ind[2:].astype(int)
    source_inf_count = source_inf_count[2:]
    # getting indices of possible sources
    inds = np.arange(inf_source.shape[0])
    mask_possibly_infecters = (states != S.STATE_S) & (states != S.STATE_E)
    inds = inds[mask_possibly_infecters]
    mask_not_infecters = np.isin(inds, source_inf_ind, invert=True)
    inds = inds[mask_not_infecters]
    nonzeropercent = len(source_inf_ind) / (len(source_inf_ind) + len(inds))
    infectiouspercent = (len(source_inf_ind) + len(inds)) / inf_source.shape[0]
    # adding possible sources to actual sources
    if get_zero_infecters:
        source_inf_ind = np.concatenate((source_inf_ind, inds))
        source_inf_count = np.concatenate((source_inf_count, np.zeros(len(inds))))
    return source_inf_ind, source_inf_count, nonzeropercent, infectiouspercent


def fit_regression(x, y, lr):
    if lr > 0:
        cs = np.polyfit(x, y, lr)
        reg_curve = np.zeros(shape=x.size)
        for i in range(lr + 1):
            reg_curve += cs[i] * np.power(x, lr - i)
        return reg_curve
    else:
        lr = -lr
        cs = np.polyfit(x, 1.0 / y, lr)
        reg_curve = np.zeros(shape=x.size)
        for i in range(lr + 1):
            reg_curve += cs[i] * np.power(x, lr - i)
        return 1.0 / reg_curve


def is_numpy(a):
    return type(a).__module__ == np.__name__


def try_convert(s):
    if type(s) == str:
        if s.lower() == "true":
            return True
        elif s.lower() == "false":
            return False
        try:
            return int(s)
        except ValueError:
            try:
                return float(s)
            except ValueError:
                return s
    else:
        return s


def convert(l):
    if type(l) is list:
        r = []
        for s in l:
            r.append(try_convert(s))
        return r
    else:
        return try_convert(l)


def recursive_tolist(value: Any) -> Any:
    """Convert all numpy inside nested dicts, lists, tuples and sets to list.

    Args:
        value (Any): Nested dicts, lists, ..., with numpy inside.

    Returns:
        Any: Original structure with all numpy converted to list.
    """
    if is_numpy(value):
        return value.tolist()
    elif type(value) is dict:
        for k in value.keys():
            value[k] = recursive_tolist(value[k])
    elif type(value) is list:
        for i in range(len(value)):
            value[i] = recursive_tolist(value[k])
    elif type(value) in [tuple, set]:
        if type(value) is set:
            value = list(value)
            conv = set
        else:
            conv = tuple
        temp_list = []
        for i in range(len(value)):
            temp_list.append(recursive_tolist(value[i]))
        value = conv(temp_list)
    return value


##### Loading and saving functions
def save_parameters_json(
    parameters: dict[Any], filename: str, append: bool = False
) -> None:
    """Save a parameters dict on a json file.

    Args:
        parameters (dict[Any]): Parameters dict
        filename (str): JSON file path
        append (_type_, optional): Append to file (this can
            generate invalid json). Defaults to False:bool.
    """
    parameters = recursive_tolist(parameters)
    if append:
        mode = "a"
    else:
        mode = "w"
    with open(filename, mode) as f:
        json.dump(parameters, f, indent=5)


def load_parameters_json(filename):
    with open(filename, "r") as f:
        parameters = json.load(f)
    if type(parameters) is dict:
        for key in parameters.keys():
            parameters[key] = convert(parameters[key])
    return parameters


def load_demographics(city, state, parameters, filename=S.DEMO_DATA_FILE):
    """Loads population data and basic services information from database.

    Args:
        city (str): Name of the city.
        state (str): Two characters state abbreviation.
        parameters (dict): Parameters dictionary
        filename (str, optional): File with the csv database. Defaults to DEMO_DATA_FILE.
    """
    # Remove accents and convert to upper case city and state
    city_name = city
    city = city.upper()
    state = state.upper()
    loaded = False
    try:
        data = pd.read_csv(filename)
        loaded = True
    except:
        print(
            "["
            + S.MSG_TEXTS[S.MSG_WRNG]
            + "] Coudn't open demographic database from file: \""
            + filename
            + '". Proceeding with default demographic data'
        )
    if loaded:
        filter_city = data["CITY"].str.match(city)
        filter_state = data["STATE"].str.match(state)
        line = data[filter_city & filter_state]
        if len(line) == 1:
            parameters["city_name"] = city_name
            parameters["city_area"] = float(line["AREA_URB"].iloc[0])
            parameters["population_ages"] = [
                int(line["0 a 4 anos"].iloc[0]),  # 0-4 yo
                int(line["5 a 9 anos"].iloc[0]),  # 5-9 yo
                int(line["10 a 14 anos"].iloc[0]),  # 10-14 yo
                int(line["15 a 19 anos"].iloc[0]),  # 15-19 yo
                int(line["20 a 24 anos"].iloc[0]),  # 20-24 yo
                int(line["25 a 29 anos"].iloc[0]),  # 25-29 yo
                int(line["30 a 34 anos"].iloc[0]),  # 30-34 yo
                int(line["35 a 39 anos"].iloc[0]),  # 35-39 yo
                int(line["40 a 44 anos"].iloc[0]),  # 40-44 yo
                int(line["45 a 49 anos"].iloc[0]),  # 45-49 yo
                int(line["50 a 54 anos"].iloc[0]),  # 50-54 yo
                int(line["55 a 59 anos"].iloc[0]),  # 55-59 yo
                int(line["60 a 64 anos"].iloc[0]),  # 60-64 yo
                int(line["65 a 69 anos"].iloc[0]),  # 65-69 yo
                int(line["70 a 74 anos"].iloc[0]),  # 70-74 yo
                int(line["75 a 79 anos"].iloc[0]),  # 75-79 yo
                int(line["80 a 84 anos"].iloc[0]),  # 80-84 yo
                int(line["85 a 89 anos"].iloc[0]),  # 85-89 yo
                int(line["90 a 94 anos"].iloc[0]),  # 90-94 yo
                int(line["95 a 99 anos"].iloc[0]),  # 95-99 yo
                int(line["100 anos ou mais"].iloc[0]),  # 100+ yo
            ]
            parameters["persons_per_home"] = float(
                line["Mean number of persons per house"].iloc[0]
            )
            parameters["number_of_markets"] = int(line["Number of markets"].iloc[0])
            parameters["number_of_hospitals"] = int(line["Number of hospitals"].iloc[0])
            parameters["number_of_schools"] = int(line["Number of schools"].iloc[0])
            parameters["number_of_restaurants"] = int(
                line["Number of restaurants"].iloc[0]
            )
            print(
                "["
                + S.MSG_TEXTS[S.MSG_PRGS]
                + "] Loaded demographic data for "
                + city
                + "-"
                + state
            )
        elif len(line > 1):
            print(
                "["
                + S.MSG_TEXTS[S.MSG_WRNG]
                + "] More than one city found in demographic database. Proceeding with default demographic data"
            )
        else:
            print(
                "["
                + S.MSG_TEXTS[S.MSG_WRNG]
                + "] City not found in demographic database, check name and state. Proceeding with default demographic data"
            )
    return parameters


def save_csv(outfile, susceptible, exposed, infectious, recovered):
    data = {
        "day": np.arange(susceptible.size),
        "susceptible": susceptible,
        "exposed": exposed,
        "infectious": infectious,
        "recovered": recovered,
    }

    df = pd.DataFrame(data)
    df.to_csv(outfile)


def remove_duplicates(l):
    new = []
    for i in l:
        if not i in new:
            new.append(i)
    return new


def load_from_comms(
    comms,
    data,
    to_store=[],
    to_store_srvc=[],
    skip_defaults=False,
    cfilter={},
    append=False,
):
    # If skip_defaults is given will store only results in given list (to_store)
    if not skip_defaults:
        to_store += S.TO_STORE
        to_store_srvc += S.TO_STORE_SRVC
    to_store = remove_duplicates(to_store)
    to_store_srvc = remove_duplicates(to_store_srvc)
    # If no appending create some basic groups and store parameters
    if not append:
        data["parameters"] = recursive_copy(comms[0].given_parameters)
        data["triaged_parameters"] = recursive_copy(comms[0].parameters)
        data["realizations"] = {}
    # For every community given
    for comm in comms:
        to_store_comm = recursive_copy(to_store) + comm.TO_STORE
        to_store = remove_duplicates(to_store)
        data["realizations"][comm.random_seed] = {}
        rlz = data["realizations"][comm.random_seed]
        # Store results
        for result in to_store_comm:
            try:
                if result[2] and not type(rlz) is dict:
                    rlz[result[0], cfilter] = eval(result[1])
                else:
                    rlz[result[0]] = eval(result[1])
            except:
                print(
                    "[Warning] Error loading parameter: {} = {}".format(
                        result[0], result[1]
                    )
                )
        # Store services data
        if not "services" in rlz.keys():
            srv = []
            for srvc in comm.srv.all_services:
                srv.append({})
                for srvc_par in to_store_srvc:
                    try:
                        if srvc_par[2] and not type(rlz) is dict:
                            srv[-1][srvc_par[0], cfilter] = eval(srvc_par[1])
                        else:
                            srv[-1][srvc_par[0]] = eval(srvc_par[1])
                    except:
                        print(
                            "[Warning] Error loading parameter of {}: {} = {}".format(
                                srvc.name, srvc_par[0], srvc_par[1]
                            )
                        )
            rlz["services"] = srv
    return data


def save_hdf5(
    comms,
    out_file,
    to_store=[],
    to_store_srvc=[],
    skip_defaults=False,
    try_append=False,
    timeout=60,
):
    """Store simulation parameters and results in hdf5 files.

    Args:
        comms (list): A list of comunities to be stored (all comunities must have
            the same parameters and vary only the seed).
        out_file (string): Filename to save data.
        to_store (list, optional): Extras results to be stored, must follow
            `TO_STORE` format. Defaults to [].
        to_store_srvc (list, optional): Extra services data to be stored, must
            follow `TO_STORE_SRVC` format. Defaults to [].
        skip_defaults (bool, optional): Skip default to stores (`TO_STORE` and
            `TO_STORE_SRVC`). Defaults to False.
        try_append (bool, optional): Try to append to an existing file (file must have
            the same parameters). Defaults to False.
    """
    # If try_append check if file exists, if yes open it in append mode, else create the file
    if try_append and os.path.isfile(out_file):
        mode = "a"
        append = True
    else:
        mode = "w"
        append = False
    # Try to open the output file, retry up to 100000 times, another thread might be using same file
    ti = time.time()
    fopen = False
    while not fopen:
        try:
            out = h5dict.File(
                out_file, mode, fallback_mode="pickle"
            )  # conversions_filters=S.HDF5_CONVESIONS)
            fopen = True
        except:
            time.sleep(0.1)
            if (time.time() - ti) > timeout:
                raise
    if mode == "w":
        out["version"] = S.VERSION
    else:
        if out["version"] != S.VERSION:
            print(
                "[Error] Trying to append to a hdf5 file with a different version, file version is {}, current version is {}.".format(
                    out["version"], S.VERSION
                )
            )
            out.close()
            return None
    cfilter = {"compression": "lzf"}
    load_from_comms(comms, out, to_store, to_store_srvc, skip_defaults, cfilter, append)
    out.close()


def ask_for_file():
    import tkinter.filedialog as fd
    import tkinter as tk

    tkwindow = tk.Tk()
    filename = fd.askopenfilename(
        initialdir=S.OUT_DIR,
        title="Open data",
        filetypes=(("HDF5 Data", "*.hdf5"), ("all files", "*.*")),
    )
    tkwindow.destroy()
    return filename


def ask_retry(error_message):
    import tkinter.messagebox as mb
    import tkinter as tk

    tkwindow = tk.Tk()
    retry = mb.askretrycancel(
        title="Error", message="Error trying to open file.\n\n{}".format(error_message)
    )
    tkwindow.destroy()
    return retry


def load_hdf5(filename="", keep_open=False, version_check=True):
    """Loads simulation parameters and results from an hdf5 file.

    Args:
        filename (str, optional): Filename to load, if not given a prompt window
            will be shown to the user to select the file.

    Returns:
        dict, string: Loaded data and filename for the loaded file.
    """
    gotData = False
    retry = False
    # If filename is not given ask user to select a file
    if filename == "":
        filename = ask_for_file()
    data = dict()
    while not gotData:
        # Try to open the file and load data
        try:
            results = h5dict.File(
                filename,
                "r",
                fallback_mode="pickle",
                # , conversions_filters=S.HDF5_CONVESIONS,
            )
            if version_check:
                try:
                    if not results["version"] in S.COMPATIBLE_HDF5:
                        error_message = "Selected file is from COMORBUSS {} and not compatible with current version of COMORBUSS.".format(
                            results["version"]
                        )
                        raise RuntimeError("Wrong hdf5 version.")
                except KeyError:
                    error_message = "Selected file is from an old version of COMORBUSS (pre v0.3.0) and not compatible with current version of COMORBUSS."
                    raise
            if not keep_open:
                data = results.to_dict()
                results.close()
            gotData = True
        # If file can't be opened ask user again for file
        except:
            gotData = False
            for line in traceback.format_exception(*sys.exc_info()):
                # print(line)
                pass
            try:
                retry = ask_retry(error_message)
            except:
                retry = ask_retry(line)
            if retry:
                filename = ask_for_file()
        # if user clicks cancel exitis without data
        if not retry:
            if not gotData:
                results = {}
            break
    if keep_open:
        return results, filename
    else:
        return data, filename


def plot_enc_pct_matrix(
    tracing,
    time_series=[],
    query_label=0,
    slice_rng=slice(1, None),
    size=(5, 4),
    title="",
):
    """This function returns a probability matrix. The entry (i, j) of the matrix
    is the percentage of time steps in slice_rng for which: 1-there are
    encounters between particles i and j, and 2-the time_series for the step
    where the encounter ocurred matches the query_label. This matrix can be
    interpreted as the probability that one particle encounters another when
    time_series matches query_label"""
    if title == "":
        title = "Query Label {:}".format(query_label)
    [enc_pct_matrix, _] = get_enc_pct_matrix(
        tracing, time_series, query_label, slice_rng
    )
    coo_mtx = enc_pct_matrix.tocoo()
    plt.figure(figsize=size)
    plt.title(title)
    ax = plt.gca()
    ax.set_xlim(0, coo_mtx.shape[0])
    ax.set_ylim(0, coo_mtx.shape[1])
    sc = plt.scatter(coo_mtx.row, coo_mtx.col, c=coo_mtx.data, s=1)
    plt.colorbar(sc)
    plt.tight_layout(pad=0.5)
    plt.show()
    return enc_pct_matrix


def plot_xy(
    filename,
    x,
    data,
    labels,
    size=(5, 4),
    bands=False,
    multiple=False,
    lr=False,
    color=S.green,
    predicted=False,
):
    plt.figure(figsize=size)
    lines = []
    if lr and multiple:
        if lr == 2:
            labels[2].append("Qua. Reg.")
        elif lr == 1:
            labels[2].append("Lin. Reg.")
        elif lr == -1:
            labels[2].append("Inv. Reg.")
    if predicted and multiple:
        labels[2].append("Predicted")
    if multiple:
        # count the number of curves data
        count = len(data)
        # get the color of each set of data
        colors = plt.cm.cool(np.linspace(0.3, 0.8, count))
        # plot each curve
        for i in range(count):
            c = colors[i]
            if bands:
                plt.plot(x, data[i][0], color=c)
                lines.append(Line2D([0], [0], color=c, linewidth=2, linestyle="-"))
                plt.fill_between(
                    x,
                    data[i][0] - data[i][1],
                    data[i][0] + data[i][1],
                    color=c,
                    alpha=0.2,
                )
                plt.plot(x, data[i][2], color=c, ls=":")
                plt.plot(x, data[i][3], color=c, ls=":")
                if lr:
                    reg_curve = fit_regression(x, data[i][0], lr)
                    plt.plot(x, reg_curve, color=c, ls="--")
            else:
                plt.plot(x, data[i], color=c)
                if lr:
                    reg_curve = fit_regression(x, data[i], lr)
                    plt.plot(x, reg_curve, color=c, ls="--")
            if predicted and len(predicted) > 1:
                plt.plot(x, predicted[i], color=c, ls="-.")
        if lr:
            lines.append(Line2D([0], [0], color=S.black, linewidth=1, linestyle="--"))
        if predicted:
            if len(predicted) == 1:
                plt.plot(x, predicted[0], color=S.black, ls="-.")
            lines.append(Line2D([0], [0], color=S.black, linewidth=1, linestyle="-."))

    else:
        if bands:
            plt.plot(x, data[0], color=color)
            plt.fill_between(
                x, data[0] - data[1], data[0] + data[1], color=color, alpha=0.2
            )
            plt.plot(x, data[2], color=color, ls=":")
            plt.plot(x, data[3], color=color, ls=":")
            if lr:
                reg_curve = fit_regression(x, data[0], lr)
        else:
            plt.plot(x, data, color=color)
            if lr:
                reg_curve = fit_regression(x, data, lr)
        if lr:
            plt.plot(x, reg_curve, color=S.black, ls="--")
            lines.append(Line2D([0], [0], color=S.black, linewidth=1, linestyle="--"))
            if lr == 2:
                labels.append(["Qua. Reg."])
            elif lr == 1:
                labels.append(["Lin. Reg."])
            elif lr == -1:
                labels.append(["Inv. Reg."])

        if predicted:
            plt.plot(x, predicted, color=S.black, ls="-.")
            lines.append(Line2D([0], [0], color=S.black, linewidth=1, linestyle="-."))
            if lr:
                labels[2].append("Predicted")
            else:
                labels.append(["Predicted"])
    plt.xlabel(labels[0])
    plt.ylabel(labels[1])
    if multiple or lr:
        plt.legend(lines, labels[2], loc="upper right")
    plt.tight_layout(pad=0.5)
    plt.savefig(filename)
    plt.close()


##### SEIR related functions
def SEIR_d_seir(y, t, b, a, g):
    return np.array(
        [-b * y[0] * y[2], +b * y[0] * y[2] - a * y[1], a * y[1] - g * y[2], g * y[2]]
    )


def SEIR_simulate_seir(y0, b, a, g, T, dt):
    return integrate.odeint(
        func=SEIR_d_seir, y0=y0, t=np.arange(0, T, dt), args=(b, a, g)
    )


def SEIR_get_Ndays_SEIR(b, a, g, y0, Ndays=80):
    T, dt = Ndays * 24, 0.1
    simulated = SEIR_simulate_seir(y0, b, a, g, T, dt)
    m = np.argmax(simulated[:, 2]) * dt

    if T > 1.5 * m:
        return int(1.5 * m / 24) + 2
    else:
        return SEIR_get_Ndays_SEIR(b, a, g, y0, Ndays=2 * Ndays)


def SEIR_get_Ndays(
    inf_radius, inf_probability, inf_duration, inf_incubation, demo_dens, y0
):
    b = np.pi * inf_radius * inf_radius * demo_dens * inf_probability
    a = 1 / (inf_incubation * 24)
    g = 1 / (inf_duration * 24)

    Ndays = 1
    # evaluates a mesh close to this points
    for tb in np.arange(0.7, 1.4, 0.3) * b:
        for ta in np.arange(0.7, 1.4, 0.3) * a:
            for tg in np.arange(0.7, 1.4, 0.3) * g:
                aux_Ndays = SEIR_get_Ndays_SEIR(tb, ta, tg, y0)
                if aux_Ndays > Ndays:
                    Ndays = aux_Ndays

    return Ndays


def SEIR_loss(opt_params, params):
    y0, S, E, I, R, dt, T = params
    b, a, g = np.abs(opt_params)
    sim = SEIR_simulate_seir(y0, b, a, g, T, dt)
    to_use = np.all((sim > 0), axis=1)

    if np.sum(to_use * 1) > 0:
        err = np.abs(np.mean(np.divide(S[to_use], sim[:, 0][to_use])) - 1)
        err += np.abs(np.mean(np.divide(E[to_use], sim[:, 1][to_use])) - 1)
        err += np.abs(np.mean(np.divide(I[to_use], sim[:, 2][to_use])) - 1)
        err += np.abs(np.mean(np.divide(R[to_use], sim[:, 3][to_use])) - 1)
    else:
        err = 4
    return err


def SEIR_optimize_seir(S, E, I, R, dt, b0=0.01, a0=0.013, g0=0.008):
    x0 = np.array([b0, a0, g0])
    y0 = np.array([S[0], E[0], I[0], R[0]])
    params = [y0, S, E, I, R, dt, E.size * dt]
    optimal = minimize(SEIR_loss, x0, args=params, method="Nelder-Mead")
    return np.abs(optimal.x)


def SEIR_single_fit(param):
    S, E, I, R, dt = param
    return SEIR_optimize_seir(S, E, I, R, dt)


def SEIR_fit(to_fit, nproc=1):
    pool = Pool(processes=nproc)
    results = pool.map(SEIR_single_fit, to_fit)
    return np.stack(results, axis=0)


def is_notebook() -> bool:
    """Check if in a in a interactive window (notebook, vscode intective, etc).

    Returns:
        bool: True if in a inteactive window.
    """
    try:
        shell = get_ipython().__class__.__name__
        if shell == "ZMQInteractiveShell":
            return True  # Jupyter notebook or qtconsole
        elif shell == "TerminalInteractiveShell":
            return False  # Terminal running IPython
        else:
            return False  # Other type (?)
    except NameError:
        return False  # Probably standard Python interpreter
