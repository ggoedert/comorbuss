import random
from typing import Callable
import numpy as np
import networkx as nx
from numpy.random import default_rng
from .disease import disease
from .aux_classes import circularlist, harray, mem_clear
from . import settings as S
from .tools import normalize
from numba import njit

##### Lower level class: Population
class population(mem_clear):
    """This class stores the attributes that characterize a population and the
        methods for its initialization and progression.

    Attributes:
        Nparticles (int): Number of particles in the simulation.
        rng (Generator): Random number generator.
        random (np.random): Stores np.ramdon generator to use exclusively with NetworkX.
        disease (disease): Object with disease information and methods.
        homes (dict): Dictionary with information on every home.
        workersAvailable (list): List with available workers.
        max_encounters (int): Max number of encounter used on max_encounters filter.
        in_lckd (np.ndarray bool): Lockdown turned on or not for each step.
        tracing (list): List with the tracing for every step, tracing is stored as an
            dict of lists.
        tracing_infectious (list): List with the tracing of infectious particles for every
            step, tracing is stored as an dict of lists.
        infection_tree (Graph): Graph with disease spread information.

    #### Particles general attributes

    Attributes bellow describes every particle in the current step.

    Attributes:
        pid (np.ndarray int): Particle
        home_id (np.ndarray int): Id of the home for each particle, in the list of all homes.
        neighborhood (np.ndarray int) Id of the neighborhood for each particle.
        ages (np.ndarray int): Age group for each particle.
        placement (np.ndarray int): Placement marker for each particle. Placement tells where the
            particle is in the simulation (eg.: home, environment, services).
        activity (np.ndarray int): Activity marker for each particle. Activity tells what the
            particle is doing (eg.: free, working, visiting).
        has_tracing (np.ndarray bool): Mask for the tracing capability of every particle.
        workplace_id (np.ndarray int): Id of the service where particle works
        workplace_instance (np.ndarray int): Instance of the service where particles works.
        workplace_room (np.ndarray int): Room of the service particle works.
        worker_type (np.ndarray int): Type of worker for each particle.

    #### Particles states attributes

    Attributes:
        states (np.ndarray int): State of the infection for every particle.
        symptoms (np.ndarray int): State of symptoms for every particles.
        quarantine_states (np.ndarray int): Quarantine states for every particle.
        diag_states (np.ndarray int): Last diagnostic state for every particle.

    #### Particles infection information attributes

    Attributes:
        inf_source (np.ndarray int): Source of infection of each particle, pid of the particle
            that was responsible for the infection of the corresponding particle (NO_INFEC not
            infected, -1 initially infected).
        inf_placement (np.ndarray int): Placement of particles at time of infection (NO_INFEC ->
            not infected).
        inf_vector_sympt (np.ndarray int): Symptomatic state of the infection vector at the time
            the infection occurred (NO_INFEC -> no infection).
        time_exposed (np.ndarray float): Time in which a particle becomes exposed.
        time_infectious (np.ndarray float): Time in which a particle becomes infectious.
        time_recovered (np.ndarray float): Time in which a particle becomes recovered.
        time_quarantined (np.ndarray float): Time in which a particle was quarantined.
        time_traced (np.ndarray float): Time in which a particle was traced.

    ### Time series

    Attributes:
        states_ts (np.ndarray): Time series of states attribute (conditional to
            `store_time_series` parameter).
        symptoms_ts (np.ndarray): Time series of symptoms attribute (conditional to
            `store_time_series` parameter).
        quarantine_states_ts (np.ndarray): Time series of quarantine_states attribute (conditional
            to `store_time_series` parameter).
        diag_states_ts (np.ndarray): Time series of diag_states attribute (conditional to
            `store_time_series` parameter).
        positions_ts (np.ndarray): Time series of positions attribute (conditional to
            `store_time_series` parameter).
        placement_ts (np.ndarray): Time series of placement attribute (conditional to
            `store_time_series` parameter).
        susceptible (np.ndarray): Time series of the number of susceptible particles on each step.
        exposed (np.ndarray): Time series of the number of exposed particles on each step.
        infectious (np.ndarray): Time series of the number of infectious particles on each step.
        recovered (np.ndarray): Time series of the number of recovered particles on each step.
        deceased (np.ndarray): Time series of the number of deceased particles on each step.
        R0t (np.ndarray int): Cumulative mean of spreads on time, converging to Basic Reproduction Number (R0)
        spreads (np.ndarray int): At each time, number of particles infected by the particle with
            corresponding column index.
    """

    def __init__(self, parameters, comm, homes, neighborhood):
        self.comm = comm
        self.event_log = comm.event_log
        self.clk = comm.clk

        self.rng = default_rng(parameters["random_seed"])
        self.random = np.random
        self.random.seed(parameters["random_seed"])
        random.seed(parameters["random_seed"])

        self.Nparticles = parameters["Nparticles"]
        Nsteps = parameters["Nsteps"]
        self.pid = np.arange(self.Nparticles)
        np.random.default_rng
        self.home_id = homes
        self.homes_ids = np.sort(np.unique(homes))
        self.homes = self.gen_homes()
        self.neighborhood = neighborhood
        self.workplace_id = -1 * np.ones(self.Nparticles, dtype=int)
        self.workplace_instance = -1 * np.ones(self.Nparticles, dtype=int)
        self.workplace_room = -1 * np.ones(self.Nparticles, dtype=int)
        self.worker_type = -1 * np.ones(self.Nparticles, dtype=int)

        self.encounters_generators = []

        self.workersAvailable = np.copy(self.pid)
        if "net_ages" in parameters:
            self.ages = parameters["net_ages"]
        else:
            self.ages = self.allocate_ages(parameters)

        # See values in settings.py
        self.placement = S.PLC_HOME * np.ones(self.Nparticles, dtype=int)
        self.activity = S.ACT_FREE * np.ones(self.Nparticles, dtype=int)

        self.inf_source = harray(
            -1 * np.ones(self.Nparticles, dtype=int), buffer_limit=10
        )
        self.inf_placement = harray(
            S.NO_INFEC * np.ones(self.Nparticles), buffer_limit=10
        )
        self.inf_vector_sympt = harray(
            S.NO_INFEC * np.ones(self.Nparticles), buffer_limit=10
        )

        self.time_exposed = harray(
            S.NO_INFEC * np.ones(self.Nparticles), buffer_limit=10
        )
        self.time_infectious = harray(
            S.NO_INFEC * np.ones(self.Nparticles), buffer_limit=10
        )
        self.time_recovered = harray(
            S.NO_INFEC * np.ones(self.Nparticles), buffer_limit=10
        )
        self.time_activated = harray(
            S.NO_INFEC * np.ones(self.Nparticles), buffer_limit=10
        )
        self.time_quarantined = harray(np.zeros(self.Nparticles), buffer_limit=10)
        self.time_traced = harray(S.inf * np.ones(self.Nparticles), buffer_limit=10)
        self.time_diagnosed = harray(-1 * np.ones(self.Nparticles), buffer_limit=10)

        self.max_encounters = parameters["max_encounters"]
        self.filters = {
            "encounters": [],
            "possibly_infected": [],
            "all_new_infections": [],
            "new_infections": [],
        }
        if parameters["limit_encounters"]:
            self.add_filter(self.max_encounters_filter, "encounters")
        if parameters["check_encounters_simmetry"]:
            self.add_filter(self.check_simmetry_filter, "encounters")
            self.add_attribute(
                "asymmetric_counts", np.zeros(self.comm.Nsteps), store=True
            )
            self.add_attribute(
                "networks_counts", np.zeros(self.comm.Nsteps), store=True
            )
            self.net_sizes_counts = np.zeros(self.Nparticles, dtype=int)

        self.in_lckd = np.full(Nsteps, False)

        # For now virulence needs initial state to be exposed to work
        self.states, self.symptoms = self.init_states(
            parameters["inf0_perc"],
            parameters["inf0_perc_symp"],
            parameters["homes_inf0_frac"],
            parameters["homes_inf0_dict"],
        )
        self.init_states_times()
        self.initial_states = np.copy(self.states)
        self.initial_symptoms = np.copy(self.symptoms)
        mask_initial_not_susceptible = self.states != S.STATE_S
        self.disease = disease(parameters, self)
        self.quarantine_states = S.QRNT_FREE * np.ones(self.Nparticles)

        # Counters for states
        self.susceptible = np.zeros(Nsteps)
        self.exposed = np.zeros(Nsteps)
        self.infectious = np.zeros(Nsteps)
        self.recovered = np.zeros(Nsteps)
        self.deceased = np.zeros(Nsteps)

        # Counters for symptomatic states
        self.pre_symptomatic = np.zeros(Nsteps)
        self.asymptomatic = np.zeros(Nsteps)
        self.symptomatic = np.zeros(Nsteps)
        self.severe_symptomatic = np.zeros(Nsteps)

        # Cumulative data for stop conditions
        self.severe_cumulative = 0
        self.deceased_cumulative = 0
        self.infections_cumulative = 0
        self.symptoms_cumulative = 0

        self.inf_source[mask_initial_not_susceptible] = -1
        self.infection_tree = nx.DiGraph()
        attributes = {}
        for pid in self.pid:
            self.infection_tree.add_node(pid)
            attributes[pid] = {"age_group": self.ages[pid]}
        nx.set_node_attributes(self.infection_tree, attributes)
        self.empty_attributes = {p: {} for p in self.pid}
        self.spreads = np.zeros([Nsteps, self.Nparticles], dtype=int)
        self.challenges = np.zeros([Nsteps, self.Nparticles], dtype=int)
        self.encounters = np.zeros(Nsteps)
        self.encounters_infectious = np.zeros(Nsteps)
        self.possible_infections = np.zeros(Nsteps)
        self.has_tracing = self.gen_adhesion_mask(parameters["tracing_percent"])

        # Memory usage decisions
        # Store time series and complete tracing
        if parameters["store_time_series"]:
            self.states_ts = S.STATE_S * np.ones((Nsteps, self.Nparticles))
            self.states_ts[0, :] = self.states
            self.symptoms_ts = S.NO_INFEC * np.ones((Nsteps, self.Nparticles))
            self.symptoms_ts[0, :] = self.symptoms
            self.quarantine_states_ts = S.QRNT_FREE * np.ones((Nsteps, self.Nparticles))
            self.quarantine_states_ts[0, :] = self.quarantine_states
            self.placement_ts = np.zeros((Nsteps, self.Nparticles))
            self.placement_ts[0, :] = self.placement
            # Tracing related functions and attributes
            self.enable_tracing()
        # Don't store time series to save memory
        else:
            self.states_ts = self.states
            self.symptoms_ts = self.symptoms
            self.quarantine_states_ts = self.quarantine_states
            self.placement_ts = self.placement
            # Empty tracing related functions and attributes
            self.tracing_buffer_len = 1
            self.update_tracing = lambda *_: None
            self.gen_tracing = lambda *_: ([], [])
            self.get_tracing = lambda *_: []
            self.tracing = circularlist(size=self.tracing_buffer_len)
            self.tracing.append(({}, 0))
            self.tracing_infectious = circularlist(size=self.tracing_buffer_len)
            self.tracing_infectious.append(({}, 0))

        # Before infection functions:
        self.before_infection_functions = []

        # Update states masks
        self.mask_new_infected = np.zeros(self.Nparticles, dtype=bool)
        self.encounters_masks = []

        # history of contacts per placement
        self.plc_contact_history = {}

        self.R0t = np.zeros(Nsteps)
        # Calculate the count of states for the initial configuration
        self.count_cases()

    def __repr__(self):
        return "<COMORBUSS population {} particles>".format(self.Nparticles)

    def add_attribute(self, name, value, store=False, compress=False):
        """Adds an attribute to the population object.

        Args:
            name (str): Name of the attribute.
            value (Any): Initial value for the attribute.
            store (bool, optional): Select to store the attribute at the end of the
                simulation. Defaults to False.
            compress (bool, optional): Select if the attribute should be stored
                compressed or not. Defaults to False.
        """
        setattr(self, name, value)
        if store:
            self.comm.add_to_store(name, "comm.pop.{}".format(name), compress)

    def init_states(self, inf0, inf0_symp, homes_inf0_frac=0.5, homes_inf0_dict={}):
        """Create array of initial states and initial symptoms

        Args:
            states (np.ndarray): array of states initialized with susceptible
            symptoms (np.ndarray): arplotray of symptoms initialized with S.NO_INFEC
            inf0 (float): list of percentages of population in each state [S, E, I, R]
            inf0_symp (float): percentage of infectious population with each symptom
                [asymp, symp, severe]
        """
        if homes_inf0_frac > 0 and homes_inf0_frac <= 1:
            if homes_inf0_dict == {}:
                self.event_log("Initialized states using init_states_homes", S.MSG_PRGS)
                return self.init_states_homes(inf0, inf0_symp, homes_inf0_frac)
            else:
                self.event_log("Initialized states using homes_inf0_dict", S.MSG_PRGS)
                return self.init_states_cond_prob(
                    inf0, inf0_symp, homes_inf0_frac, homes_inf0_dict
                )
        else:
            return self.init_states_old(inf0, inf0_symp)

    def gen_value_by_age(self, values):
        attribute = np.ones(self.Nparticles)
        if type(values) in [int, float, np.int_, np.float64]:
            values = values * np.ones(len(S.ALL_AGES))
        if len(values) < len(S.ALL_AGES):
            new_values = []
            for i in range(len(S.ALL_AGES)):
                if i < len(values):
                    new_values.append(values[i])
                else:
                    new_values.append(values[-1])
            values = new_values
        for ageIndex, value in enumerate(values):
            mask_age = self.ages == ageIndex
            if type(value) in [int, float, np.int_, np.float64]:
                attribute[mask_age] = value
            elif type(value) is tuple:
                n_age = np.count_nonzero(mask_age)
                attribute[mask_age] = self.rng.choice(
                    value[0], size=n_age, p=normalize(value[1])
                )
            else:
                raise ValueError("Wrong value for a parameter by age: {}".format(value))
        return attribute

    def init_states_cond_prob(self, inf0, inf0_symp, homes_inf0_frac, homes_inf0_dict):
        # Initialize all particles with susceptible states and no symptoms
        states = S.STATE_S * np.ones(self.Nparticles, dtype=int)
        symptoms = S.NO_INFEC * np.ones(self.Nparticles)
        # Define useful symptom-related arrays
        symp0_f = normalize(np.array(inf0_symp))
        symp0_symptoms = [S.SYMPT_NO, S.SYMPT_YES, S.SYMPT_SEVERE]
        # Shuffle homes
        homes_ids = np.copy(self.homes_ids)
        self.rng.shuffle(homes_ids)
        nhomes = len(homes_ids)
        # Define various dictionaries to help processing
        st_dict = {"A": S.STATE_S, "B": S.STATE_E, "C": S.STATE_I, "D": S.STATE_R}
        str_dict = {S.STATE_S: "A", S.STATE_E: "B", S.STATE_I: "C", S.STATE_R: "D"}
        avail_st_dict = {
            S.STATE_S: np.round(self.Nparticles * np.array(inf0[0])),
            S.STATE_E: np.round(self.Nparticles * np.array(inf0[1])),
            S.STATE_I: np.round(self.Nparticles * np.array(inf0[2])),
            S.STATE_R: np.round(self.Nparticles * np.array(inf0[3])),
        }
        weight_dict = {S.STATE_E: 0, S.STATE_I: 1, S.STATE_R: 10}
        # Initialize homes with active infections
        home_i = 0
        while avail_st_dict[S.STATE_E] > 0 or avail_st_dict[S.STATE_I] > 0:
            if home_i >= nhomes:
                break
            # get particles in this home
            home_parts_mask = self.home_id == homes_ids[home_i]
            part_ids = self.pid[home_parts_mask]
            # TO DO: consider sampling according to susceptibility
            self.rng.shuffle(part_ids)
            nparts = np.size(part_ids)
            # make sure there are configurations of homes with this size
            if nparts in homes_inf0_dict.keys():
                dict = homes_inf0_dict[nparts]
                # store all possible configurations in keys
                keys = list(dict.keys())
                # remove configurations with states not available
                if avail_st_dict[S.STATE_S] <= 0:
                    keys = [
                        entry for entry in keys if (str_dict[S.STATE_S] not in entry)
                    ]
                if avail_st_dict[S.STATE_E] <= 0:
                    keys = [
                        entry for entry in keys if (str_dict[S.STATE_E] not in entry)
                    ]
                if avail_st_dict[S.STATE_I] <= 0:
                    keys = [
                        entry for entry in keys if (str_dict[S.STATE_I] not in entry)
                    ]
                if avail_st_dict[S.STATE_R] <= 0:
                    keys = [
                        entry for entry in keys if (str_dict[S.STATE_R] not in entry)
                    ]
                # get weights for each available configuration, and normalize them
                values = [dict[key] for key in keys]
                values = normalize(values)
                keys = np.array(keys)
                # initialize home
                if len(keys) > 0:
                    # randomly pick a configuration from values array
                    st_conf = self.rng.choice(keys, p=values)
                    for i in range(nparts):
                        # if the infection state proportion is fullfilled, let it susceptible
                        if avail_st_dict[st_dict[st_conf[i]]] == 0:
                            continue
                        # decrease count of available infection states
                        avail_st_dict[st_dict[st_conf[i]]] -= 1
                        # fill up state and symptom
                        if st_dict[st_conf[i]] != S.STATE_S:
                            states[part_ids[i]] = st_dict[st_conf[i]]
                            symptoms[part_ids[i]] = self.rng.choice(
                                symp0_symptoms, p=symp0_f
                            )
                            symptoms[part_ids[i]] *= weight_dict[st_dict[st_conf[i]]]
            home_i = home_i + 1
        # initialize homes with only susceptible or recovered people
        while (avail_st_dict[S.STATE_S] > 0) and (avail_st_dict[S.STATE_R] > 0):
            if home_i >= nhomes:
                break
            home_parts_mask = self.home_id == homes_ids[home_i]
            part_ids = self.pid[home_parts_mask]
            # TO DO: consider sampling according to susceptibility
            self.rng.shuffle(part_ids)
            nparts = np.size(part_ids)
            for i in range(int(round(np.minimum(homes_inf0_frac, 1.0) * nparts))):
                avail_st_dict[S.STATE_R] -= 1
                states[part_ids[i]] = S.STATE_R
                symptoms[part_ids[i]] = self.rng.choice(symp0_symptoms, p=symp0_f)
                symptoms[part_ids[i]] *= weight_dict[S.STATE_R]
            home_i = home_i + 1
        # provided error information
        if (
            (home_i == nhomes)
            or (avail_st_dict[S.STATE_E] > 0)
            or (avail_st_dict[S.STATE_I] > 0)
            or (avail_st_dict[S.STATE_R] > 0)
        ):
            self.event_log(
                "Error: could not initialize homes with requested "
                + "conditional probabilities. Either the dictionary "
                + "was not complete regarding the home sizes, or the "
                + "proportion of the compartiments could not be met: "
                + "home sizes={}, num. of particles missing initialization: {}".format(
                    homes_inf0_dict.keys(), avail_st_dict.values()
                ),
                S.MSG_ERROR,
            )
        return states, symptoms

    def init_states_homes(self, inf0, inf0_symp, homes_inf0_frac):
        # Initialize all particles with susceptible states
        states = S.STATE_S * np.ones(self.Nparticles, dtype=int)
        symptoms = S.NO_INFEC * np.ones(self.Nparticles)
        # Calculate the number of particles and the fractions to be initialized in each compartment
        inf0_n = np.round(self.Nparticles * np.array(inf0[1:]))
        inf0_f = normalize(inf0_n)
        nparticles = np.sum(inf0_n)  # number of particles to be initialized
        symp0_n = np.round(nparticles * np.array(inf0_symp))
        symp0_f = normalize(symp0_n)
        # Define states, symptoms and symptoms weights to assign
        inf0_states = [S.STATE_E, S.STATE_I, S.STATE_R]
        symp0_symptoms = [S.SYMPT_NO, S.SYMPT_YES, S.SYMPT_SEVERE]
        states_symp_weight = [0, 1, 10]
        # Initialize controls for the homes
        homes_ids = np.copy(self.comm.geo.homes_ids)
        self.rng.shuffle(homes_ids)
        # Generate random number for all particles and homes to facilitate pseudo randomness
        inf0_p = self.rng.uniform(size=self.Nparticles)
        symp0_p = self.rng.uniform(size=self.Nparticles)
        home_inf_p = self.rng.uniform(size=self.Nparticles)

        # Select homes to infect
        for home in homes_ids:
            mask_to_infect = (self.home_id == home) & (home_inf_p <= homes_inf0_frac)
            # Iterate on all symptoms
            for i in range(len(symp0_symptoms)):
                # Select particles in this house to assign this symptom
                mask_symptom = (
                    mask_to_infect
                    & (symp0_p >= np.sum(symp0_f[:i]))
                    & (symp0_p < np.sum(symp0_f[: i + 1]))
                )
                # Remove some particles if already have enough particles with this symptom
                while symp0_n[i] < np.sum(mask_symptom):
                    pid_to_exclude = self.pid[mask_symptom][-1]
                    mask_symptom[pid_to_exclude] = False
                    # move this particle to next symptom
                    symp0_p[pid_to_exclude] = np.sum(symp0_f[: i + 1])
                symptoms[mask_symptom] = symp0_symptoms[i]
                # Control the number of selected particles
                symp0_n[i] -= np.sum(mask_symptom)
            # Iterate on all states
            for i in range(len(inf0_states)):
                # Select particles in this house to assign this state
                mask_state = (
                    mask_to_infect
                    & (inf0_p >= np.sum(inf0_f[:i]))
                    & (inf0_p < np.sum(inf0_f[: i + 1]))
                )
                # Remove some particles if already have enough particles with this state
                while inf0_n[i] < np.sum(mask_state):
                    pid_to_exclude = self.pid[mask_state][-1]
                    mask_state[pid_to_exclude] = False
                    # move this particle to next state
                    inf0_p[pid_to_exclude] = np.sum(inf0_f[: i + 1])
                states[mask_state] = inf0_states[i]
                # Control the number of selected particles
                inf0_n[i] -= np.sum(mask_state)
                # Apply symptoms weights
                symptoms[mask_state] = symptoms[mask_state] * states_symp_weight[i]
            if np.sum(inf0_n) == 0 or np.sum(symp0_n) == 0:
                break
        if not (np.sum(inf0_n) == 0 or np.sum(symp0_n) == 0):
            self.event_log(
                "Error initializing infections, infected {} of particles in all homes but could not complete the fraction of infected particles asked in inf0_perc. Try increasing homes_inf0_frac or decreasing inf0_perc".format(
                    homes_inf0_frac
                ),
                S.MSG_ERROR,
            )
        return states, symptoms

    def init_states_old(self, inf0, inf0_symp):
        # Initialize all particles with susceptible states
        states = S.STATE_S * np.ones(self.Nparticles, dtype=int)
        symptoms = S.NO_INFEC * np.ones(self.Nparticles)
        # initialize the percentage of exposed
        E0id = []
        if inf0[1] >= 1.0 / self.Nparticles:
            E0N = int(inf0[1] * self.Nparticles)
            E0id = self.rng.choice(self.pid, E0N, replace=False)
            states[E0id] = S.STATE_E
            symptoms[E0id] = S.NO_INFEC
        mask_not_exposed = np.isin(self.pid, E0id, invert=True)
        # initialize the percentage of recovered
        R0id = []
        if inf0[3] >= 1.0 / self.Nparticles:
            R0N = int(inf0[3] * self.Nparticles)
            R0id = self.rng.choice(self.pid[mask_not_exposed], R0N, replace=False)
            states[R0id] = S.STATE_R
            symptoms[R0id] = S.NO_INFEC
        mask_not_recovered = np.isin(self.pid, R0id, invert=True)
        # initialize the percentage of infected
        I0id = []
        if inf0[2] >= 1.0 / self.Nparticles:
            I0N = int(inf0[2] * self.Nparticles)
            I0id = self.rng.choice(
                self.pid[mask_not_exposed & mask_not_recovered], I0N, replace=False
            )
            states[I0id] = S.STATE_I
            symptoms[I0id] = S.SYMPT_YES
            # index for looping over the infected states
            indI = 0
            # initializing asymptomatic
            Asym0N = int(inf0_symp[0] * I0N)
            for i in range(Asym0N):
                symptoms[I0id[i]] = S.SYMPT_NO
                indI = i
            # initializing severely symptomatic
            SySev0N = int(inf0_symp[2] * I0N)
            for i in range(indI + 1, indI + 1 + SySev0N, 1):
                symptoms[I0id[i]] = S.SYMPT_SEVERE
                indI = i
        return states, symptoms

    def init_states_times(self):
        mask_exposed = self.states == S.STATE_E
        mask_infectious = self.states == S.STATE_I
        mask_activated = mask_infectious & (self.symptoms != S.SYMPT_NYET)
        mask_recovered = self.states == S.STATE_R
        self.time_exposed[mask_exposed | mask_infectious | mask_recovered] = 0
        self.time_infectious[mask_infectious | mask_recovered] = 0
        self.time_activated[mask_activated | mask_recovered] = 0
        self.time_recovered[mask_recovered] = 0

    def allocate_ages(self, parameters):
        """Allocate ages to the population, giving priority so that kids don't live alone.

        Args:
            homes (list): List of the home ids for each particle.
            parameters (dict): Simulation parameters.

        Returns:
            np.ndarray: Age group of each particle.
        """
        nHomes = parameters["homesNumber"]
        ageGroups = parameters["ageGroups"]
        orderToAssign = parameters["ageAssignment"]
        homesId = np.arange(nHomes, dtype=int)
        unasignedParticles = np.copy(self.pid)
        unasignedPerHome = np.zeros(np.max(self.home_id) + 1, dtype=int)
        for i, v in zip(*np.unique(self.home_id, return_counts=True)):
            unasignedPerHome[i] = v
        if len(unasignedPerHome) < len(homesId):
            unasignedPerHome = np.pad(
                unasignedPerHome, (0, len(homesId) - len(unasignedPerHome))
            )
        ages = np.zeros(self.Nparticles, dtype=int)
        particlesPerGroup = np.rint(ageGroups * self.Nparticles).astype(int)
        for group in orderToAssign:
            for _ in range(0, particlesPerGroup[group]):
                if unasignedParticles.size:
                    homeId = self.rng.choice(
                        homesId,
                        p=(unasignedPerHome / sum(unasignedPerHome)),
                        replace=False,
                    )
                    maksUnasignedInHome = (self.home_id == homeId) & np.isin(
                        self.pid, unasignedParticles
                    )
                    particleId = self.rng.choice(
                        self.pid[maksUnasignedInHome], replace=False
                    )
                    ages[particleId] = group
                    unasignedParticles = unasignedParticles[
                        unasignedParticles != particleId
                    ]
                    unasignedPerHome[homeId] = unasignedPerHome[homeId] - 1
        return ages

    def gen_homes(self):
        """Generates a dictionary with homes information.

        Returns:
            dict: Dictionary with homes information.
        """
        homes = {}
        for home, pid in zip(self.home_id, self.pid):
            if not home in homes.keys():
                homes[home] = {"pids": []}
            homes[home]["pids"].append(pid)
        for home in homes.keys():
            homes[home]["pids"] = np.array(homes[home]["pids"])
            homes[home]["mask"] = np.isin(self.pid, homes[home]["pids"])
            homes[home]["nparticles"] = len(homes[home]["pids"])
            # n = len(homes[home]['pids'])
            # if n == 1:
            #     homes[home]['net'] = nx.Graph()
            #     homes[home]['net'].add_node(1)
            #     homes[home]['mask_encounters'] = np.zeros((1, 1), dtype=bool)
            # elif n > 1:
            #     homes[home]['net'] = nx.erdos_renyi_graph(n, self.home_net_par['mean_contacts']/(n-1), seed=self.random)
            #     homes[home]['mask_encounters'] = nx.to_numpy_matrix(homes[home]['net'], dtype=bool)
            # else:
            #     homes.pop('home', None)
        return dict(sorted(homes.items()))

    def print_homes_states(
        self, hide_home_id=False, hide_particle_id=False, symptoms=False
    ):
        for home in self.homes:
            if not hide_home_id:
                print("\nHome id: {}".format(home))
            str = ""
            for pid in self.homes[home]["pids"]:
                if not hide_particle_id:
                    str += " {}: ".format(pid)
                if symptoms:
                    str += "{}".format(S.SYMPT_CHAR[self.symptoms[pid]])
                else:
                    str += "{}".format(S.STATE_CHAR[self.states[pid]])
            print(str)

    def gen_adhesion_mask(self, percentage):
        """Create a boolean array indicating if particle of corresponding index is
            going to adhere to a given policy.

        Args:
            percentage (float): Percentage of the population to be chosen.

        Returns:
            np.ndarray: Array of booleans with the adhesion of each particle.
        """
        chosen = self.rng.choice(
            self.pid, int(self.Nparticles * percentage), replace=False
        )
        return np.isin(self.pid, chosen)

    def update_inf_tree_attributes(self, mask, attribute, value):
        """Stores information in the infection tree

        Args:
            mask (np.ndarray): Mask with particles to update.
            attribute (str): Name of the attribute to be stored.
            value (np.ndarray): Data to be stored.
        """
        attributes = {}
        attribute = "[{}]{}".format(self.clk.step, attribute)
        for p in self.pid[mask]:
            attributes[p] = {attribute: value}
        nx.set_node_attributes(self.infection_tree, attributes)

    def count_cases(self):
        """Count the number of cases at the last completed step of the time evolution"""
        # Counts the current number of particles in each state and stores the sum on
        # the respective attribute
        self.susceptible[self.clk.step] = np.count_nonzero(
            self.states == S.STATE_S, axis=0
        )
        self.exposed[self.clk.step] = np.count_nonzero(self.states == S.STATE_E, axis=0)
        self.infectious[self.clk.step] = np.count_nonzero(
            self.states == S.STATE_I, axis=0
        )
        self.recovered[self.clk.step] = np.count_nonzero(
            self.states == S.STATE_R, axis=0
        )
        self.deceased[self.clk.step] = np.count_nonzero(
            self.states == S.STATE_D, axis=0
        )

        self.pre_symptomatic[self.clk.step] = np.count_nonzero(
            self.symptoms == S.SYMPT_NYET, axis=0
        )
        self.asymptomatic[self.clk.step] = np.count_nonzero(
            self.symptoms == S.SYMPT_NO, axis=0
        )
        self.symptomatic[self.clk.step] = np.count_nonzero(
            self.symptoms == S.SYMPT_YES, axis=0
        )
        self.severe_symptomatic[self.clk.step] = np.count_nonzero(
            self.symptoms == S.SYMPT_SEVERE, axis=0
        )

    def mean_time_in_state(self, state):
        """Calculates the mean time for all population in a given state.

        Args:
            state (int): id of the state, available states: S.STATE_S: 0,
                S.STATE_E = 1 and S.STATE_I: 2

        Returns:
            float: Mean time in the given state.
        """
        if state == S.STATE_S:
            time = self.time_exposed
        elif state == S.STATE_E:
            time = self.time_infectious - self.time_exposed
        elif state == S.STATE_I:
            time = self.time_recovered - self.time_infectious
        else:
            print(
                "Invalid state, mean only available to S.STATE_S, S.STATE_E and S.STATE_I."
            )
            return 0
        maskNotInf = (time > S.ninf) & (time < S.inf)
        return np.mean(time[maskNotInf])

    def count_per_age_group(self, group):
        """Counts the number of particles in a age group

        Args:
            group (int): Id of the age group to be calculated

        Returns:
            int: Number of particles in the given age group
        """
        return self.ages[self.ages == group].size

    def mark_for_new_position(self, to_change, placement, activity):
        """Mark particles for changes in position

        Args:
            to_change (np.ndarray): Mask or list of pids for particles to be changed
            placement (int): New placement
            activity (int): New activity
        """
        self.placement[to_change] = placement
        self.activity[to_change] = activity

    def enable_tracing(self, buffer_length=None):
        """Enables tracing. To be used only during initialization.

        Args:
            buffer_length (int, optional): Number of steps of the required tracing.
                If None will store tracing for entire simulation. Defaults to None.
        """
        if buffer_length == None:
            self.tracing_buffer_len = None
            self.update_tracing = self.update_tracing_norm
            self.get_tracing = self.get_tracing_norm
            self.gen_tracing = self.gen_tracing_all
            self.tracing = [{} for _ in range(self.comm.Nsteps)]
            self.tracing_infectious = [{} for _ in range(self.comm.Nsteps)]
        elif self.tracing_buffer_len != None:
            self.update_tracing = self.update_tracing_buffer
            self.get_tracing = self.get_tracing_buff
            self.gen_tracing = self.gen_tracing_all
            if buffer_length >= self.tracing_buffer_len:
                self.tracing_buffer_len = buffer_length
                self.tracing = circularlist(size=self.tracing_buffer_len)
                self.tracing.append(({}, 0))
                self.tracing_infectious = circularlist(size=self.tracing_buffer_len)
                self.tracing_infectious.append(({}, 0))

    def get_tracing_norm(self, step, particle=None):
        if particle in self.tracing[step].keys():
            return self.tracing[step][particle]
        elif particle == None:
            return self.tracing[step]
        else:
            return []

    def get_tracing_buff(self, step, particle=None):
        i = step - self.clk.step - 1
        if particle in self.tracing[i][0]:
            return self.tracing[i][0][particle]
        elif particle == None:
            return self.tracing[i][0]
        else:
            return []

    def update_tracing_norm(self, tracing, new_trace):
        trace = new_trace[0]
        pids = new_trace[1]
        for i, pid in enumerate(pids):
            tracing[self.clk.step][pid] = trace[i]

    def update_tracing_buffer(self, tracing, new_trace):
        # Test if last item in buffer from last step
        if self.clk.step > tracing[-1][1]:
            # Append empty dictionary to buffer for current step
            tracing.append((dict(), self.clk.step))
        trace = new_trace[0]
        pids = new_trace[1]
        for i, pid in enumerate(pids):
            tracing[-1][0][pid] = trace[i]

    @staticmethod
    @njit
    def gen_tracing_all(mask_encounters, mask_particles, nparticles):
        """Trace all encounters for each particle
            mask_partipledof encounters to be traced

        Returns:
            dict: Lists of pid that each particle encountered
        """
        had_encounters = np.count_nonzero(mask_encounters, axis=1) > 0
        tracing = []
        tracing_pids = []
        ids = np.arange(nparticles)
        to_pid = np.where(mask_particles)[0]
        for i, pid in zip(ids[had_encounters], to_pid[had_encounters]):
            tracing.append([to_pid[i] for i in ids[mask_encounters[i, :]]])
            tracing_pids.append(pid)
        return tracing, tracing_pids

    def trace_particles(self, mask_origin, back_time):
        """Use tracing information to identify particles that had contact with particles in a mask.

        Args:
            mask_origin (np.ndarray): Mask with the particles search contacts.
            back_time (float): Window of time to search from current step.

        Returns:
            np.ndarray: Mask with traced particles.
        """
        mask_tracing = mask_origin & self.has_tracing
        if np.sum(mask_tracing) > 0:
            traced_particles = []
            trace_back_step = np.max(
                [0, self.clk.time_to_steps(self.clk.time - back_time)]
            )
            for i in range(trace_back_step, self.clk.step):
                for p in self.pid[mask_tracing]:
                    traced_particles.append(self.get_tracing(i, p))
            traced_particles = np.unique(np.concatenate(traced_particles, axis=0))
            mask_with_tracing = np.isin(traced_particles, self.pid[self.has_tracing])
            mask_traced = np.isin(self.pid, traced_particles[mask_with_tracing])
        else:
            mask_traced = mask_tracing
        return mask_traced

    def max_encounters_filter(self, mask_encounters, mask_particles):
        number_of_encounters = np.count_nonzero(np.triu(mask_encounters), axis=1)
        mask_max_encounters = number_of_encounters > self.max_encounters
        if np.count_nonzero(mask_max_encounters) > 0:
            ids = np.arange(mask_max_encounters.shape[0], dtype=int)
            for i in ids[mask_max_encounters]:
                chosen_encounters = self.rng.choice(
                    ids[mask_encounters[i, :]], self.max_encounters, replace=False
                )
                mask_encounters[i, :] = np.zeros(mask_encounters.shape[1], dtype=bool)
                mask_encounters[i, chosen_encounters] = True
            np.fill_diagonal(mask_encounters, False)
            mask_encounters = np.triu(mask_encounters)
            mask_encounters = mask_encounters + mask_encounters.T
        return mask_encounters

    def check_simmetry_filter(self, mask_encounters, _):
        m = np.array(mask_encounters, dtype=int)
        self.net_sizes_counts[mask_encounters.shape[0]] += 1
        if np.any(m - m.T):
            if self.asymmetric_counts[self.clk.step] == 0:
                self.event_log(
                    "One or more encounter matrix is asymmetric on this step, run "
                    "analysis.plot_data_line(['asymmetric_counts']) to see the count "
                    "of asymmetric matrices per step, and report to the developers",
                    S.MSG_ERROR,
                )
            self.asymmetric_counts[self.clk.step] += 1
        self.net_sizes_counts[mask_encounters.shape[0]] += 1
        self.networks_counts[self.clk.step] += 1
        return mask_encounters

    def add_net_generator(self, plc, net_generator):
        """Add a new encounters generator to be processed every step.

        Args:
            plc (int): Placement marker.
            net_generator (net_generator): An object with a gen method.
        """
        self.plc_contact_history[plc] = []
        self.encounters_generators.append((plc, net_generator))

    def gen_encounters_and_infections(self, attributes):
        """Generates encounters masks for each context."""
        # Empty list to store encounters masks
        self.encounters_masks = []
        mask_alive = self.states != S.STATE_D
        contacts = 0
        self.mask_new_infected[:] = False
        counts_accumulator = np.zeros(4)

        for plc, net_generator in self.encounters_generators:
            mask_particles = (self.placement == plc) & mask_alive
            nparticles = np.count_nonzero(mask_particles)
            if nparticles > 1:
                if hasattr(net_generator, "mask_new_infections"):
                    gen_new_inf = net_generator.mask_new_infections
                else:
                    gen_new_inf = self.disease.mask_new_infections
                for nparticles, mask_particles, mask_encounters in net_generator.gen(
                    mask_alive
                ):
                    contacts += np.count_nonzero(mask_encounters) / 2
                    counts_accumulator += self.process_encounters(
                        nparticles,
                        mask_particles,
                        mask_encounters,
                        plc,
                        net_generator.inf_prob_weight,
                        attributes,
                        gen_new_inf,
                    )
                if hasattr(net_generator, "srvc"):
                    net_generator.srvc.visitors_count[self.clk.step] = np.count_nonzero(
                        mask_particles
                        & ~np.isin(
                            self.pid,
                            np.concatenate(
                                (
                                    net_generator.srvc.workers_ids,
                                    net_generator.srvc.guests_ids,
                                )
                            ),
                        )
                    )
            self.plc_contact_history[plc] = self.plc_contact_history[plc] + [contacts]

        return counts_accumulator

    def process_encounters(
        self,
        nparticles,
        mask_particles,
        mask_encounters,
        plc,
        plc_inf_prob_weight,
        attributes,
        gen_new_inf,
    ):
        """Process encounters from an encounters mask.

        Args:
            nparticles (int): Number of particles in this context.
            mask_particles (np.ndarray): Mask of the particles in this context.
            mask_encounters (np.ndarray): Mask of nparticles x nparticles size
                with the encounters in this context.
            plc (int): Placement marker.
            plc_inf_prob_weight (float): Infection weight of the placement.
            attributes (dict): Attributes dict to be stored in the infection tree.
            gen_new_inf (function): Function to generate new infections.

        Returns:
            [int, int, int, int]: Counts of encounters, encounters with infectious,
                possible infections and new infections.
        """
        mask_new_infected = np.zeros(nparticles, dtype=bool)

        # Get infectious and susceptible particles in this context
        mask_infectious = self.states[mask_particles] == S.STATE_I
        mask_susceptible = self.states[mask_particles] == S.STATE_S
        # Apply filters
        mask_encounters = self.apply_filter(
            mask_encounters, mask_particles, "encounters"
        )
        self.update_tracing(
            self.tracing, self.gen_tracing(mask_encounters, mask_particles, nparticles)
        )
        ninfectious = int(np.count_nonzero(mask_infectious))
        possible_infections = 0
        encounters_infectious = 0
        new_infections = 0
        if ninfectious > 0:
            # Get a relation between index in this context and pids the the population
            to_pid = self.pid[mask_particles]
            # Get only encounters generated bu infectious particles
            mask_encounters_infectious = mask_encounters * mask_infectious.reshape(
                (nparticles, 1)
            )
            # Get only encounters to susceptible particles
            possibly_infected = (
                mask_susceptible.reshape((1, nparticles)) * mask_encounters_infectious
            )
            # Apply filters
            possibly_infected = self.apply_filter(
                possibly_infected, mask_particles, "possibly_infected"
            )
            self.update_tracing(
                self.tracing,
                self.gen_tracing(mask_encounters, mask_particles, nparticles),
            )
            # Trace encounters I -> S
            self.update_tracing(
                self.tracing_infectious,
                self.gen_tracing(possibly_infected, mask_particles, nparticles),
            )
            # Get new infections in this context from disease
            mask_new_infections = gen_new_inf(
                possibly_infected,
                mask_particles,
                nparticles,
                self.disease.inf_probability,
                self.disease.inf_susceptibility,
                plc_inf_prob_weight,
                self.disease.rng.uniform(0.0, 1.0, size=(nparticles, nparticles)),
            )
            # Apply filters
            mask_new_infections = self.apply_filter(
                mask_new_infections, mask_particles, "all_new_infections"
            )
            # Check for infection caused by multiple agents
            mask_have_multiple_infecters = (
                np.count_nonzero(mask_new_infections, axis=0) > 1
            )
            ids = np.arange(nparticles)
            for i in ids[mask_have_multiple_infecters]:
                chosen_source = self.rng.choice(ids[mask_new_infections[:, i]])
                mask_new_infections[:, i] = False
                mask_new_infections[chosen_source, i] = True
            # Apply filters
            mask_new_infections = self.apply_filter(
                mask_new_infections, mask_particles, "new_infections"
            )
            # Generate the mask of new infections on this context
            mask_new_infected = np.any(mask_new_infections, axis=0)
            ninf = np.count_nonzero(mask_new_infected)
            # Stores important information
            if ninf > 0:
                source, infected = mask_new_infections.nonzero()
                new_infections = ninf
                self.inf_source[to_pid[infected]] = to_pid[source]
                self.infection_tree.add_edges_from(
                    zip(to_pid[source], to_pid[infected])
                )
                nx.set_edge_attributes(
                    self.infection_tree,
                    {
                        edge: {
                            "[{}]infection_time".format(self.clk.step): self.clk.time
                        }
                        for edge in zip(to_pid[source], to_pid[infected])
                    },
                )
                for i in range(len(infected)):
                    attributes[to_pid[infected[i]]].update(
                        {
                            "[{}]inf_source".format(self.clk.step): to_pid[source[i]],
                            "[{}]time_exposed".format(self.clk.step): self.clk.time,
                            "[{}]inf_placement".format(self.clk.step): self.placement[
                                to_pid[infected[i]]
                            ],
                            "[{}]vector_symptoms".format(self.clk.step): self.symptoms[
                                to_pid[source[i]]
                            ],
                        }
                    )
                self.mask_new_infected[mask_particles] = mask_new_infected
                self.spreads[self.clk.step, mask_particles] = np.count_nonzero(
                    mask_new_infections, axis=1
                )
            possible_infections = np.count_nonzero(possibly_infected)
            encounters_infectious = np.count_nonzero(mask_encounters_infectious)
            self.challenges[self.clk.step, mask_particles] = np.count_nonzero(
                possibly_infected, axis=0
            )
        encounters = np.count_nonzero(mask_encounters) / 2

        return [encounters, encounters_infectious, possible_infections, new_infections]

    def apply_filter(self, mask_encounters, mask_particles, filter_type):
        for f in self.filters[filter_type]:
            mask_encounters = f(mask_encounters, mask_particles)
        return mask_encounters

    def add_filter(self, filter_function: Callable, filter_type: str) -> None:
        """Adds a contacts filter.

        Args:
            filter_function (Callable): Filter function, must accept two parameters:
                * mask_contacts: NxN mask of contacts between particles;
                * mask_particles: N mask of particles in this context.
            filter_type (str): Moment to apply filter:
                * `encounters`: Applies to the encounters matrix;
                * `possibly_infected`: Applies to the matrix of possibly infected particles.
                * `all_new_infections`: Applies to the matrix of new infections before removing
                    duplicate infections;
                * `new_infections`: Applies to the matrix of new infections.
        """
        if filter_type not in self.filters:
            raise ValueError(
                "{} filter type is not available see population.add_filter documentation.".format(
                    filter_type
                )
            )
        self.filters[filter_type].append(filter_function)

    def to_recovered(self, attributes):
        """Do states transitions: {Symptomatic, Asymptomatic} --> Recovered or deceased.

        Args:
            attributes (dict): Attributes dict to be stored in the infection tree."""
        # Check if any infected particle recovered in the last time step
        mask_recover, mask_deceased = self.disease.mask_to_recovered(
            self.states, self.symptoms
        )
        self.maskNotYetSympRecovered = (self.symptoms == S.SYMPT_NYET) & mask_recover
        self.states[mask_recover] = S.STATE_R
        self.states[mask_deceased] = S.STATE_D
        self.mark_for_new_position(mask_deceased, S.PLC_CEMT, S.ACT_deceased)
        mask_recover_n_deceased = mask_recover | mask_deceased
        self.symptoms[mask_recover_n_deceased] = (
            10 * self.symptoms[mask_recover_n_deceased]
        )
        self.symptoms[self.maskNotYetSympRecovered] = S.NO_INFEC
        self.time_recovered[mask_recover_n_deceased] = self.clk.time
        for p in self.pid[mask_deceased]:
            attributes[p].update(
                {"[{}]time_deceased".format(self.clk.step): self.clk.time}
            )
        for p in self.pid[mask_recover]:
            attributes[p].update(
                {"[{}]time_recovered".format(self.clk.step): self.clk.time}
            )

        # Stores important information
        self.deceased_cumulative += np.count_nonzero(mask_deceased)

    def to_infectious(self, attributes):
        """Do states transitions: Exposed --> Infectious.

        Args:
            attributes (dict): Attributes dict to be stored in the infection tree."""
        # Check if any particle already exposed becomes an infectious one
        mask_become_infectious = self.disease.mask_to_infectious(self.states)
        self.states[mask_become_infectious] = S.STATE_I
        self.time_infectious[mask_become_infectious] = self.clk.time
        self.symptoms[mask_become_infectious] = S.SYMPT_NYET
        for p in self.pid[mask_become_infectious]:
            attributes[p].update(
                {"[{}]time_infectious".format(self.clk.step): self.clk.time}
            )

    def to_symptoms(self, attributes):
        """Do states transitions: Infectious --> {Symptomatic, Asymptomatic}.

        Args:
            attributes (dict): Attributes dict to be stored in the infection tree."""
        # Check if after appropriate number of days, an infectious particle develops symptoms
        (
            mask_symptomatic,
            mask_asymptomatic,
            mask_severe,
        ) = self.disease.mask_to_symptoms(
            self.states, self.symptoms, self.time_infectious
        )
        self.symptoms[mask_symptomatic] = S.SYMPT_YES
        self.symptoms[mask_asymptomatic] = S.SYMPT_NO
        self.symptoms[mask_severe] = S.SYMPT_SEVERE
        self.time_activated[
            mask_symptomatic | mask_asymptomatic | mask_severe
        ] = self.clk.time
        for p in self.pid[mask_symptomatic]:
            attributes[p].update(
                {
                    "[{}]time_activated".format(self.clk.step): self.clk.time,
                    "[{}]symptoms".format(self.clk.step): S.SYMPT_YES,
                }
            )
        for p in self.pid[mask_asymptomatic]:
            attributes[p].update(
                {
                    "[{}]time_activated".format(self.clk.step): self.clk.time,
                    "[{}]symptoms".format(self.clk.step): S.SYMPT_NO,
                }
            )
        for p in self.pid[mask_severe]:
            attributes[p].update(
                {
                    "[{}]time_activated".format(self.clk.step): self.clk.time,
                    "[{}]symptoms".format(self.clk.step): S.SYMPT_SEVERE,
                }
            )

        # Stores important information
        self.severe_cumulative += np.count_nonzero(mask_severe)
        self.symptoms_cumulative += np.count_nonzero(mask_symptomatic & mask_severe)

    def to_exposed(self, attributes):
        """Do states transitions: Susceptible --> Exposed.

        Args:
            attributes (dict): Attributes dict to be stored in the infection tree."""
        # Execute before infection functions
        for f in self.before_infection_functions:
            f()

        # Iterate on all contexts encounters data
        counts = self.gen_encounters_and_infections(attributes)

        # Stores important information
        self.infections_cumulative += counts[3]
        self.time_exposed[self.mask_new_infected] = self.clk.time
        self.states[self.mask_new_infected] = S.STATE_E
        self.inf_placement[self.mask_new_infected] = self.placement[
            self.mask_new_infected
        ]
        self.inf_vector_sympt[self.mask_new_infected] = self.symptoms[
            self.inf_source[self.mask_new_infected]
        ]
        self.count_cases()
        self.possible_infections[self.clk.step] = counts[2]
        self.encounters[self.clk.step] = counts[0]
        self.encounters_infectious[self.clk.step] = counts[1]

    def update_states(self):
        """Update the state of the disease in every particle at the current time"""
        # Dictionary to store infection tree attributes
        attributes = dict(self.empty_attributes)
        self.to_recovered(attributes)
        self.to_infectious(attributes)
        self.to_symptoms(attributes)
        self.to_exposed(attributes)
        nx.set_node_attributes(self.infection_tree, attributes)
