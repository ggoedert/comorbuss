from .community import community
from .population import population
from .disease import disease
from . import tools, lab, settings
from .lab import Analysis, Simulation, SimulationAnalysis
from .settings import VERSION
from .modules import *

__version__ = VERSION

tools.check_h5dict_version()
