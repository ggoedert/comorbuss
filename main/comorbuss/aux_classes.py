from datetime import datetime
import time
from typing import Any, Union, List

import numpy as np
from numba import njit
import networkx as nx


TO_DELETE = [np.ndarray, int, float, bool, nx.Graph, nx.DiGraph]


class mem_clear:
    def clear(self, parents=[]):
        processed = []
        while True:
            for i in range(len(dir(self))):
                if dir(self)[i] not in processed:
                    break
                if i + 1 == len(dir(self)):
                    return None
            attr_name = dir(self)[i]
            processed.append(attr_name)
            if not (attr_name[:2] == "__" and attr_name[-2:] == "__"):
                if (
                    isinstance(getattr(self, attr_name), mem_clear)
                    and not getattr(self, attr_name) in parents
                ):
                    getattr(self, attr_name).clear(parents + [self])
                    delattr(self, attr_name)
                elif hasattr(getattr(self, attr_name), "close"):
                    getattr(self, attr_name).close()
                    delattr(self, attr_name)
                elif type(getattr(self, attr_name)) in TO_DELETE:
                    delattr(self, attr_name)
                # elif not callable(getattr(self, attr_name)):
                #     print(
                #         self.__class__.__name__,
                #         attr_name,
                #         type(getattr(self, attr_name)),
                #     )


class RandomDistibution(mem_clear):
    """Stores a random number generator and distribution parameters make and samples from it."""

    def __init__(
        self,
        *args,
        distribution="uniform",
        lower_limit=None,
        upper_limit=None,
        **kwargs
    ) -> None:
        """Stores a random number generator and distribution parameters make and samples from it.

        Args:
            *args: Arguments to be passed to the distribution on sample.
            distribution (str, optional): Any distribution from numpy.random. Defaults to 'uniform'.
            lower_limit (float): Lower limit for samples. Samples below lower_limit will be replaced by it.
            upper_limit (float): Upper limit for samples. Samples above upper_limit will be replaced by it.
            **kwargs: Keyword arguments to be passed to the distribution on sample.
        """
        self.dist = distribution
        self.args = args
        self.limits = (lower_limit, upper_limit)
        self.kwargs = kwargs
        self.factor = 1.0

    def __repr__(self) -> str:
        repr_str = "{} distribution".format(self.dist)
        if len(self.args) > 0:
            repr_str += ", args={}".format(self.args)
        if len(self.kwargs) > 0:
            repr_str += ", kwargs={}".format(self.kwargs)
        if self.limits[0] != None or self.limits[1] != None:
            repr_str += ", limits={}".format(self.limits)
        return repr_str + "."

    def sample(self, rng, size: int = 1) -> float:
        """Sample values from distribution.

        Args:
            size (int, optional): Size to be sampled. Defaults to 1.

        Returns:
            float: Samples.
        """
        dist = getattr(rng, self.dist)
        samples = dist(size=size, *self.args, **self.kwargs) * self.factor

        # Apply limits
        if self.limits[0] != None:
            samples[samples < self.limits[0]] = self.limits[0]
        if self.limits[1] != None:
            samples[samples > self.limits[1]] = self.limits[1]
        # Return values
        if size == 1:
            return samples[0]
        else:
            return samples

    def set_limits(self, lower=None, upper=None) -> None:
        self.limits = (lower, upper)

    def set_factor(self, factor: float) -> None:
        """Sets a multiplicative factor to be applied to generated numbers.
            This will multiply to previous set factors, to avoid this use reset_factor.

        Args:
            factor (float): Multiplicative factor.
        """
        self.factor *= factor

    def reset_factor(self, factor: float) -> None:
        """Sets a multiplicative factor to be applied to generated numbers ignoring
            previous set factors.

        Args:
            factor (float): Multiplicative factor.
        """
        self.factor = factor


class harray(np.ndarray):
    """Creates an numpy array with an history of the values in each position.

    Accepts the same arguments as np.asarray().

    !!! warning
        This implementation is not optimized to heavy write operations, write
            performance WILL DEGRADE as the history size increases.

    History data can be accessed on the `hist_data` attribute, not used positions
        will contain NaN as value.
    """

    def __new__(
        cls, *args, buffer_limit: int = 0, clean_history: bool = False, **kwargs
    ) -> np.ndarray:
        """Uses as np.asarray().

        Args:
            buffer_limit (int, optional): Creates a buffer to store history,
                uses more memory but uses less processing time for bigger
                histories. WARNING: `dump_buffer()` must be called before
                reading history. Defaults to 0.
            clean_history (bool, optional): Clean up buffer every time
                `dump_buffer()` is called. Without clean up there will be
                NaN values in the middle of the history. Defaults to False.

        Returns:
            np.ndarray: Instanced array.
        """
        # Instance the array
        obj = np.asarray(*args, **kwargs).view(cls)
        # Stores buffer parameters
        if buffer_limit > 0:
            obj.has_buffer = True
            obj.buffer_limit = buffer_limit
            obj.clean_history = clean_history
        else:
            obj.has_buffer = False
        # Instances buffer array and history array
        obj.hist_data = np.zeros((obj.shape + (0,)), dtype=obj.dtype)
        obj.hist_buffer = np.copy(obj).reshape(obj.shape + (1,))
        # Test if we can store NaN on the same type
        try:
            test_hist = np.copy(obj).flatten()
            test_hist[0] = np.nan
        # If not convert history to float
        except ValueError:
            obj.hist_data = obj.hist_data.astype(float)
            obj.hist_buffer = obj.hist_buffer.astype(float)
        if ~np.isnan(test_hist[0]):
            obj.hist_data = obj.hist_data.astype(float)
            obj.hist_buffer = obj.hist_buffer.astype(float)
        # Return created object
        return obj

    def close(self):
        if hasattr(self, "hist_data"):
            del self.hist_data
            del self.hist_buffer
            if self.has_buffer:
                del self.buffer_limit
                del self.clean_history
            del self.has_buffer

    def __setitem__(self, key: Any, value: Any) -> None:
        """Set value to positions and store previous values in the history.

        Args:
            key (Any): Key to write.
            value (Any): Value to write.
        """
        # Roll new data in to history
        self._roll(key, value)
        # Pass new data to parent class
        return super().__setitem__(key, value)

    def _roll(self, key: Any, value: Any) -> None:
        """Internal method used to insert new values to the history and roll
            previous values.

        Args:
            key (Any): Key to write.
            value (Any): Value to write.
            depth (int, optional): Position in the history. Defaults to 0.
        """
        # If all positions in value are NaN there is nothing more to roll
        if np.all(np.isnan(value)):
            return None
        # Checks if the history has the necessary depth
        last_collum = key + (-1,) if type(key) is tuple else (key, -1)
        if np.any(~np.isnan(self.hist_buffer[last_collum])):
            # If not increases the depth (adds a position to last axis)
            pad_shape = ((0, 0),) * (len(self.hist_buffer.shape) - 1) + ((0, 1),)
            self.hist_buffer = np.pad(
                self.hist_buffer, pad_width=pad_shape, constant_values=np.nan
            )
        # Roll data
        read_key = key + (slice(-1),) if type(key) is tuple else (key, slice(-1))
        write_key = (
            key + (slice(1, None),) if type(key) is tuple else (key, slice(1, None))
        )
        self.hist_buffer[write_key] = self.hist_buffer[read_key]

        # Stores new data
        data_key = key + (0,) if type(key) is tuple else (key, 0)
        self.hist_buffer[data_key] = value

        if self.has_buffer:
            if self.hist_buffer.shape[-1] >= self.buffer_limit:
                self.dump_buffer()
        else:
            self.hist_data = self.hist_buffer

    def dump_buffer(self, clean_history: bool = False) -> None:
        """Dumps buffer to the history.

        Args:
            clean_history (bool, optional): Also clean up history.
                Defaults to False.
        """
        self.hist_data = np.concatenate((self.hist_buffer, self.hist_data), axis=-1)
        self.hist_buffer = np.nan * np.ones(
            self.shape + (1,),
            dtype=self.hist_buffer.dtype,
        )
        if self.clean_history or clean_history:
            self.clean_up()

    def clean_up(self):
        """Remove unused positions (np.nan) from the history."""
        self.hist_data = np.apply_along_axis(self._remove_nan, -1, self.hist_data)
        new_lenght = np.max(
            np.apply_along_axis(self._count_not_nan, -1, self.hist_data)
        )
        trim_slice = (slice(None),) * (len(self.hist_data.shape) - 1) + (
            slice(new_lenght),
        )
        self.hist_data = self.hist_data[trim_slice]
        # self._clean_up()

    @staticmethod
    @njit
    def _count_not_nan(row):
        return np.sum(~np.isnan(row))

    @staticmethod
    @njit
    def _remove_nan(row):
        mask_not_nan = ~np.isnan(row)
        n = np.sum(mask_not_nan)
        row[:n] = row[mask_not_nan]
        row[n:] = np.nan
        return row


COEFF_MRKR = "_coeff"
CRRCT_MRKR = "_correct"


class coefficient_container:
    def __getattribute__(self, name: str) -> Any:
        """Gets an attribute, if it has coefficients applies them.

        Args:
            name (str): Attribute name.

        Returns:
            Any: Attribute value with coefficients applied.
        """
        try:
            return super(coefficient_container, self).__getattribute__(
                name + CRRCT_MRKR
            )
        except AttributeError:
            return super(coefficient_container, self).__getattribute__(name)

    def _update_values(self, name):
        coefficients = super(coefficient_container, self).__getattribute__(
            name + COEFF_MRKR
        )
        attribute = super(coefficient_container, self).__getattribute__(name)
        value = np.copy(attribute)
        for c in coefficients.values():
            value *= c
        setattr(self, name + CRRCT_MRKR, value)

    def add_coefficient(self, name: str, value: np.ndarray, identifier: str) -> None:
        """Adds an attribute coefficient.

        Args:
            name (str): Attribute name.
            value (np.ndarray): Coefficient name (needs to have the same shape as the attribute).
            identifier (str): String identifier for the coefficient.

        Raises:
            ValueError: Raises if type of attribute or coefficient is not np.nd.array, or
                if shapes do not match.
        """
        # Gets attribute to check coefficient shape and types
        attribute = getattr(self, name)
        # Check if attribute
        if type(attribute) is not np.ndarray:
            raise RuntimeError(
                "Tried to set an coefficient for {}.{} attribute that is not an np.ndarray.".format(
                    self.__repr__(), name
                )
            )
        if type(value) is not np.ndarray:
            raise RuntimeError(
                "Tried to set an coefficient for {}.{} attribute, but coefficient not an np.ndarray.".format(
                    self.__repr__(), name
                )
            )
        # Check shape
        if attribute.shape != value.shape:
            raise ValueError(
                "Tried to set a coefficient for {}.{} with shape {}, but coefficient shape is {}.".format(
                    self.__repr__(), name, attribute.shape, value.shape
                )
            )

        # Stores coefficient
        if hasattr(self, name + COEFF_MRKR):
            coeff_dict = getattr(self, name + COEFF_MRKR)
        else:
            coeff_dict = {}
        coeff_dict[identifier] = value
        setattr(self, name + COEFF_MRKR, coeff_dict)
        self._update_values(name)

    def del_coefficient(self, name: str, identifier: str) -> None:
        """Deletes an attribute coefficient if it exists.

        Args:
            name (str): Attribute name.
            identifier (str): String identifier for the coefficient.
        """
        if hasattr(self, name + COEFF_MRKR):
            coeff_dict = getattr(self, name + COEFF_MRKR)
            del coeff_dict[identifier]
            setattr(self, name + COEFF_MRKR, coeff_dict)

    def get_coefficient(self, name: str, identifier: str) -> np.ndarray:
        """Returns a coefficient.

        Args:
            name (str): Attribute name.
            identifier (str): String identifier for the coefficient.

        Returns:
            np.ndarray: Coefficient array.
        """
        return getattr(self, name + COEFF_MRKR)[identifier]

    def set_coefficient(self, name: str, value: np.ndarray, identifier: str) -> None:
        """Sets a coefficient.

        Args:
            name (str): Attribute name.
            value (np.ndarray): Coefficient name (needs to have the same shape as the attribute).
            identifier (str): String identifier for the coefficient.
        """
        return self.add_coefficient(name, value, identifier)


class empty_module(mem_clear):
    def __init__(self, parameters, comm, name="empty_module"):
        """Initialization method for a COMORBUSS module.

        parameters and comm will be passed to your module at initialization.

        Args:
            parameters (dict): Parameters for this module.
            comm (comorbuss.community): Community object.
            name (str, optional): [description]. Defaults to "empty_module".
        """
        self.name = name

        self.comm = comm
        self.pop = comm.pop
        self.clk = comm.clk
        self.event_log = lambda msg, type: comm.event_log(
            "[{}] {}".format(name, msg), type
        )
        self.rng = np.random.default_rng(comm.random_seed)
        self.parameters = parameters

        # Execution settings
        # self.exec_tod = "self.free_hours[0]" # A string to be run inside the community object to determine the time of execution of the module
        # self.run_at_end = True # Run at the start of the step or after all other mechanics

    def store_parameters(self, defaults: dict = {}) -> None:
        """Store module parameters as attributes.

        Args:
            defaults (dict, optional): Defaults dictionary.
        """
        for key in self.parameters.keys():
            if key in defaults:
                value = self.parameters.get(key, defaults[key])
            else:
                value = self.parameters.get(key)
            setattr(self, key, value)

    def init_distribution(
        self, dist: Union[float, list], name: str, type: int
    ) -> np.ndarray:
        """Initializes a distribution using the modules random number generator.

        Args:
            dist (Union[float, list]): Given parameter for the attribute.
            name (str): Name of the attribute.
            param_type (int): Marker for the default behavior for the attribute:

                - `S.DIST_CONT`: the constant value will be assined for all particles,
                - `S.DIST_EXP`: the value will be used in a exponential distribution to assign
                    values for particles.

        Returns:
            np.ndarray: Generated values.
        """
        # This avoid a circular import
        from .tools import init_distribution

        return init_distribution(dist, name, type, self.pop, self.rng, self.event_log)

    def __repr__(self):
        return "<{} COMORBUSS module>".format(self.name)

    def run_after_init(self):
        """Method to be run at the end of the community's initialization."""
        pass

    def log_results(self):
        """Logs results related to this module."""
        pass


class list_container:
    def __getitem__(self, key):
        return self.list[key]

    def __len__(self):
        return len(self.list)

    def iter(self):
        return self.list.iter


class empty_analysis:
    def __init__(self, analysis):
        self.analysis = analysis

        # Important methods from the analysis object
        self.plt = analysis.plt
        self.start_plt = analysis.start_plt
        self.stop_plt = analysis.stop_plt
        self.args_parse = analysis.args_parse
        self.check_data = analysis.check_data
        self.get_data = analysis.get_data
        self.get_srvc_data = analysis.get_srvc_data
        self.seeds = analysis.seeds
        self.get_seeds = analysis.get_seeds
        self.raise_runtime = analysis.raise_runtime
        self.do_plot = analysis.do_plot
        self.plot_line = analysis.plot_line
        self.plot_events = analysis.plot_events

        # Data from the analysis object
        self.parameters = analysis.parameters
        self.data = analysis.data
        self.hdf5_file = analysis.hdf5_file

    @classmethod
    def from_comm(cls, comm, **loader_args):
        from .lab import Analysis

        return cls(Analysis.from_comm(comm, **loader_args))

    @classmethod
    def from_hdf5(cls, hdf5_file="", **loader_args):
        from .lab import Analysis

        return cls(Analysis.from_hdf5(hdf5_file, **loader_args))


class circularlist(object):
    def __init__(self, size, data=[]):
        """Initialization"""
        self.index = 0
        self.size = size
        self._data = list(data)[-size:]

    def close(self):
        del self._data
        del self.index
        del self.size

    def append(self, value):
        """Append an element"""
        if len(self._data) == self.size:
            self._data[self.index] = value
        else:
            self._data.append(value)
        self.index = (self.index + 1) % self.size

    def __getitem__(self, key):
        """Get element by index, relative to the current index"""
        if len(self._data) == self.size:
            return self._data[(key + self.index) % self.size]
        else:
            return self._data[key]

    def __repr__(self):
        """Return string representation"""
        return self._data.__repr__() + " (" + str(len(self._data)) + " items)"

    def __iter__(self):
        return (self[i] for i in range(self.size))


# %%
class clock(mem_clear):
    """Provides a clock for the simulation.

    Attributes:
        dt (float): Time step in hours.
        time (float): Current time in hours since the start of the simulation.
        step (int): Number of steps since the start of the simulation.
        tod (float): Time of the day in the simulation.
        today (int): Number of days since the start of the simulation.
        days (floay): Time since the start of the simulation in days.
        dow (int): Current day of the week, varies from 0 to 6, where 0 is
            sunday and 6 saturday.
        day_of_the_week (str): Current day of the week in text.
    """

    def __init__(self, day_of_the_week=0, start_date=0, dt=1.0):
        self.dt = dt
        self.time = 0.0
        self.step = 0
        self.tod = 0.0
        self.step_of_the_day = 0
        self.today = 0
        self.days = 0.0
        self.week = 0.0
        self.first_dow = day_of_the_week
        self.set_start_date(start_date)
        self.dow = self.first_dow
        self.day_of_the_week = DAYS_OF_THE_WEEK[self.dow]

    def __repr__(self):
        return "<Clock day {:.1f}, time {:.1f}, step {}>".format(
            self.days, self.time, self.step
        )

    def set_start_date(self, start_date):
        if type(start_date) is datetime:
            self.start_date = start_date
            self.first_dow = self.start_date.isoweekday() % 7

    def tick(self):
        """Advances internal clock by one time step dt."""
        self.time += self.dt
        self.step += 1
        self.tod = self.time % 24
        self.step_of_the_day = self.time_to_steps(self.tod)
        self.today = int(self.time / 24)
        self.days = self.time / 24
        self.dow = (self.today + self.first_dow) % 7
        self.week = int((self.today + self.first_dow) / 7)
        self.day_of_the_week = DAYS_OF_THE_WEEK[self.dow]

    def between_hours(self, start, end):
        """Returns True if current time of the day is inside interval [start, end).

        Args:
            start (float): Start of the interval.
            end (float): End of the interval.

        Returns:
            bool: Result of the test.
        """
        return (self.tod >= start and self.tod < end and start < end) or (
            (self.tod >= start or self.tod < end) and start > end
        )

    def between_hours_array(self, start, end):
        return ((self.tod >= start) & (self.tod < end) & (start < end)) | (
            ((self.tod >= start) | (self.tod < end)) & (start > end)
        )

    def at_time_of_day(self, time):
        """Returns True at or imediatly after the time of the day informed.

        Args:
            end (float): Time of the day to test.

        Returns:
            bool: Result of the test.
        """
        return ((self.tod - self.dt) < time) and (self.tod >= time)

    def at_time_of_day_array(self, end):
        return ((self.tod - self.dt) < time) & (self.tod >= time)

    def at_time(self, time):
        """Returns True at or immediately after the end time informed.

        Args:
            end (float): Time of the day to test.

        Returns:
            bool: Result of the test.
        """
        return ((self.time - self.dt) < time) and (self.time >= time)

    def at_time_array(self, time):
        return ((self.time - self.dt) < time) & (self.time >= time)

    def time_since(self, time):
        return self.time - time

    def days_since(self, time, truncate=False):
        return self._trunc(self.time_to_days(self.time_since(time)), truncate)

    def mask_time_passed(self, time, lenght):
        return ((self.time - time - self.dt) < lenght) & ((self.time - time) >= lenght)

    def days_to_time(self, days):
        return days * 24.0

    def days_to_steps(self, days):
        return self.time_to_steps(self.days_to_time(days))

    def time_to_steps(self, time):
        return self._trunc(time / self.dt, True)

    def time_to_days(self, time):
        return time / 24.0

    def _trunc(self, value, truncate=False):
        if truncate:
            return np.array(value, dtype=int)
        else:
            return value


DAYS_OF_THE_WEEK = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
]
