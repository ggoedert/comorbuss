import os
from types import MethodType
import h5dict
from networkx.drawing import layout
import numpy as np
import networkx as nx
import pandas as pd
import seaborn as sns
from pprint import pprint
from networkx.drawing.nx_agraph import graphviz_layout
from datetime import datetime, timedelta
from .. import settings as S
from ..aux_classes import clock
from .. import tools
from matplotlib.ticker import PercentFormatter
from .. import modules

red = S.COLORS["red"]
green = S.COLORS["green"]
blue = S.COLORS["blue"]
black = S.COLORS["purple"]

import matplotlib as mpl

# Check if we can show plots, if not use Agg backend
if not (os.environ.get("DISPLAY") or tools.is_notebook()):
    mpl.use("Agg")
import matplotlib.pyplot as plt

myfont = {"size": 8}
mpl.rc("font", **myfont)

from matplotlib.colors import to_rgb
from matplotlib.patches import Patch
from matplotlib.lines import Line2D


class Analysis:
    def __init__(
        self, data, parameters, given_parameters, dataOk, hdf5=None, use_plotly=False
    ):
        self.data = data
        self.post_processed_data = None
        self.parameters = parameters
        self.given_parameters = given_parameters
        self.dataOk = dataOk
        self.hdf5_file = hdf5
        if hdf5 != None:
            self.filename = hdf5.name
        else:
            self.filename = ""
        self.plt = plt

        if use_plotly:
            global go
            import plotly.graph_objects as go

            self.start_plt = self.start_plt_plotly
            self.stop_plt = self.stop_plt_plotly
            self.plot_line = self.plot_line_plotly
            self.plot_events = lambda *_, **__: None

    @classmethod
    def from_comm(cls, comm, use_plotly=False, **loader_args):
        """Initialize an Analysis object from a community object.

        Args:
            comm (community): Community to extract data.

        Returns:
            Analysis: An Analysis object.
        """
        parameters = tools.recursive_copy(comm.parameters)
        given_parameters = tools.recursive_copy(comm.given_parameters)
        try:
            data = tools.load_from_comms([comm], {}, **loader_args)
            return cls(
                data["realizations"],
                parameters,
                given_parameters,
                True,
                use_plotly=use_plotly,
            )
        except:
            print("Coudn't load default to_store from given community.")
            return cls({}, {}, False)

    @classmethod
    def from_hdf5(cls, hdf5_file="", use_plotly=False, **loader_args):
        """Initialize an Analysis object from an hdf5 file.

        Args:
            hdf5_
            if seed == "":
                seed = list(results["realizations"].keys())[0]file (str): Path to the hdf5 file, if not informed a prompt will be open for the user
                to select the file.
            seed (int, optional): An execution seed in the hdf5 file to be loaded, if not informed will load
                the first in the file.

        Returns:
            Analysis: An Analysis object.
        """
        results, filename = tools.load_hdf5(hdf5_file, keep_open=True, **loader_args)
        try:
            return cls(
                results["realizations"],
                results["triaged_parameters"].to_dict(),
                results["parameters"].to_dict(),
                True,
                hdf5=results,
                use_plotly=use_plotly,
            )
        except:
            return cls({}, {}, False)

    def __getattr__(self, name):
        for module_name in self.parameters["modules"].keys():
            module_name = module_name.split(".")[-1]
            try:
                module = getattr(modules, module_name)
            except AttributeError:
                module = getattr(modules, module_name.split(".")[-1])
            if hasattr(module, "analysis_functions"):
                if hasattr(module.analysis_functions, name):
                    from types import MethodType

                    return MethodType(getattr(module.analysis_functions, name), self)
        raise AttributeError(
            "Analysis class has no attribute {}, and there is no analysis function with this name in the used modules.".format(
                name
            )
        )

    def seeds(self):
        return list(self.data.keys())

    def get_seeds(self, seeds=None):
        if seeds == None:
            return self.seeds()
        loaded_seeds = self.seeds()
        if type(seeds) is int:
            return [loaded_seeds[seeds]]
        elif type(seeds) is list:
            for seed in seeds:
                if not seed in loaded_seeds:
                    self.raise_runtime("{} seed is not on loaded data.".format(seed))
            return seeds
        elif not seeds in loaded_seeds:
            self.raise_runtime("{} seed is not on loaded data.".format(seeds))
        return [seeds]

    def check_data(self, required_data, seeds, srvc_check=False):
        if not self.dataOk:
            self.raise_runtime()
        for seed in seeds:
            for key in required_data:
                if srvc_check:
                    if type(self.data[seed]["services"]) is list:
                        sids = range(len(self.data[seed]["services"]))
                    else:
                        sids = self.data[seed]["services"].keys()
                    for sid in sids:
                        if not key in self.data[seed]["services"][sid].keys():
                            self.raise_runtime(
                                "List {} is required for services, but key {} was not stored for seed {}.".format(
                                    required_data, key, seed
                                )
                            )
                            return False
                elif type(key) in [list, tuple]:
                    data = self.data[seed]
                    for k in key:
                        if not k in data.keys():
                            self.raise_runtime(
                                "{} is required, but {} and was not stored on seed {}.".format(
                                    required_data, key, seed
                                )
                            )
                            return False
                        data = data[k]
                else:
                    if not key in self.data[seed].keys():
                        self.raise_runtime(
                            "{} is required, but {} and was not stored on seed {}.".format(
                                required_data, key, seed
                            )
                        )
                        return False
        return True

    def _get_source(self, key, seed):
        if key == None:
            source = self.data[seed]
        else:
            source = self.data[seed]
            if type(key) in [list, tuple]:
                for k in key:
                    source = source[k]
            else:
                source = source[key]
        return source

    def list_data(self, parameter=None, seeds=None):
        """Lists all available data for seeds.

        Args:
            parameter (str, list, optional): If informed will inform data contained in a key.
            seeds (list, optional): If informed will filter for given seeds.

        Returns:
            list: List of available data
        """
        seeds = self.get_seeds(seeds)
        source = self._get_source(parameter, seeds[0])
        available_data = set(source.keys())
        for seed in seeds:
            source = self._get_source(parameter, seed)
            seed_data = set(source.keys())
            available_data = available_data.intersection(seed_data)
        return list(available_data)

    def get_data(self, required_data, seeds=None):
        """Get data from hdf5 file.

        Args:
            required_data (list): List of data to get.
            seeds (list, optional): If informed will filter for given seeds.

        Returns:
            dict: A dictionary of required_data, in each item is passed a list the data for each seed.
        """
        seeds = self.get_seeds(seeds)
        if not self.check_data(required_data, seeds):
            return None
        data = dict()
        data["seeds"] = seeds
        for key in required_data:
            data[key] = []
            for s in seeds:
                if type(key) in [list, tuple]:
                    data_source = self.data[s]
                    for k in key:
                        data_source = data_source[k]
                    data[key].append(data_source)
                else:
                    data[key].append(self.data[s][key])
        return data

    def list_srvc_data(self, seeds=None):
        """Lists all available service data for seeds.

        Args:
            seeds (list, optional): If informed will filter for given seeds.

        Returns:
            list: List of available data
        """
        seeds = self.get_seeds(seeds)
        available_data = set(self.data[seeds[0]]["services"][0].keys())
        for seed in seeds:
            seed_data = set(self.data[seed]["services"][0].keys())
            available_data = available_data.intersection(seed_data)
        return list(available_data)

    def get_srvc_data(self, required_data, seeds=None):
        """Get service data from hdf5 file.

        Args:
            required_data (list): List of data to get.
            seeds (list, optional): If informed will filter for given seeds.

        Returns:
            dict: A dictionary of required_data, in each item is passed a list the data for each seed.
        """
        seeds = self.get_seeds(seeds)
        if not self.check_data(required_data, seeds, srvc_check=True):
            return None
        data = []
        for sid in range(len(self.parameters["services"])):
            srvc_data = dict()
            srvc_data["seeds"] = seeds
            for d in required_data:
                srvc_data[d] = []
                for s in seeds:
                    srvc_data[d].append(self.data[s]["services"][sid][d])
            data.append(srvc_data)
        return data

    def raise_runtime(self, message=""):
        if message == "":
            message = "Tried to plot without loading data, use from_comm or from_hdf5 to instance object with data."
        raise RuntimeWarning(message)

    def start_plt_plotly(self, size=None, title=None):
        if size[0] <= 50:
            size = (size[0] * 100, size[1])
        if size[1] <= 50:
            size = (size[0], size[1] * 100)
        layout = dict(
            width=size[0],
            height=size[1],
            template="simple_white",
        )
        self.fig = go.Figure(layout=layout)

    def stop_plt_plotly(self, **kwargs):
        xlabel = kwargs.get("xlabel", False)
        if xlabel:
            self.fig.update_layout(xaxis_title=xlabel)
        ylabel = kwargs.get("ylabel", False)
        if ylabel:
            self.fig.update_layout(yaxis_title=ylabel)
        self.fig.update_layout(hovermode="x")
        self.fig.update_layout(yaxis=dict(tickformat=",.0%"))
        self.fig.update_layout(
            legend=dict(yanchor="top", y=0.99, xanchor="right", x=0.99)
        )
        tight_layout = kwargs.get("tight_layout", True)
        if tight_layout:
            self.fig.update_layout(margin=dict(l=20, r=20, t=20, b=20))
        self.fig.show()

    def plot_line_plotly(
        self,
        x,
        y,
        label=None,
        fill_alpha=0.2,
        interval=slice(None),
        bands=True,
        colormap=None,
        **plt_args
    ):
        y = np.array(y) / 100
        x = np.array(x)
        if "color" in plt_args:
            if type(plt_args["color"]) is not str:
                if type(plt_args["color"]) is tuple:
                    plt_args["color"] = "rgb{}".format(plt_args["color"])
        if y.shape[0] == 1:
            self.fig.add_trace(
                go.Scatter(
                    x=x[interval],
                    y=y[0][interval],
                    mode="lines",
                    line=dict(color=plt_args["color"]),
                    name=label,
                )
            )
        else:
            pass
            # mean, sigma, perc_low, perc_high = self.get_bands(x, y)

    def start_plt(self, size=(5, 4), title=None):
        plt.figure(figsize=size)
        if title != None:
            self.plt.title(title)

    def stop_plt(self, **kwargs):
        dates = kwargs.get("dates", False)
        dates_args = kwargs.get("dates_args", {})
        if dates:
            self.add_dates(**dates_args)
        xlabel = kwargs.get("xlabel", False)
        if xlabel:
            self.plt.xlabel(xlabel)
        ylabel = kwargs.get("ylabel", False)
        log_y = kwargs.get("log_y", False)
        if log_y:
            if ylabel:
                ylabel = ylabel + " (log)"
            self.plt.yscale("log")
        if ylabel:
            self.plt.ylabel(ylabel)
        xlim = kwargs.get("xlim", None)
        if xlim != None:
            self.plt.xlim(xlim)
        ylim = kwargs.get("ylim", None)
        if ylim != None:
            self.plt.ylim(ylim)
        l = None
        legend = kwargs.get("legend", False)
        legend_args = kwargs.get("legend_args", {})
        if legend or legend_args != {}:
            l = self.plt.legend(**legend_args)
        bands_legend = kwargs.get("bands_legend", False)
        bands_legend_args = kwargs.get("bands_legend_args", {})
        if bands_legend:
            self.bands_legend(original_legend=l, **bands_legend_args)
        tight_layout = kwargs.get("tight_layout", True)
        if tight_layout:
            self.plt.tight_layout(pad=0.5)
        filename = kwargs.get("filename", None)
        if filename != None:
            self.plt.savefig(filename, bbox_inches="tight")
        else:
            self.plt.show()

    def bands_legend(self, color=S.COLORS_LIST[0], original_legend=None, **kwargs):
        half_color = (np.array((1.0, 1.0, 1.0)) + np.array(to_rgb(color))) * 0.5
        legends = [
            Line2D([0], [0], color=color, label="mean"),
            Patch(
                facecolor=half_color, edgecolor=color, alpha=0.4, label="std. deviation"
            ),
            Line2D([0], [0], color=color, ls=":", label="95 percentile"),
        ]

        kwargs = self.args_parse(kwargs, {"loc": "upper center"})
        ax = self.plt.gca()
        ax.legend(handles=legends, **kwargs)
        if original_legend != None:
            ax.add_artist(original_legend)

    def add_dates(self, start_day=None, end_day=None, xticks=6):
        if type(self.parameters["start_date"]) is datetime:
            Ndays = float(self.parameters["Ndays"])
            start_day = start_day or 0
            end_day = end_day or Ndays
            start_date = self.parameters["start_date"] + timedelta(days=start_day)
            Ndays = end_day - start_day
            xdelta = Ndays / (xticks - 1)
            tdelta = timedelta(days=Ndays) / (xticks - 1)
            x = []
            t = []
            for i in range(xticks):
                x.append(start_day + i * xdelta)
                date = start_date + i * tdelta
                t.append(date.strftime("%d-%b"))
            self.plt.xticks(x, t)
            self.plt.xlabel("Date")
        else:
            print("[WARNING] Data doesn't have a valid start_date, can't plot dates.")

    def days_to_interval(self, start_day, end_day, dt):
        if start_day != None:
            start_step = int((start_day * 24) / dt)
        else:
            start_step = None
        if end_day != None:
            end_step = int((end_day * 24) / dt) + 1
        else:
            end_step = None
        return slice(start_step, end_step)

    def args_parse(self, args, default_args):
        for key in default_args:
            if not key in args:
                args[key] = default_args[key]
            elif (type(args[key]) is dict) and (type(default_args[key]) is dict):
                args[key] = self.args_parse(args[key], default_args[key])
        return args

    def do_plot(
        self,
        t,
        dt,
        data,
        colors,
        seeds,
        filename=None,
        size=(5, 4),
        title=None,
        events=False,
        events_type=S.MSG_EVENT,
        skip=[],
        start_day=None,
        end_day=None,
        bands=True,
        colorsmap=None,
        start=True,
        close=True,
        dates=True,
        xlim=None,
        ylim=None,
        xlabel=False,
        ylabel=False,
        log_y=False,
        show_legend=True,
        legend_args={},
        show_bands_legend=True,
        bands_legend_args={},
        plt_args={},
    ):
        """Internal method used to plot one or more curve.

        ### **plot_args:

        Args:
            filename (str, optional): File name to save the graph, if not informed will only show the plot.
            size (tuple, optional): Size to generate the plot. Defaults to (5, 4).
            title (str, optional): Title to be printed on the plot.
            events (bool, optional): Overlay simulation events on the plot. Defaults to False.
            events_type (int, optional): Type of logs to overlay as events. Defaults to S.MSG_EVENTS.
            skip (list, optional): List of curves to skip plotting. Use the same string used in the legend
                of the plot to skip curves.
            start_day (float, optional): Plot only the interval from start_day. Defaults to 0.
            end_day (float, optional): Plot only the interval to end_day. Defaults to last day.
            bands (bool, optional): Plot standard deviation bands and 95% percentiles if data has
                multiple seeds. Defaults to True.
            colorsmap (arr[str], optional): If provided, the list of string is used to initialize colormaps,
                used in the shading of bands
            close (bool, optional): If False doesn't show, save or close the plot so that the user can add
                more information to plot using the plt attribute from the class. Defaults to False.
            dates (bool, optional): Use dates instead of days in the x axis. Defaults to True.
            xlabel (str, optional): Label for the x axis.
            ylabel (str, optional): Label for the y axis.
            log_y (bool, optional): Sets the y scale to log. Defaults to False
            show_legend (bool, optional): Hides legends if False. Defaults to True.
            legend_args (dict, optional): Arguments to be passed to pyplot.legend when drawing the legend.
            show_bands_legend (bool, optional): Hides bands legends if False. Defaults to True.
            bands_legend_args (dict, optional): Arguments to be passed to pyplot.legend when drawing the
                bands legend.
            plt_args (dict, optional): Additional arguments to be passed to pyplot.plot when drawing the curves.

        Internal parameters:

        Args:
            t ([type]): [description]
            dt ([type]): [description]
            data ([type]): [description]
            colors ([type]): [description]
            seeds ([type]): [description]
        """
        interval = self.days_to_interval(start_day, end_day, dt)
        if start:
            self.start_plt(size=size, title=title)
        if bands:
            bands = bands and (len(seeds) > 1)
        for i, k in enumerate(data.keys()):
            if not k in skip:
                colormap = None
                if bands and (colorsmap != None):
                    try:
                        colormap = colorsmap[i]
                    except:
                        pass
                self.plot_line(
                    t,
                    data[k],
                    interval=interval,
                    bands=bands,
                    colormap=colormap,
                    color=colors[i],
                    label=k,
                    **plt_args
                )
        if events:
            self.plot_events(seeds, events_type=events_type)
        dates_args = {"start_day": start_day, "end_day": end_day}
        if not close and dates:
            self.add_dates(**dates_args)
        show_bands_legend = show_bands_legend and bands
        if close:
            self.stop_plt(
                filename=filename,
                xlim=xlim,
                ylim=ylim,
                xlabel=xlabel,
                ylabel=ylabel,
                log_y=log_y,
                dates=dates,
                dates_args=dates_args,
                legend=show_legend,
                legend_args=legend_args,
                bands_legend=show_bands_legend,
                bands_legend_args=bands_legend_args,
            )

    def get_bands(self, x, y):
        mean = np.mean(y, axis=0)
        sigma = np.std(y, axis=0)
        perc_low = np.percentile(y, 2.5, axis=0)
        perc_high = np.percentile(y, 97.5, axis=0)
        return mean, sigma, perc_low, perc_high

    def plot_line(
        self,
        x,
        y,
        label=None,
        fill_alpha=0.2,
        interval=slice(None),
        bands=True,
        colormap=None,
        **plt_args
    ):
        y = np.array(y)
        x = np.array(x)
        if y.shape[0] == 1:
            self.plt.plot(x[interval], y[0][interval], label=label, **plt_args)
        else:
            mean, sigma, perc_low, perc_high = self.get_bands(x, y)
            p = self.plt.plot(x[interval], mean[interval], label=label, **plt_args)
            if bands:
                from scipy import stats

                plt_args["color"] = p[0].get_color()
                if colormap == None:
                    self.plt.fill_between(
                        x[interval],
                        (mean[interval] - sigma[interval]),
                        (mean[interval] + sigma[interval]),
                        alpha=fill_alpha,
                        **plt_args
                    )
                else:
                    nhours = 12
                    npoints = 25
                    ax = self.plt.gca()
                    for i in range(0, len(x) - nhours, nhours):
                        values = np.mean(y[:, i : (i + nhours)], axis=1)
                        g_kde = stats.gaussian_kde(values)
                        [cmapv, cmapstep] = np.linspace(
                            np.mean(mean[i : (i + nhours)] - sigma[i : (i + nhours)]),
                            np.mean(mean[i : (i + nhours)] + sigma[i : (i + nhours)]),
                            npoints,
                            retstep=True,
                        )
                        g_kdev = g_kde.evaluate(cmapv + cmapstep * 0.5)
                        normal = plt.Normalize(np.min(g_kdev), np.max(g_kdev))
                        g_kdev = g_kde.evaluate(cmapv)
                        try:
                            cmap = plt.cm.ScalarMappable(
                                norm=normal, cmap=colormap
                            ).to_rgba(g_kdev)
                        except:
                            cmap = plt.cm.jet(normal(g_kdev))
                        for vp, vn, cm in zip(cmapv[:-1], cmapv[1:], cmap[:-1]):
                            ax.fill_between(
                                [x[i], x[i + nhours]],
                                np.full(2, vp),
                                np.full(2, vn),
                                alpha=fill_alpha,
                                facecolor=cm,
                            )
                self.plt.plot(x[interval], perc_low[interval], ls=":", **plt_args)
                self.plt.plot(x[interval], perc_high[interval], ls=":", **plt_args)

    def plot_events(self, seeds, events_type=S.MSG_EVENT):
        seeds = self.get_seeds(seeds)
        if len(seeds) != 1:
            print("[WARNING] Can only plot events with single seed.")
        else:
            data = self.get_data(["events"], seeds)
            ax = self.plt.gca()
            minY, maxY = ax.get_ylim()
            closed = False
            opened = False
            for event in data["events"][0]:
                if event[2] == events_type:
                    if "closing" in event[0].lower():
                        if closed:
                            self.plt.plot(
                                [event[1] / 24.0, event[1] / 24.0],
                                [minY, maxY],
                                "--",
                                color=S.COLORS["red"],
                            )
                        else:
                            self.plt.plot(
                                [event[1] / 24.0, event[1] / 24.0],
                                [minY, maxY],
                                "--",
                                color=S.COLORS["red"],
                                label="Services Closed",
                            )
                            closed = True
                    elif "opening" in event[0].lower():
                        if opened:
                            self.plt.plot(
                                [event[1] / 24.0, event[1] / 24.0],
                                [minY, maxY],
                                "--",
                                color=S.COLORS["green"],
                            )
                        else:
                            self.plt.plot(
                                [event[1] / 24.0, event[1] / 24.0],
                                [minY, maxY],
                                "--",
                                color=S.COLORS["green"],
                                label="Services Reopened",
                            )
                            opened = True
                    else:
                        self.plt.plot(
                            [event[1] / 24.0, event[1] / 24.0],
                            [minY, maxY],
                            "--",
                            label=event[0],
                        )

    def plot_data_line(
        self,
        data_to_plot,
        functions=None,
        seeds=None,
        colors=S.COLORS_LIST,
        **plot_args
    ):
        # Load data
        data = self.get_data(data_to_plot, seeds)
        seeds = data["seeds"]
        if type(functions) is list:
            data = {
                data_name: f(np.array(data[data_name]))
                for data_name, f in zip(data_to_plot, functions)
            }
        else:
            data = {data_name: np.array(data[data_name]) for data_name in data_to_plot}
        Nsteps = self.parameters["Nsteps"]
        dt = self.parameters["dt"]
        t = dt * np.arange(Nsteps) / 24

        # Plot graphs
        self.do_plot(t, dt, data, colors, seeds, **plot_args)

    def seir_data(self, seeds):
        # Load data for seir plots
        data = self.get_data(
            ["susceptible", "exposed", "infectious", "recovered", "deceased"], seeds
        )
        Nparticles = self.parameters["Nparticles"]
        seeds = data["seeds"]
        data = {
            "susceptible": 100.0 * np.array(data["susceptible"]) / Nparticles,
            "exposed": 100.0 * np.array(data["exposed"]) / Nparticles,
            "infectious": 100.0 * np.array(data["infectious"]) / Nparticles,
            "recovered": 100.0
            * np.array(data["recovered"])
            / Nparticles,  # +100.*np.array(data['deceased'])/Nparticles,
            "deceased": 100.0 * np.array(data["deceased"]) / Nparticles,
        }
        return data, seeds

    ##### Ploting functions
    def plot_SEIR(self, seeds=None, **plot_args):
        """Save a SEIR graph.

        !!! Important
            This plot uses the do_plot method, see [plot_args](#plot_args) to see all available arguments.

        Args:
            seeds (list, optional): List of seeds to plot, if None will plot all seeds available.
            **plot_args (kwargs): Arguments for the do_plot method (see [plot_args](#plot_args)).
        """
        # Prepare Data
        data, seeds = self.seir_data(seeds)
        Nsteps = self.parameters["Nsteps"]
        dt = self.parameters["dt"]
        t = dt * np.arange(Nsteps) / 24

        # Plot graphs
        def_plot_args = {
            "xlabel": "Days",
            "ylabel": "% Population",
        }
        plot_args = self.args_parse(plot_args, def_plot_args)
        self.do_plot(t, dt, data, S.COLORS_STATES, seeds, **plot_args)

    def plot_SEIR_stack(
        self,
        filename=None,
        size=(5, 4),
        seed=-1,
        show_legend=True,
        dates=True,
        events_type=0,
        title=None,
    ):
        """Save a stacked SEIR graph. Can only plot one seed.

        Args:
            filename (str, optional): File name to save the graph, if not informed will only show the plot.
            size (tuple, optional): Size to generate the plot. Defaults to (5, 4).
            title (str, optional): Title to be printed on the plot.
            seed (str, optional): Seed to be plotted, if None will plot last loaded.
            dates (bool, optional): Use dates instead of days in the x axis. Defaults to True.
            show_legend (bool, optional): Hides legends if False. Defaults to True.
        """
        # Prepare Data
        if type(seed) is list:
            if len(seed) != 1:
                print(
                    "[WARNING] Can only plot stacked SEIR with single seed, plotting last seed loaded."
                )
        data, seed = self.seir_data(seed)
        Nsteps = self.parameters["Nsteps"]
        dt = self.parameters["dt"]
        t = dt * np.arange(Nsteps) / 24

        # Plot graphs
        self.start_plt(size=size, title=title)
        stack = {}
        for k in reversed(list(data.keys())):
            stack[k] = data[k][0]
        self.plt.stackplot(
            t,
            stack.values(),
            colors=list(reversed(S.COLORS_STATES)),
            labels=stack.keys(),
        )
        self.plt.xlim(0, self.parameters["Ndays"])
        self.plt.ylim(0, 100)
        if show_legend:
            self.plt.legend(loc="upper left")
        self.stop_plt(
            filename=filename, xlabel="Days", ylabel="% Population", dates=dates
        )

    def plot_R(self, seeds=None, **plot_args):
        """Save a R graph.

        !!! Important
            This plot uses the do_plot method, see [plot_args](#plot_args) to see all available arguments.

        Args:
            seeds (list, optional): List of seeds to plot, if None will plot all seeds available.
            **plot_args (kwargs): Arguments for the do_plot method (see [plot_args](#plot_args)).
        """
        # Prepare Data
        data = self.get_data(["Rot"], seeds)
        seeds = data["seeds"]
        data = {"R0t": np.array(data["Rot"])}
        Nsteps = self.parameters["Nsteps"]
        dt = self.parameters["dt"]
        t = dt * np.arange(Nsteps) / 24

        # Plot graphs
        def_plot_args = {
            "xlabel": "Days",
            "ylabel": "$R_{0t}$",
            "legend_args": {
                "labels": ["$R_0 $ = {:.3g} ".format(np.mean(data["R0t"][:, -1]))],
                "loc": "upper right",
            },
            "bands_legend_args": {"color": S.COLORS["blue"]},
        }
        plot_args = self.args_parse(plot_args, def_plot_args)
        self.do_plot(t, dt, data, [S.COLORS["blue"]], seeds, **plot_args)

    def plot_symptomatic_states(self, seeds=None, **plot_args):
        """Plots the number of particles in each symptomatic state in time.

        !!! Important
            This plot uses the do_plot method, see [plot_args](#plot_args) to see all available arguments.

        Args:
            seeds (list, optional): List of seeds to plot, if None will plot all seeds available.
            **plot_args (kwargs): Arguments for the do_plot method (see [plot_args](#plot_args)).
        """
        # Prepare Data
        data = self.get_data(
            [
                "pre_symptomatic",
                "asymptomatic",
                "symptomatic",
                "severe_symptomatic",
                "exposed",
            ],
            seeds,
        )
        Nparticles = self.parameters["Nparticles"]
        seeds = data["seeds"]
        data = {
            "mild symp": np.array(data["symptomatic"]) * 100 / Nparticles,
            "severe symp": np.array(data["severe_symptomatic"]) * 100 / Nparticles,
            "asymptomatic": np.array(data["asymptomatic"]) * 100 / Nparticles,
            "pre-symptomatic": np.array(data["pre_symptomatic"]) * 100 / Nparticles,
            "incubating": np.array(data["exposed"]) * 100 / Nparticles,
        }
        dt = self.parameters["dt"]
        t = dt * np.arange(self.parameters["Nsteps"]) / 24

        # Plot graphs
        def_plot_args = {
            "xlabel": "Days",
            "ylabel": "% Population",
            "bands_legend_args": {"color": S.COLORS_SYMPT[0]},
        }
        plot_args = self.args_parse(plot_args, def_plot_args)
        self.do_plot(t, dt, data, S.COLORS_SYMPT, seeds, **plot_args)

    def plot_symptoms(self, seeds=None, **plot_args):
        """Plots the percentage of symptomatic and asymptomatic particles in time.

        !!! Important
            This plot uses the do_plot method, see [plot_args](#plot_args) to see all available arguments.

        Args:
            seeds (list, optional): List of seeds to plot, if None will plot all seeds available.
            **plot_args (kwargs): Arguments for the do_plot method (see [plot_args](#plot_args)).
        """
        # Prepare Data
        data = self.get_data(["symptoms"], seeds)
        seeds = data["seeds"]
        symptoms = np.array(data["symptoms"])
        symptomatic_cumulative = np.sum(
            (symptoms > 0) & (symptoms != S.NO_INFEC), axis=2
        )
        asymptomatic_cumulative = np.sum(
            (symptoms < 0) & (symptoms != S.NO_INFEC), axis=2
        )
        data = {
            "symptomatic": 100
            * symptomatic_cumulative
            / np.maximum(1.0, symptomatic_cumulative + asymptomatic_cumulative),
            "asymptomatic": 100
            * asymptomatic_cumulative
            / np.maximum(1.0, symptomatic_cumulative + asymptomatic_cumulative),
        }
        Nsteps = self.parameters["Nsteps"]
        dt = self.parameters["dt"]
        t = dt * np.arange(Nsteps) / 24

        # Plot graphs
        def_plot_args = {
            "xlabel": "Days",
            "ylabel": "% Infected",
            "legend_args": {
                "labels": [
                    "Prob(sympt.) = {:.3g} %".format(
                        (
                            100
                            * np.mean(symptomatic_cumulative[:, -1])
                            / (
                                np.mean(symptomatic_cumulative[:, -1])
                                + np.mean(asymptomatic_cumulative[:, -1])
                            )
                        )
                    )
                ],
                "loc": "upper right",
            },
            "bands_legend_args": {"color": S.COLORS["red"]},
        }
        plot_args = self.args_parse(plot_args, def_plot_args)
        self.do_plot(
            t, dt, data, [S.COLORS["red"], S.COLORS["blue"]], seeds, **plot_args
        )

    def plot_infection_placement(
        self, filename=None, seeds=None, size=(5, 4), skip=[], close=True, title=None
    ):
        """Plot infection placement data.

        Args:
            filename (str, optional): File name to save the graph, if not informed will only show the plot.
            size (tuple, optional): Size to generate the plot. Defaults to (10, 4).
            title (str, optional): Title to be printed on the plot.
            seeds (list, optional): List of seeds to plot, if None will plot all seeds available.
            skip (list, optional): List of placements names to skip plotting.
            close (bool, optional): If False doesn't show, save or close the plot so that the user can add
                more information to plot using the plt attribute from the class. Defaults to False.
        """
        seeds = self.get_seeds(seeds)
        if len(seeds) == 1:
            self.plot_infection_placement_single_seed(
                filename, seeds, size, skip, close, title
            )
        else:
            self.plot_infection_placement_multi_seed(
                filename, seeds, size, skip, close, title
            )

    def plot_infection_placement_single_seed(
        self, filename=None, seed=-1, size=(5, 4), skip=[], close=True, title=None
    ):
        # Prepare data
        seed = seed or -1
        if type(seed) is list:
            if len(seed) != 1:
                print(
                    "[WARNING] Can only plot infections with single seed, plotting last seed loaded."
                )
        data = self.get_data(["inf_vector_sympt", "inf_placement"], seed)
        vec_symptoms = data["inf_vector_sympt"][0]
        placement = data["inf_placement"][0]
        # Classify infections by symptomatic state of vector
        data_symp = {
            "mild symptomatic": np.sum(vec_symptoms == S.SYMPT_YES),
            "severe symptomatic": np.sum(vec_symptoms == S.SYMPT_SEVERE),
            "asymptomatic": np.sum(vec_symptoms == S.SYMPT_NO),
            "pre-symptomatic": np.sum(vec_symptoms == S.SYMPT_NYET),
        }
        Ninfections = np.sum(list(data_symp.values()))
        for k in data_symp.keys():
            data_symp[k] = data_symp[k] * 100 / np.max([Ninfections, 1.0])
        # Classify infections by the location if occurred
        places = {"Home": S.PLC_HOME, "Environment": S.PLC_ENV}
        if S.PLC_TRSP in placement:
            places["Transport"] = S.PLC_TRSP
        for srvc in self.parameters["services"]:
            places[srvc["name"]] = srvc["id"]
        symptoms = {
            "mild symptomatic": S.SYMPT_YES,
            "severe symptomatic": S.SYMPT_SEVERE,
            "asymptomatic": S.SYMPT_NO,
            "pre-symptomatic": S.SYMPT_NYET,
        }
        data_symptoms = {}
        for sympt, mrkr in symptoms.items():
            data_symptoms[sympt] = []
            for plc in places.values():
                data_symptoms[sympt].append(
                    np.sum((vec_symptoms == mrkr) & (placement == plc))
                    * 100
                    / np.max([Ninfections, 1.0])
                )

        # Plot graphs
        self.start_plt(size=size, title=title)
        x = np.arange(len(places))
        cumm = np.zeros(len(places))
        for i, k in zip(range(len(data_symptoms)), data_symptoms.keys()):
            self.plt.bar(
                x, data_symptoms[k], bottom=cumm, color=S.COLORS_SYMPT[i], label=k
            )
            cumm = cumm + data_symptoms[k]
        self.plt.xticks(x, places.keys(), rotation=45, horizontalalignment="right")
        if close:
            self.stop_plt(
                filename=filename,
                xlabel="Location infection occurred",
                ylabel="% of Infections",
                legend=True,
            )

    def plot_infection_placement_multi_seed(
        self, filename=None, seeds=None, size=(5, 4), skip=[], close=True, title=None
    ):
        # Prepare data
        data = self.get_data(["inf_placement"], seeds)
        inf_placement = np.array(data["inf_placement"])
        places = {"Home": S.PLC_HOME, "Environment": S.PLC_ENV}
        if S.PLC_TRSP in inf_placement:
            places["Transport"] = S.PLC_TRSP
        for srvc in self.parameters["services"]:
            places[srvc["name"]] = srvc["id"]
        for name in skip:
            try:
                del places[name]
            except KeyError:
                print(
                    "[WARNING] {} in skip but not a valid placement name, ignoring it.".format(
                        name
                    )
                )
        data = np.zeros((inf_placement.shape[0], len(places)))
        infs = np.zeros(inf_placement.shape[0])
        for s in range(inf_placement.shape[0]):
            Ninfections = np.sum(inf_placement[s, :] != S.NO_INFEC)
            infs[s] = Ninfections
            for p, plc in enumerate(places.values()):
                data[s, p] = np.sum(inf_placement[s, :] == plc) * 100 / Ninfections

        # Plot graphs
        self.start_plt(size=size, title=title)
        x = np.arange(1, len(places) + 1)
        self.plt.boxplot(data)
        ylim = (0, plt.ylim()[1])
        self.plt.ylim(ylim)
        self.plt.xticks(x, places.keys(), rotation=45, horizontalalignment="right")
        if close:
            self.stop_plt(
                filename=filename,
                xlabel="Location infection occurred",
                ylabel="% of Infections",
            )

        # Write important data to file
        experiment_name = self.filename
        experiment_name = experiment_name.replace(".hdf5", "")
        f = open("{}_inf_pct_plc.txt".format(experiment_name), "w")
        f.write(
            "placement labels: {:}\n".format(["".join(item) for item in places.keys()])
        )
        total_infs = np.mean(infs)
        total_infs_std = np.std(infs)
        plc_infs = np.mean(data, axis=0)
        plc_infs_std = np.std(data, axis=0)
        f.write("total inf: mean={:}, std={:}\n".format(total_infs, total_infs_std))
        f.write("placement inf %: mean={:}, std={:}\n".format(plc_infs, plc_infs_std))
        f.close()

    def plot_service_infections(
        self, filename=None, seeds=None, size=(10, 4), skip=[], close=True, title=None
    ):
        """Plots proportion of infections relating clients and workers inside each service

        Args:
            to do

        Returns:
            Three graphs, the first telling the proportion of infection derived from workers and clients, the second from workers to workers and clients, and the third from clients to workers and clients

        PS: This code has been developed by Edmilson Roque - USP <edmilson.roque.usp@gmail.com>
        """
        # Making sure uncertainties is installed
        try:
            from uncertainties import ufloat
            from uncertainties import unumpy as unp
        except Exception as e:
            print(
                e,
                "Please install package 'uncertainties' in order to compare simulations amongst each other",
            )

        colors_deep = sns.color_palette("muted")
        S.COLORS_DEEP = {
            "blue": colors_deep[0],
            "orange": colors_deep[1],
            "green": colors_deep[2],
            "red": colors_deep[3],
            "purple": colors_deep[4],
            "brown": colors_deep[5],
            "pink": colors_deep[6],
            "grey": colors_deep[7],
            "yellow": colors_deep[8],
            "teal": colors_deep[9],
        }
        """Colors for ploting graphs, less bright variant
        (available colors: blue, orange, green, red, purple, brown, pink,
         gray, yellow, teal)"""

        S.COLORS_DEEP_LIST = list(S.COLORS_DEEP.values())

        self.start_plt(size=size, title=title)
        fig, ax = self.plt.subplots(1, 3, dpi=300, figsize=size)  # , sharey=True)
        # fig.suptitle('Proportion of infections at service', fontsize=10)

        places = self.post_processed_data["service_labels"][1]
        num_srvc = len(places)

        mean_infs = np.mean(self.post_processed_data["num_infs"][1])
        std_infs = np.std(self.post_processed_data["num_infs"][1])
        mean_infs_from_w = np.mean(self.post_processed_data["infs_from_w"][1], axis=0)
        std_infs_from_w = np.std(self.post_processed_data["infs_from_w"][1], axis=0)
        mean_infs_wtow = np.mean(
            self.post_processed_data["infs_from_w_to_w"][1], axis=0
        )
        std_infs_wtow = np.std(self.post_processed_data["infs_from_w_to_w"][1], axis=0)
        mean_infs_wtoc = np.mean(
            self.post_processed_data["infs_from_w_to_c"][1], axis=0
        )
        std_infs_wtoc = np.std(self.post_processed_data["infs_from_w_to_c"][1], axis=0)
        mean_infs_from_c = np.mean(self.post_processed_data["infs_from_c"][1], axis=0)
        std_infs_from_c = np.std(self.post_processed_data["infs_from_c"][1], axis=0)
        mean_infs_ctow = np.mean(
            self.post_processed_data["infs_from_c_to_w"][1], axis=0
        )
        std_infs_ctow = np.std(self.post_processed_data["infs_from_c_to_w"][1], axis=0)
        mean_infs_ctoc = np.mean(
            self.post_processed_data["infs_from_c_to_c"][1], axis=0
        )
        std_infs_ctoc = np.std(self.post_processed_data["infs_from_c_to_c"][1], axis=0)

        infs = ufloat(mean_infs, std_infs)

        # Infections at service from workers/clients
        perc_infs_from_w = np.zeros(num_srvc)
        std_perc_infs_from_w = np.zeros(num_srvc)
        for ind, from_w in enumerate(unp.uarray(mean_infs_from_w, std_infs_from_w)):
            perc_infs_from_w[ind] = 100 * (from_w / infs).n
            std_perc_infs_from_w[ind] = 100 * (from_w / infs).s
        ax[0].bar(
            places,
            perc_infs_from_w,
            0.5,
            yerr=std_perc_infs_from_w,
            color=S.COLORS_DEEP_LIST[4],
            ecolor=S.COLORS_DEEP_LIST[2],
        )
        perc_infs_from_c = np.zeros(num_srvc)
        std_perc_infs_from_c = np.zeros(num_srvc)
        for ind, from_c in enumerate(unp.uarray(mean_infs_from_c, std_infs_from_c)):
            perc_infs_from_c[ind] = 100 * (from_c / infs).n
            std_perc_infs_from_c[ind] = 100 * (from_c / infs).s
        ax[0].bar(
            places,
            perc_infs_from_c,
            0.5,
            bottom=perc_infs_from_w,
            color=S.COLORS_DEEP_LIST[1],
            ecolor=S.COLORS_DEEP_LIST[9],
        )
        ax[0].set_xticks(np.arange(0, len(places), 1, dtype=int))
        ax[0].set_xticklabels(places, rotation=45, horizontalalignment="right")
        ax[0].set_title("Infs. derived from workers/clients", fontsize=8)
        ax[0].set_ylabel("% of infections w resp. to all new infections")
        ax[0].legend(["from workers", "from clients"])

        # Infections at service from workers to workers/clients
        perc_infs_wtow = np.zeros(num_srvc)
        std_perc_infs_wtow = np.zeros(num_srvc)
        for ind, (wtow, from_w) in enumerate(
            zip(
                unp.uarray(mean_infs_wtow, std_infs_wtow),
                unp.uarray(mean_infs_from_w, std_infs_from_w),
            )
        ):
            if from_w.n != 0:
                perc_infs_wtow[ind] = 100 * (wtow / from_w).n
                std_perc_infs_wtow[ind] = 100 * (wtow / from_w).s
            else:
                perc_infs_wtow[ind] = 0
                std_perc_infs_wtow[ind] = 0
        ax[1].bar(
            places,
            perc_infs_wtow,
            0.5,
            yerr=std_perc_infs_wtow,
            color="indigo",
            ecolor=S.COLORS_DEEP_LIST[2],
        )
        perc_infs_wtoc = np.zeros(num_srvc)
        std_perc_infs_wtoc = np.zeros(num_srvc)
        for ind, (wtoc, from_w) in enumerate(
            zip(
                unp.uarray(mean_infs_wtoc, std_infs_wtoc),
                unp.uarray(mean_infs_from_w, std_infs_from_w),
            )
        ):
            if from_w.n != 0:
                perc_infs_wtoc[ind] = 100 * (wtoc / from_w).n
                std_perc_infs_wtoc[ind] = 100 * (wtoc / from_w).s
            else:
                perc_infs_wtoc[ind] = 0
                std_perc_infs_wtoc[ind] = 0
        ax[1].bar(
            places,
            perc_infs_wtoc,
            0.5,
            bottom=perc_infs_wtow,
            color="darkorchid",
            ecolor=S.COLORS_DEEP_LIST[9],
        )
        # yerr=std_perc_infs_wtow,
        ax[1].hlines(
            50,
            -0.3,
            len(places) - 0.55,
            linestyles="dashed",
            colors=S.COLORS_DEEP_LIST[9],
        )
        ax[1].set_xticks(np.arange(0, len(places), 1, dtype=int))
        ax[1].set_xticklabels(places, rotation=45, horizontalalignment="right")
        ax[1].set_title("Infs. from workers to workers/clients", fontsize=8)
        ax[1].set_ylabel("% of infections w resp. to worker infections")
        ax[1].legend(["50%", "to workers", "to clients"])

        # Infections at service from clients to workers/clients
        perc_infs_ctow = np.zeros(num_srvc)
        std_perc_infs_ctow = np.zeros(num_srvc)
        for ind, (ctow, from_c) in enumerate(
            zip(
                unp.uarray(mean_infs_ctow, std_infs_ctow),
                unp.uarray(mean_infs_from_c, std_infs_from_c),
            )
        ):
            if from_c.n != 0:
                perc_infs_ctow[ind] = 100 * (ctow / from_c).n
                std_perc_infs_ctow[ind] = 100 * (ctow / from_c).s
            else:
                perc_infs_ctow[ind] = 0
                std_perc_infs_ctow[ind] = 0
        ax[2].bar(
            places,
            perc_infs_ctow,
            0.5,
            yerr=std_perc_infs_ctow,
            color="indigo",
            ecolor=S.COLORS_DEEP_LIST[2],
        )
        perc_infs_ctoc = np.zeros(num_srvc)
        std_perc_infs_ctoc = np.zeros(num_srvc)
        for ind, (ctoc, from_c) in enumerate(
            zip(
                unp.uarray(mean_infs_ctoc, std_infs_ctoc),
                unp.uarray(mean_infs_from_c, std_infs_from_c),
            )
        ):
            if from_c.n != 0:
                perc_infs_ctoc[ind] = 100 * (ctoc / from_c).n
                std_perc_infs_ctoc[ind] = 100 * (ctoc / from_c).s
            else:
                perc_infs_ctoc[ind] = 0
                std_perc_infs_ctoc[ind] = 0
        ax[2].bar(
            places,
            perc_infs_ctoc,
            0.5,
            bottom=perc_infs_ctow,
            color="darkorchid",
            ecolor=S.COLORS_DEEP_LIST[9],
        )
        # yerr=std_perc_infs_ctow,
        ax[2].hlines(
            50,
            -0.3,
            len(places) - 0.55,
            linestyles="dashed",
            colors=S.COLORS_DEEP_LIST[9],
        )
        ax[2].set_xticks(np.arange(0, len(places), 1, dtype=int))
        ax[2].set_xticklabels(places, rotation=45, horizontalalignment="right")
        ax[2].set_title("Infs. from clients to workers/clients", fontsize=8)
        ax[2].set_ylabel("% of infections w resp. to client infections")
        ax[2].legend(["50%", "to workers", "to clients"])

        if close:
            self.stop_plt(filename=filename)

    def plot_inf_source_symptoms(
        self,
        filename=None,
        seed=-1,
        hide_zeros=False,
        size=(5, 4),
        title=None,
        pie=True,
        close=True,
    ):
        """Plot vector symptom data. Can only plot one seed.

        Args:
            filename (str, optional): File name to save the graph, if not informed will only show the plot.
            hide_zeros (bool, optional): Hide placements with no infections. Defaults to False.
            size (tuple, optional): Size to generate the plot. Defaults to (10, 4).
            title (str, optional): Title to be printed on the plot.
            seed (str, optional): Seed to be plotted, if None will plot last loaded.
            close (bool, optional): If False doesn't show, save or close the plot so that the user can add
                more information to plot using the plt attribute from the class. Defaults to False.
        """
        if type(seed) is list:
            if len(seed) != 1:
                print(
                    "[WARNING] Can only plot infections with single seed, plotting last seed loaded."
                )
        data = self.get_data(["inf_vector_sympt"], seed)
        vec_symptoms = data["inf_vector_sympt"][0]
        # Classify infections by symptomatic state of vector
        data_symp = {
            "mild symptomatic": np.sum(vec_symptoms == S.SYMPT_YES),
            "severe symptomatic": np.sum(vec_symptoms == S.SYMPT_SEVERE),
            "asymptomatic": np.sum(vec_symptoms == S.SYMPT_NO),
            "pre-symptomatic": np.sum(vec_symptoms == S.SYMPT_NYET),
        }
        Ninfections = np.sum(list(data_symp.values()))
        for k in data_symp.keys():
            data_symp[k] = data_symp[k] * 100 / np.max([Ninfections, 1.0])

        # Plot graphs
        title = title or "Infection source symptoms"
        self.start_plt(size=size, title=title)
        if pie:
            autotexts = self.plt.pie(
                data_symp.values(),
                labels=data_symp.keys(),
                colors=S.COLORS_SYMPT,
                autopct="%1.1f%%",
            )[2]
            self.plt.setp(autotexts, color="white")
        else:
            x = np.arange(len(data_symp))
            self.plt.bar(x, data_symp.values())  # ,
            self.plt.xticks(
                x, data_symp.keys(), rotation=45, horizontalalignment="right"
            )
        if close:
            self.stop_plt(filename=filename, tight_layout=True)

    def plot_encounters(self, seeds=None, plot_per_day=True, **plot_args):
        """Plots the number of encounters.

        !!! Important
            This plot uses the do_plot method, see [plot_args](#plot_args) to see all available arguments.

        Args:
            seeds (list, optional): List of seeds to plot, if None will plot all seeds available.
            plot_per_day (bool, optional): Consolidate the data on a one day resolution.
            **plot_args (kwargs): Arguments for the do_plot method (see [plot_args](#plot_args)).
        """
        data = self.get_data(
            ["encounters", "encounters_infectious", "possible_infections"], seeds
        )
        seeds = data["seeds"]
        encounters = np.array(data["encounters"])
        encounters_infectious = np.array(data["encounters_infectious"])
        possible_infections = np.array(data["possible_infections"])
        Nparticles = self.parameters["Nparticles"]
        Nsteps = self.parameters["Nsteps"]
        dt = self.parameters["dt"]
        n_seeds = len(data["seeds"])
        if plot_per_day:
            encounters = (
                np.sum(
                    encounters.reshape((n_seeds, int(Nsteps * dt / 24), int(24 / dt))),
                    axis=2,
                )
                / Nparticles
            )
            encounters_infectious = (
                np.sum(
                    encounters_infectious.reshape(
                        (n_seeds, int(Nsteps * dt / 24), int(24 / dt))
                    ),
                    axis=2,
                )
                / Nparticles
            )
            possible_infections = (
                np.sum(
                    possible_infections.reshape(
                        (n_seeds, int(Nsteps * dt / 24), int(24 / dt))
                    ),
                    axis=2,
                )
                / Nparticles
            )
            t = np.arange(int(Nsteps * dt / 24))
            yLabel = "N of encounters per day*particle"
        else:
            t = dt * np.arange(Nsteps) / 24
            yLabel = "N of encounters per step"
        data = {
            "all encounters": encounters,
            "infectious -> any": encounters_infectious,
            "infectious -> susceptible": possible_infections,
        }
        colors = [S.COLORS["red"], S.COLORS["green"], S.COLORS["blue"]]

        # Plot graphs
        def_plot_args = {
            "xlabel": "Days",
            "ylabel": yLabel,
            "legend_args": {"loc": "upper left"},
            "bands_legend_args": {"color": S.COLORS["red"], "loc": "upper right"},
        }
        plot_args = self.args_parse(plot_args, def_plot_args)
        self.do_plot(t, dt, data, colors, seeds, **plot_args)

    def plot_quarantine(self, seeds=None, **plot_args):
        """Plots the number of encounters.

        !!! Important
            This plot uses the do_plot method, see [plot_args](#plot_args) to see all available arguments.

        Args:
            seeds (list, optional): List of seeds to plot, if None will plot all seeds available.
            **plot_args (kwargs): Arguments for the do_plot method (see [plot_args](#plot_args)).
        """
        data = self.get_data(["quarantined"], seeds)
        seeds = data["seeds"]
        quarantined = np.array(data["quarantined"])
        Nparticles = self.parameters["Nparticles"]
        data = {"All": None}
        for qrnt in self.parameters["quarantines"]:
            data[qrnt["name"]] = quarantined[:, int(qrnt["id"]), :] * 100 / Nparticles
            try:
                data["All"] = data["All"] + data[qrnt["name"]]
            except:
                data["All"] = data[qrnt["name"]]
        if len(self.parameters["quarantines"]) <= 1:
            del data["All"]
        Nsteps = self.parameters["Nsteps"]
        dt = self.parameters["dt"]
        t = dt * np.arange(Nsteps) / 24

        # Plot graphs
        def_plot_args = {
            "xlabel": "Days",
            "ylabel": "% population in quarantine",
            "legend_args": {"loc": "upper left"},
            "bands_legend_args": {"color": S.COLORS_LIST[0], "loc": "upper right"},
        }
        plot_args = self.args_parse(plot_args, def_plot_args)
        self.do_plot(t, dt, data, S.COLORS_LIST, seeds, **plot_args)

    def plot_diagnosed(self, seeds=None, cumulative=True, **plot_args):
        """Plots the number of encounters.

        !!! Important
            This plot uses the do_plot method, see [plot_args](#plot_args) to see all available arguments.

        Args:
            seeds (list, optional): List of seeds to plot, if None will plot all seeds available.
            cumulative (bool, optional): Plot the cumulative diagnostics curve as well. Defaults to True.
            **plot_args (kwargs): Arguments for the do_plot method (see [plot_args](#plot_args)).
        """
        data = self.get_data(["diagnosed", "diagnosed_cum"], seeds)
        seeds = data["seeds"]
        Nparticles = self.parameters["Nparticles"]
        diag_cumm = np.array(data["diagnosed_cum"])
        data = {"diagnosed": np.array(data["diagnosed"]) * 100.0 / Nparticles}
        if cumulative:
            data["diagnosed cumulative"] = diag_cumm * 100.0 / Nparticles
        Nsteps = self.parameters["Nsteps"]
        dt = self.parameters["dt"]
        t = dt * np.arange(Nsteps) / 24

        # Plot graphs
        def_plot_args = {
            "xlabel": "Days",
            "ylabel": "% population diagnosed",
            "legend_args": {"loc": "upper left"},
            "bands_legend_args": {"color": S.COLORS["orange"], "loc": "center left"},
        }
        plot_args = self.args_parse(plot_args, def_plot_args)
        self.do_plot(
            t, dt, data, [S.COLORS["orange"], S.COLORS["red"]], seeds, **plot_args
        )

    def plot_inf_probability(
        self,
        filename=None,
        seed=-1,
        bins=100,
        size=(5, 4),
        title=None,
        remove_zeros=False,
        close=True,
    ):
        """Plot a histogram of the maximum infection probability of all particles. Can only plot one seed.

        Args:
            filename (str, optional): File name to save the graph, if not informed will only show the plot.
            bins (int, optional): Number of bins in the histogram. Defaults to 100.
            size (tuple, optional): Size to generate the plot. Defaults to (5, 4).
            title (str, optional): Title to be printed on the plot.
            seed (str, optional): Seed to be plotted, if None will plot last loaded.
            remove_zeros (bool, optional): Ignore particles with infection probability equals to zero.
                Defaults to False.
            close (bool, optional): If False doesn't show, save or close the plot so that the user can add
                more information to plot using the plt attribute from the class. Defaults to False.
        """
        if type(seed) is list:
            if len(seed) != 1:
                print(
                    "[WARNING] Can only plot infection probability with single seed, plotting last seed loaded."
                )
        data = self.get_data(["inf_prob"], seed)
        inf_prob = np.array(data["inf_prob"])
        if remove_zeros:
            inf_prob = inf_prob[inf_prob > 0]
        mean = np.mean(inf_prob)
        median = np.median(inf_prob)

        # Plot graphs
        self.start_plt(size=size, title=title)
        count, _, _ = self.plt.hist(inf_prob, bins=bins, density=False)
        self.plt.plot(
            [mean, mean], [0, np.max(count)], "--", label="mean = {:}".format(mean)
        )
        self.plt.plot(
            [median, median],
            [0, np.max(count)],
            "--",
            label="median = {:}".format(median),
        )
        if close:
            self.stop_plt(
                filename=filename,
                xlabel="Infection probability",
                ylabel="Number of particles",
            )

    def plot_isolation_percentage(
        self,
        filename=None,
        size=(5, 4),
        seeds=None,
        dates=True,
        start_day=None,
        end_day=None,
        title=None,
        events=False,
        events_type=0,
    ):
        """Plot a isolation curve in time.

        Args:
            filename (str, optional): File name to save the graph, if not informed will only show the plot.
            size (tuple, optional): Size to generate the plot. Defaults to (5, 4).
            title (str, optional): Title to be printed on the plot.
            seeds (list, optional): List of seeds to plot, if None will plot all seeds available.
            dates (bool, optional): Use dates instead of days in the x axis. Defaults to True.
            start_day (float, optional): Plot only the interval from start_day. Defaults to 0.
            end_day (float, optional): Plot only the interval to end_day. Defaults to last day.
            events (bool, optional): Overlay simulation events on the plot. Defaults to False.
            events_type (int, optional): Type of logs to overlay as events. Defaults to S.MSG_EVENTS.
        """
        # Prepare Data
        data = self.get_data(["isol_pct"], seeds)
        isol_ts = np.array(data["isol_pct"])
        Ndays = self.parameters["Ndays"]
        dt = self.parameters["dt"]
        t = np.arange(Ndays)
        interval = self.days_to_interval(start_day, end_day, dt)

        # Plot graphs
        self.start_plt(size=size, title=title)
        self.plot_line(t, isol_ts, color="red", label="isolated")
        if events:
            self.plot_events(seeds, events_type=events_type)
            self.plt.legend()
        dates_args = {"start_day": start_day, "end_day": end_day}
        self.stop_plt(
            filename=filename,
            xlabel="Days",
            ylabel="% Isolated",
            dates=dates,
            dates_args=dates_args,
        )

    def plot_homes_infection_hist(
        self, filename=None, size=(5, 4), seed=-1, title=None, bins=10, close=True
    ):
        """Plots a histogram of the percentage of infection that occurred inside each home. Can only
            plot one seed.

        Args:
            filename (str, optional): File name to save the graph, if not informed will only show the plot.
            size (tuple, optional): Size to generate the plot. Defaults to (5, 4).
            title (str, optional): Title to be printed on the plot.
            seed (str, optional): Seed to be plotted, if None will plot last loaded.
            bins (int, optional): Number of bins in the histogram. Defaults to 100.
            close (bool, optional): If False doesn't show, save or close the plot so that the user can add
                more information to plot using the plt attribute from the class. Defaults to False.
        """
        if type(seed) is list:
            if len(seed) != 1:
                print(
                    "[WARNING] Can only plot homes infection histogram with single seed, plotting last seed loaded."
                )
        data = self.get_data(["homesNumber", "home_id", "inf_placement"], seed)
        Nhomes = data["homesNumber"][0]
        serv_per_id = data["home_id"][0]
        inf_placement = data["inf_placement"][0]
        [inf_servs, tot_servs, inf_servs_percent] = tools.get_service_infection_percent(
            Nhomes, serv_per_id, inf_placement
        )

        # Plot graphs
        self.start_plt(size=size, title=title)
        plt.hist(inf_servs_percent, bins=bins)
        if close:
            self.stop_plt(
                filename=filename,
                xlabel="Percentage of particles infected at home",
                ylabel="Number of homes",
            )
        return inf_servs, tot_servs, inf_servs_percent

    def plot_secondary_infections_hist(
        self, filename=None, size=(5, 4), title=None, seed=-1, close=True
    ):
        """Plot a secondary infections histogram. Can only plot one seed.

        Args:
            filename (str, optional): File name to save the graph, if not informed will only show the plot.
            size (tuple, optional): Size to generate the plot. Defaults to (5, 4).
            title (str, optional): Title to be printed on the plot.
            seed (str, optional): Seed to be plotted, if None will plot last loaded.
            close (bool, optional): If False doesn't show, save or close the plot so that the user can add
                more information to plot using the plt attribute from the class. Defaults to False.
        """
        if type(seed) is list:
            if len(seed) != 1:
                print(
                    "[WARNING] Can only plot secondary infections with single seed, plotting last seed loaded."
                )
        data = self.get_data(["inf_source", "states"], seed)
        [
            source_inf_ind,
            source_inf_count,
            nonzeropercent,
            infectiouspercent,
        ] = tools.get_source_inf_count(data["inf_source"][0], data["states"][0][-2, :])
        nbins = np.max(source_inf_count)

        # Plot graphs
        self.start_plt(size=size, title=title)
        self.plt.hist(source_inf_count, bins=nbins)
        self.plt.legend(
            ["Effective = {:.1f} of infectious".format(nonzeropercent * 100)]
        )
        if close:
            self.stop_plt(
                filename=filename,
                xlabel="Number of secondary infections",
                ylabel="Number of particles",
            )
        return source_inf_ind, source_inf_count, nonzeropercent, infectiouspercent

    def plot_homes_hist(
        self, data="Symptoms", bins=10, filename=None, size=(5, 4), title=None, seed=-1
    ):
        """Plots histograms of the percentage of particles on each category on all homes.
            Can only plot one seed.

        Available data:

        * `symptoms`: Plots percentages of mild symptomatic, severe symptomatic, asymptomatic
            and not infected;
        * `infections`: Plots percentages of Infected ans not infected.

        Args:
            data (str, optional): Data to be plotted, \"Symptoms\" or \"Infections\".
                Defaults to "Symptoms".
            bins (int, optional): Number of bins in the histogram. Defaults to 10.
            filename (str, optional): File name to save the graph, if not informed will only show the plot.
            size (tuple, optional): Size for the plot. Defaults to (5, 4).
            title (str, optional): Title to be printed on the plot.
            seed (str, optional): Seed to be plotted, if None will plot last loaded.

        Raises:
            RuntimeWarning: Invalid data selected or data unavailible.
        """
        if type(seed) is list:
            if len(seed) != 1:
                print(
                    "[WARNING] Can only plot homes histograms with single seed, plotting last seed loaded."
                )
        if data.lower() == "symptoms":
            data_loaded = self.get_data(["symptoms", "homes"], seed)
            data_source = data_loaded["symptoms"][0][-1]
            data_dict = {
                "Mild Symptomatic": [S.SYMPT_YES, S.SYMPT_YES * 10],
                "Severe Symptomatic": [S.SYMPT_SEVERE, S.SYMPT_SEVERE * 10],
                "Asymptomatic": [S.SYMPT_NO, S.SYMPT_NO * 10, S.SYMPT_NYET],
                "Not Infected": [S.NO_INFEC],
            }
            data_color = S.COLORS_SYMPT[: len(data_dict)]
        elif data.lower() == "infections":
            data_loaded = self.get_data(["states", "homes"], seed)
            data_source = data_loaded["states"][0][-1]
            data_dict = {
                "Not Infected": [S.STATE_S],
                "Infected": [S.STATE_E, S.STATE_I, S.STATE_R],
            }
            data_color = [S.COLORS["blue"], S.COLORS["red"]]
        else:
            raise RuntimeWarning("{} is not a valid data.".format(data))
        homes = data_loaded["homes"][0]
        data_array = np.zeros((len(homes), len(data_dict)))
        for i, home in enumerate(homes.values()):
            for j, l in enumerate(data_dict.values()):
                for pid in home["pids"]:
                    if data_source[pid] in l:
                        data_array[i, j] += 1
            data_array[i, :] /= home["nparticles"]

        for j, k in enumerate(data_dict.keys()):
            title = title or "Histogram of {} at homes".format(data.lower())
            self.start_plt(size=size, title=title)
            heights, x = np.histogram(data_array[:, j], bins=bins)
            heights = heights / np.sum(heights)
            x = x[:-1] + (x[1] - x[0]) / 2
            width = x[1] - x[0]
            self.plt.bar(x, heights, width=width, color=data_color[j], label=k)
            ax = self.plt.gca()
            ax.yaxis.set_major_formatter(PercentFormatter(1))
            ax.xaxis.set_major_formatter(PercentFormatter(1))
            if filename != None and len(data_dict) > 1:
                f = "{}_{}.{}".format(
                    ".".join(filename.split(".")[:-1]), j, filename.split(".")[-1]
                )
            else:
                f = filename
            self.stop_plt(
                filename=f,
                xlabel="Percentage of the family",
                ylabel="Percentage of homes",
                legend=True,
            )

    def plot_contact_or_encounters_age_group(
        self,
        plotContacts=True,
        filename=None,
        slice_rng=slice(1, None),
        size=(10, 4),
        seed=-1,
        title=None,
        close=True,
    ):
        if type(seed) is list:
            if len(seed) != 1:
                print(
                    "[WARNING] Can only plot encounters or contacts matrix with single seed, plotting last seed loaded."
                )
        data = self.get_data(["ages", "tracing"], seed)
        tracing = []
        for i in range(len(data["tracing"][0])):
            d = nx.to_dict_of_lists(data["tracing"][0][i])
            d = {int(pid): [int(i) for i in d[pid]] for pid in d.keys()}
            tracing.append(d)
        if plotContacts:
            [array_age_group, matrix_age_group] = tools.get_contact_array_age_group(
                tracing, data["ages"][0], self.parameters["dt"]
            )
        else:
            [array_age_group, matrix_age_group] = tools.get_encounters_array_age_group(
                tracing, data["ages"][0], S.ALL_AGES, self.parameters["dt"]
            )
        bars = S.AGE_DEF
        pos = S.ALL_AGES

        self.start_plt(size=size, title=title)
        self.plt.subplot(1, 2, 1)
        self.plt.plot(pos, array_age_group)
        self.plt.xticks(pos, bars, rotation=45, horizontalalignment="right")
        if plotContacts:
            self.plt.xlabel("Age Groups")
            self.plt.ylabel("Number of contacts / day")
            self.plt.title("Mean number of total contacts per age group / day")
            self.plt.subplot(1, 2, 2)
            self.plt.title("Mean number of contacts between age groups / day")
        else:
            self.plt.xlabel("Age Groups")
            self.plt.ylabel("#(encounters) / #(particles in age group) day")
            self.plt.title("Mean number of total encounters per age group / day")
            self.plt.subplot(1, 2, 2)
            self.plt.title("Mean number of encounters between age groups / day")
        ax = self.plt.gca()
        ax.set_xlim(0, matrix_age_group.shape[0] - 1)
        ax.set_ylim(0, matrix_age_group.shape[1] - 1)
        sc = self.plt.imshow(matrix_age_group)
        sc.set_cmap("jet")
        self.plt.colorbar(sc)
        self.plt.tight_layout(pad=0.5)
        self.plt.xticks(pos[::5], bars[::5], rotation=45, horizontalalignment="right")
        self.plt.yticks(pos[4::5], bars[4::5], rotation=45, horizontalalignment="right")
        if close:
            self.stop_plt(filename=filename, xlabel="Age Groups", ylabel="Age Groups")
        return array_age_group, matrix_age_group

    def plot_contact_matrix_age_group(
        self,
        filename=None,
        slice_rng=slice(1, None),
        size=(10, 4),
        title=None,
        seed=-1,
        close=True,
    ):
        """Plot a contacts matrix per age group. Contacts are unique in a day,
            encounters are not (particle A can have multiple encounters with particle B in a
            single day, but only one contact).

        Args:
            filename (str, optional): File name to save the graph, if not informed will only show the plot.
            slice_rng (slice, optional): Slice of days (steps?) to plot. Defaults to slice(1, None).
            size (tuple, optional): Size for the plot. Defaults to (10, 4).
            title (str, optional): Title to be printed on the plot.
            seed (str, optional): Seed to be plotted, if None will plot last loaded.
            close (bool, optional): If False doesn't show, save or close the plot so that the user can add
                more information to plot using the plt attribute from the class. Defaults to False.
        """
        return self.plot_contact_or_encounters_age_group(
            True, filename, slice_rng, size, seed, title, close
        )

    def plot_encounters_matrix_age_group(
        self,
        filename=None,
        slice_rng=slice(1, None),
        size=(10, 4),
        title=None,
        seed=-1,
        close=True,
    ):
        """Plot an encounters matrix per age group. Contacts are unique in a day,
            encounters are not (particle A can have multiple encounters with particle B in a
            single day, but only one contact).

        Args:
            filename (str, optional): File name to save the graph, if not informed will only show the plot.
            slice_rng (slice, optional): Slice of days (steps?) to plot. Defaults to slice(1, None).
            size (tuple, optional): Size for the plot. Defaults to (10, 4).
            title (str, optional): Title to be printed on the plot.
            seed (str, optional): Seed to be plotted, if None will plot last loaded.
            close (bool, optional): If False doesn't show, save or close the plot so that the user can add
                more information to plot using the plt attribute from the class. Defaults to False.
        """
        return self.plot_contact_or_encounters_age_group(
            False, filename, slice_rng, size, seed, title, close
        )

    def plot_encounters_age_group(
        self,
        filename=None,
        slice_rng=slice(1, None),
        size=(5, 4),
        title=None,
        seed=-1,
        close=True,
    ):
        """Plots number of encounters per age group on a slice of time.

        Args:
            filename (str, optional): File name to save the graph, if not informed will only show the plot.
            slice_rng (slice, optional): Slice of (steps?) to plot. Defaults to slice(1, None).
            size (tuple, optional): Size for the plot. Defaults to (5, 4).
            title (str, optional): Title to be printed on the plot.
            seed (str, optional): Seed to be plotted, if None will plot last loaded.
            close (bool, optional): If False doesn't show, save or close the plot so that the user can add
                more information to plot using the plt attribute from the class. Defaults to False.
        """
        if type(seed) is list:
            if len(seed) != 1:
                print(
                    "[WARNING] Can only plot encounters per age group with single seed, plotting last seed loaded."
                )
        data = self.get_data(["ages", "tracing"], seed)
        tracing = []
        for i in range(len(data["tracing"][0])):
            d = nx.to_dict_of_lists(data["tracing"][0][i])
            d = {int(pid): [int(i) for i in d[pid]] for pid in d.keys()}
            tracing.append(d)
        matrix_age_group = tools.get_mean_encounter_time_array_age_group(
            tracing, data["ages"][0], self.parameters["dt"]
        )
        bars = S.AGE_DEF
        pos = S.ALL_AGES

        self.start_plt(size=size, title=title)
        self.plt.title("Mean number of contacts between age groups / day")
        ax = self.plt.gca()
        ax.set_xlim(0, matrix_age_group.shape[0] - 1)
        ax.set_ylim(0, matrix_age_group.shape[1] - 1)
        sc = self.plt.imshow(matrix_age_group)
        sc.set_cmap("jet")
        self.plt.colorbar(sc)
        self.plt.tight_layout(pad=0.5)
        self.plt.xticks(pos[::5], bars[::5], rotation=45, horizontalalignment="right")
        self.plt.yticks(pos[4::5], bars[4::5], rotation=45, horizontalalignment="right")
        if close:
            self.stop_plt(filename=filename, xlabel="Age Groups", ylabel="Age Groups")

    def plot_age_group_histogram(
        self,
        data="infected",
        percentage=False,
        filename=None,
        size=(5, 4),
        title=None,
        seed=-1,
        close=True,
    ):
        """Plots a specift count of particles per age group. Can only plot one seed.

        Available counts:

        * `infected`: number of infected particles.
        * `vaccinated`: number of vaccinated particles.
        * `deceased`: number of deceased particles.
        * `particles`: total number of particles

        Args:
            data (str, optional): Type of count to plot. Defaults to "infected".
            percentage (bool, optional): Y axis as a percentage. Defaults to False.
            filename (str, optional): File name to save the graph, if not informed will only show the plot.
            size (tuple, optional): Size for the plot. Defaults to (5, 4).
            title (str, optional): Title to be printed on the plot.
            seed (str, optional): Seed to be plotted, if None will plot last loaded.
            close (bool, optional): If False doesn't show, save or close the plot so that the user can add
                more information to plot using the plt attribute from the class. Defaults to False.
        """
        if type(seed) is list:
            if len(seed) != 1:
                print(
                    "[WARNING] Can only plot age group histograms with single seed, plotting last seed loaded."
                )
        data_loaded = self.get_data(["inf_tree"], seed)
        particles = False
        if data.lower() == "infected":
            tag = "time_exposed"
        elif data.lower() == "vaccinated":
            tag = "time_vaccinated"
        elif data.lower() == "deceased":
            tag = "time_deceased"
        elif data.lower() == "particles":
            data = "of " + data
            tag = "age_group"
            particles = True
        else:
            self.raise_runtime("Invalid data {}, can't plot.".format(data))
        data_plot = np.zeros(len(S.ALL_AGES))
        count = np.zeros(len(S.ALL_AGES))
        inf_tree = data_loaded["inf_tree"][0]
        for node in inf_tree.nodes:
            for key in inf_tree.nodes[node].keys():
                if key.split("]")[-1] == tag:
                    data_plot[inf_tree.nodes[node]["age_group"]] += 1
                    break
            count[inf_tree.nodes[node]["age_group"]] += 1
        if particles:
            count = self.parameters["Nparticles"]
        if percentage:
            data_plot = (data_plot / count) * 100.0

        self.start_plt(size=size, title=title)
        plt.bar(S.AGE_DEF, data_plot)
        plt.xticks(rotation=45, horizontalalignment="right")
        if close:
            if percentage:
                self.stop_plt(
                    filename=filename,
                    xlabel="Age Groups",
                    ylabel="Percentage {}".format(data),
                )
            else:
                self.stop_plt(
                    filename=filename,
                    xlabel="Age Groups",
                    ylabel="Number {}".format(data),
                )

    def plot_time_between_hist(
        self,
        time_in=["time_infectious"],
        time_out=["time_recovered", "time_dead"],
        bins=10,
        percentage=False,
        filename=None,
        size=(5, 4),
        title=None,
        seed=-1,
        close=True,
    ):
        """Plot a histogram of the duration between two times saved in the infection tree for all particles.
            Can only plot one seed.

        Available times:

        * `time_exposed`
        * `time_infectious`
        * `time_activated`
        * `time_recovered`
        * `time_deceased`
        * `time_vaccinated`
        * `time_immune`
        * `time_quarantined`
        * `time_released_quarantine`

        Args:
            time_in (list, optional): A list of times to be accepted as a start time, if a particle
                has more than one the first in the list will be used. Defaults to ["time_infectious"].
            time_out (list, optional): A list of times to be accepted as a end time, if a particle
                has more than one the first in the list will be used. Defaults to ["time_recovered", "time_dead"].
            bins (int, optional): Number of bins in the histogram. Defaults to 10.
            percentage (bool, optional): Y axis as a percentage. Defaults to False.
            filename (str, optional): File name to save the graph, if not informed will only show the plot.
            size (tuple, optional): Size for the plot. Defaults to (5, 4).
            title (str, optional): Title to be printed on the plot.
            seed (str, optional): Seed to be plotted, if None will plot last loaded.
            close (bool, optional): If False doesn't show, save or close the plot so that the user can add
                more information to plot using the plt attribute from the class. Defaults to False.
        """
        if type(seed) is list:
            if len(seed) != 1:
                print(
                    "[WARNING] Can only plot time between histograms with single seed, plotting last seed loaded."
                )
        data_loaded = self.get_data(["inf_tree"], seed)
        inf_tree = data_loaded["inf_tree"][0]
        data = []
        time_in = [t.lower() for t in time_in]
        time_out = [t.lower() for t in time_out]
        for node in inf_tree.nodes:
            in_ok = False
            out_ok = False
            for time in time_in:
                for key in inf_tree.nodes[node].keys():
                    if key.split("]")[-1] == time:
                        time_in_value = inf_tree.nodes[node][time]
                        in_ok = True
                        break
                if in_ok:
                    break
            for time in time_out:
                for key in inf_tree.nodes[node].keys():
                    if key.split("]")[-1] == time:
                        time_out_value = inf_tree.nodes[node][time]
                        out_ok = True
                        break
                if out_ok:
                    break
            if in_ok and out_ok:
                data.append(time_out_value - time_in_value)

        self.start_plt(size=size, title=title)
        plt.hist(data, bins=bins)
        plt.xticks(rotation=45, horizontalalignment="right")
        if close:
            self.stop_plt(filename=filename, xlabel="Time", ylabel="Number")

    def plot_inf_tree(
        self, filename=None, size=(20, 20), seed=-1, color=None, title=None, close=True
    ):
        """Plots the infection tree. Can only plot one seed.

        Available color markers:
        * `placement`: placement where the infection occurred.

        Args:
            filename (str, optional): File name to save the graph, if not informed will only show the plot.
            size (tuple, optional): Size for the plot. Defaults to (20, 20).
            seed (str, optional): Seed to be plotted, if None will plot last loaded.
            color (str, optional): A color marker. Defaults to None.
            title (str, optional): Title for the plot. Defaults to None.
            close (bool, optional): If False doesn't show, save or close the plot so that the user can add
                more information to plot using the plt attribute from the class. Defaults to False.
        """
        if type(seed) is list:
            if len(seed) != 1:
                print(
                    "[WARNING] Can only plot time between histograms with single seed, plotting last seed loaded."
                )
        data_loaded = self.get_data(["inf_tree"], seed)
        inf_tree = data_loaded["inf_tree"][0]
        plt.figure(figsize=size)
        if title != None:
            plt.title(title)
        nodes = {}
        for node, data in inf_tree.nodes(data=True):
            if "time_exposed" in data:
                nodes[node] = data
        if color == "placement":
            colors_orign = sns.color_palette("bright")
            colors = []
            plc_colors = {}
            i_plc = 0
            for node, plc in inf_tree.nodes(data="inf_placement"):
                if node in nodes.keys():
                    if plc in plc_colors.keys():
                        colors.append(plc_colors[plc])
                    else:
                        if plc == None:
                            colors.append(colors_orign[-1])
                        else:
                            plc_colors[plc] = colors_orign[i_plc]
                            i_plc += 1
                            colors.append(plc_colors[plc])
            print(plc_colors)
        else:
            colors = "blue"
        pos = graphviz_layout(inf_tree, prog="twopi", args="-Goverlap=scalexy")
        nx.draw(
            inf_tree,
            pos,
            nodelist=nodes.keys(),
            node_size=100,
            font_color="white",
            font_size=4,
            node_color=colors,
            with_labels=True,
        )
        plt.tight_layout(pad=0.5)
        if close:
            if filename == None:
                plt.show()
            else:
                plt.savefig(filename)

    def plot_total_infections_hist(
        self, filename=None, title=None, seeds=None, size=(5, 4), close=True
    ):
        """Plots a histogram of the total number of infections at the end of the simulation for all seeds.

        Args:
            filename (str, optional): File name to save the graph, if not informed will only show the plot.
            size (tuple, optional): Size for the plot. Defaults to (20, 20).
            seeds (list, optional): List of seeds to plot, if None will plot all seeds available.
            title (str, optional): Title for the plot. Defaults to None.
            close (bool, optional): If False doesn't show, save or close the plot so that the user can add
                more information to plot using the plt attribute from the class. Defaults to False.
        """
        # Organize data
        data = self.get_data(["states"], seeds)
        if len(data["seeds"]) == 1:
            print(
                "[ERROR] Can only plot total infections histogram with more than one seed."
            )
            return None
        last_states = np.array(data["states"])[:, -1, :]  # [seed, step, particle]
        count_infected = np.sum((last_states != 0), axis=1)

        self.start_plt(size=size, title=title)
        self.plt.hist(count_infected)
        if close:
            self.stop_plt(
                filename=filename, xlabel="Number of infections", ylabel="Count"
            )

    def plot_services_visitors(
        self, time_window="day", seeds=None, per_instance=False, **plot_args
    ):
        """Plots the mean number of visitors on services in an week, whole simulation or a day window of time.

        !!! Important
            This plot uses the do_plot method, see [plot_args](#plot_args) to see all available arguments.

        Args:
            time_window (str, optional): "week", "day" or "simulation". Defaults to "day".
            per_instance (bool, optional): If True will divide the number of visitors by the number of instances. Defaults to False.
            seeds (list, optional): List of seeds to plot, if None will plot all seeds available.
        """
        loaded_data = self.get_srvc_data(["visitors_count"], seeds=seeds)
        dt = self.parameters["dt"]
        seeds = loaded_data[0]["seeds"]
        Nsteps = self.parameters["Nsteps"]

        if time_window == "week":
            t_window = int(7 * 24 / dt)
        elif time_window == "day":
            t_window = int(24 / dt)
        elif time_window == "simulation":
            t_window = Nsteps
        else:
            self.raise_runtime(
                "Invalid time window {}, select week or day.".format(time_window)
            )

        data = {}
        for sid, srvc in enumerate(loaded_data):
            visits = np.zeros((len(seeds), t_window, 2))
            visitors_count = np.array(srvc["visitors_count"])
            if per_instance:
                visitors_count /= self.parameters["services"][sid]["number"]
            for s in range(len(seeds)):
                clk = clock(start_date=self.parameters["start_date"], dt=dt)
                for _ in range(self.parameters["Nsteps"]):
                    if time_window == "week":
                        i = int(clk.dow * 24 / dt + clk.tod / dt)
                    elif time_window == "day":
                        i = int(clk.tod / dt)
                    elif time_window == "simulation":
                        i = clk.step
                    if not (
                        time_window == "day"
                        and not self.parameters["services"][sid]["days"][clk.dow]
                    ):
                        visits[s, i, 0] += visitors_count[s, clk.step]
                        visits[s, i, 1] += 1
                    clk.tick()
            srvc_name = self.parameters["services"][sid]["name"]
            data["{} visitors".format(srvc_name)] = visits[:, :, 0] / visits[:, :, 1]
        if time_window == "simulation":
            t = dt * np.arange(Nsteps) / 24
        else:
            t = np.arange(t_window)
        colors = S.COLORS_LIST[: len(data)]

        def_plot_args = {
            "xlabel": "Hour of the {}".format(time_window),
            "ylabel": "Number of visitors",
            "legend_args": {"loc": "upper left"},
            "bands_legend_args": {"color": colors[0], "loc": "upper right"},
            "dates": time_window == "simulation",
        }
        if time_window == "simulation":
            def_plot_args["xlabel"] = "Date"
        if per_instance:
            def_plot_args["ylabel"] += " per instance"
        plot_args = self.args_parse(plot_args, def_plot_args)
        self.do_plot(t, dt, data, colors, seeds, **plot_args)

    def placement_statistics(
        self,
        placement,
        txtfile=None,
        filename=None,
        plot_hist=True,
        size=(5, 4),
        include_guests=False,
        seeds=None,
    ):
        """Show placement visitation statistics.

        Args:
            placement (int or str): Placement id or placement name to show statistics.
            txtfile (str, optional): File name to save text output, if not informed will only show it.
            plot_hist (bool, optional): Plots a histogram of the number of visits. Defaults to True.
            filename (str, optional): File name to save the histogram, if not informed will only show the plot.
            size (tuple, optional): Size to generate the plot. Defaults to (5, 4).
            include_guests (bool, optional): Includes guests as visitors. Defaults to False.
            seeds (list, optional): List of seeds to plot, if None will plot all seeds available.
        """
        # Load data and parameters
        data = self.get_data(["placement"], seeds)
        data_srvc = self.get_srvc_data(
            ["name", "placement", "workers", "chosen_instance", "guests"], seeds
        )
        placements = np.array(data["placement"])
        seeds = data["seeds"]
        Nparticles = self.parameters["Nparticles"]
        Nsteps = self.parameters["Nsteps"]
        Nseeds = len(seeds)
        Ndays = self.parameters["Ndays"]
        pid = np.arange(Nparticles)

        # Prepare data
        is_srvc = False
        if not placement in [S.PLC_HOME, S.PLC_ENV, "Homes", "Environment"]:
            # Find service
            srvc_found = False
            for srvc in data_srvc:
                if srvc["placement"][0] == placement:
                    srvc_found = True
                    break
                if srvc["name"][0] == placement:
                    srvc_found = True
                    placement = srvc["placement"][0]
                    break
            if not srvc_found:
                self.raise_runtime(
                    "{} is not a valid placement or placement name.".format(placement)
                )
            # Select visitors
            mask_visitors = np.array(srvc["chosen_instance"]) != -1
            for i in range(Nseeds):
                mask_workers = np.isin(pid, srvc["workers"][i])
                mask_visitors[i, mask_workers] = False
            is_srvc = True
            title = "{} visitations".format(srvc["name"][0])
        elif placement in [S.PLC_HOME, S.PLC_ENV, "Homes", "Environment"]:
            if placement == "Homes":
                placement = S.PLC_HOME
            if placement == "Environment":
                placement = S.PLC_ENV
            # If not service all particles are visitors
            mask_visitors = np.ones((Nseeds, Nparticles), dtype=bool)
            title = "Placement = {}".format(placement)
        else:
            self.raise_runtime(
                "{} is not a valid placement or placement name.".format(placement)
            )

        # Prepare to count visits and steps in placement
        count_visit = np.zeros((Nseeds, Nparticles))
        count_steps = np.zeros((Nseeds, Nparticles))
        for i in range(Nseeds):
            last_placement = placements[i, 0, :]
            for step in range(1, Nsteps):
                mask_change_plc = last_placement != placements[i, step, :]
                last_placement = placements[i, step, :]
                mask_plc = placements[i, step, :] == placement
                mask_visit = mask_change_plc & mask_plc
                if is_srvc and not include_guests:
                    mask_guest = np.isin(pid, srvc["guests"][i][step])
                    mask_visit = mask_visit & ~mask_guest
                    mask_plc = mask_plc & ~mask_guest
                count_visit[i, mask_visit] += 1
                count_steps[i, mask_plc] += 1

        # Calculate values
        mean_n = np.mean(count_visit[mask_visitors])
        duration = count_steps / count_visit
        mask_duration = duration > 0
        mean_duration = np.mean(duration[mask_duration & mask_visitors])

        # Plot histogram
        if plot_hist:
            weights = (
                np.ones(count_visit[mask_visitors].shape)
                / count_visit[mask_visitors].shape
            )
            self.start_plt(size=size, title=title)
            self.plt.hist(
                count_visit[mask_visitors],
                weights=weights,
                bins=int(np.max(count_visit[mask_visitors])),
            )
            ax = self.plt.gca()
            ax.yaxis.set_major_formatter(PercentFormatter(1))
            self.stop_plt(
                filename=filename, xlabel="Number of visits", ylabel="% population"
            )
        out_strs = []

        # Save/print text output
        if Nseeds > 1:
            out_strs.append(
                "Mean number of available visitors: {}".format(
                    np.sum(mask_visitors) / Nseeds
                )
            )
        else:
            out_strs.append(
                "Number of available visitors: {}".format(np.sum(mask_visitors))
            )
        out_strs.append("Mean number of visitations: {}".format(mean_n))
        out_strs.append("Mean visitation period: {} Days".format(Ndays / mean_n))
        out_strs.append("Mean visitation duration: {} Steps".format(mean_duration))
        if txtfile != None:
            f = open(txtfile, "w")
        for str in out_strs:
            try:
                f.write(str + "\n")
            except:
                print(str)
        if txtfile != None:
            f.close()

    def plot_close(self):
        plt.pause(0.1)
        plt.close("all")

    def hdf5_close(self):
        self.hdf5_file.close()

    def close(self):
        try:
            self.hdf5_close()
        except:
            pass
        self.plot_close()

    def get_statistics(self, seeds=None, filename=None, schools=[]):
        data = self.get_data(
            ["inf_source", "inf_placement", "inf_tree", "ages", "states"], seeds
        )
        inf_source_s = data["inf_source"]
        inf_placement_s = data["inf_placement"]
        inf_tree_s = data["inf_tree"]
        ages_s = data["ages"]
        states_s = data["states"]
        nServices = len(self.parameters["services"])
        service_labels = [srvc["name"] for srvc in self.parameters["services"]]
        placement_labels = [srvc["id"] for srvc in self.parameters["services"]]
        workers_s = self.get_srvc_data(["workers"], seeds)
        nSeeds = len(inf_tree_s)

        if "school_monitoring" in self.hdf5_file["parameters"]:
            schools = self.hdf5_file["parameters"]["school_monitoring"][
                "schools"
            ].to_dict()
            for i in range(len(schools)):
                if type(schools[i]) is bytes:
                    schools[i] = schools[i].decode("ascii")
        else:
            schools = []

        age_groups = np.zeros(shape=(nSeeds, len(S.AGE_DEF)))
        num_infs = np.zeros(nSeeds)
        num_infs_50p = np.zeros(nSeeds)
        num_infs_60p = np.zeros(nSeeds)
        num_infs_70p = np.zeros(nSeeds)
        num_hosp = np.zeros(nSeeds)
        num_hosp_50p = np.zeros(nSeeds)
        num_hosp_60p = np.zeros(nSeeds)
        num_hosp_70p = np.zeros(nSeeds)
        num_death = np.zeros(nSeeds)
        num_death_50p = np.zeros(nSeeds)
        num_death_60p = np.zeros(nSeeds)
        num_death_70p = np.zeros(nSeeds)
        num_infs_started_out = np.zeros(nSeeds)
        num_infs_at_service = np.zeros(shape=(nSeeds, nServices))
        num_infs_root_inf_at_service = np.zeros(shape=(nSeeds, nServices))
        num_infs_from_w_at_home = np.zeros(shape=(nSeeds, nServices))
        num_infs_from_w_at_home_std = np.zeros(shape=(nSeeds, nServices))
        out_degree_workers = np.zeros(shape=(nSeeds, nServices))
        out_degree_workers_std = np.zeros(shape=(nSeeds, nServices))
        infs_from_w = np.zeros(shape=(nSeeds, nServices))
        infs_from_w_to_w = np.zeros(shape=(nSeeds, nServices))
        infs_from_w_to_c = np.zeros(shape=(nSeeds, nServices))
        infs_from_c = np.zeros(shape=(nSeeds, nServices))
        infs_from_c_to_w = np.zeros(shape=(nSeeds, nServices))
        infs_from_c_to_c = np.zeros(shape=(nSeeds, nServices))

        # used only for schools monitoring
        if schools != []:
            cases_in_schools = np.zeros(nSeeds)
            number_in_schools = np.zeros(nSeeds)
            schools_closed = np.zeros(nSeeds)
            classrooms_closed = np.zeros(nSeeds)
            i_count = np.zeros(nSeeds)
            i_total = np.zeros(nSeeds)
            c_count = np.zeros(nSeeds)
            c_total = np.zeros(nSeeds)

        workers_frac = []
        for i in range(nSeeds):
            inf_source = inf_source_s[i]
            inf_placement = inf_placement_s[i]
            inf_tree = inf_tree_s[i]
            ages = ages_s[i]
            states = states_s[i]

            # people in age groups
            [_, age_groups_counts] = np.unique(ages, return_counts=True)
            age_groups[i][: len(age_groups_counts)] = age_groups_counts

            # number of hospitalizations: assuming hospitalization is the first quarantine
            for id, node in enumerate(inf_tree.nodes):
                for key in inf_tree.nodes[node].keys():
                    if "quarantine_id" in key:
                        one_more_hospitalization = int(inf_tree.nodes[node][key] == 0)
                        num_hosp[i] += one_more_hospitalization
                        if ages[id] >= 10:
                            num_hosp_50p[i] += one_more_hospitalization
                        if ages[id] >= 12:
                            num_hosp_60p[i] += one_more_hospitalization
                        if ages[id] >= 14:
                            num_hosp_70p[i] += one_more_hospitalization
                        break

            # number of deaths
            num_death[i] = np.count_nonzero(states == S.STATE_D)
            num_death_50p[i] = np.count_nonzero((states == S.STATE_D) & (ages >= 10))
            num_death_60p[i] = np.count_nonzero((states == S.STATE_D) & (ages >= 12))
            num_death_70p[i] = np.count_nonzero((states == S.STATE_D) & (ages >= 14))

            # number of infections derived from roots not infected at home
            mask_not_inf_at_home = (inf_placement == -2) | (inf_placement >= 0)
            not_inf_at_home_subtree_nodes = []
            for id, is_inf in enumerate(mask_not_inf_at_home):
                if is_inf:
                    not_inf_at_home_subtree_nodes = (
                        not_inf_at_home_subtree_nodes
                        + list(nx.nodes(nx.dfs_tree(inf_tree, str(id))))
                    )
            not_inf_at_home_subtree_nodes = np.unique(not_inf_at_home_subtree_nodes)
            num_infs_started_out[i] = len(not_inf_at_home_subtree_nodes)

            # total number of new infections
            num_infs[i] = np.count_nonzero(inf_source >= 0)
            num_infs_50p[i] = np.count_nonzero((inf_source >= 0) & (ages >= 10))
            num_infs_60p[i] = np.count_nonzero((inf_source >= 0) & (ages >= 12))
            num_infs_70p[i] = np.count_nonzero((inf_source >= 0) & (ages >= 14))

            for j in range(nServices):
                placement_label = placement_labels[j]
                worker_ids = workers_s[j]["workers"][i]

                # out edge of service workers
                oedl = [[] for i in worker_ids]
                for ind, wid in enumerate(worker_ids):
                    oedl[ind] = list(int(x[1]) for x in inf_tree.edges(str(wid)))

                # out degrees of service workers
                odgl = list(
                    x[1] for x in inf_tree.out_degree([str(x) for x in worker_ids])
                )

                # total number of infections ocurred at services
                mask_infected_at_srvc = inf_placement == placement_label
                total_infs_plc = np.count_nonzero(mask_infected_at_srvc)
                num_infs_at_service[i, j] = total_infs_plc

                # number of infections ocurred at services derived from workers
                neigh_w = []
                for x in worker_ids:
                    neigh_w = neigh_w + [int(i) for i in inf_tree.neighbors(str(x))]
                neigh_w = np.array(neigh_w)
                if len(neigh_w) > 0:
                    infs_from_w[i, j] = np.sum(mask_infected_at_srvc[neigh_w])
                    infs_from_w_to_w[i, j] = np.sum(
                        mask_infected_at_srvc[neigh_w[np.isin(neigh_w, worker_ids)]]
                    )
                infs_from_w_to_c[i, j] = infs_from_w[i, j] - infs_from_w_to_w[i, j]

                # number of infections ocurred at services derived from clients
                par_inf_srvc = []
                for x in np.arange(len(mask_infected_at_srvc))[mask_infected_at_srvc]:
                    par_inf_srvc = par_inf_srvc + [
                        int(i) for i in inf_tree.predecessors(str(x))
                    ]
                par_inf_srvc = np.unique(np.array(par_inf_srvc))
                neigh_c = []
                for x in par_inf_srvc[~np.isin(par_inf_srvc, worker_ids)]:
                    neigh_c = neigh_c + [int(i) for i in inf_tree.neighbors(str(x))]
                neigh_c = np.array(neigh_c)
                infs_from_c[i, j] = total_infs_plc - infs_from_w[i, j]
                if len(neigh_c) > 0:
                    infs_from_c_to_w[i, j] = np.sum(
                        mask_infected_at_srvc[neigh_c[np.isin(neigh_c, worker_ids)]]
                    )
                infs_from_c_to_c[i, j] = infs_from_c[i, j] - infs_from_c_to_w[i, j]

                # total number of infections derived from roots infected at services
                inf_srvc_subtree_nodes = []
                for id, is_inf in enumerate(mask_infected_at_srvc):
                    if is_inf:
                        inf_srvc_subtree_nodes = inf_srvc_subtree_nodes + list(
                            nx.nodes(nx.dfs_tree(inf_tree, str(id)))
                        )
                inf_srvc_subtree_nodes = np.unique(inf_srvc_subtree_nodes)
                inf_srvc_subtree_nodes = np.array(
                    list(map(int, inf_srvc_subtree_nodes))
                )
                n_subtree_nodes = 0
                if len(inf_srvc_subtree_nodes) > 0:
                    n_subtree_nodes = len(
                        inf_srvc_subtree_nodes
                    )  # includes infections at the services
                    # n_subtree_nodes = np.count_nonzero(inf_placement[inf_srvc_subtree_nodes] != placement_label) #excludes infections at the services

                if odgl == []:
                    num_infs_from_w_at_home[i, j] = 0
                    num_infs_from_w_at_home_std[i, j] = 0
                    out_degree_workers[i, j] = 0
                    out_degree_workers_std[i, j] = 0
                else:
                    for ind in range(len(oedl)):
                        if oedl[ind] == []:
                            oedl[ind] = 0
                        else:
                            oedl[ind] = np.count_nonzero(
                                inf_placement[np.array(oedl[ind])] == S.PLC_HOME
                            )
                    num_infs_from_w_at_home[i, j] = np.mean(oedl)
                    num_infs_from_w_at_home_std[i, j] = np.std(oedl)
                    odgl = np.array(
                        odgl + [0 for _ in range(len(worker_ids) - len(odgl))]
                    )
                    out_degree_workers[i, j] = np.mean(odgl)
                    out_degree_workers_std[i, j] = np.std(odgl)

                if i == 0:
                    num_workers = len(worker_ids)
                    workers_frac.append(num_workers / len(inf_source))

                # infections derived from roots infected at services
                num_infs_root_inf_at_service[i, j] = n_subtree_nodes

                if service_labels[j] in schools:
                    cases_in_schools[i] += np.count_nonzero(
                        np.isin(
                            states[worker_ids],
                            [S.STATE_E, S.STATE_I, S.STATE_R, S.STATE_D],
                        )
                    )
                    number_in_schools[i] += len(worker_ids)

        # Counts percentage of time schools where closed
        for i, school in enumerate(schools):
            I_COUNT = ("school_monitoring", school, "instances_count")
            I_TOTAL = ("school_monitoring", school, "instances_total")
            C_COUNT = ("school_monitoring", school, "classrooms_count")
            C_TOTAL = ("school_monitoring", school, "classrooms_total")
            S_COUNT = ("school_monitoring", school, "students_count")
            S_TOTAL = ("school_monitoring", school, "rooms_total")
            data = self.get_data(
                [I_COUNT, I_TOTAL, S_COUNT, S_TOTAL, C_COUNT, C_TOTAL], None
            )
            for s in range(nSeeds):
                i_count[s] += np.sum(data[I_COUNT])
                i_total[s] += np.sum(data[I_TOTAL])
                c_count[s] += np.sum(data[C_COUNT])
                c_total[s] += np.sum(data[C_TOTAL])
        if schools != []:
            for s in range(nSeeds):
                schools_closed[s] = i_count[s] / i_total[s]
                classrooms_closed[s] = c_count[s] / c_total[s]

        if self.post_processed_data == None:
            self.post_processed_data = {}
        self.post_processed_data["seeds"] = ("seeds", np.array(self.get_seeds(seeds)))
        self.post_processed_data["nSeeds"] = ("number of seeds", nSeeds)
        self.post_processed_data["nServices"] = ("number of services", nServices)
        self.post_processed_data["service_labels"] = ("service labels", service_labels)
        self.post_processed_data["num_infs"] = ("number of new infections", num_infs)
        self.post_processed_data["num_infs_50p"] = (
            "number of new infections among 50 years+ old people",
            num_infs_50p,
        )
        self.post_processed_data["num_infs_60p"] = (
            "number of new infections among 60 years+ old people",
            num_infs_60p,
        )
        self.post_processed_data["num_infs_70p"] = (
            "number of new infections among 70 years+ old people",
            num_infs_70p,
        )
        self.post_processed_data["num_hosp"] = ("number of hospitalizations", num_hosp)
        self.post_processed_data["num_hosp_50p"] = (
            "number of hospitalizations among 50 years+ old people",
            num_hosp_50p,
        )
        self.post_processed_data["num_hosp_60p"] = (
            "number of hospitalizations among 60 years+ old people",
            num_hosp_60p,
        )
        self.post_processed_data["num_hosp_70p"] = (
            "number of hospitalizations among 70 years+ old people",
            num_hosp_70p,
        )
        self.post_processed_data["num_death"] = ("number of deaths", num_death)
        self.post_processed_data["num_death_50p"] = (
            "number of deaths among 50 years+ old people",
            num_death_50p,
        )
        self.post_processed_data["num_death_60p"] = (
            "number of deaths among 60 years+ old people",
            num_death_60p,
        )
        self.post_processed_data["num_death_70p"] = (
            "number of deaths among 70 years+ old people",
            num_death_70p,
        )
        self.post_processed_data["age_groups"] = (
            "number of people in each age group",
            age_groups,
        )
        self.post_processed_data["num_infs_started_out"] = (
            "number of new infections derived from out home infections (including them)",
            num_infs_started_out,
        )
        self.post_processed_data["num_infs_at_service"] = (
            "number of infections occurred at the service",
            num_infs_at_service,
        )
        self.post_processed_data["num_infs_root_inf_at_service"] = (
            "number of new infections derived from infections occurred at the service (including them)",
            num_infs_root_inf_at_service,
        )
        self.post_processed_data["num_infs_from_w_at_home"] = (
            "mean number of infections at workers homes which have been caused by them",
            num_infs_from_w_at_home,
        )
        self.post_processed_data["num_infs_from_w_at_home_std"] = (
            "std deviation of the number of infections at workers homes which have been caused by them",
            num_infs_from_w_at_home_std,
        )
        self.post_processed_data["out_degree_workers"] = (
            "mean number of infections caused by a worker",
            out_degree_workers,
        )
        self.post_processed_data["out_degree_workers_std"] = (
            "std deviation of the number of infections caused by a worker",
            out_degree_workers_std,
        )
        self.post_processed_data["infs_from_w"] = (
            "infections at service derived from workers",
            infs_from_w,
        )
        self.post_processed_data["infs_from_w_to_w"] = (
            "worker infections at service, derived from workers",
            infs_from_w_to_w,
        )
        self.post_processed_data["infs_from_w_to_c"] = (
            "client infections at service, derived from workers",
            infs_from_w_to_c,
        )
        self.post_processed_data["infs_from_c"] = (
            "infections at service derived from clients",
            infs_from_c,
        )
        self.post_processed_data["infs_from_c_to_w"] = (
            "worker infections at service, derived from clients",
            infs_from_c_to_w,
        )
        self.post_processed_data["infs_from_c_to_c"] = (
            "client infections at service, derived from clients",
            infs_from_c_to_c,
        )
        self.post_processed_data["workers_frac"] = (
            "fraction of the population that is worker at the service",
            workers_frac,
        )

        if schools != []:
            self.post_processed_data["cases_in_schools"] = (
                "Total number of cases in school population.",
                cases_in_schools,
            )
            self.post_processed_data["number_in_schools"] = (
                "Total number of school population.",
                number_in_schools,
            )
            self.post_processed_data["schools_closed"] = (
                "Percentage of time schools where closed.",
                schools_closed,
            )
            self.post_processed_data["classrooms_closed"] = (
                "Percentage of time classrooms where closed.",
                classrooms_closed,
            )

        # Storing post processed data to hdf5
        self.store_post_processed_data(filename=filename)

        # Storing post processed data to csv file
        temp_array = np.array(
            [
                0.0
                for key in self.post_processed_data.keys()
                if (
                    (key != "service_labels")
                    and (key != "nServices")
                    and (key != "seeds")
                    and (key != "age_groups")
                )
            ]
        )
        data_csv = {
            "keyword": [
                key
                for key in self.post_processed_data.keys()
                if (
                    (key != "service_labels")
                    and (key != "nServices")
                    and (key != "seeds")
                    and (key != "age_groups")
                )
            ],
            "description": [
                self.post_processed_data[key][0]
                for key in self.post_processed_data.keys()
                if (
                    (key != "service_labels")
                    and (key != "nServices")
                    and (key != "seeds")
                    and (key != "age_groups")
                )
            ],
            "mean": np.copy(temp_array),
            "std": np.copy(temp_array),
        }
        for label in self.post_processed_data["service_labels"][1]:
            data_csv[label + " mean"] = np.copy(temp_array)
            data_csv[label + " std"] = np.copy(temp_array)
        data_csv["mean"][0] = nSeeds
        data_csv["std"][0] = 0.0
        data_csv["mean"][1] = np.mean(num_infs)
        data_csv["std"][1] = np.std(num_infs)
        data_csv["mean"][2] = np.mean(num_infs_50p)
        data_csv["std"][2] = np.std(num_infs_50p)
        data_csv["mean"][3] = np.mean(num_infs_60p)
        data_csv["std"][3] = np.std(num_infs_60p)
        data_csv["mean"][4] = np.mean(num_infs_70p)
        data_csv["std"][4] = np.std(num_infs_70p)
        data_csv["mean"][5] = np.mean(num_hosp)
        data_csv["std"][5] = np.std(num_hosp)
        data_csv["mean"][6] = np.mean(num_hosp_50p)
        data_csv["std"][6] = np.std(num_hosp_50p)
        data_csv["mean"][7] = np.mean(num_hosp_60p)
        data_csv["std"][7] = np.std(num_hosp_60p)
        data_csv["mean"][8] = np.mean(num_hosp_70p)
        data_csv["std"][8] = np.std(num_hosp_70p)
        data_csv["mean"][9] = np.mean(num_death)
        data_csv["std"][9] = np.std(num_death)
        data_csv["mean"][10] = np.mean(num_death_50p)
        data_csv["std"][10] = np.std(num_death_50p)
        data_csv["mean"][11] = np.mean(num_death_60p)
        data_csv["std"][11] = np.std(num_death_60p)
        data_csv["mean"][12] = np.mean(num_death_70p)
        data_csv["std"][12] = np.std(num_death_70p)
        data_csv["mean"][13] = np.mean(num_infs_started_out)
        data_csv["std"][13] = np.std(num_infs_started_out)
        service_keys = [
            "num_infs_at_service",
            "num_infs_root_inf_at_service",
            "num_infs_from_w_at_home",
            "num_infs_from_w_at_home_std",
            "out_degree_workers",
            "out_degree_workers_std",
            "infs_from_w",
            "infs_from_w_to_w",
            "infs_from_w_to_c",
            "infs_from_c",
            "infs_from_c_to_w",
            "infs_from_c_to_c",
        ]
        for indk, service_key in enumerate(service_keys):
            data_mean = np.mean(self.post_processed_data[service_key][1], axis=0)
            data_std = np.std(self.post_processed_data[service_key][1], axis=0)
            for ind, (m, s) in enumerate(zip(data_mean, data_std)):
                data_csv[service_labels[ind] + " mean"][indk + 14] = m
                data_csv[service_labels[ind] + " std"][indk + 14] = s
        for ind, frac in enumerate(workers_frac):
            data_csv[service_labels[ind] + " mean"][-1] = frac
        df = pd.DataFrame(data_csv, columns=[x for x in data_csv.keys()])
        mean_age_groups = np.mean(age_groups, axis=0)
        std_age_groups = np.std(age_groups, axis=0)
        for ind, age in enumerate(S.AGE_DEF):
            df.loc[len(df.index)] = [
                age,
                "particles in the left mentioned age group",
                mean_age_groups[ind],
                std_age_groups[ind],
            ] + [0.0 for _ in range(4, len(df.columns))]
        fname = filename
        if fname == None:
            fname = self.filename
            fname = fname.replace(".hdf5", "")
            fname = fname + "_service_statistics"
        df.to_csv(fname + ".csv")

    def store_post_processed_data(self, filename=None):
        # Storing post processed data to hdf5 file
        fname = filename
        if fname == None:
            fname = self.filename
            fname = fname.replace(".hdf5", "")
        hdf5 = h5dict.File(fname + "_post_processed.hdf5", "w")
        for key in self.post_processed_data.keys():
            hdf5[key] = self.post_processed_data[key]
        hdf5.close()

    def load_post_processed_data(self, filename=None):
        # Storing post processed data to hdf5 file
        fname = filename
        if fname == None:
            fname = self.filename
            fname = fname.replace(".hdf5", "")
        hdf5 = h5dict.File(fname + "_post_processed.hdf5", "r")
        self.post_processed_data = hdf5.to_dict()
        hdf5.close()

    def compare(self, an, what_compare=None, seeds=None, filename=None, schools=[]):
        # Making sure uncertainties is installed
        try:
            from uncertainties import ufloat
            from uncertainties import unumpy as unp
        except Exception as e:
            print(
                e,
                "Please install package 'uncertainties' in order to compare simulations amongst each other",
            )
        # Getting data to be compared to
        if an.post_processed_data == None:
            try:
                an.load_post_processed_data()
            except:
                an.get_statistics(seeds, schools=schools)
        data_base = an.post_processed_data
        if what_compare == None:
            what_compare = [
                x
                for x in data_base.keys()
                if (
                    (x != "nSeeds")
                    and (x != "nServices")
                    and (x != "service_labels")
                    and (x != "workers_frac")
                    and (x != "seeds")
                )
            ]
        udata_base = {}
        for item in what_compare:
            udata_base[item] = unp.uarray(
                np.mean(data_base[item][1], axis=0), np.std(data_base[item][1], axis=0)
            )
        # Getting data to be compared from
        if self.post_processed_data == None:
            try:
                self.load_post_processed_data()
            except:
                self.get_statistics(seeds, schools=schools)
        data_compare = self.post_processed_data
        # Storing masks for comparing same seeds
        mask_seeds_base = np.isin(data_base["seeds"][1], data_compare["seeds"][1])
        ind_seeds_base = np.arange(len(mask_seeds_base), dtype=int)[mask_seeds_base]
        mask_seeds_compare = np.isin(data_compare["seeds"][1], data_base["seeds"][1])
        ind_seeds_compare = np.arange(len(mask_seeds_compare), dtype=int)[
            mask_seeds_compare
        ]
        seeds_compatible = np.count_nonzero(mask_seeds_base)
        # Setting up hdf5 data file
        data_hdf5 = {}
        # Setting up csv data file
        data_csv = {
            "keyword": [key for key in what_compare],
            "description": [data_compare[key][0] for key in what_compare],
            "mean": [0.0 for _ in what_compare],
            "std": [0.0 for _ in what_compare],
        }
        for label in data_base["service_labels"][1]:
            data_csv[label + " mean"] = [0.0 for _ in what_compare]
            data_csv[label + " std"] = [0.0 for _ in what_compare]
        # Make comparisons, store data for csv and hdf5 files
        udata_compared = {}
        for ind, item in enumerate(what_compare):
            if len(data_base[item][1].shape) != len(data_compare[item][1].shape):
                udata_compared[item] = unp.uarray(
                    np.zeros(udata_base[item].shape), np.zeros(udata_base[item].shape)
                )
                continue
            if len(data_base[item][1].shape) == 2:
                mask_base = np.isin(
                    data_base["service_labels"][1], data_compare["service_labels"][1]
                )
                ind_base = np.arange(len(mask_base), dtype=int)[mask_base]
                mask_compare = np.isin(
                    data_compare["service_labels"][1], data_base["service_labels"][1]
                )
                ind_compare = np.arange(len(mask_compare), dtype=int)[mask_compare]
                services_compatible = np.count_nonzero(mask_base)
                if seeds_compatible > 1:
                    ux = np.zeros(shape=(seeds_compatible, services_compatible))
                    for indi, indi_base, indi_compare in zip(
                        np.arange(seeds_compatible, dtype=int),
                        ind_seeds_base,
                        ind_seeds_compare,
                    ):
                        for indj, indj_base, indj_compare in zip(
                            np.arange(services_compatible, dtype=int),
                            ind_base,
                            ind_compare,
                        ):
                            if data_base[item][1][indi_base, indj_base] != 0:
                                ux[indi, indj] = (
                                    data_compare[item][1][indi_compare, indj_compare]
                                    / data_base[item][1][indi_base, indj_base]
                                )
                    data_hdf5[item] = (
                        data_base[item][0],
                        (ux - np.ones(ux.shape)) * 100,
                    )
                    ux = unp.uarray(np.mean(ux, axis=0), np.std(ux, axis=0))
                else:
                    ux = unp.uarray(
                        np.mean(data_compare[item][1], axis=0),
                        np.std(data_compare[item][1], axis=0),
                    )[mask_compare]
                    for i, (uf, vf) in enumerate(zip(ux, udata_base[item][mask_base])):
                        if vf == 0:
                            ux[i] = ufloat(0, 0)
                        else:
                            ux[i] = uf / vf
                udata_compared[item] = (ux - np.ones(ux.shape)) * 100.0
                service_labels = np.array(data_compare["service_labels"][1])[
                    mask_compare
                ]
                for uf, label in zip(udata_compared[item], service_labels):
                    data_csv[label + " mean"][ind] = uf.n
                    data_csv[label + " std"][ind] = uf.s
            else:
                if seeds_compatible > 1:
                    ux = np.zeros(shape=(seeds_compatible))
                    for indi, indi_base, indi_compare in zip(
                        np.arange(seeds_compatible, dtype=int),
                        ind_seeds_base,
                        ind_seeds_compare,
                    ):
                        if data_base[item][1][indi_base] != 0:
                            ux[indi] = (
                                data_compare[item][1][indi_compare]
                                / data_base[item][1][indi_base]
                            )
                    data_hdf5[item] = (
                        data_base[item][0],
                        (ux - np.ones(ux.shape)) * 100,
                    )
                    udata_compared[item] = (
                        ufloat(np.mean(ux), np.std(ux)) - 1.0
                    ) * 100.0
                else:
                    udata_compared[item] = (
                        ufloat(
                            np.mean(data_compare[item][1]),
                            np.std(data_compare[item][1]),
                        )
                        / udata_base[item]
                        - 1.0
                    ) * 100.0
                data_csv["mean"][ind] = udata_compared[item].n
                data_csv["std"][ind] = udata_compared[item].s
        # Store comparison data to hdf5 file
        fname = filename
        if fname == None:
            fname = self.filename
            fname = fname.replace(".hdf5", "")
            fname = "{}_comparison".format(fname)
        hdf5 = h5dict.File(fname + ".hdf5", "w")
        for key in data_hdf5:
            hdf5[key] = data_hdf5[key]
        hdf5["baseline"] = ("baseline", an.filename)
        hdf5.close()
        # Store comparison data to csv file
        for key in data_csv.keys():
            data_csv[key] = data_csv[key] + [""]
        data_csv["keyword"][-1] = "baseline"
        data_csv["description"][-1] = an.filename
        fname = filename
        if fname == None:
            fname = self.filename
            fname = fname.replace(".hdf5", "")
            fname = "{}_comparison".format(fname)
        df = pd.DataFrame(data_csv, columns=[x for x in data_csv.keys()])
        df.to_csv(fname + ".csv")
        return udata_compared, data_hdf5

    def get_home_inf_statistics(
        self, seeds=None, homes_inf0_pct_filename=None, homes_inf0_dict_filename=None
    ):
        self.homes_inf0_dict = {}
        self.homes_inf0_pct = {}
        homes_start_inf_frac = []
        homes_end_inf_frac = []
        data = self.get_data(
            [
                "initial_states",
                "states",
                "homesNumber",
                "home_id",
                "inf_source",
                "inf_tree",
            ],
            seeds,
        )
        initial_states_s = data["initial_states"]
        final_states_s = data["states"]
        homesNumber_s = data["homesNumber"]
        home_id_s = data["home_id"]
        inf_source_s = data["inf_source"]
        inf_tree_s = data["inf_tree"]
        nSeeds = len(inf_tree_s)
        for s in range(nSeeds):
            initial_states = initial_states_s[s]
            final_states = final_states_s[s]
            homesNumber = homesNumber_s[s]
            home_id = home_id_s[s]
            inf_source = inf_source_s[s]
            inf_tree = inf_tree_s[s]

            availHomes_mask = np.full(homesNumber, True)
            availHomes_mask[
                home_id[
                    (initial_states == S.STATE_I)
                    | (initial_states == S.STATE_R)
                    | (initial_states == S.STATE_D)
                ]
            ] = False
            onlySus_mask = np.full(homesNumber, True)
            for ind, source in enumerate(inf_source):
                onlySus_mask[home_id[ind]] = onlySus_mask[home_id[ind]] and (
                    source == -1
                )
            availHomes_mask = availHomes_mask & (~onlySus_mask)
            for ind, avail in enumerate(availHomes_mask):
                if avail:
                    part_in_home_mask = home_id == ind
                    not_active = (
                        np.count_nonzero(
                            (final_states[part_in_home_mask] == S.STATE_E)
                            | (final_states[part_in_home_mask] == S.STATE_I)
                        )
                        == 0
                    )
                    availHomes_mask[ind] = availHomes_mask[ind] and not_active

            nAvailHomes = np.count_nonzero(availHomes_mask)
            homeHistory = [[] for _ in range(nAvailHomes)]
            i = 0
            for ind, avail in enumerate(availHomes_mask):
                if avail:
                    part_in_home_mask = home_id == ind
                    nParts = np.count_nonzero(part_in_home_mask)
                    if nParts not in self.homes_inf0_pct:
                        self.homes_inf0_pct[nParts] = np.zeros(nParts)
                    nPartsRec = np.count_nonzero(
                        (final_states[part_in_home_mask] == S.STATE_R)
                        | (final_states[part_in_home_mask] == S.STATE_D)
                    )
                    self.homes_inf0_pct[nParts][nPartsRec - 1] += 1
                    history = [[] for _ in range(nParts)]
                    j = 0
                    for id, part in enumerate(part_in_home_mask):
                        if part:
                            ids = str(id)
                            if final_states[id] == S.STATE_S:
                                history[j] = [0]
                            else:
                                history[j] = []
                                if "time_exposed" in inf_tree.nodes[ids].keys():
                                    history[j] = history[j] + [
                                        inf_tree.nodes[ids]["time_exposed"]
                                    ]
                                else:
                                    history[j] = history[j] + [0]
                                if "time_infectious" in inf_tree.nodes[ids].keys():
                                    history[j] = history[j] + [
                                        inf_tree.nodes[ids]["time_infectious"]
                                    ]
                                else:
                                    history[j] = history[j] + [0]
                                if "time_recovered" in inf_tree.nodes[ids].keys():
                                    history[j] = history[j] + [
                                        inf_tree.nodes[ids]["time_recovered"]
                                    ]
                                elif "time_deceased" in inf_tree.nodes[ids].keys():
                                    history[j] = history[j] + [
                                        inf_tree.nodes[ids]["time_deceased"]
                                    ]
                                else:
                                    history[j] = history[j] + [0]
                            j = j + 1
                    homeHistory[i] = history
                    i = i + 1

            for history in homeHistory:
                npart = len(history)
                if npart not in self.homes_inf0_dict:
                    self.homes_inf0_dict[npart] = {}
                times = []
                for timeh in history:
                    if timeh == [0]:
                        continue
                    times = times + timeh
                times = list(dict.fromkeys(times))
                times = np.array(times)
                times = np.sort(times)
                tlen = np.max(times) - np.min(times)
                for ti, tf in zip(times[:-1], times[1:]):
                    deltat = tf - ti
                    tm = 0.5 * deltat + ti
                    key = ""
                    for timeh in history:
                        if timeh == [0]:
                            key = key + "A"
                        elif tm < timeh[0]:
                            key = key + "A"
                        elif (tm > timeh[0]) and (tm < timeh[1]):
                            key = key + "B"
                        elif (tm > timeh[1]) and (tm < timeh[2]):
                            key = key + "C"
                        elif tm > timeh[2]:
                            key = key + "D"
                    key = "".join(sorted(key))
                    if key not in self.homes_inf0_dict[npart].keys():
                        self.homes_inf0_dict[npart][key] = 0
                    self.homes_inf0_dict[npart][key] = (
                        self.homes_inf0_dict[npart][key] + deltat / tlen
                    )

            homes_start_inf_frac_tmp = []
            homes_end_inf_frac_tmp = []
            temp = []
            for ind, avail in enumerate(availHomes_mask):
                temp = temp + [np.count_nonzero(home_id == ind)]
                if avail:
                    part_in_home_mask = home_id == ind
                    parts_in_home = np.count_nonzero(part_in_home_mask)
                    sus_parts_in_home = np.count_nonzero(
                        inf_source[part_in_home_mask] == -1
                    )
                    homes_start_inf_frac_tmp = homes_start_inf_frac_tmp + [
                        1.0 / parts_in_home
                    ]
                    homes_end_inf_frac_tmp = homes_end_inf_frac_tmp + [
                        1.0 - sus_parts_in_home / parts_in_home
                    ]
            homes_start_inf_frac = homes_start_inf_frac + homes_start_inf_frac_tmp
            homes_end_inf_frac = homes_end_inf_frac + homes_end_inf_frac_tmp
            # print('avrg persons per home ', np.mean(np.array(temp)), s)
        self.homes_start_inf_frac = homes_start_inf_frac
        self.homes_end_inf_frac = homes_end_inf_frac

        experiment_name = self.filename
        experiment_name = experiment_name.replace(".hdf5", "")

        f = open("{}_home_inf.txt".format(experiment_name), "w")
        print(
            "homes_start_inf_frac = {}".format(np.mean(np.array(homes_start_inf_frac))),
            file=f,
        )
        print(
            "homes_end_inf_frac = {}".format(np.mean(np.array(homes_end_inf_frac))),
            file=f,
        )
        f.close()

        if homes_inf0_pct_filename == None:
            homes_inf0_pct_filename = "{}_homes_inf0_pct.txt".format(experiment_name)
        f = open(homes_inf0_pct_filename, "w")
        for key in self.homes_inf0_pct.keys():
            self.homes_inf0_pct[key] = tools.normalize(self.homes_inf0_pct[key])
        print("homes_inf0_pct: ", file=f)
        pprint(self.homes_inf0_pct, stream=f)
        f.close()

        if homes_inf0_dict_filename == None:
            homes_inf0_dict_filename = "{}_homes_inf0_dict.txt".format(experiment_name)
        f = open(homes_inf0_dict_filename, "w")
        self.homes_inf0_dict["homes_inf0_frac"] = np.mean(np.array(homes_end_inf_frac))
        print("homes_inf0_dict: ", file=f)
        pprint(self.homes_inf0_dict, stream=f)
        f.close()

        return self.homes_inf0_pct, self.homes_inf0_dict
