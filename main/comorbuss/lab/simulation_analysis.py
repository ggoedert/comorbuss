import os
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
from .. import settings as S
from .analysis import Analysis
from .simulation import Simulation
from tqdm import tqdm
from multiprocessing import Pool

import matplotlib.patches as mpatches


def add_label_violin(violin, labels, label, i):
    color = violin["bodies"][i].get_facecolor().flatten()
    labels.append((mpatches.Patch(color=color), label))
    return labels


class caller:
    """Stores a function to further calls."""

    def __init__(self, function, *args, **kwargs):
        self.function = function
        self.args = args
        self.kwargs = kwargs

    def __call__(self, *args, **kwargs):
        args = self.args + args
        kwargs.update(self.kwargs)
        self.function(*args, **kwargs)


class SimulationAnalysis(Analysis):
    """Interface to make plots of multiple hdf5 files generated with Simulation.

    All plot methods of the [Analysis class](../analysis/) are available in the SimulationAnalysis class.

    !!! important
        When calling an Analysis method from a SimulationAnalysis object every instance of "$id" in a str
            parameter will be replaced with the identifier of that hdf5, this can be useful to use with
            parameters like filename or title.

        **Example:**

            from comorbuss import SimulationAnalysis

            analysis = SimulationAnalysis.from_folder("/data/folder")
            analysis.plot_SEIR(filename="$id.pdf")
            analysis.close()
    """

    def __init__(self, As):
        self.As = As
        self.plot_method = None
        print("Loaded {} hdf5 files for processing.".format(len(As)))

    @classmethod
    def empty_from_files(cls, filenames):
        """Instances an empty SimulationAnalysis object from a list/array/tuples of hdf5 filenames

        Args:
            filenames: A list/numpy.array/tuple array of filenames

        Returns:
            SimulationAnalysis: SimulationAnalysis object.
        """
        As = {}
        for simid, hdf5 in enumerate(filenames):
            As[simid] = Analysis({}, {}, False)
            As[simid].filename = hdf5
        return cls(As)

    @classmethod
    def from_files(cls, filenames):
        """Instances a SimulationAnalysis object from a list/array/tuples of hdf5 filenames

        Args:
            filenames: A list/numpy.array/tuple array of filenames

        Returns:
            SimulationAnalysis: SimulationAnalysis object.
        """
        As = {}
        for simid, hdf5 in enumerate(filenames):
            As[simid] = Analysis.from_hdf5(hdf5)
        return cls(As)

    @classmethod
    def from_sim(cls, sim):
        """Instance an SimulationAnalysis object from a Simulation object.

        Args:
            sim (Simulation): Simulation object to load data.

        Returns:
            SimulationAnalysis: SimulationAnalysis object.
        """
        As = {}
        for simid, hdf5 in sim.get_hdf5_files().items():
            As[simid] = Analysis.from_hdf5(hdf5)
        return cls(As)

    @classmethod
    def from_exp_parameters(cls, *args, **kwargs):
        """Instance an SimulationAnalysis object from experiment parameters. Accepts same
            parameters as the [Simulation class](#comorbuss.lab.Simulation.__init__).

        Returns:
            SimulationAnalysis: SimulationAnalysis object.
        """
        sim = Simulation(*args, **kwargs)
        return SimulationAnalysis.from_sim(sim)

    @classmethod
    def from_folder(cls, folder, strings_to_ignore=[]):
        """Instance an SimulationAnalysis object from all hdf5 files in a folder.

        Args:
            folder (str): Path to the folder where the hdf5 files are stored.

        Returns:
            SimulationAnalysis: SimulationAnalysis object.
        """
        files = os.listdir(path=folder)
        files.sort(key=lambda f: os.path.getctime(os.path.join(folder, f)))
        As = {}
        for f in files:
            ignore = any([s in f for s in strings_to_ignore])
            if f.lower()[-5:] == ".hdf5" and not ignore:
                hdf5 = os.path.join(folder, f)
                simid = os.path.split(f)[-1][:-5]
                As[simid] = Analysis.from_hdf5(hdf5)
        return cls(As)

    def plot_all(self, plot_function_name, *args, progress=False, nproc=1, **kwargs):
        if nproc > 1:
            self._plot_all_parallel(plot_function_name, *args, nproc=nproc, **kwargs)
        else:
            self._plot_all_sequential(plot_function_name, *args, progress=progress, **kwargs)

    def _plot_all_sequential(self, plot_function_name, *args, progress=False, **kwargs):
        iterable = self.As.items()
        if progress:
            iterable = tqdm(iterable)
        for simid, A in iterable:
            self._plot_single(plot_function_name, simid, A, *args, **kwargs)

    def _plot_all_parallel(self, plot_function_name, *args, nproc=1, **kwargs):
        ps = []
        for simid, A in self.As.items():
            ps.append(
                dict(
                    hdf5=A.filename,
                    simid=simid,
                    args=args,
                    kwargs=kwargs,
                    plot_function_name=plot_function_name,
                    plot_single=self._plot_single,
                )
            )
        pool = Pool(nproc)
        pool.map(self._plot_single_parralel, ps)
        pool.close()

    @staticmethod
    def _plot_single_parralel(args):
        A = Analysis.from_hdf5(args["hdf5"])
        print(
            "Calling {} for {}.".format(args["plot_function_name"], args["simid"])
        )
        args["plot_single"](
            args["plot_function_name"],
            args["simid"],
            A,
            *args["args"],
            **args["kwargs"],
        )

    @staticmethod
    def _plot_single(plot_function_name, simid, A, *args, **kwargs):
        if "plot" in plot_function_name and not "close" in plot_function_name:
            kwargs["title"] = simid
        newkwargs = dict(kwargs)
        for key in newkwargs:
            if type(kwargs[key]) is str:
                newkwargs[key] = newkwargs[key].replace("$id", simid)
        newargs = []
        for arg in args:
            if type(arg) is str:
                arg = arg.replace("$id", simid)
            newargs.append(arg)
        plot_function = getattr(A, plot_function_name)
        plot_function(*newargs, **newkwargs)

    def __getattr__(self, name):
        return caller(self.plot_all, name)

    def make_comparison(
        self,
        simbase=None,
        simscompare=[],
        what_compare=None,
        seeds=None,
        filename=None,
        labels_sima=None,
    ):
        # Checking whether initialization has been done correctly
        if not bool(self.As):
            raise Exception("No hdf5 files to compare from/to")
        # Triage of arguments
        if simbase == None:
            simbase = [item for item in self.As.keys()][0]
        if simscompare == []:
            simscompare = self.As.keys()
        # Extraction of data
        if self.As[simbase].post_processed_data == None:
            try:
                self.As[simbase].load_post_processed_data()
            except:
                self.As[simbase].get_statistics(seeds)
        udata_compared = {}
        data_hdf5_compared = {}
        for simid in simscompare:
            if simid == simbase:
                continue
            (udata_compared[simid], data_hdf5_compared[simid]) = self.As[simid].compare(
                self.As[simbase], what_compare, seeds, filename=filename
            )
        # Export of results to txt file TODO: CSV
        for item in [x for x in udata_compared.values()][0].keys():
            fname = filename
            if fname == None:
                fname = self.As[simbase].filename
                fname = fname.replace(".hdf5", "")
                fname = "{}_comparison".format(fname)
            f = open(fname + "_" + item + ".txt", "w")
            f.write(
                "Comparison to baseline from: {}\n".format(self.As[simbase].filename)
            )
            f.write(
                "Comparing the following measure: {}\n".format(
                    self.As[simbase].post_processed_data[item][0]
                )
            )
            for simid in simscompare:
                if (simid == simbase) or (simid not in udata_compared):
                    continue
                if isinstance(udata_compared[simid][item], np.ndarray):
                    strs = "["
                    for x in udata_compared[simid][item]:
                        strs = strs + ("{0:.2f}+/-{1:.2f}, ".format(x.n, x.s))
                    strs = strs[:-2] + "]"
                else:
                    strs = "{0:.2f}+/-{1:.2f}".format(
                        udata_compared[simid][item].n, udata_compared[simid][item].s
                    )
                f.write("{:}: {:}\n".format(self.As[simid].filename, strs))
            f.close()
        if labels_sima == None:
            labels_sima = [sim.filename for sim in self.As]
        # Export of results to plots
        if what_compare == None:
            what_compare = [
                x
                for x in self.As[simbase].post_processed_data.keys()
                if (
                    (x != "nSeeds")
                    and (x != "nServices")
                    and (x != "service_labels")
                    and (x != "workers_frac")
                    and (x != "seeds")
                )
            ]
        for item in what_compare:
            fname = filename
            if fname == None:
                fname = self.As[simbase].filename
                fname = fname.replace(".hdf5", "")
                fname = "{}_comparison".format(fname)
            data_violin = {0: [[] for _ in simscompare]}
            prob_violin = {0: [[] for _ in simscompare]}
            simid_violin = {0: [[] for _ in simscompare]}
            simidi = 0
            for simid in simscompare:
                if (simid == simbase) or (simid not in data_hdf5_compared):
                    continue
                if len(data_hdf5_compared[simid][item][1].shape) == 2:
                    for ind in range(data_hdf5_compared[simid][item][1].shape[1]):
                        if not (ind in data_violin.keys()):
                            data_violin[ind] = [[] for _ in simscompare]
                            prob_violin[ind] = [[] for _ in simscompare]
                            simid_violin[ind] = [[] for _ in simscompare]
                        data_violin[ind][simidi] = data_hdf5_compared[simid][item][1][
                            :, ind
                        ]
                        try:
                            prob_violin[ind][simidi] = stats.gaussian_kde(
                                data_violin[ind][simidi]
                            ).integrate_box_1d(-100, 0.0)
                        except:
                            prob_violin[ind][simidi] = 0
                        simid_violin[ind][simidi] = simid
                    simidi += 1
                else:
                    data_violin[0][simidi] = data_hdf5_compared[simid][item][1]
                    prob_violin[0][simidi] = stats.gaussian_kde(
                        data_violin[0][simidi]
                    ).integrate_box_1d(-100, 0.0)
                    simid_violin[0][simidi] = simid
                    simidi += 1
            for key in data_violin.keys():
                data_violin[key] = [
                    entry for entry in data_violin[key] if (entry != [])
                ]
                # prob_violin[key] = [entry for entry in prob_violin[key] if (entry != [])]
                simid_violin[key] = [
                    entry for entry in simid_violin[key] if (entry != [])
                ]
            len_data = len(data_violin.keys())
            plt.figure(figsize=(5, 4 * len_data))
            fig, axs = plt.subplots(len_data, figsize=(5, 4 * len_data))
            if len_data == 1:
                r = axs.violinplot(data_violin[0], showmeans=True, showmedians=True)
                r["cmedians"].set_color("b")
                labels = []
                for ind, id in enumerate(simid_violin[0]):
                    labels = add_label_violin(
                        r,
                        labels,
                        "sim "
                        + str(ind)
                        + ": perc="
                        + str(round(np.mean(data_violin[0][ind] * 10) / 10))
                        + "%, prob<0="
                        + str(round(prob_violin[0][ind] * 1000) / 10)
                        + "%: "
                        + labels_sima[id],
                        ind,
                    )
                axs.legend(*zip(*labels), loc=2)
            else:
                for i in range(len_data):
                    r = axs[i].violinplot(
                        data_violin[i], showmeans=True, showmedians=True
                    )
                    r["cmedians"].set_color("b")
                    labels = []
                    for ind, id in enumerate(simid_violin[i]):
                        labels = add_label_violin(
                            r,
                            labels,
                            "sim "
                            + str(ind)
                            + ": perc="
                            + str(round(np.mean(data_violin[i][ind] * 10) / 10))
                            + "%, prob<0="
                            + str(round(prob_violin[i][ind] * 1000) / 10)
                            + "%: "
                            + labels_sima[id],
                            ind,
                        )
                    axs.legend(*zip(*labels), loc=2)
            fig.suptitle("Percentage change in {}".format(item))
            plt.xlabel("Simulations")
            plt.savefig(fname + "_" + item + ".pdf")
            plt.show()
        return udata_compared
