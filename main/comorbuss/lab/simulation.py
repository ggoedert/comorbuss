import os
import time
import itertools
import h5dict
import subprocess
import numpy as np
from pprint import pprint
from multiprocessing import Pool
from .. import settings as S
from ..community import community
from ..triage import triage_parameters
from .. import tools


def replace_print(f=None, str="\t\t"):
    import builtins

    _print = print
    if f == None:
        builtins.print = lambda *args, **kwargs: _print(str, *args, **kwargs)
    else:
        builtins.print = f
    return _print


def realization(args):
    """Executes a single seed with a given set of parameters and stores the hdf5 file.

    Args:
        parameters (dict): Parameters dictionary.
        hdf5_file (str): File to store data.
        to_store (list): Data to be stored.
        to_store_srvc (list): Service data to be stored.
    """
    hdf5_file = args["hdf5_file"]
    to_store = args["to_store"]
    to_store_srvc = args["to_store_srvc"]
    pre_simulate = args["pre_simulate"] or (lambda *_: None)
    post_simulate = args["post_simulate"] or (lambda *_: None)
    parameters = args["parameters"]
    write_timeout = args["write_timeout"]
    sim_id = args["sim_id"]

    old_print = replace_print(
        str="\t\t[{}_seed_{}]".format(sim_id, parameters["random_seed"])
    )
    ti = time.time()
    comm = community(**parameters)
    pre_simulate(comm)
    comm.simulate()
    post_simulate(comm)
    if "R0t" in [item for item, _, _ in to_store]:
        comm.compute_R0t()
    replace_print(old_print)
    print(
        "\t[{}]Executed seed {} with {}s".format(
            sim_id, parameters["random_seed"], time.time() - ti
        )
    )
    tools.save_hdf5(
        [comm],
        hdf5_file,
        to_store=to_store,
        to_store_srvc=to_store_srvc,
        skip_defaults=True,
        try_append=True,
        timeout=write_timeout,
    )
    print("\t[{}]Stored seed {} data.".format(sim_id, parameters["random_seed"]))
    comm.clear()
    del comm


class FixedParamsSimulation:
    def __init__(
        self,
        seeds,
        parameters,
        hdf5_file,
        sim_id="",
        nproc=1,
        to_store=[],
        to_store_srvc=[],
        pre_simulate=None,
        post_simulate=None,
        append_data=False,
        write_timeout=60,
    ):
        """Run multiple seeds with a fixed set of parameters.

        Args:
            seeds (list): Seeds to run.
            parameters (dict): Parameters dictionary.
            hdf5_file (str): File to store data.
            sim_id (str, optional): Name of this configuration.
            nproc (int, optional): Number of simultaneous threads to run. Defaults to 1.
            to_store (list, optional): Data to be stored. If not informed will store default TO_STORE.
            to_store_srvc (list, optional): Service data to be stored. If not informed will store default TO_STORE_SRVC.
            pre_simulate (function, optional): A function to be run between community initialization and simulation,
                it must accept a community object as parameter.
            post_simulate (function, optional): A function to be run after simulation of the community,
                it must accept a community object as parameter.
            append_data (bool, optional): Try to append seeds that are not in the hdf5 file, will not run seeds already
                stored on the hdf5 file. Defaults to False.
            write_timeout (int, optional): Lenght of time to keep trying to write on unavailable hdf5 file.
        """
        self.sim_id = sim_id
        self.seeds = [int(s) for s in seeds]
        self.nproc = nproc
        self.parameters = tools.recursive_copy(parameters)
        self.triaged_parameters = triage_parameters(dict(parameters), gen_pop=False)[0]
        self.pre_simulate = pre_simulate
        self.post_simulate = post_simulate
        self.to_store = to_store
        self.write_timeout = write_timeout
        self.hdf5_file = hdf5_file
        self.to_store_srvc = to_store_srvc
        if append_data:
            self.append_data = True
            try:
                self.check_seeds()
            except:
                print(
                    "\tCouldn't load seeds from {} file, will run all seeds from this configuration.".format(
                        self.hdf5_file
                    )
                )
        if not append_data:
            if os.path.isfile(hdf5_file):
                os.remove(hdf5_file)

    def check_seeds(self):
        """Remove seeds already on hdf5 file."""
        stored_seeds = []
        with h5dict.File(self.hdf5_file, "r") as hdf5:
            stored_seeds = hdf5["realizations"].keys()
        removed_seeds = []
        for seed in stored_seeds:
            seed = int(seed)
            if seed in self.seeds:
                self.seeds.remove(seed)
                removed_seeds.append(seed)
        if removed_seeds != []:
            removed_seeds.sort()
            print(
                "\tSeeds: {} found on hdf5 file: {} will not be simulated.".format(
                    removed_seeds, self.hdf5_file
                )
            )

    def gen_ps(self):
        ps = []
        for seed in self.seeds:
            parameters = tools.recursive_copy(self.parameters)
            parameters["random_seed"] = seed
            ps.append({})
            ps[-1]["sim_id"] = self.sim_id
            ps[-1]["parameters"] = parameters
            ps[-1]["to_store"] = self.to_store
            ps[-1]["to_store_srvc"] = self.to_store_srvc
            ps[-1]["pre_simulate"] = self.pre_simulate
            ps[-1]["post_simulate"] = self.post_simulate
            ps[-1]["hdf5_file"] = self.hdf5_file
            ps[-1]["write_timeout"] = self.write_timeout
        return ps

    def simulate(self):
        """Run simulations."""
        ps = self.gen_ps()
        pool = Pool(processes=self.nproc)
        pool.map(realization, ps)
        pool.close()


class Simulation:
    """The Simulation class handles multiple simulations with multiple seeds and/or multiple combinations of parameters.

    !!! seealso
        Usage examples can be found in the [jupyter-examples](https://gitlab.com/ggoedert/comorbuss/-/tree/master/jupyter-examples)
            folder in the repository:

        * [Simulation_example.ipynb](https://gitlab.com/ggoedert/comorbuss/-/blob/master/jupyter-examples/Simulation_example.ipynb)
        * [meanfield.ipynb](https://gitlab.com/ggoedert/comorbuss/-/blob/master/jupyter-examples/meanfield.ipynb)
    """

    def __init__(
        self,
        experiment_name,
        seeds,
        fixed_parameters={},
        iteration_parameters=[],
        recording_data=[],
        recording_data_srvc=[],
        nproc=1,
        pre_simulate=None,
        post_simulate=None,
        out=None,
        append_data=False,
        store_git_hash=False,
        write_timeout=60,
    ):
        """Instances and initialize a Simulation object.

        Args:
            experiment_name (str): Name of the experiment, will me used to name files and folders.
            fixed_parameters (dict): Fixed parameters.
            iteration_parameters (dict or list): Parameters to iterate during simulation.

                * **If list of dicts**: Will run a simulation for each dict, replacing the parameters in
                    each dict.
                * **If dict of list**: Will run a simulation for each multiplicative combination of the
                    parameters.
            recording_data (list, optional): List with all data to be recorded after the simulation.
            recording_data_srvc (list, optional): List with all service data to be recorded after the simulation.
            pre_simulate (function, optional): A function to be run between community initialization and simulation,
                it must accept a community object as parameter.
            post_simulate (function, optional): A function to be run after simulation of the community,
                it must accept a community object as parameter.
            seeds (list): List of seeds.
            nproc (int, optional): Number of concurrent threads. Defaults to 1.
            append_data (bool, optional): Append data to existing files, will not simulate existing seeds.
                Defaults to False.
            out (str, optional): Output directory.
            store_git_hash (bool, optional): Runs 'git rev-parse HEAD' on current folder and stores it to
                configurations files. Defaults to False.
            write_timeout (int, optional): Lenght of time to keep trying to write on unavailable hdf5 file.
        """
        self.nproc = nproc
        self.experiment_name = experiment_name
        self.seeds = seeds
        self.count_seeds = len(seeds)
        self.append_data = append_data
        self.rng = np.random.default_rng(int(time.time()))
        if store_git_hash:
            self.git_hash = subprocess.check_output(["git", "rev-parse", "HEAD"])

        # Select data to be recorded
        self.recording_data = recording_data
        self.recording_data_srvc = recording_data_srvc
        # initialize to_store array
        self.to_store = []
        to_store_default = S.TO_STORE
        # compute to_store
        for i in range(len(to_store_default)):
            if to_store_default[i][0] in self.recording_data:
                self.to_store.append(to_store_default[i])
        # initialize service to_store array
        self.to_store_srvc = []
        to_store_srvc_default = S.TO_STORE_SRVC
        # compute service to_store
        for i in range(len(to_store_srvc_default)):
            if to_store_srvc_default[i][0] in self.recording_data_srvc:
                self.to_store_srvc.append(to_store_srvc_default[i])
        self.write_timeout = write_timeout

        # Stores pre and post simulate functions
        self.pre_simulate = pre_simulate
        self.post_simulate = post_simulate

        # Set output directory
        self.out = out or os.path.join("out", "simulations", self.experiment_name)
        self.out = os.path.abspath(self.out)
        tools.check_dir(self.out)

        # Set parameters
        self.fixed_parameters = fixed_parameters
        self.iteration_parameters = iteration_parameters

        if not "city_name" in self.fixed_parameters:
            self.fixed_parameters["city_name"] = self.experiment_name
        self.fixed_parameters["log_progress"] = False

        self.iterable = self.gen_configurations(
            self.fixed_parameters, self.iteration_parameters, self.experiment_name
        )

        # Calculate complexity
        self.complexity = self.gen_complexity(self.iterable)
        self.sum_complexity = np.sum(self.complexity)

    # INTERNAL METHODS
    def gen_configurations(self, fp, ip, ename):
        # Generate configurations from fixed_parameters and iteration_parameters
        configs = []
        ccount = 0
        # iteration parameters as list of dicts
        if type(ip) is list:
            # If iteration_parameters is empty generate only one configuration with fixed parameters
            if len(ip) == 0:
                configs.append(self.new_config(ename, fp))
            # If iteration_parameters is list of dicts generate one configuration for each dict
            else:
                for iparam in ip:
                    if type(iparam) is dict:
                        ccount += 1
                        if "name" in iparam:
                            name = iparam["name"]
                            del iparam["name"]
                        else:
                            name = "config_{}".format(ccount)
                        configs.append(self.new_config(name, fp, iparam))
                    else:
                        raise RuntimeError(
                            "iteration_parameters must be list of dicts, empty list or dict of lists."
                        )
        # iteration parameters as dict of lists
        elif type(ip) is dict:
            for _, value in ip.items():
                try:
                    list(value)
                except:
                    raise RuntimeError(
                        "iteration_parameters must be list of dicts, empty list or dict of lists."
                    )
            # Do the combination product of the parameters variations
            ip_names = list(ip.keys())
            ip_values = [ip[name] for name in ip_names]
            iterable = list(itertools.product(*ip_values))
            # Generate configurations from it
            for combination in iterable:
                ccount += 1
                name = "config_{}".format(ccount)
                iparam = {}
                for i, value in enumerate(combination):
                    iparam[ip_names[i]] = value
                configs.append(self.new_config(name, fp, iparam))
        else:
            raise RuntimeError(
                "iteration_parameters must be list of dicts, empty list or dict of lists."
            )

        return configs

    def new_config(self, name, fp, ip={}):
        # Generate an new config from fixed_parameters and iteration parameters
        config = {
            "name": name,
            "iteration_parameters": ip,
            "fixed_parameters": tools.recursive_copy(fp),
            "parameters": tools.recursive_copy(fp),
            "hdf5_file": os.path.join(self.out, "{}.hdf5".format(name)),
            "params_file": os.path.join(self.out, "{}.txt".format(name)),
        }
        if hasattr(self, "git_hash"):
            config["git_hash"] = self.git_hash
        for param, value in config["iteration_parameters"].items():
            if type(param) is tuple:
                is_tuple_of_tuple = True
                for key in param:
                    is_tuple_of_tuple &= type(key) is tuple
            else:
                is_tuple_of_tuple = False
            if not is_tuple_of_tuple:
                param = (param,)
            for key in param:
                if type(key) is tuple:
                    if key[0] not in config["parameters"]:
                        raise ValueError(
                            "{} iteration parameter tried to replace a sub parameter, but {} is not on fixed parameters.".format(
                                key, key[0]
                            )
                        )
                    config["parameters"][key[0]] = self.apply_sub_parameter(
                        key[1:],
                        tools.recursive_copy(config["parameters"][key[0]]),
                        value,
                    )
                else:
                    config["parameters"][key] = value
        triaged_param = triage_parameters(dict(config["parameters"]), gen_pop=False)[0]
        config["Nsteps"] = triaged_param["Nsteps"]
        config["Nparticles"] = triaged_param["Nparticles"]
        return config

    def apply_sub_parameter(self, key, parameter, value):
        if len(key) == 1:
            parameter = tools.recursive_copy(parameter)
            parameter[key[0]] = value
        else:
            if type(parameter[key[0]]) is tuple:
                parameter[key[0]] = (
                    parameter[key[0]][0],
                    self.apply_sub_parameter(
                        key[1:], tools.recursive_copy(parameter[key[0]][1]), value
                    ),
                )
            else:
                parameter[key[0]] = self.apply_sub_parameter(
                    key[1:], tools.recursive_copy(parameter[key[0]]), value
                )
        return parameter

    def gen_complexity(self, configs):
        # Calculates the complexity for each configuration
        complexity = []
        for param in configs:
            complexity.append(param["Nparticles"] * param["Nsteps"])
        return complexity

    # EXTERNAL/INTERNAL METHODS
    def get_hdf5_files(self):
        """Generates a dict of the hdf5 files for the set simulations with the sim_id/name as the keys.

        Returns:
            dict: Paths to the HDF5 files.
        """
        files = {}
        for config in self.iterable:
            files[config["name"]] = config["hdf5_file"]
        return files

    def get_iteration_parameters(self):
        """Outputs the iteration parameters as a list of dicts.

        Returns:
            dict: Iteration parameters as list of dicts.
        """
        ips = []
        for config in self.iterable:
            ips.append(tools.recursive_copy(config["iteration_parameters"]))
            ips[-1]["name"] = config["name"]
        return ips

    def print_iteration_parameters(self):
        """Prints the iteration parameters as a list of dicts."""
        pprint(self.get_iteration_parameters())

    def get_confs(self, node, nnodes, ntype):
        confs = self.iterable
        sum_complexity = self.sum_complexity
        if nnodes > 1 and ntype == "configurations":
            confs = [c for i, c in enumerate(confs) if i % nnodes == node]
            complexity = self.gen_complexity(confs)
            sum_complexity = np.sum(complexity)
        return confs, sum_complexity

    def get_seeds(self, node, nnodes, ntype):
        seeds = self.seeds
        if nnodes > 1 and ntype == "seeds":
            seeds = [s for i, s in enumerate(seeds) if i % nnodes == node]
        return seeds

    def simulate(self, node=0, number_of_nodes=1, node_type="seeds"):
        """Runs the simulations

        Args:
            node (int, optional): [description]. Defaults to 0.
            number_of_nodes (int, optional): Divides the simulation in multiples nodes.
                Defaults to 1.
            node_type (str, optional): Specify where to divide nodes, `seeds` or `configurations`.
                Defaults to 'seed'.
        """
        # initialize time and percentage
        ts = time.time()
        p_done = 0

        # run simulation for all configurations
        print("Simulating experiment {}...".format(self.experiment_name))
        confs, sum_complexity = self.get_confs(node, number_of_nodes, node_type)
        count_conf = len(confs)
        for this_conf, params in enumerate(confs):
            # load simulation data
            sim_id = params["name"]
            sim_params = params["parameters"]
            sim_hdf5 = params["hdf5_file"]

            # Saves simulation data to disk
            with open(params["params_file"], "w") as f:
                pprint(params, stream=f)

            # Runs simulation
            print(
                "({}/{}) Computing configuration {}/{}".format(
                    this_conf + 1, count_conf, self.experiment_name, sim_id
                )
            )

            this_conf = number_of_nodes * this_conf + node
            p_now = self.complexity[this_conf] / sum_complexity
            t_spent = time.time() - ts
            t_exp = -1 if p_done == 0 else t_spent / p_done
            print(
                "Time projection at {:.2f}s/{:.2f}s, {:.2f}% done, {:.2f}% processing now.".format(
                    t_spent, t_exp, p_done * 100, p_now * 100
                )
            )
            sim = FixedParamsSimulation(
                self.get_seeds(node, number_of_nodes, node_type),
                sim_params,
                sim_hdf5,
                sim_id=sim_id,
                to_store=self.to_store,
                nproc=self.nproc,
                to_store_srvc=self.to_store_srvc,
                pre_simulate=self.pre_simulate,
                post_simulate=self.post_simulate,
                append_data=self.append_data,
                write_timeout=self.write_timeout,
            )
            print(
                "Running for {} particles with {} steps".format(
                    sim.triaged_parameters["Nparticles"],
                    sim.triaged_parameters["Nsteps"],
                )
            )  # ,   , memory around {:.2f}gb
            # 4.9e-8*sim.triaged_parameters['Nparticles']*sim.triaged_parameters['Nsteps']*self.count_seeds))
            sim.simulate()
            p_done += self.complexity[this_conf] / sum_complexity

    def simulate_all(
        self, node=0, number_of_nodes=1, node_type="seeds", randomize_order=False
    ):
        confs, _ = self.get_confs(node, number_of_nodes, node_type)
        if randomize_order:
            self.rng.shuffle(confs)
        confs_names = []
        ps = []
        seeds = self.get_seeds(node, number_of_nodes, node_type)
        for this_conf, params in enumerate(confs):
            # load simulation data
            sim_id = params["name"]
            sim_params = params["parameters"]
            sim_hdf5 = params["hdf5_file"]
            confs_names.append(sim_id)
            print("\tCollecting simulations for {} configutation.".format(sim_id))
            sim = FixedParamsSimulation(
                seeds,
                sim_params,
                sim_hdf5,
                sim_id=sim_id,
                to_store=self.to_store,
                nproc=self.nproc,
                to_store_srvc=self.to_store_srvc,
                pre_simulate=self.pre_simulate,
                post_simulate=self.post_simulate,
                append_data=self.append_data,
                write_timeout=self.write_timeout,
            )
            ps += sim.gen_ps()
        print(
            "Computing {} seeds on {} configurations ({} total simulations): {}".format(
                len(seeds), len(confs), len(ps), confs_names
            )
        )
        pool = Pool(processes=self.nproc)
        pool.map(realization, ps)
        pool.close()
