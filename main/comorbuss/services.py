from . import settings as S
from .decisions import DECISON_FUNCTIONS
from . import net_generators
import numpy as np
from numpy.random import default_rng
from scipy.spatial import cKDTree
from .aux_classes import empty_module, mem_clear
from .decisions import to_close_services, to_reopen_services

__name__ = "Services"


class services_infrastructure(empty_module):
    """Provides infrastructure to inialize and store services.

    Attributes:
        services_parameters (dict): Parameters dictionary used for services initialization.
        service (dict): Dict where services can be accessed by name.
        all_services (list): List with all services indexed by id.
    """

    def __init__(self, parameters, comm):
        srvc_parameters = parameters["services"]
        super(services_infrastructure, self).__init__(
            srvc_parameters, comm, name=__name__
        )

        # Stores ids name and if service closes in parameters
        for i, srvc in enumerate(self.parameters):
            srvc["id"] = i
        # Empty lists to store initialized services
        self.service = {}
        self.all_services = []
        self.services_visitable = []
        for srvc in self.parameters:
            name = srvc["name"]
            self.assign_srvc_neighborhood(srvc)
            self.assign_work(srvc, self.comm)
            self.assign_visitors(srvc, self.comm)
            self.assign_workers_family(srvc, self.comm)
            self.service[name] = service(srvc, self.comm, self)
            self.all_services.append(self.service[name])

        if parameters["services_close"] and len(self.all_services) > 0:
            self.comm.add_to_execution(self.close_or_open_services, priority=51)
        self.comm.add_to_execution(self.services_visit, priority=76)

        self.reduce_workers = parameters["reduce_workers"]
        if self.reduce_workers:
            self.reduce_workers_ds = parameters["reduce_workers_series"]

        self.reduce_visitors = parameters["reduce_visitors"]
        if self.reduce_visitors:
            self.reduce_visitors_ds = parameters["reduce_visitors_series"]

    def __repr__(self):
        return "<COMORBUSS services infrastructure: {}>".format(self.service.keys())

    def exists(self, name):
        try:
            self[name]
            return True
        except KeyError:
            return False

    def __getitem__(self, key):
        try:
            return self.all_services[key]
        except TypeError:
            return self.service[key]

    def services_visit(self):
        """Executes visitation dynamics for services. The order in which the
        services are processed is shuffled, so that the mean visitation period
        is respected for many time steps, independently on the order in which
        particles are assigned to visit a service.
        """
        order = np.arange(len(self.all_services), dtype=int)
        self.rng.shuffle(order)
        for index in order:
            srvc = self.all_services[index]
            srvc.update_visit_probab()
            srvc.return_home()
            srvc.visit()

    # Closing of services
    def close_or_open_services(self):
        """Close or opens services according to the decision and time series set."""
        for srvc in self.all_services:
            if not srvc.service_is_closed:
                if to_close_services(self.comm, srvc.closing_par, srvc):
                    srvc.close_service()
                    self.event_log("Closing " + srvc.name, S.MSG_EVENT)
            elif srvc.service_is_closed:
                if to_reopen_services(self.comm, srvc.closing_par, srvc):
                    srvc.open_service()
                    self.event_log("Opening " + srvc.name, S.MSG_EVENT)

    # Assigns a neighborhood for each instance of the service
    def assign_srvc_neighborhood(self, service_param):
        service_param["neighborhood"] = np.zeros(service_param["number"], dtype=int)
        if "number_per_nbhd" in service_param.keys():
            j = 0
            for i, n in enumerate(service_param["number_per_nbhd"]):
                service_param["neighborhood"][j : j + n] = i
                j = j + n

    # Randomly assign particles to work in services
    def assign_work(self, service_param, comm):
        """Prepare workers to be assigned to services,  This method ramdomly pools
        workers from population.workersAvailable and assign them workplaces and shifts.

        Args:
            service_param (dict): Parameters for the service
        """
        service_param["workers_parameters"] = service_param["workers"]
        service_param["workers_ids"] = []
        service_param["workers_instance"] = []
        service_param["workers_room"] = []
        service_param["workers_shift_start"] = []
        service_param["workers_shift_end"] = []
        service_param["workers_shift"] = []
        service_param["workers_types"] = []
        for wrkr in service_param["workers_parameters"]:
            # Calculate number of shifts and the number of workers to be chosen
            nShifts = np.shape(wrkr["shifts"])[0]
            service_param["number_of_shifts"] = nShifts
            nWorkers = int(wrkr["number"] * service_param["number"])
            if nShifts > 0 and nWorkers > 0:
                # Counts the number of suitable and available workers
                maskAvailableInGroup = np.isin(
                    comm.pop.pid, comm.pop.workersAvailable
                ) & np.isin(comm.pop.ages, wrkr["age_groups"])
                nAvailable = np.count_nonzero(maskAvailableInGroup)
                # Limit the number of workers to be chosen accordingly to the number of available workers
                if nWorkers > nAvailable:
                    comm.event_log(
                        "There is not enough workers available for {} for service {}, {} is needed but only {} is available.".format(
                            wrkr["name"],
                            service_param["name"],
                            nWorkers,
                            nAvailable,
                        ),
                        S.MSG_WRNG,
                    )
                    nWorkers = nAvailable
                    wrkr["number"] = int(
                        nAvailable / service_param["number"]
                    )  # Get the workers
                workersIds = comm.randgencom.choice(
                    comm.pop.pid[maskAvailableInGroup], nWorkers, replace=False
                )
                # Delete workers from pool of available workers
                maskDelete = ~np.isin(comm.pop.workersAvailable, workersIds)
                comm.pop.workersAvailable = comm.pop.workersAvailable[maskDelete]
                maskWorkers = np.isin(comm.pop.pid, workersIds)
                comm.pop.workplace_id[maskWorkers] = service_param["id"]

                for j, wid in enumerate(workersIds):
                    service_param["workers_ids"].append(wid)
                    # Assign workers to workplace sequentially with workplaces (assign N workers to first workplace, then N to second workplace...)
                    instance = int(
                        np.min(
                            [
                                np.trunc(j / wrkr["number"]),
                                service_param["number"] - 1,
                            ]
                        )
                    )
                    # Temporarily assign all workers to first room
                    room = 0
                    # Assign workers to shift sequentially with workers (first worker to first shift, second worker to second shift...)
                    shift_id = j % nShifts
                    shift = wrkr["shifts"][j % nShifts]
                    type = wrkr["id"]

                    # Store selected values
                    service_param["workers_instance"].append(instance)
                    comm.pop.workplace_instance[wid] = instance
                    service_param["workers_room"].append(room)
                    comm.pop.workplace_room[wid] = room
                    service_param["workers_shift_start"].append(shift[0])
                    service_param["workers_shift_end"].append(shift[1])
                    service_param["workers_shift"].append(j % nShifts)
                    comm.pop.worker_type[wid] = type
                    service_param["workers_types"].append(type)
        service_param["workers_ids"] = np.array(service_param["workers_ids"])
        service_param["workers_instance"] = np.array(service_param["workers_instance"])
        service_param["workers_room"] = np.array(service_param["workers_room"])
        service_param["workers_shift_start"] = np.array(
            service_param["workers_shift_start"]
        )
        service_param["workers_shift_end"] = np.array(
            service_param["workers_shift_end"]
        )
        service_param["workers_shift"] = np.array(service_param["workers_shift"])
        service_param["workers_types"] = np.array(service_param["workers_types"])

        if service_param["rooms"] != []:
            self.generate_rooms(service_param, comm)

    def generate_rooms(self, service_param, comm):
        assigned_room = np.zeros(len(service_param["workers_ids"]), dtype=bool)
        for instance in range(service_param["number"]):
            for shift in range(service_param["number_of_shifts"]):
                current_room = 1
                for room in service_param["rooms"]:
                    # Get workers without room assignment
                    available_workers = dict()
                    # Iterate on all types of workers for that room
                    for key in room:
                        if key in ["number", "name"]:
                            continue
                        # Iterate in all types of workers for this service
                        for wrkr in service_param["workers_parameters"]:
                            # If the type of worker is in this room add them to the available workers list
                            if key.lower() == wrkr["name"].lower():
                                mask_workers = (
                                    (service_param["workers_instance"] == instance)
                                    & (service_param["workers_types"] == wrkr["id"])
                                    & ~assigned_room
                                    & (service_param["workers_shift"] == shift)
                                )
                                available_workers[key] = service_param["workers_ids"][
                                    mask_workers
                                ]

                    # check if this room has a predetermined number
                    if "number" in room:
                        if room["number"] > 0:
                            number_of_rooms = room["number"]
                        else:
                            number_of_rooms = S.inf
                    else:
                        number_of_rooms = S.inf

                    # Generate current room
                    room_count = 0
                    while True:
                        # Determine if there is workers available of all types and if not completed the number of rooms
                        min_wrkr_len = np.min(
                            [
                                len(available_workers[k])
                                for k in available_workers
                                if room[k] != 0
                            ]
                        )
                        if min_wrkr_len == 0:
                            break
                        if room_count >= number_of_rooms:
                            break

                        # Select workers for current room
                        selected_workers = []
                        for name in available_workers:
                            n_to_get = room[name]
                            if n_to_get > len(available_workers[name]):
                                n_to_get = len(available_workers[name])
                            selected = available_workers[name][:n_to_get]
                            available_workers[name] = available_workers[name][n_to_get:]
                            selected_workers = np.concatenate(
                                (selected_workers, selected)
                            )

                        # Generate masks to write new rooms to selected workers
                        mask_pop_selected = np.isin(comm.pop.pid, selected_workers)
                        mask_wrkr_selected = np.isin(
                            service_param["workers_ids"], selected_workers
                        )

                        # Write new rooms to selected workers
                        assigned_room[mask_wrkr_selected] = True
                        comm.pop.workplace_room[mask_pop_selected] = current_room
                        service_param["workers_room"][mask_wrkr_selected] = current_room

                        # Increase current room
                        current_room += 1
                        room_count += 1

        # Check if any worker where not assigned room
        if np.count_nonzero(~assigned_room) > 0:
            n = np.count_nonzero(~assigned_room)
            self.event_log(
                "{} service has rooms but {} workers where not assigned any rooms,"
                " check the rooms parameter for this service.".format(
                    service_param["name"], n
                ),
                S.MSG_WRNG,
            )

    # Select closest service for particles to visit
    def assign_visitors(self, service_param, comm):
        """Compute closest service per individual in population.

        Args:
            service_param (dict): Parameters for current service
        Returns:
            np.array: chosen services id for all population
        """
        service_param["visitors"] = -1 * np.ones(comm.pop.Nparticles, dtype=int)
        if service_param["number"] > 0:
            if "number_per_nbhd" in service_param.keys() or comm.networks:
                if (
                    not "number_per_nbhd" in service_param.keys()
                    or service_param["number_per_nbhd"] == []
                ):
                    service_param["number_per_nbhd"] = [service_param["number"]]
                iids = np.arange(service_param["number"])
                for nbhd in range(len(service_param["number_per_nbhd"])):
                    mask_in_nbhd = comm.pop.neighborhood == nbhd
                    mask_srvc_in_nbhd = service_param["neighborhood"] == nbhd
                    iids_in_nbhd = iids[mask_srvc_in_nbhd]
                    service_param["visitors"][mask_in_nbhd] = comm.pop.rng.choice(
                        iids_in_nbhd, np.count_nonzero(mask_in_nbhd)
                    )
            else:
                sid = service_param["id"]
                seq_services_ids = comm.geo.services_ids[sid].reshape(-1)
                mytree = cKDTree(comm.geo.centers[seq_services_ids])
                indexes = mytree.query(comm.geo.homes[:, :2])[1]
                home_to_nearest_service_id = np.floor(
                    indexes / (comm.geo.dservices[sid] * comm.geo.dservices[sid])
                ).astype(int)
                service_param["visitors"] = home_to_nearest_service_id[comm.pop.home_id]
            maskNotInAgeGroup = ~np.isin(comm.pop.ages, service_param["age_groups"])
            service_param["visitors"][maskNotInAgeGroup] = -1
            for i in range(len(service_param["workers_ids"])):
                service_param["visitors"][
                    service_param["workers_ids"][i]
                ] = service_param["workers_instance"][i]
        visitation_period = service_param["visitation_period"]
        service_param["visitation_period"] = S.inf * np.ones(comm.Nparticles)
        mask_visitors = service_param["visitors"] != -1
        service_param["visitation_period"][mask_visitors] = visitation_period
        service_param["rooms"] = np.zeros(service_param["visitors"].shape, dtype=int)
        service_param["rooms"][service_param["workers_ids"]] = service_param[
            "workers_room"
        ]

    def assign_workers_family(self, service_param, comm):
        mask_assigned = np.zeros(comm.Nparticles, dtype=bool)
        if service_param["number"] > 0:
            for i, wid in enumerate(service_param["workers_ids"]):
                home_id = comm.pop.home_id[wid]
                type = service_param["workers_types"][i]
                mask_same_home = comm.pop.home_id == home_id
                mask_age_group = np.isin(
                    comm.pop.ages,
                    service_param["workers_parameters"][type]["family_age_groups"],
                )
                mask_visitors = mask_same_home & mask_age_group & ~mask_assigned
                service_param["visitors"][mask_visitors] = service_param[
                    "workers_instance"
                ][i]
                service_param["visitation_period"][mask_visitors] = 1 / (
                    (1 / service_param["visitation_period"][mask_visitors])
                    + (
                        1
                        / service_param["workers_parameters"][type][
                            "family_visitation_period"
                        ]
                    )
                )
                mask_assigned[mask_visitors] = True


class service(mem_clear):
    """Class for services implementation.

    Attributes:
        name (sting): Name of the service.
        id (int): Id of this service.
        number (int): Number of instances of this service.
        rooms (int): List of rooms in this service.
        chosen_instance (np.ndarray): List of the preferred instance of this service for all population
            (-1 particle will not go to this service).
        visit_period (float): Normal period for visitation in days.
        base_visit_probab (float): Normal base visitation probability per step of simulation.
        visit_probab (float): Current visitation probability per step of simulation.
        isol_period (float): Period for visitation for particles in lockdowns.
        isol_probab (float): Base visitation probability for particles in lockdown.
        visit_duration (float): Duration of visitation in hours.
        return_probab (float): Probability for the end of an visitation.
        guest_ids (list): List to store temporary guests (not visitors, currently only used to
            hospitalized particles).
        workers_ids (np.ndarray): Id of the workers for this service.
        mask_workers (np.ndarray): Boolean mask of the population that identifies workers of this service.
        workers_instance (np.ndarray): Instance each worker is assigned, follows workers_ids order.
        workers_shift_start (np.ndarray): Time the shift of each worker start, follows workers_ids order.
        workers_shift_end (np.ndarray): Time the shift of each worker end, follows workers_ids order.
        workers_room (np.ndarray): Room each worker is assigned, follows workers_ids order.
        workers_types (np.ndarray): Type of worker for each worker, follows workers_ids order.
        workers_parameters (list): Original parameters used on the assignment of the workers.
        working_hours (list): Working hours for this service: [opening, closing].
        placement (int): Placement tag for use in this service.
        close (bool): Stores if service closes with interventions.
        service_is_closed (bool): Stores if service is closed.
        centersAddress (list): Address of the center point for all instances of this service
            (used with distance mechanics).
        limits (list): Limits for all instances of this service (used with distance mechanics).
        service_address (np.ndarray): Preferred address for visitation/work for all particles
            (used with distance mechanics).
        grids (list): List with the internal grids addresses for all instances of this service
            (used with distance mechanics).
        grid_address (np.ndarray): Addresses inside internal grids for all particles used when
            fixed point in grid is active for this service (used with distance mechanics).
        controlledAccess (bool): If false particles can walk inside service (only relevant with distance
            mechanics)
        net_type (int): Type of net generator to be used in this service (used with networks mechanics).
        net_par (dict): Parameters for the network generator (used with networks mechanics).
    """

    def __init__(self, service_parameters, comm, srv):
        # Random number generator for the service
        self.randgenserv = default_rng(comm.random_seed)

        # Pointer for the rest of the simulation
        self.event_log = comm.event_log
        self.comm = comm
        self.clk = comm.clk
        self.pop = self.comm.pop
        self.srv = srv

        # General attributes
        self.name = service_parameters["name"]
        self.id = service_parameters["id"]
        self.number = service_parameters["number"]
        self.base_chosen_instance = service_parameters["visitors"]
        self.instance = np.copy(self.base_chosen_instance)
        self.room = service_parameters["rooms"]
        self.instances = range(self.number)
        self.rooms = np.sort(np.unique(self.room))

        # Visitation attributes
        self.visit_period = service_parameters["visitation_period"]
        self.base_visit_probab = comm.dt / self.visit_period
        self.visit_probab = np.copy(self.base_visit_probab)
        self.visit_duration = 1.0
        self.return_probab = comm.dt / self.visit_duration
        self.workers_ids = service_parameters["workers_ids"]
        # self.workers_ids.sort()
        self.mask_workers = np.isin(self.pop.pid, self.workers_ids)
        self.workers_instance = service_parameters["workers_instance"]
        self.workers_shift_start = service_parameters["workers_shift_start"]
        self.workers_shift_end = service_parameters["workers_shift_end"]
        self.workers_room = service_parameters["workers_room"]
        self.workers_type = service_parameters["workers_types"]
        self.workers_parameters = service_parameters["workers_parameters"]
        self.guests_ids = []
        self.guests_ids_ts = [[]]
        self.working_hours = service_parameters["hours"]
        self.working_days = service_parameters["days"]
        self.placement = self.id
        self.previous_placement = S.PLC_HOME * np.ones(comm.Nparticles, dtype=int)
        self.previous_activity = S.ACT_FREE * np.ones(comm.Nparticles, dtype=int)

        # Interventions parameters
        self.close = service_parameters["close"]
        self.inf_prob_weight = service_parameters["inf_prob_weight"]
        self.to_close = DECISON_FUNCTIONS[service_parameters["decision"]]["start"]
        self.to_open = DECISON_FUNCTIONS[service_parameters["decision"]]["stop"]
        self.closing_par = service_parameters["decision_par"]
        self.service_is_closed = False
        self.no_workers_available = False
        self.instance_is_closed = [False for _ in self.instances]
        self.isolation_visit_frac = service_parameters["isolation_visit_frac"]
        self.expected_visits = np.sum(self.visit_probab) * (
            1.0 + self.comm.social_isolation_mean * (self.isolation_visit_frac - 1.0)
        )
        self.visitors_frac = 1.0
        self.workers_active = np.ones(len(self.workers_ids), dtype=bool)
        self.workers_frac = 1.0
        self.cumm_visit_probab = np.zeros(self.comm.Nparticles)
        self.day_series = service_parameters["day_series"]

        # Network parameters attributes
        self.net_type = service_parameters["net_type"]
        self.net_par = service_parameters["net_par"]
        if type(self.net_type) is str:
            generator_class = getattr(net_generators, self.net_type.lower())
        elif type(self.net_type) is int:
            generator_class = net_generators.SERVICE_NET_GENS[self.net_type]
        else:
            self.event_log(
                "{} is an invalid net_type for {} service, assuming default net.".format(
                    self.net_type, self.name
                ),
                S.MSG_WRNG,
            )
            generator_class = net_generators.default
            self.net_type = "default"
            self.net_par = DEFAULTS_NET["net_par"]
        self.net_generator = generator_class(self, self.net_par, self.comm)
        self.pop.add_net_generator(self.id, self.net_generator)
        self.mask_is_worker = np.isin(comm.pop.pid, self.workers_ids)

        self.visitors_count = np.zeros(comm.Nsteps, dtype=int)

        # Add methods to execution list
        self.comm.add_to_execution(
            self.reduce_workers_ds, priority=55, time_of_the_day=0
        )
        self.comm.add_to_execution(
            self.reduce_visitors_ds, priority=55.1, time_of_the_day=0
        )
        self.comm.add_to_execution(self.work, priority=75)

    def __repr__(self):
        return "<{} service>".format(self.name)

    def in_this_service(self):
        return self.pop.placement == self.id

    def in_instance(self, instance):
        return self.in_this_service() & (self.instance == instance)

    def in_room(self, instance, room):
        return self.in_instance(instance) & (self.room == room)

    def update_visit_probab(self):
        self.visit_probab[:] = self.base_visit_probab * self.visitors_frac
        self.visit_probab[self.comm.isol_mask_prev] = (
            self.base_visit_probab[self.comm.isol_mask_prev]
            * self.isolation_visit_frac
            * self.visitors_frac
        )

    def reduce_visitors(self, frac):
        self.visitors_frac = frac
        self.update_visit_probab()

    def reduce_visitors_ds(self):
        if self.srv.reduce_visitors:
            if self.srv.reduce_visitors_ds.shape[1] > self.id:
                self.reduce_visitors(
                    self.srv.reduce_visitors_ds[self.clk.today, self.id]
                )

    def normalize_visitors(self):
        self.visitors_frac = 1
        self.update_visit_probab()

    def reduce_workers(self, frac):
        if self.workers_frac > frac:
            wids = np.arange(len(self.workers_ids), dtype=int)
            reduce_frac = self.workers_frac - frac
            wids_to_stop = []
            for i in range(self.number):
                mask_workers = self.workers_instance == i
                nWorkers = np.count_nonzero(mask_workers)
                nto_reduce = int(round(nWorkers * reduce_frac))
                wids_to_stop += list(
                    self.randgenserv.choice(
                        wids[mask_workers], size=nto_reduce, replace=False
                    )
                )
            self.workers_active[wids_to_stop] = False
            self.event_log(
                "Pids: {:} stopped working on service {:}.".format(
                    self.workers_ids[wids_to_stop], self.name
                ),
                S.MSG_EVENT,
            )
        elif self.workers_frac < frac:
            wids = np.arange(len(self.workers_ids), dtype=int)
            increase_frac = frac - self.workers_frac
            wids_to_return = []
            for i in range(self.number):
                mask_workers = self.workers_instance == i
                mask_inactive = mask_workers & ~self.workers_active
                nWorkers = np.count_nonzero(mask_workers)
                nto_return = int(round(nWorkers * increase_frac))
                wids_to_return += list(
                    self.randgenserv.choice(
                        wids[mask_inactive], size=nto_return, replace=False
                    )
                )
            self.workers_active[wids_to_return] = True
            self.event_log(
                "Pids: {:} returned working on service {:}.".format(
                    self.workers_ids[wids_to_return], self.name
                ),
                S.MSG_EVENT,
            )
        self.workers_frac = frac

    def reduce_workers_ds(self):
        if self.srv.reduce_workers:
            if self.srv.reduce_workers_ds.shape[1] > self.id:
                self.reduce_workers(self.srv.reduce_workers_ds[self.clk.today, self.id])

    def normalize_workers(self):
        self.event_log(
            "Pids: {:} returned working on service {:}.".format(
                self.workers_ids[~self.workers_active], self.name
            ),
            S.MSG_EVENT,
        )
        self.workers_active = np.ones(len(self.workers_ids), dtype=bool)
        self.workers_frac = 1.0

    def open_service(self):
        """Opens a service."""
        self.service_is_closed = False

    def close_service(self):
        """Closes a service."""
        self.service_is_closed = True

    def restore_instances(self, instance):
        mask_instance = self.base_chosen_instance == instance
        self.instance[mask_instance] = instance

    def reassign_instances(self, instance, available_instances):
        mask_instance = self.instance == instance
        n = np.count_nonzero(mask_instance)
        self.instance[mask_instance] = self.randgenserv.choice(
            available_instances, size=n
        )

    def close_dont_reassign_instances(self, instance):
        # This will not prevent workers from working, only visitors!!!!!
        self.instance_is_closed[instance] = True
        mask_instance = self.instance == instance
        self.instance[mask_instance] = -1

    def reopen_instances(self, instance):
        self.restore_instances(instance)
        self.instance_is_closed[instance] = False

    def allocate_guests(self, new_guest_id):
        """Add particles to the list of guests.

        Args:
            new_guest_id (np.ndarray): List of the ids to be added.
        """
        if (not self.service_is_closed) & (len(new_guest_id) > 0):
            self.guests_ids = np.concatenate((self.guests_ids, new_guest_id))

    def deallocate_guests(self, remove_guests_ids):
        """Removes particles from the list of guests.

        Args:
            remove_guests_ids (np.ndarray): List of ids to be removed.
        """
        if len(remove_guests_ids) > 0:
            self.guests_ids = self.guests_ids[
                ~np.isin(self.guests_ids, remove_guests_ids)
            ]

    def in_working_hours(self):
        return (
            self.clk.between_hours(self.working_hours[0], self.working_hours[1])
            and self.working_days[self.clk.dow]
        )

    def closing_time(self):
        return (
            self.clk.at_time_of_day(self.working_hours[1])
            and self.working_days[self.clk.dow]
        )

    def return_particles(self, mask_return):
        activity = self.previous_activity[mask_return]
        placement = self.previous_placement[mask_return]
        mask_not_in_quarantine = activity != S.ACT_QRNT
        placement[
            mask_not_in_quarantine
        ] = S.PLC_HOME  # particles not in quarantine send home
        for srvc_id in np.unique(
            self.previous_placement[mask_return][mask_not_in_quarantine]
        ):
            if srvc_id >= 0:
                mask_other_srvc = self.previous_placement[mask_return] == srvc_id
                activity[mask_other_srvc] = self.srv[srvc_id].previous_activity[
                    mask_return
                ][mask_other_srvc]
        self.pop.mark_for_new_position(mask_return, placement, activity)

    def get_particles(self, mask_get, activity):
        self.previous_placement[mask_get] = self.pop.placement[mask_get]
        self.previous_activity[mask_get] = self.pop.activity[mask_get]
        self.pop.mark_for_new_position(mask_get, self.placement, activity)

    def visit(self):
        """Randomly chooses free particles to go to their service during business hours."""
        # During working hours chooses particles to visit according to visitor_probab and isol_probab
        if self.number > 0 and self.in_working_hours() and not self.service_is_closed:
            # Get particles that can visit service
            mask_not_in_quarantine = ~np.isin(
                self.pop.quarantine_states, self.comm.quarantines.confining_quarantines
            )
            mask_act_free = self.pop.activity <= S.ACT_FREE
            mask_act_work = self.pop.activity == S.ACT_WORK
            mask_could_visit = mask_not_in_quarantine & mask_act_free
            # Select particles to visit, using it's respective probabilities visitor_probab and isol_probab
            mask_chosen_instance = self.instance > -1
            mask_could_visit = mask_could_visit & mask_chosen_instance
            probab = self.randgenserv.uniform(0.0, 1.0, size=(self.pop.Nparticles))
            visit_probab = 1 - ((1 - self.visit_probab) ** self.cumm_visit_probab)
            mask_go = mask_could_visit & (probab < visit_probab)
            # Updates placement, activity and movement for selected particles
            self.get_particles(mask_go, S.ACT_VISIT)
            # Reset cumulative probability for particles that can visit and accumulates for working particles
            self.cumm_visit_probab[mask_act_free | self.mask_workers] = 1
            mask_visitors_working = mask_chosen_instance & mask_act_work
            self.cumm_visit_probab[mask_visitors_working] += 1

    def return_home(self):
        """Get particles back home after visitation."""
        # Return particles according to return_probab during working hours
        if self.number > 0 and self.in_working_hours():
            probab = self.randgenserv.uniform(0.0, 1.0, size=(self.pop.Nparticles))
            mask_in_service = self.pop.placement == self.placement
            mask_visiting = self.pop.activity == S.ACT_VISIT
            mask_return = (
                mask_in_service & mask_visiting & (probab < self.return_probab)
            )
        # Return all visitors home at the end of working hours
        elif self.number > 0 and not self.in_working_hours():
            mask_in_service = self.pop.placement == self.placement
            mask_visiting = self.pop.activity == S.ACT_VISIT
            mask_return = mask_in_service & mask_visiting
        else:
            return None
        self.return_particles(mask_return)

    def work(self):
        """Gets particles to work and return them home."""
        if len(self.workers_ids) > 0:
            maskInShift = self.clk.between_hours_array(
                self.workers_shift_start, self.workers_shift_end
            )
            maskInShift = maskInShift & self.working_days[self.clk.dow]
            try:
                maskShiftEnd = (maskInShift == False) & (
                    self.last_mask_in_shift == True
                )
            except AttributeError:
                maskShiftEnd = np.zeros(len(maskInShift), dtype=bool)
            self.last_mask_in_shift = maskInShift

            # Check if particles working in service should return home
            workersAfterShift = self.workers_ids[
                maskShiftEnd
            ]  # Get pid of workers after the end of the shift
            workersActivity = self.pop.activity[
                workersAfterShift
            ]  # Get activity of all workers
            working = workersActivity == S.ACT_WORK  # Select only workers with ACT_WORK
            workersToReturn = workersAfterShift[
                working
            ]  # Select only worker with ACT_WORK and after the end of the shift
            mask_return = np.isin(self.pop.pid, workersToReturn)
            self.return_particles(mask_return)

            if self.no_workers_available:
                self.service_is_closed = False

            # Check if particles should go to work
            if not self.service_is_closed:
                maskInShiftActive = maskInShift & self.workers_active
                workersInShift = self.workers_ids[
                    maskInShiftActive
                ]  # Get pid of workers in shift hours
                workersQuarantine = self.pop.quarantine_states[
                    workersInShift
                ]  # Get quarantine state of workers
                workersState = self.pop.states[workersInShift]
                maskWorkersAvailable = ~np.isin(
                    workersQuarantine, self.comm.quarantines.confining_quarantines
                ) & (workersState != S.STATE_D)
                workersWorking = workersInShift[
                    maskWorkersAvailable
                ]  # Select only workers in shift hours and off quarantine
                mask_to_work = np.isin(self.pop.pid, workersWorking) & (
                    self.pop.placement != self.placement
                )
                self.get_particles(mask_to_work, S.ACT_WORK)

                # check if there is enough workers in every instance
                if self.in_working_hours():
                    instances_w_workers = np.unique(
                        self.base_chosen_instance[workersWorking]
                    )
                    if len(instances_w_workers) == 0:
                        self.service_is_closed = True
                        if not self.no_workers_available:
                            self.no_workers_available = True
                            self.event_log(
                                "Closing {} service due to all workers in quarantine.".format(
                                    self.name
                                ),
                                S.MSG_EVENT,
                            )
                        return
                    else:
                        if self.no_workers_available:
                            self.no_workers_available = False
                            self.event_log(
                                "Opening {} service, there is workers back.".format(
                                    self.name
                                ),
                                S.MSG_EVENT,
                            )
                    for instance in self.instances:
                        if self.instance_is_closed[instance]:
                            if instance in instances_w_workers:
                                self.restore_instances(instance)
                                self.event_log(
                                    "Opening a instance of {}, there is workers back.".format(
                                        self.name
                                    ),
                                    S.MSG_EVENT,
                                )
                                self.instance_is_closed[instance] = False
                        else:
                            if not instance in instances_w_workers:
                                self.reassign_instances(instance, instances_w_workers)
                                self.event_log(
                                    "Closing a instance of {} service due to all workers in quarantine".format(
                                        self.name
                                    ),
                                    S.MSG_EVENT,
                                )
                                self.instance_is_closed[instance] = True


#%% Constants for services generation
DEFAULTS_DIST = {
    "grid_width": 5,
    "area": 0,  # unit_area
    "move_inside": False,
    "controlled_access": True,
    "visitors_fixed_point": True,
    "workers_fixed_point": True,
    "reserved_urban_area": 0.0,
}

DEFAULTS_NET = {
    "net_type": S.NET_DEFAULT,
    "net_par": {
        "m": 3,
    },
}

MARKETS_NET = {
    "net_type": S.NET_MARKETS,
    "net_par": {
        "worker_mean_contacts": 1,
        "visitor_mean_contacts": 2,
        "visitor_to_worker_mean_contacts": 0.25,
        "cashier_prob": 0.4,
        "cashiers_pool_frac": 0,
    },
}

MARKETS_WORKERS = {
    "name": "Markets workers",
    "number": 5,
    "shifts": [[7, 19]],
}

MARKETS_CASHIERS = {
    "name": "Cashiers",
    "number": 2,
    "shifts": [[7, 19]],
}

MARKETS = {
    "name": "Markets",
    "number": -1,
    "hours": [7, 19],
    "days": [0, 1, 1, 1, 1, 1, 1],
    "age_groups": np.arange(4, 21),
    "visitation_period": 5,
    "workers": [dict(MARKETS_WORKERS), dict(MARKETS_CASHIERS)],
    "close": False,
    "inf_prob_weight": 1.0,
}
"""Default settings for markets"""
MARKETS.update(MARKETS_NET)
MARKETS.update(DEFAULTS_DIST)
MARKETS.update({"grid_width": 8})

DISEASE_WORKER_PROB = 0.3

HOSPITALS_NET = {
    "net_type": S.NET_HOSPITALS,
    "net_par": {
        "disease_worker_prob": DISEASE_WORKER_PROB,
        "worker_mean_contacts": 2.0,
        "disease_worker_mean_contacts": 2.9,
        "disease_worker_to_worker_mean_contacts": 0.2,
        "visitor_mean_contacts": 2.0,
        "visitor_to_worker_mean_contacts": 1.0,
        "guest_to_disease_worker_mean_contacts": 0.15,
        "expose_visitors_to_disease_workers": False,
        "disease_workers_pool_frac": 0,
    },
}

HOSPITALS_WORKERS = {
    "name": "Hospitals workers",
    "number": 60 * (1 - DISEASE_WORKER_PROB),
    "shifts": [[0, 6], [6, 18], [18, 0]],
}

HOSPITALS_DISEASE_WORKERS = {
    "name": "Disease workers",
    "number": 60 * DISEASE_WORKER_PROB,
    "shifts": [[0, 6], [6, 18], [18, 0]],
}

HOSPITALS = {
    "name": "Hospitals",
    "number": -1,
    "hours": [0, 24],
    "days": [1, 1, 1, 1, 1, 1, 1],
    "age_groups": S.ALL_AGES,
    "visitation_period": 90,
    "workers": [dict(HOSPITALS_WORKERS), dict(HOSPITALS_DISEASE_WORKERS)],
    "close": False,
    "inf_prob_weight": 1.0,
}
"""Default settings for hospitals"""
HOSPITALS.update(HOSPITALS_NET)
HOSPITALS.update(DEFAULTS_DIST)

SCHOOLS_NET = {
    "net_type": S.NET_SCHOOLS,
    "net_par": {
        "break_classrooms": 5,
        "break_period": 2.0,
        "break_mean_contacts": 4.0,
    },
}

WORKERS_STUDENTS = {
    "name": "Students",
    "population_fraction": 0.7,
    "shifts": [[7, 12]],
    "age_groups": [1, 2, 3],
    "family_visitation_period": 60,
    "family_age_groups": np.arange(4, 12),
}

WORKERS_TEACHERS = {
    "name": "Teachers",
    "number": 50,
    "shifts": [[7, 12]],
}

ROOMS_CLASSROOM = {
    "name": "Classroom",
    "Students": 20,
    "Teachers": 1,
}

ROOMS_TEACHERS_ROOM = {
    "name": "Teachers room",
    "Teachers": 10,
}

# Used to ensure students separation even if there isn't enough teachers
ROOMS_TEACHERLESS_CLASSROOM = {
    "name": "Theacherless classroom",
    "Students": 20,
}

SCHOOLS = {
    "name": "Schools",
    "number": -1,
    "hours": [7, 12],
    "days": [0, 1, 1, 1, 1, 1, 0],
    "age_groups": [],
    "visitation_period": S.inf,
    "workers": [dict(WORKERS_STUDENTS), dict(WORKERS_TEACHERS)],
    "rooms": [
        dict(ROOMS_CLASSROOM),
        dict(ROOMS_TEACHERLESS_CLASSROOM),
        dict(ROOMS_TEACHERS_ROOM),
    ],
    "close": True,
    "inf_prob_weight": 1.0,
}
"""Default settings for schools"""
SCHOOLS.update(SCHOOLS_NET)
SCHOOLS.update(DEFAULTS_DIST)

RESTAURANTS_NET = {
    "net_type": S.NET_RESTAURANTS_2,
    "net_par": {
        "waiter_prob": 1.0,
        "workers_mean_contacts": 3.0,
        "persons_per_table": 4,
    },
}

RESTAURANTS_WORKERS = {
    "number": 5,
    "shifts": [[12, 22]],
}

RESTAURANTS = {
    "name": "Restaurants",
    "number": -1,
    "hours": [12, 22],
    "days": [1, 1, 1, 1, 1, 1, 1],
    "age_groups": np.arange(4, 21),
    "visitation_period": 7,
    "workers": [dict(RESTAURANTS_WORKERS)],
    "close": True,
    "inf_prob_weight": 1.0,
}
"""Default settings for restaurants"""
RESTAURANTS.update(RESTAURANTS_NET)
RESTAURANTS.update(DEFAULTS_DIST)

STREET_MARKETS_NET = {
    "net_type": S.NET_STREET_MARKETS,
    "net_par": {
        "seller_mean_contacts": 1.8,
        "visitor_mean_contacts": 15.0,
        "visitor_to_seller_mean_contacts": 3.0,
    },
}

STREET_MARKETS_WORKERS = {
    "number": 185,
    "shifts": [[7, 13]],
    "age_groups": np.arange(5, 18),
}

STREET_MARKETS = {
    "name": "Street Markets",
    "number": -1,
    "hours": [7, 13],
    "days": [0, 0, 0, 0, 0, 0, 1],
    "age_groups": np.arange(4, 21),
    "visitation_period": 30,
    "workers": [dict(STREET_MARKETS_WORKERS)],
    "close": True,
    "inf_prob_weight": 1.0,
}
"""Default settings for street markets"""
STREET_MARKETS.update(STREET_MARKETS_NET)
STREET_MARKETS.update(DEFAULTS_DIST)

HOTELS = {
    "name": "Hotels",
    "number": -1,
    "visitation_period": S.inf,
    "workers": [],
    "net_type": S.NET_ISOLATE,
}
"""A place to isolate all particles."""

GENERIC_SERVICES_WORKERS = {
    "population_fraction": 0.59,
}

ROOMS_OFFICES = {
    "name": "Offices",
    "Workers": 10,
}

GENERIC = {
    "name": "Generic Services",
    "number": -1,
    "hours": [7, 16],
    "days": [0, 1, 1, 1, 1, 1, 0],
    "age_groups": [],
    "visitation_period": np.inf,
    "workers": [dict(GENERIC_SERVICES_WORKERS)],
    "rooms": [dict(ROOMS_OFFICES)],
    "close": True,
    "inf_prob_weight": 1.0,
}
GENERIC.update(DEFAULTS_NET)
GENERIC.update(DEFAULTS_DIST)
GENERIC.update({"reserved_urban_area": 0.5})

WORKERS_DEFAULTS = {
    "name": "Workers",
    "number": -1,
    "population_fraction": -1.0,
    "age_groups": np.arange(4, 12),
    "shifts": [[7, 16]],
    "family_visitation_period": 30,
    "family_age_groups": [],
}

WORKERS_DEFAULTS_TYPES = {
    "name": str,
    "number": int,
    "population_fraction": float,
    "age_groups": (np.array, {"dtype": int}),
    "shifts": list,
    "family_visitation_period": float,
    "family_age_groups": (np.array, {"dtype": int}),
}

DEFAULTS = {
    "name": "None",
    "number": np.array(1),
    "hours": [7, 16],
    "days": [0, 1, 1, 1, 1, 1, 1],
    "age_groups": S.ALL_AGES,
    "visitation_period": 7,
    "rooms": 1,
    "isolation_visit_frac": 0.5,
    "workers": [dict(WORKERS_DEFAULTS)],
    "rooms": [],
    "close": True,
    "inf_prob_weight": 1.0,
    "decision": -1,
    "decision_par": {},
}
"""Default service settings."""
DEFAULTS.update(DEFAULTS_NET)
DEFAULTS.update(DEFAULTS_DIST)

# Convetion functions for all parameters, used in triage
DEFAULTS_TYPES = {
    "name": str,
    "number": (np.array, {"dtype": int}),
    "hours": list,
    "days": (np.array, {"dtype": bool}),
    "age_groups": (np.array, {"dtype": int}),
    "visitation_period": float,
    "isolation_visit_frac": float,
    "workers": list,
    "rooms": list,
    "close": bool,
    "inf_prob_weight": float,
    "decision": int,
    "decision_par": dict,
    "net_type": int,
    "net_par": dict,
    "grid_width": int,
    "area": float,
    "move_inside": bool,
    "controlled_access": bool,
    "visitors_fixed_point": bool,
    "workers_fixed_point": bool,
    "reserved_urban_area": float,
    "day_series": (np.array, {"dtype": int}),
}

DEFAULT_SERVICES = [MARKETS, HOSPITALS, SCHOOLS, RESTAURANTS]

DEFAULT_NUMBERS = {
    "Markets": 256,
    "Hospitals": 16,
    "Schools": 160,
    "Restaurants": 740,
    "Generic": 3700,
}
