from ...aux_classes import empty_module
from ...triage.triage_functions import parse_from_defaults
import numpy as np

__name__ = "reduce_inf_prob_time"

DEFAULTS = {
    "active": True,
    "factor": -1,
    "hours": [],
}

DEFAULTS_TYPES = {
    "active": bool,
    "factor": float,
    "hours": (np.array, {"dtype": float}),
}


def triage(module_param, sim_param, clk, event_log):
    # Allow user to just pass True or False to enable/disable module with default settings.
    if type(module_param) is bool:
        param = {}
        param.update(DEFAULTS)
        param["active"] = module_param
        module_param = param
    else:
        parse_from_defaults(
            module_param, "reduce_inf_prob_time", DEFAULTS, DEFAULTS_TYPES, event_log
        )
    # If hours is empty use reverse of free_hours
    if len(module_param["hours"]) == 0:
        module_param["hours"] = [sim_param["free_hours"][1], sim_param["free_hours"][0]]
    # If factor is -1 use 1/number of hours
    if module_param["factor"] == -1:
        nhours = module_param["hours"][1] - module_param["hours"][0]
        if nhours < 0:
            nhours += 24
        module_param["factor"] = 1 / nhours
    return module_param


class module(empty_module):
    def __init__(self, param, comm):
        super(module, self).__init__(param, comm, name=__name__)
        self.event_log = comm.event_log
        if param["active"]:
            self.comm.add_to_execution(
                self.enable, priority=28, time_of_the_day=param["hours"][0]
            )
            self.comm.add_to_execution(
                self.disable, priority=20, time_of_the_day=param["hours"][1]
            )
            self.pop.disease.add_coefficient(
                "inf_probability", np.ones(self.pop.Nparticles), "reduce_inf_prob"
            )
            self.factor = param["factor"]

    def enable(self):
        self.pop.disease.set_coefficient(
            "inf_probability",
            self.factor * np.ones(self.pop.Nparticles),
            "reduce_inf_prob",
        )

    def disable(self):
        self.pop.disease.set_coefficient(
            "inf_probability",
            np.ones(self.pop.Nparticles),
            "reduce_inf_prob",
        )
