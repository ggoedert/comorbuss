from ... import settings as S
from ...aux_classes import clock, empty_analysis
from ...tools import efficacy_calc

# from ...lab import Analysis
import numpy as np
from matplotlib.lines import Line2D
from .. import vaccinations
from io import StringIO
import pandas as pd
from tqdm.auto import tqdm

__name__ = "vaccination_trial"


class TrialAnalysis(empty_analysis):
    def __init__(
        self,
        analysis,
        number_to_combine=4,
        stop_criteria=150,
        testing_window=7,
        cut_off_day=14,
    ):
        super(TrialAnalysis, self).__init__(analysis)

        self.analysis = analysis
        self.number_to_combine = number_to_combine
        self.stop_criteria = stop_criteria
        self.testing_window = testing_window

        PARTICIPANTS = (__name__, "participants")
        VACC_GROUP = (__name__, "vaccine_group")
        PLAC_GROUP = (__name__, "placebo_group")
        DIAG = (__name__, "diagnostic_states")
        CHALL = (__name__, "challenges_csv")
        data = self.get_data(
            [PARTICIPANTS, VACC_GROUP, PLAC_GROUP, DIAG, CHALL, "time_vaccinated"], None
        )

        # Collect necessary data
        Nparticles = self.parameters["Nparticles"]
        Ndays = self.parameters["Ndays"]
        dt = self.parameters["dt"]
        clk = clock(dt=dt)
        Nwindows = np.floor(Ndays / testing_window).astype(int)
        self.Nwindows = Nwindows
        Nseeds = len(data["seeds"])
        seeds = np.arange(0, Nseeds)
        min_participants = np.min([len(a) for a in data[PARTICIPANTS]])
        participants = np.array([a[:min_participants] for a in data[PARTICIPANTS]])
        vaccine_group = np.array([a[:min_participants] for a in data[VACC_GROUP]])
        placebo_group = np.array([a[:min_participants] for a in data[PLAC_GROUP]])
        day_vaccinated = np.zeros(participants.shape, dtype=int)
        for i in seeds:
            day_vaccinated[i] = clk.time_to_days(
                data["time_vaccinated"][i][participants[i]]
            ).astype(int)
        vacc_start = np.min(day_vaccinated)
        ndays_vacc = Ndays - vacc_start
        diag_states = np.array([a[:, :min_participants] for a in data[DIAG]])

        # Read challenges dfs
        challenges_dfs = []
        for csv in data[CHALL]:
            challenges_dfs.append(pd.read_csv(StringIO(csv)))
        self.challenges_dfs = challenges_dfs

        # Creates participants and both groups masks
        mask_participants = np.ones(participants.shape, dtype=bool)
        mask_vaccine = np.zeros(participants.shape, dtype=bool)
        mask_placebo = np.zeros(participants.shape, dtype=bool)
        n_vacc_group = np.zeros(Nseeds)
        n_plac_group = np.zeros(Nseeds)
        for i in range(Nseeds):
            mask_participants[i] = diag_states[i, cut_off_day] != S.DIAG_YES
            mask_vaccine[i] = (
                np.isin(participants[i], vaccine_group[i]) & mask_participants[i]
            )
            n_vacc_group[i] = np.count_nonzero(mask_vaccine[i])
            mask_placebo[i] = (
                np.isin(participants[i], placebo_group[i]) & mask_participants[i]
            )
            n_plac_group[i] = np.count_nonzero(mask_placebo[i])

        # Counts cases in each group for each testing day
        cases = np.zeros((Nseeds, Nwindows), dtype=int)
        cases_vacc = np.zeros((Nseeds, Nwindows), dtype=int)
        cases_plac = np.zeros((Nseeds, Nwindows), dtype=int)
        challenges_vacc = np.zeros((Nseeds, Nwindows), dtype=int)
        challenges_plac = np.zeros((Nseeds, Nwindows), dtype=int)
        chellenged_vacc = np.zeros((Nseeds, Nwindows), dtype=int)
        chellenged_plac = np.zeros((Nseeds, Nwindows), dtype=int)
        for i in tqdm(range(Nseeds)):
            chall_df = self.challenges_dfs[i]
            pids = np.array(chall_df.pid)
            inf_sus = np.array(chall_df.inf_susceptibility)
            times = np.array(chall_df.time)
            for w in tqdm(range(Nwindows), leave=False):
                for p in range(participants.shape[1]):
                    if mask_participants[i, p]:
                        day = day_vaccinated[i, p] + w * testing_window
                        last_day = day - testing_window
                        pid = participants[i, p]
                        challenges = np.count_nonzero(
                            (pids == pid)
                            & (times >= clk.days_to_time(last_day) + 8)
                            & (times < clk.days_to_time(day) + 8)
                            & (times >= clk.days_to_time(cut_off_day) + 8)
                            & (inf_sus > 0)
                        )
                        challenged = np.count_nonzero(
                            (pids == pid)
                            & (times < clk.days_to_time(day) + 8)
                            & (times >= clk.days_to_time(cut_off_day) + 8)
                            & (inf_sus > 0)
                        )
                        if challenges > 0:
                            if mask_vaccine[i, p]:
                                challenges_vacc[i, w] += challenges
                            elif mask_placebo[i, p]:
                                challenges_plac[i, w] += challenges
                        if challenged > 0:
                            if mask_vaccine[i, p]:
                                chellenged_vacc[i, w] += 1
                            elif mask_placebo[i, p]:
                                chellenged_plac[i, w] += 1
                        if day >= Ndays:
                            day = -1
                        if diag_states[i, day, p] == S.DIAG_YES:
                            cases[i, w] += 1
                            if mask_vaccine[i, p]:
                                cases_vacc[i, w] += 1
                            elif mask_placebo[i, p]:
                                cases_plac[i, w] += 1

        # Do combinations
        if number_to_combine > 1:
            combination = self.gen_combination(seeds, number_to_combine)
            cases = self.combine_seeds(cases, combination)
            cases_plac = self.combine_seeds(cases_plac, combination)
            cases_vacc = self.combine_seeds(cases_vacc, combination)
            n_vacc_group = self.combine_seeds(n_vacc_group, combination)
            n_plac_group = self.combine_seeds(n_plac_group, combination)
            chellenged_vacc = self.combine_seeds(chellenged_vacc, combination)
            challenges_vacc = self.combine_seeds(challenges_vacc, combination)
            chellenged_plac = self.combine_seeds(chellenged_plac, combination)
            challenges_plac = self.combine_seeds(challenges_plac, combination)
            self.combination = combination

        self.cases = cases
        self.cases_plac = cases_plac
        self.cases_vacc = cases_vacc
        self.n_vacc_group = n_vacc_group
        self.n_plac_group = n_plac_group
        self.challenged_vacc = chellenged_vacc
        self.challenges_vacc = challenges_vacc
        self.challenged_plac = chellenged_plac
        self.challenges_plac = challenges_plac

    @staticmethod
    def gen_combination(seeds, number_to_combine):
        from itertools import combinations

        return list(combinations(seeds, number_to_combine))

    @staticmethod
    def combine_seeds(array, combinations):
        Ncomb = len(combinations)
        if len(array.shape) == 1:
            array_sum = np.zeros(Ncomb)
            for i in range(Ncomb):
                array_sum[i] = np.sum(array[list(combinations[i])])
        elif len(array.shape) == 2:
            array_sum = np.zeros([Ncomb, array.shape[1]])
            for i in range(Ncomb):
                array_sum[i, :] = np.sum(array[list(combinations[i]), :], axis=0)
        else:
            array_sum = None
        return array_sum

    def plot_cases(self, filename=None, size=(10, 4), title="Vaccination trial cases"):
        colors = list(S.COLORS_LIST)
        legends = []
        self.start_plt(size, title)
        x = np.arange(1, self.Nwindows + 1) * self.testing_window
        xlim = (0, (self.Nwindows + 1) * self.testing_window)
        for cases, name in [
            (self.cases, "all"),
            (self.cases_plac, "placebo"),
            (self.cases_vacc, "vaccine"),
        ]:
            # Plot cases
            color = colors.pop(0)
            ax = self.plt.gca()
            self.plt.violinplot(
                cases,
                positions=x,
                widths=0.5 * self.testing_window,
                showmeans=True,
                showmedians=False,
            )
            legends.append(Line2D([0], [0], color=color, label=name))
        color = colors.pop(0)
        s = self.plt.plot(xlim, [self.stop_criteria, self.stop_criteria], "--")
        legends.append(
            Line2D([0], [0], color=color, label="stop_criteria", linestyle="--")
        )
        self.stop_plt(
            filename=filename,
            xlabel="Days from vaccination",
            ylabel="Number of cases",
            xlim=xlim,
            legend=True,
            legend_args={"handles": legends, "loc": "upper left"},
        )

    def calculate_efficacy(self):
        Nseeds = self.cases.shape[0]
        Nweeks = self.cases.shape[1]
        efficacy = np.zeros((Nseeds, Nweeks))
        efficacy_lower = np.zeros((Nseeds, Nweeks))
        efficacy_upper = np.zeros((Nseeds, Nweeks))
        self.ab = np.zeros((Nseeds, Nweeks))
        self.cd = np.zeros((Nseeds, Nweeks))
        self.a = np.zeros((Nseeds, Nweeks))
        self.b = np.zeros((Nseeds, Nweeks))
        self.c = np.zeros((Nseeds, Nweeks))
        self.d = np.zeros((Nseeds, Nweeks))
        for seed in range(Nseeds):
            for week in range(Nweeks):
                cases_vacc = self.cases_vacc[seed, week]
                cases_plac = self.cases_plac[seed, week]
                not_cases_vacc = self.n_vacc_group[seed] - cases_vacc
                not_cases_plac = self.n_plac_group[seed] - cases_plac
                self.a[seed, week] = cases_vacc
                self.b[seed, week] = not_cases_vacc
                self.c[seed, week] = cases_plac
                self.d[seed, week] = not_cases_plac
                self.ab[seed, week] = cases_vacc + not_cases_vacc
                self.cd[seed, week] = cases_plac + not_cases_plac
                effic, limits = efficacy_calc(
                    cases_vacc,
                    not_cases_vacc,
                    cases_plac,
                    not_cases_plac,
                    return_limits=True,
                )
                efficacy[seed, week] = effic
                efficacy_lower[seed, week] = limits[0]
                efficacy_upper[seed, week] = limits[1]
        self.efficacy = efficacy
        self.efficacy_lower = efficacy_lower
        self.efficacy_upper = efficacy_upper

    def calculate_efficacy_1(self):
        Nseeds = self.cases.shape[0]
        Nweeks = self.cases.shape[1]
        efficacy = np.zeros((Nseeds, Nweeks))
        efficacy_lower = np.zeros((Nseeds, Nweeks))
        efficacy_upper = np.zeros((Nseeds, Nweeks))
        for seed in range(Nseeds):
            for week in range(Nweeks):
                effic, limits = efficacy_calc(
                    self.cases_vacc[seed, week],
                    np.sum(self.challenges_vacc[seed][:week]),
                    self.cases_plac[seed, week],
                    np.sum(self.challenges_plac[seed][:week]),
                    return_limits=True,
                )
                efficacy[seed, week] = effic
                efficacy_lower[seed, week] = limits[0]
                efficacy_upper[seed, week] = limits[1]
        self.efficacy = efficacy
        self.efficacy_lower = efficacy_lower
        self.efficacy_upper = efficacy_upper

    def calculate_efficacy_2(self):
        Nseeds = self.cases.shape[0]
        Nweeks = self.cases.shape[1]
        efficacy = np.zeros((Nseeds, Nweeks))
        efficacy_lower = np.zeros((Nseeds, Nweeks))
        efficacy_upper = np.zeros((Nseeds, Nweeks))
        for seed in range(Nseeds):
            for week in range(Nweeks):
                effic, limits = efficacy_calc(
                    self.cases_vacc[seed, week],
                    self.challenged_vacc[seed, week],
                    self.cases_plac[seed, week],
                    self.challenged_plac[seed, week],
                    return_limits=True,
                )
                efficacy[seed, week] = effic
                efficacy_lower[seed, week] = limits[0]
                efficacy_upper[seed, week] = limits[1]
        self.efficacy = efficacy
        self.efficacy_lower = efficacy_lower
        self.efficacy_upper = efficacy_upper

    def get_target_efficacy(self):
        return 0.7
        for module, parameters in self.parameters["modules"]:
            if module == "vaccination_trial":
                return parameters["vaccination"]["effectiveness"]

    def plot_efficacy(self, seeds=None, **plot_args):
        """Save a efficacy graph.

        !!! Important
            This plot uses the do_plot method, see [plot_args](#plot_args) to see all available arguments.

        Args:
            seeds (list, optional): List of seeds to plot, if None will plot all seeds available.
            **plot_args (kwargs): Arguments for the do_plot method (see [plot_args](#plot_args)).
        """
        # Prepare Data
        if not hasattr(self, "efficacy"):
            self.calculate_efficacy()
        data = {"calculated efficacy": self.efficacy}
        colors = [S.COLORS["blue"]]
        t = np.arange(self.Nwindows)
        seeds = np.arange(self.efficacy.shape[0])

        # Get target
        data["target_efficacy"] = self.get_target_efficacy() * np.ones(
            (len(seeds), self.Nwindows)
        )
        colors.append(S.COLORS["red"])
        dt = 1

        # Plot graphs
        def_plot_args = {
            "xlabel": "Week",
            "ylabel": "Efficacy",
            "bands_legend_args": {
                "color": colors[0],
                "loc": "lower center",
            },
            "dates": False,
        }
        plot_args = self.args_parse(plot_args, def_plot_args)
        self.do_plot(t, dt, data, colors, seeds, **plot_args)

    def plot_efficacy_new(self, seeds=None, **plot_args):
        if not hasattr(self, "efficacy"):
            self.calculate_efficacy()
        efficacy = np.mean(self.efficacy, axis=0)
        efficacy_lower = np.mean(self.efficacy_lower, axis=0)
        efficacy_upper = np.mean(self.efficacy_upper, axis=0)
        t = np.arange(self.Nwindows)
        target_efficacy = self.get_target_efficacy()

        self.start_plt()
        self.plt.plot(t, efficacy, color=S.COLORS["blue"], label="calculated efficacy")
        self.plt.fill_between(
            t, efficacy_lower, efficacy_upper, color=S.COLORS["blue"], alpha=0.2
        )
        self.plt.plot(
            [t[0], t[-1]],
            [target_efficacy, target_efficacy],
            color=S.COLORS["red"],
            linestyle="--",
            label="target efficacy",
        )
        def_plot_args = {
            "xlabel": "Week",
            "ylabel": "Efficacy",
            "legend": True,
            "legend_args": {
                "loc": "lower right",
            },
            "dates": False,
        }
        plot_args = self.args_parse(plot_args, def_plot_args)
        self.stop_plt(**plot_args)

    def find_end_week(self):
        pass

    def calculate_efficacy_period(self):
        pass
