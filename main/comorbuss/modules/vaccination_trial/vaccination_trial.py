from ...tools import filter_parse, recursive_copy
from ... import settings as S
from ..vaccinations import vaccinations
from ..diagnostics import add_diagnostic
from .. import diagnostics
from ...aux_classes import empty_module
import numpy as np
import pandas as pd

__name__ = "vaccination_trial"


def triage(parameters, t_parameters, clk, event_log):
    from ...triage.triage_functions import parse_from_defaults

    parameters = recursive_copy(parameters)
    parse_from_defaults(parameters, __name__, DEFAULTS, DEFAULTS_TYPES, event_log)
    parameters["vaccination"] = vaccinations.triage(
        [parameters["vaccination"]], t_parameters, clk, event_log
    )[0]
    return parameters


DEFAULT_DIAG = {
    "name": "Trial diag",
    "sensitivity_function": diagnostics.sensitivity_PCR,
    "specificity": 0.9,
    "retest_delay": 7.0,
    "result_delay": 0.0,
    "test_delay": 0.0,
    "allow_retest": "negative",
}

DEFAULTS = {
    "name": __name__,
    "include_family": True,
    "exclude_target": False,
    "placebo_ratio": 0.5,
    "target_number": -1,
    "target_filter": vaccinations.FILTER_DIAG_NO,
    "vaccination": {},
    "diagnostics": {},
    "one_challenge": False,
    "one_challenge_only_vaccine_group": True,
    "one_challenge_start_day": 14,
}

DEFAULTS_TYPES = {
    "name": str,
    "include_family": bool,
    "exclude_target": bool,
    "placebo_ratio": float,
    "target_number": int,
    "target_filter": tuple,
    "vaccination": [dict, tuple],
    "diagnostics": [dict, tuple],
    "one_challenge": bool,
    "one_challenge_only_vaccine_group": bool,
    "one_challenge_start_day": float,
}


class module(empty_module):
    def __init__(self, trial_param, comm):
        super(module, self).__init__(trial_param, comm, name=__name__)

        self.exec_tod = "self.free_hours[0]"
        self.scenario_id = "VacTrial"
        # Initialize vaccination
        placebo_vaccination = dict(trial_param["vaccination"])
        trial_param["vaccination"]["filter"] = "self.comm.{}.mask_vaccine_group".format(
            self.name
        )
        trial_param["vaccination"]["id"] = 99
        placebo_vaccination["filter"] = "self.comm.{}.mask_placebo_group".format(
            self.name
        )
        placebo_vaccination["effectiveness"] = [0.0]
        placebo_vaccination["id"] = 98
        self.pop.add_attribute(
            "time_vaccinated", -1 * np.ones(self.pop.Nparticles), store=True
        )
        self.pop.add_attribute(
            "immunity_state",
            vaccinations.VACC_NYET * np.ones(self.pop.Nparticles, dtype=int),
            store=True,
        )
        self.vaccination = vaccinations.vaccination(
            trial_param["vaccination"], self.comm, self.rng
        )
        self.placebo_vaccination = vaccinations.vaccination(
            placebo_vaccination, self.comm, self.rng
        )

        # Initialize diagnostics
        self.to_test = np.zeros(self.pop.Nparticles, dtype=bool)
        immunity_states_list = [
            self.vaccination.id,
            self.placebo_vaccination.id,
            self.vaccination.id * 100,
            self.placebo_vaccination.id * 100,
        ]
        trial_diag = trial_param["diagnostics"]
        if type(trial_diag) is tuple:
            trial_diag[1][
                "filter_particles"
            ] = "self.comm.{}.mask_in_trial & np.isin(self.pop.immunity_state, {})".format(
                self.name, immunity_states_list
            )
        else:
            trial_diag[
                "filter_particles"
            ] = "self.comm.{}.mask_in_trial & np.isin(self.pop.immunity_state, {})".format(
                self.name, immunity_states_list
            )
        self.diagnostics = add_diagnostic(
            trial_diag, "vaccination trial diagnostic", self.comm, DEFAULT_DIAG
        )

        # Selects particles to trial
        self.particles = {}
        include_family = trial_param["include_family"]
        exclude_target = trial_param["exclude_target"]
        target_number = trial_param["target_number"]
        placebo_ratio = trial_param["placebo_ratio"]
        self.vaccination.doses_per_day *= 1 - placebo_ratio
        self.placebo_vaccination.doses_per_day *= placebo_ratio
        self.target_filter, fok = filter_parse(
            trial_param["target_filter"],
            self.comm,
            allowed_filters=["tracing", "service", "module", "workers"],
        )
        if not fok:
            self.event_log(
                "Could not parse vaccination trial target_filter.", S.MSG_ERROR
            )
            self.comm.initOk = False
        try:
            mask_target_population = eval(self.target_filter)
        except:
            import traceback, sys

            print("Previous traceback:")
            for line in traceback.format_exception(*sys.exc_info()):
                print(line)
            raise RuntimeError(
                "Error evaluating target_filter for vaccination trial, parsed filter is: {}".format(
                    self.target_filter
                )
            )
        target_pids = self.pop.pid[mask_target_population]

        # Select family of target particles
        self.target_families = {}
        self.vaccine_families = {}
        self.placebo_families = {}
        if include_family:
            # Count available particles
            n_available = 0
            for pid in target_pids:
                n_available += np.count_nonzero(
                    self.pop.home_id == self.pop.home_id[pid]
                )
                if exclude_target:
                    n_available -= 1
            n_target = int(np.min([target_number, n_available]))

            self.rng.shuffle(target_pids)
            vaccine_group_number = int(n_target * (1 - placebo_ratio))
            number_selected = 0
            vaccine_group_pids = []
            selected_pids = []
            for pid in target_pids:
                mask_family = self.pop.home_id == self.pop.home_id[pid]
                if exclude_target:
                    mask_family[pid] = False
                self.target_families[pid] = self.pop.pid[mask_family]
                # Put particles in vaccine group first
                if number_selected < vaccine_group_number:
                    self.vaccine_families[pid] = []
                    for p in self.target_families[pid]:
                        vaccine_group_pids.append(p)
                        self.vaccine_families[pid].append(p)
                        selected_pids.append(p)
                        number_selected += 1
                        if number_selected >= vaccine_group_number:
                            break
                # Then in placebo group
                else:
                    self.placebo_families[pid] = []
                    for p in self.target_families[pid]:
                        self.placebo_families[pid].append(p)
                        selected_pids.append(p)
                        number_selected += 1
                        if number_selected >= n_target:
                            break
                if number_selected >= n_target:
                    break
        # Put only target on trial
        elif not exclude_target:
            # Randomly excludes particles to adhere to target number
            if target_number > 0 and target_number < len(target_pids):
                selected_pids = self.rng.choice(
                    target_pids, target_number, replace=False
                )
            else:
                selected_pids = target_pids
            vaccine_group_number = int(len(selected_pids) * (1 - placebo_ratio))
            vaccine_group_pids = self.rng.choice(
                selected_pids, vaccine_group_number, replace=False
            )
        else:
            raise RuntimeError(
                "Can not run {} trial vaccination excluding target and family.".format(
                    self.name
                )
            )

        # Remove possible duplicates
        selected_pids = np.unique(selected_pids)

        # Stores particles data
        self.particles = {}
        for pid in selected_pids:
            pid_dict = {
                "id": pid,
                "in_trial": True,
            }
            if pid in target_pids:
                pid_dict["type"] = "target"
                pid_dict["target_id"] = pid
            else:
                pid_dict["type"] = "ring"
                # If in ring find target pid and stores it's data
                for target_pid, target_family in self.target_families.items():
                    if pid in target_family:
                        if not target_pid in self.particles:
                            self.particles[target_pid] = {
                                "id": target_pid,
                                "in_trial": False,
                                "type": "excluded_target",
                            }
                            pid_dict["target_id"] = target_pid
                            break
            if pid in vaccine_group_pids:
                pid_dict["vaccine"] = True
            else:
                pid_dict["vaccine"] = False
            pid_dict["diagnostics"] = []
            self.particles[pid] = pid_dict
        self.diagnostics_results = []

        # Check if got enough particles
        if len(selected_pids) < target_number:
            self.event_log(
                "{} vaccination trial will run below target number, could only select "
                "{} particles using {} filter.".format(
                    self.name, len(selected_pids), self.target_filter
                ),
                S.MSG_WRNG,
            )

        # Stores vaccine and placebo groups
        self.mask_in_trial = np.isin(self.pop.pid, selected_pids)
        self.mask_vaccine_group = np.isin(self.pop.pid, vaccine_group_pids)
        self.mask_placebo_group = self.mask_in_trial & ~self.mask_vaccine_group

        self.last_time_diagnosed = self.pop.time_diagnosed[self.mask_in_trial]

        # Cumulative counts for stop condition
        self.severe_cumulative = np.zeros(self.comm.Nsteps)
        self.deceased_cumulative = np.zeros(self.comm.Nsteps)
        self.infections_cumulative = np.zeros(self.comm.Nsteps)
        self.diagnosed_cumulative = np.zeros(self.comm.Nsteps)
        self.symptoms_cumulative = np.zeros(self.comm.Nsteps)
        self.diagnostic_states = S.ninf * np.ones(
            (self.comm.Ndays + 1, len(selected_pids))
        )

        self.already_diagnosed = np.zeros(self.pop.Nparticles, dtype=bool)

        self.pop.enable_tracing(buffer_length=1)
        self.mask_challenged = np.zeros(self.comm.Nparticles, dtype=bool)
        self.count_challenged = np.zeros((self.comm.Nsteps, 2))
        self.count_challenged_cumm = np.zeros((self.comm.Nsteps, 2))
        self.count_challenges = np.zeros(
            (self.comm.Nsteps, self.comm.Nparticles), dtype=int
        )
        self.count_susceptible = np.zeros((self.comm.Nsteps, 2))
        self.count_infected = np.zeros((self.comm.Nsteps, 2))
        self.challenges = []
        self.comm.add_to_execution(self.store_counts, priority=1000)

        self.comm.add_to_execution(
            self.run, priority=26, time_of_the_day=self.comm.free_hours[0]
        )

        self.one_challenge = trial_param["one_challenge"]
        if self.one_challenge:
            self.one_challenge_start_day = trial_param["one_challenge_start_day"]
            self.one_challenge_only_vacc = trial_param[
                "one_challenge_only_vaccine_group"
            ]
            self.comm.add_to_execution(self.immunize_challenged, priority=1000)

    def run(self):
        self.vaccination()
        self.placebo_vaccination()
        self.diagnostics()
        self.update_cummulatives()
        self.store_new_diag()

    def store_counts(self):
        for i, mask in enumerate([self.mask_placebo_group, self.mask_vaccine_group]):
            for pid in self.pop.pid[mask]:
                if self.pop.states[pid] == S.STATE_S:
                    contacts = self.pop.get_tracing(self.clk.step, pid)
                    chall_count = np.count_nonzero(
                        self.pop.states[contacts] == S.STATE_I
                    )
                    self.count_challenged[self.clk.step, i] += chall_count
                    if chall_count > 0:
                        self.mask_challenged[pid] = True
                        self.count_challenges[self.clk.step, pid] = chall_count
                        mask_sources = np.isin(self.pop.pid, contacts) & (
                            self.pop.states == S.STATE_I
                        )
                        for source_pid in self.pop.pid[mask_sources]:
                            self.challenges.append(
                                {
                                    "pid": pid,
                                    "source_pid": source_pid,
                                    "placement": self.pop.placement[pid],
                                    "source_inf_prob": self.pop.disease.inf_probability[
                                        self.clk.step, source_pid
                                    ],
                                    "inf_susceptibility": self.pop.disease.inf_susceptibility[
                                        pid
                                    ],
                                    "day": self.clk.days,
                                    "time": self.clk.time,
                                }
                            )
            self.count_susceptible[self.clk.step, i] = np.count_nonzero(
                self.pop.states[mask] == S.STATE_S
            )
            self.count_challenged_cumm[self.clk.step, i] = np.count_nonzero(
                self.mask_challenged[mask]
            )
            self.count_infected[self.clk.step, i] = np.count_nonzero(
                np.isin(
                    self.pop.states[mask], [S.STATE_E, S.STATE_I, S.STATE_R, S.STATE_D]
                )
            )

    def immunize_challenged(self):
        iter_list = [self.mask_vaccine_group]
        if not self.one_challenge_only_vacc:
            iter_list += [self.mask_placebo_group]
        for i, mask in enumerate(iter_list):
            for pid in self.pop.pid[mask]:
                if self.pop.states[pid] == S.STATE_S:
                    contacts = self.pop.get_tracing(self.clk.step, pid)
                    if (
                        S.STATE_I in self.pop.states[contacts]
                        and self.clk.days_since(self.pop.time_vaccinated[pid])
                        >= self.one_challenge_start_day
                    ):
                        self.pop.disease.inf_susceptibility[pid] = 0

    def log_results(self):
        # Collects vaccination date for all particles in trial
        for pid in self.particles.keys():
            if self.mask_in_trial[pid]:
                self.particles[pid]["day_vaccinated"] = self.clk.time_to_days(
                    self.pop.time_vaccinated
                )

        diagnostics_results_df = pd.DataFrame(self.diagnostics_results)
        particles_df = pd.DataFrame([particle for particle in self.particles.values()])
        challenges_df = pd.DataFrame(self.challenges)
        # Consolidate output data and add it to to store list
        self.store_data = {
            "participants": self.pop.pid[self.mask_in_trial],
            "vaccine_group": self.pop.pid[self.mask_vaccine_group],
            "placebo_group": self.pop.pid[self.mask_placebo_group],
            "severe_cumulative": self.severe_cumulative,
            "deceased_cumulative": self.deceased_cumulative,
            "infections_cumulative": self.infections_cumulative,
            "diagnosed_cumulative": self.diagnosed_cumulative,
            "symptoms_cumulative": self.symptoms_cumulative,
            "diagnostic_states": self.diagnostic_states,
            "count_susceptible": self.count_susceptible,
            "count_challenged": self.count_challenged,
            "count_challenged_cumm": self.count_challenged_cumm,
            "count_challenges": self.count_challenges,
            "count_infected": self.count_infected,
            "particles_csv": particles_df.to_csv(),
            "diagnostics_csv": diagnostics_results_df.to_csv(),
            "challenges_csv": challenges_df.to_csv(),
        }
        self.comm.add_to_store(self.name, "comm.vaccination_trial.store_data")

    def __repr__(self):
        return "<{} vaccination trial>".format(self.name)

    def update_cummulatives(self):
        self.severe_cumulative[self.clk.step] = np.sum(
            np.isin(self.pop.symptoms, [S.SYMPT_SEVERE, S.SYMPT_SEVERE * 10])
            & self.mask_in_trial
        )
        self.deceased_cumulative[self.clk.step] = np.sum(
            (self.pop.states == S.STATE_D) & self.mask_in_trial
        )
        self.infections_cumulative[self.clk.step] = np.sum(
            np.isin(self.pop.states, [S.STATE_E, S.STATE_I, S.STATE_R, S.STATE_D])
            & self.mask_in_trial
        )
        diagnosed = (self.pop.diag_states == S.DIAG_YES) & ~self.already_diagnosed
        self.already_diagnosed |= diagnosed
        self.diagnosed_cumulative[self.clk.step] += np.sum(diagnosed)
        self.symptoms_cumulative[self.clk.step] = np.sum(
            (self.pop.symptoms > 0) & self.mask_in_trial
        )
        self.diagnostic_states[self.clk.today] = self.pop.diag_states[
            self.mask_in_trial
        ]

    def store_new_diag(self):
        mask_new_diag = (
            self.last_time_diagnosed != self.pop.time_diagnosed[self.mask_in_trial]
        )
        for pid in self.pop.pid[self.mask_in_trial][mask_new_diag]:
            result = {
                "pid": pid,
                "positive": self.pop.diag_states[pid] == S.DIAG_YES,
                "false": self.pop.diag_false[pid],
                "day": self.clk.time_to_days(self.pop.time_diagnosed[pid]),
                "time": self.pop.time_diagnosed[pid],
            }
            self.diagnostics_results.append(result)
            self.particles[pid]["diagnostics"].append(
                self.pop.diag_states[pid] == S.DIAG_YES
            )
        self.last_time_diagnosed = self.pop.time_diagnosed[self.mask_in_trial]
