from ...settings import COLORS, COLORS_LIST
import numpy as np


def plot_extra_symptoms(self, seeds=None, **plot_args):
    """Plots the percentage of naive with each symptom in time.

    !!! Important
        This plot uses the do_plot method, see [plot_args](#plot_args) to see all available arguments.

    Args:
        seeds (list, optional): List of seeds to plot, if None will plot all seeds available.
        **plot_args (kwargs): Arguments for the do_plot method (see [plot_args](#plot_args)).
    """
    # Prepare Data
    data = self.get_data(["symptoms_counts", "symptoms_names"], seeds)
    seeds = data["seeds"]
    names = data["symptoms_names"]
    Nparticles = self.parameters["Nparticles"]
    symptoms_counts = np.array(data["symptoms_counts"])
    data = {}
    for i, name in enumerate(names[0]):
        data[name] = 100 * symptoms_counts[:, i, :] / Nparticles
    Nsteps = self.parameters["Nsteps"]
    dt = self.parameters["dt"]
    t = dt * np.arange(Nsteps) / 24

    # Plot graphs
    def_plot_args = {
        "xlabel": "Days",
        "ylabel": "% with symptom",
    }
    plot_args = self.args_parse(plot_args, def_plot_args)
    self.do_plot(t, dt, data, COLORS_LIST, seeds, **plot_args)
