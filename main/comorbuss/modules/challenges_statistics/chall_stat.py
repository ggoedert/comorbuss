from ...aux_classes import empty_module, harray
from ... import settings as S
import numpy as np

DEFAULTS = {
    "by_age": False,
    "by_work": False,
}

DEFAULTS_TYPES = {
    "by_age": bool,
    "by_work": bool,
}

__name__ = "challenges_statistics"


def triage(parameters, comm_parameters, clk, event_log):
    from ...triage.triage_functions import type_get

    param = {}
    for p in DEFAULTS.keys():
        param[p] = type_get(
            parameters,
            p,
            DEFAULTS[p],
            None,
            None,
            event_log,
            dtype=DEFAULTS_TYPES[p],
        )

    return param


class module(empty_module):
    def __init__(self, param, comm):
        super(module, self).__init__(param, comm, name=__name__)

        self.count = np.zeros(self.pop.Nparticles, dtype=int)
        self.count_h = harray(self.count, buffer_limit=10)
        self.count_placement = np.zeros(
            (self.pop.Nparticles, len(self.comm.srv.all_services) + 4), dtype=int
        )
        self.count_placement_h = harray(self.count_placement, buffer_limit=10)

        self.pop.add_filter(self.count_challenges, "possibly_infected")

        self.by_age = param["by_age"]
        if self.by_age:
            self.count_by_age = np.zeros((self.pop.Nparticles, len(S.ALL_AGES)))
            self.count_by_age_h = harray(self.count_by_age, buffer_limit=10)
            self.mask_ages = np.array(
                [self.pop.ages == a for a in np.arange(len(S.ALL_AGES))]
            )
            self.mask_ages = np.swapaxes(self.mask_ages, 0, 1)
            self.pop.add_filter(self.count_challenges_by_age, "possibly_infected")

        self.by_work = param["by_work"]
        if self.by_work:
            self.count_by_work = np.zeros(
                (self.pop.Nparticles, len(np.unique(self.pop.workplace_id)))
            )
            self.count_by_worke_h = harray(self.count_by_work, buffer_limit=10)
            self.mask_works = np.array(
                [
                    self.pop.workplace_id == i
                    for i in np.arange(len(np.unique(self.pop.workplace_id)))
                ]
            )
            self.mask_works = np.swapaxes(self.mask_works, 0, 1)
            self.pop.add_filter(self.count_challenges_by_work, "possibly_infected")

        self.comm.add_to_execution(self.update_history, priority=101)

    def log_results(self):
        self.count_h.dump_buffer(clean_history=True)
        self.count_placement_h.dump_buffer(clean_history=True)
        if self.by_age:
            self.count_by_age_h.dump_buffer(clean_history=True)
        if self.by_work:
            self.count_by_worke_h.dump_buffer(clean_history=True)

    def count_challenges(self, possibly_infected, mask_particles):
        self.count[mask_particles] += np.sum(possibly_infected, axis=0)
        self.count_placement[
            mask_particles, self.pop.placement[mask_particles][0] + 4
        ] += np.sum(possibly_infected, axis=0)
        return possibly_infected

    def count_challenges_by_age(self, possibly_infected, mask_particles):
        self.count_by_age[mask_particles, :] += np.matmul(
            possibly_infected, self.mask_ages[mask_particles, :]
        )
        # for age_group, mask_age in enumerate(self.mask_ages):
        #     mask_sources = mask_age[mask_particles]
        #     self.count_by_age[mask_particles, age_group] += np.sum(
        #         possibly_infected[mask_sources], axis=0
        #     )
        return possibly_infected

    def count_challenges_by_work(self, possibly_infected, mask_particles):
        self.count_by_work[mask_particles, :] += np.matmul(
            possibly_infected, self.mask_works[mask_particles, :]
        )
        # for work_id, mask_work in enumerate(self.mask_works):
        #     mask_sources = mask_work[mask_particles]
        #     self.count_by_work[mask_particles, work_id] += np.sum(
        #         possibly_infected[mask_sources], axis=0
        #     )
        return possibly_infected

    def update_history(self):
        mask_newly_infected = self.pop.time_exposed == self.clk.time

        self.count_h[mask_newly_infected] = self.count[mask_newly_infected]
        self.count[mask_newly_infected] = 0

        self.count_placement_h[mask_newly_infected, :] = self.count_placement[
            mask_newly_infected, :
        ]
        self.count_placement[mask_newly_infected] = 0

        if self.by_age:
            self.count_by_age_h[mask_newly_infected, :] = self.count_by_age[
                mask_newly_infected, :
            ]
            self.count_by_age[mask_newly_infected] = 0
