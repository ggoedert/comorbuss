from . import settings as S
from .aux_classes import clock
from .decisions import DECISON_FUNCTIONS, to_start_lockdown_day_series, \
    to_finish_lockdown_day_series, to_close_services, to_reopen_services
import numpy as np
import networkx as nx
import time, sys
from tqdm import tqdm
from numpy.random import default_rng
import matplotlib.pyplot as plt
from s import triage_parameters, get_value
from .population import population
from .geography import geography
from .services import services_infrastructure
from .quarantines import init_qarantines
from .diagnostics import init_diagnostics
from .tools import filter_parse, recursive_copy
from .net_generators import home_net, enviroment_net

        self.random_walk = parameters["random_walk"]
        self.ext_bdry_config = 0
            self.geo = geography(parameters, self.srv.services_parameters, self)              # Geography of the city, initialized through the constructor of class geography

            self.geo.particles_per_home = homes[2]
            self.geo.homes_ids = np.arange(len(self.geo.particles_per_home))
            if parameters['social_isolation'] and not parameters['simulate_meanfield']:
                self.add_to_execution(self.isolation, priority=77, time_of_the_day=self.free_hours)  # Should this be in the 50-74 priority interval? It might break something if changed! Tests are needed.

            if not int(parameters["sd_mode"]) in [1, 2, 3]:
                self.event_log(
                    "{:} is an invalid social_distancing_mode, available modes are "
                    "1, 2 and 3, social distancing will be disabled.".format(parameters["sd_mode"]),
                    MSG_WRNG,
                )
    ### Assignment methods for the populations
    # Give homes to population particles
    def assign_homes(self, parameters):
        """ Assing a home to each individual in a population.
        Randomly assigns a home address from self.geo to each particle in the population

        Returns:
            np.array, np.array, list: array with the id of the home for each particle, array with the home address for each particle, count of the number of particles in each home
        """
        if "net_homes" in parameters:
            home_id = parameters["net_homes"]
        else:
            home_id = self.randgencom.choice(self.geo.homesNumber, self.Nparticles) # Randomly choose self.Nparticles entries from of the array array [0, 1, ..., self.geo.homesNumber)
        address = np.zeros([self.Nparticles, 3])                                # Create a matrix with dimensions self.Nparticles x 2
        particles_per_home = np.zeros(self.geo.homesNumber)                     # Initialize number of particles in each home to be zero for every home

        for i in range(self.Nparticles):                                        # Assign a 2D location for the home of each particle
            address[i, :] = self.geo.homes[home_id[i], :]

        home_id_temp = list(home_id)

        for i in range(self.geo.homesNumber):                                   # Count the occurence of each home
            particles_per_home[i] = home_id_temp.count(i)

        # particles_per_homes = {i:home_id.count(i) for i in home_id}

        return [home_id, address, particles_per_home]

    # Neighborhood assignment
    def assign_neighborhoods(self, homes_addr, neigh_perc):
        available_pids = np.arange(self.Nparticles)
        neighborhoods = np.zeros(self.Nparticles)
        for i in range(1, len(neigh_perc)):
            n = int(neigh_perc[i]*self.Nparticles)
            pid_center = self.randgencom.choice(available_pids, 1)
            center = homes_addr[pid_center]
            mytree = cKDTree(homes_addr[available_pids, :2])
            pids = available_pids[mytree.query(center[0, :2], n)[1]]
            neighborhoods[pids] = i
            maskDelete = ~np.isin(available_pids, pids)
            available_pids = available_pids[maskDelete]
        return neighborhoods

    def update_position(self):
        """ Updates the position of the particles according to a day cycle """
        if self.clk.between_hours(self.free_hours[0], self.free_hours[1]):
            pold = np.copy(self.pop.positions)
            mask_plc_home = self.pop.placement == PLC_HOME
            pold[mask_plc_home] = self.pop.home_address[mask_plc_home]
            self.pop.positions = self.walk(pold, self.ext_bdry_config)
            self.pop.positions[maskAtHomeNotInQuarantine, 2] = 0.
        else:
            pold = np.copy(self.pop.positions)
            p = np.copy(pold)
            mask_frozen = self.pop.movement == MVT_FRZN
            mask_unfrozen = ~mask_frozen
            p[mask_unfrozen, :] = self.pop.home_address[mask_unfrozen]     # If it is during the night, everyone stays at home
            p[mask_frozen, :] = self.pop.positions[mask_frozen, :]
            # get particles to services outside free hours
            dp = p - pold
            self.pop.positions, _, _ = self.BC_internal(p, pold, dp)          

    ### Methods for boundary conditions
    def BC_external_hard(self, p, pold, dp):
        """ Checks if any of the new position of every partilce has exited the grid and corrects it back inside

        Parameters:
                    p    : newly calculated positions, which may have gone out of the grid
                    pold : previous positions, which certanly where inside the grid
                    dp   : dp = p - pold, distance walked in the current time step

        This method checks if any of the new position of every partilce has lefted the grid that simulates the city (in the x and y axis)
        if the new position has left the grid in the x axis, it will move the x coordinate by -2dp_x ( p_x = pold_x - dp_x )
        if the new position has left the grid in the y axis, it will move the y coordinate by -2dp_y ( p_y = pold_y - dp_y )
        """
        outside_x = np.any([p[:, 0] <= self.geo.lim_x[0], p[:, 0] >= self.geo.lim_x[1]], axis=0)   # Checks if any of the new positions of every partilce has lefted the grid in the x axis
        p[outside_x, 0] = pold[outside_x, 0] - dp[outside_x, 0]                                    # If it has left the grid in the x axis, it will move the x coordinate back by -2dp_x
        outside_y = np.any([p[:, 1] <= self.geo.lim_y[0], p[:, 1] >= self.geo.lim_y[1]], axis=0)   # Checks if any of the new positions of every partilce has lefted the grid in the y axis
        p[outside_y, 1] = pold[outside_y, 1] - dp[outside_y, 1]                                    # If it has left the grid in the x axis, it will move the y coordinate back by -2dp_y
        dp = p - pold
        return p, dp

    def BC_external_periodic(self, p, pold, dp):
        """ Checks if any of the new position of every particle has exited the grid and applies periodic boundary conditions

        Parameters:
                    p    : newly calculated positions, which may have gone out of the grid
                    pold : previous positions, which certanly where inside the grid
                    dp   : dp = p - pold, distance walked in the current time step
        """
        outside_x = np.any([p[:, 0] <= self.geo.lim_x[0], p[:, 0] >= self.geo.lim_x[1]], axis=0)   # Checks if any of the new positions of every partilce has lefted the grid in the x axis
        p[outside_x, 0] = np.mod(pold[outside_x, 0] + dp[outside_x, 0], 1)                         # If the particle leaves by dx in the x direction, place it back by entering dx in the opposite direction
        outside_y = np.any([p[:, 1] <= self.geo.lim_y[0], p[:, 1] >= self.geo.lim_y[1]], axis=0)   # Checks if any of the new positions of every partilce has lefted the grid in the y axis
        p[outside_y, 1] = np.mod(pold[outside_y, 1] + dp[outside_y, 1], 1)                         # If the particle leaves by dy in the y direction, place it back by entering dy in the opposite direction
        dp = p - pold
        return p, dp

    def BC_services(self, p, service, mask_get_out):
        """ Checking wether particles inside service should be where they are """
        mask_should_be_in_service = self.pop.placement == service.placement
        mask_in_services = np.full(p.shape[0], False)
        mask_get_in = np.full(p.shape[0], False)
        for j, mlimit in enumerate(service.limits):
            mask_in_this_service_x = ((p[:, 0] >= mlimit[0]) & (p[:, 0] <= mlimit[1]))
            mask_in_this_service_y = ((p[:, 1] >= mlimit[2]) & (p[:, 1] <= mlimit[3]))
            mask_in_this_service = mask_in_this_service_x & mask_in_this_service_y
            ### Persons that should get out of service
            if service.controlledAccess:
                mask_get_out_service = mask_in_this_service & ~mask_should_be_in_service
                mask_get_out = mask_get_out | mask_get_out_service
            ### Persons that should get inside service
            mask_in_services = mask_in_services | mask_in_this_service
            mask_get_in = mask_get_in | (mask_should_be_in_service & mask_in_this_service & \
                          (service.instance != j))
        ### Getting persons inside service
        mask_get_in = mask_get_in | (~mask_in_services & mask_should_be_in_service)
        return mask_get_out, mask_get_in

    def BC_internal(self, p, pold, dp, mask_get_out=[]):
        """ Checking wether particles inside any centers """
        if mask_get_out == []:
            mask_get_out = self.MASK_FALSE
        for srvc in self.srv.all_services:
            if srvc.number > 0:
                mask_get_out, mask_get_in = self.BC_services(p, srvc, mask_get_out)
                self.pop.srvc_grid_pos[mask_get_in] = srvc.get_grid_positon(mask_get_in)
                p[mask_get_in] = srvc.get_service_address(mask_get_in, self.pop.srvc_grid_pos[mask_get_in])
                mask_get_out = mask_get_out & ~mask_get_in
                dp = p - pold
        return p, dp, mask_get_out

    def is_incorrectly_positioned(self, p):
        outside_x = np.any([p[:, 0] <= self.geo.lim_x[0], p[:, 0] >= self.geo.lim_x[1]], axis=0)
        outside_y = np.any([p[:, 1] <= self.geo.lim_y[0], p[:, 1] >= self.geo.lim_y[1]], axis=0)
        mask_incorrectly_positioned = outside_x | outside_y
        for srvc in self.srv.all_services:
            if not srvc.controlledAccess:
                continue
            mask_in_services = np.full(p.shape[0], False)
            mask_should_be_in_service = self.pop.placement == srvc.placement
            for j, mlimit in enumerate(srvc.limits):
                mask_in_this_service_x = ((p[:, 0] >= mlimit[0]) & (p[:, 0] <= mlimit[1]))
                mask_in_this_service_y = ((p[:, 1] >= mlimit[2]) & (p[:, 1] <= mlimit[3]))
                mask_in_this_service = mask_in_this_service_x & mask_in_this_service_y
                mask_incorrectly_positioned = mask_incorrectly_positioned | \
                    (mask_in_this_service & ~mask_should_be_in_service) | \
                        (mask_should_be_in_service & mask_in_this_service & \
                          (srvc.instance != j))
                mask_in_services = mask_in_services | mask_in_this_service
            mask_incorrectly_positioned = mask_incorrectly_positioned | (~mask_in_services & mask_should_be_in_service)
            mask_alive = self.pop.states != S.STATE_D
            mask_incorrectly_positioned = mask_incorrectly_positioned & mask_alive
        return mask_incorrectly_positioned

    def circle_rectangle_intersection_y_side(self, p, r2, mlimit, index, i_pts, increasing=True):
        """ Finds intersection of a circle with a rectangle considering a
        vertical side """
        # Getting masks for when there is an intersection at the y side, and storing useful values
        arg = r2-(mlimit[index]-p[1])**2
        arg_not_neg = arg >= 0
        ct = -(not increasing)+(increasing)
        if arg_not_neg:
            sqrtarg = np.sqrt(arg)
            # Getting masks for when the left intersection point provide x coords in the allowed range, and then finding the intersection points when they exist
            x = p[0]-ct*sqrtarg
            in_x_limits = (mlimit[0] <= x) & (x <= mlimit[1])
            if in_x_limits:
                i_pts.append([x, mlimit[index]])
            # Getting masks for when the right intersection point provide x coords in the allowed range, and then finding the intersection points when they exist
            x = p[0]+ct*sqrtarg
            in_x_limits = (mlimit[0] <= x) & (x <= mlimit[1])
            if in_x_limits:
                i_pts.append([x, mlimit[index]])
        return i_pts

    def circle_rectangle_intersection_x_side(self, p, r2, mlimit, index, i_pts, increasing=True):
        """ Finds intersection of a circle with a rectangle considering a
        horizontal side """
        # Getting masks for when there is an intersection at the x side, and storing useful values
        arg = r2-(mlimit[index]-p[0])**2
        arg_not_neg = arg >= 0
        ct = -(not increasing)+(increasing)
        if arg_not_neg:
            sqrtarg = np.sqrt(arg)
            # Getting masks for when the bottom intersection point provide y coords in the allowed range, and then finding the intersection points when they exist
            y = p[1]-ct*sqrtarg
            in_y_limits = (mlimit[2] <= y) & (y <= mlimit[3])
            if in_y_limits:
                i_pts.append([mlimit[index], y])
            # Getting masks for when the top intersection point provide y coords in the allowed range, and then finding the intersection points when they exist
            y = p[1]+ct*sqrtarg
            in_y_limits = (mlimit[2] <= y) & (y <= mlimit[3])
            if in_y_limits:
                i_pts.append([mlimit[index], y])
        return i_pts

    def circle_polygon_intersection_services(self, p, services, intersec_list, contains_list):
        """ Finds intersection of a circle with a rectangle composed by the
        service limits """
        # storing useful variables
        r = self.random_walk[0]*self.dt
        r2 = r**2
        # looping over every service in the services list
        for mlimit in services:
            i_pts = []
            # checking if the circle intersects the side where y=mlimit[2]
            i_pts = self.circle_rectangle_intersection_y_side(p, r2, mlimit, \
                            2, i_pts, True)
            # checking if the circle intersects the side where x=mlimit[1]
            i_pts = self.circle_rectangle_intersection_x_side(p, r2, mlimit, \
                            1, i_pts, True)
            # checking if the circle intersects the side where y=mlimit[3]
            i_pts = self.circle_rectangle_intersection_y_side(p, r2, mlimit, \
                            3, i_pts, False)
            # checking if the circle intersects the side where x=mlimit[0]
            i_pts = self.circle_rectangle_intersection_x_side(p, r2, mlimit, \
                            0, i_pts, False)
            # checking if the point is contained in the circle
            contains = \
                ((p[0] >= mlimit[0]) & (p[0] <= mlimit[1])) \
                    & ((p[1] >= mlimit[2]) & (p[1] <= mlimit[3]))
            # appending intersections when necessary
            if len(i_pts) > 0:
                intersec_list.append(i_pts)
                contains_list.append(contains)
        return intersec_list, contains_list

    def get_allowed_ranges(self, p1, p2, a1, a2, contains, allowed_ranges):
        """ Given intersection points and angles related to this intersection,
        return the allowed angle range for a save move """
        delta = 1.0e-8
        if a2 < a1:
            ang = a1
            a1 = a2
            a2 = ang
        close_coords = (np.abs(p2[0]-p1[0]) < delta) | (np.abs(p2[1]-p1[1]) < delta)
        set1_size = a2-a1
        set2_size = a1+2.*np.pi-a2
        if contains & close_coords:
            if set1_size < set2_size:
                allowed_ranges.append([a1, a2])
            else:
                allowed_ranges.append([a2, 2*np.pi+a1])
        else:
            if set1_size > set2_size:
                allowed_ranges.append([a1, a2])
            else:
                allowed_ranges.append([a2, 2*np.pi+a1])
        return allowed_ranges

    def get_allowed_ranges_list(self, pt, intersec_list, contains_list):
        """ Given a list of intersection points for each service, return a
        list of angle ranges over which an AND relation operator can be applied
        to find an angle allowed for all ranges """
        allowed_ranges_list = []
        for i in range(len(intersec_list)):
            allowed_ranges = []
            length = len(intersec_list[i])
            len2 = length == 2
            len3 = length == 3
            len4 = length == 4
            len6 = length == 6
            len8 = length == 8
            if len2 | len3 | len4 | len6 | len8:
                p1 = intersec_list[i][0]-pt[:2]
                a1 = np.arctan2(p1[1], p1[0])
                p2 = intersec_list[i][1]-pt[:2]
                a2 = np.arctan2(p2[1], p2[0])
                allowed_ranges = self.get_allowed_ranges(p1, p2, a1, a2, contains_list[i], allowed_ranges)
                if len3:
                    allowed_ranges_list.append(([allowed_ranges[-1]]))
                    allowed_ranges.pop()
                    p1 = intersec_list[i][1]-pt[:2]
                    a1 = np.arctan2(p1[1], p1[0])
                    p2 = intersec_list[i][2]-pt[:2]
                    a2 = np.arctan2(p2[1], p2[0])
                    allowed_ranges = self.get_allowed_ranges(p1, p2, a1, a2, contains_list[i], allowed_ranges)
                if len4 | len6 | len8:
                    p1 = intersec_list[i][2]-pt[:2]
                    a1 = np.arctan2(p1[1], p1[0])
                    p2 = intersec_list[i][3]-pt[:2]
                    a2 = np.arctan2(p2[1], p2[0])
                    allowed_ranges = self.get_allowed_ranges(p1, p2, a1, a2, contains_list[i], allowed_ranges)
                    ranges_do_not_intersect = (allowed_ranges[-2][1] < allowed_ranges[-1][0]) | \
                        (allowed_ranges[-1][1] < allowed_ranges[-2][0])
                    if (not ranges_do_not_intersect):
                        allowed_ranges_list.append(([allowed_ranges[-1]]))
                        allowed_ranges.pop()
                if len6 | len8:
                    p1 = intersec_list[i][4]-pt[:2]
                    a1 = np.arctan2(p1[1], p1[0])
                    p2 = intersec_list[i][5]-pt[:2]
                    a2 = np.arctan2(p2[1], p2[0])
                    allowed_ranges = self.get_allowed_ranges(p1, p2, a1, a2, contains_list[i], allowed_ranges)
                    ranges_do_not_intersect = (allowed_ranges[-2][1] < allowed_ranges[-1][0]) | \
                        (allowed_ranges[-1][1] < allowed_ranges[-2][0])
                    if (not ranges_do_not_intersect):
                        allowed_ranges_list.append(([allowed_ranges[-1]]))
                        allowed_ranges.pop()
                if len8:
                    p1 = intersec_list[i][6]-pt[:2]
                    a1 = np.arctan2(p1[1], p1[0])
                    p2 = intersec_list[i][7]-pt[:2]
                    a2 = np.arctan2(p2[1], p2[0])
                    allowed_ranges = self.get_allowed_ranges(p1, p2, a1, a2, contains_list[i], allowed_ranges)
                    ranges_do_not_intersect = (allowed_ranges[-2][1] < allowed_ranges[-1][0]) | \
                        (allowed_ranges[-1][1] < allowed_ranges[-2][0])
                    if (not ranges_do_not_intersect):
                        allowed_ranges_list.append(([allowed_ranges[-1]]))
                        allowed_ranges.pop()
            elif self.show_warn_msgs:
                self.event_log("Untreated number of intersections of a circle with a rectangle: {:}".format(len(intersec_list[i])), MSG_WRNG)
            allowed_ranges_list.append((allowed_ranges))
        return allowed_ranges_list

    def get_best_allowed_angle(self, allowed_ranges_list):
        """ Given a set of allowed angle ranges, get the ranges included
        in all of them, and then return the middle point of the largest of
        those last mentioned ranges """
        import portion as port
        # first make sure all ranges are between -pi and pi
        allowed_ranges_list_cp = allowed_ranges_list.copy()
        for i, allowed_ranges in enumerate(allowed_ranges_list):
            for j, allowed_range in enumerate(allowed_ranges):
                pi_is_between = ((allowed_range[0] < np.pi) & (np.pi < allowed_range[1]))
                if pi_is_between:
                    allowed_ranges_list_cp[i][j] = [-np.pi, allowed_range[1]-2*np.pi]
                    allowed_ranges_list_cp[i].insert(j, [allowed_range[0], np.pi])
        allowed_ranges_list = allowed_ranges_list_cp.copy()
        # now find the smallest allowed interval
        allowed_intervals = port.openclosed(-np.pi, np.pi)
        for allowed_ranges in (allowed_ranges_list):
            allowed_intervals_local = port.empty()
            for allowed_range in (allowed_ranges):
                allowed_intervals_local = allowed_intervals_local | \
                    port.closed(allowed_range[0], allowed_range[1])
            allowed_intervals = allowed_intervals & allowed_intervals_local
        # convert the intervals do data
        min_ranges = port.to_data(allowed_intervals)
        # get the maximum range from the allowed ranges
        max_range = [0, 0]
        for min_range in (min_ranges):
            replace_max_range = (max_range[1]-max_range[0]) < (min_range[2]-min_range[1])
            if replace_max_range:
                max_range = [min_range[1], min_range[2]]
        return (max_range[0] + (max_range[1]-max_range[0])*.5)

    def enter_corridor(self, p, pold, mask_get_out):
        """ Given a position inside a disallowed service, the function alters
        it so that the new position does not violate the service boundary
        conditions, and still makes it sure the particle will walk """
        ext_bdry_as_service = [[self.geo.lim_x[0], self.geo.lim_x[1], self.geo.lim_y[0], self.geo.lim_y[1]]]
        for ind, v in enumerate(mask_get_out):
            if v:
                pt = pold[ind]
                intersec_list = []
                contains_list = []
                contains_list_tmp = []
                # getting intersections with the boundary
                # modifying the contains list array so that the code below
                # assures particles remain inside external boundary
                [intersec_list, contains_list_tmp] = self.circle_polygon_intersection_services(\
                            pt, ext_bdry_as_service, intersec_list, contains_list_tmp)
                if (len(intersec_list) > 0):
                    contains_list.append(False)
                # getting intersections with the several service boundaries
                for srvc in self.srv.all_services:
                    [intersec_list, contains_list] = self.circle_polygon_intersection_services(\
                                pt, srvc.limits, intersec_list, contains_list)
                # getting the allowed ranges list of angles according to the intersection points found
                allowed_ranges_list = self.get_allowed_ranges_list(pt, intersec_list, contains_list)
                # getting middle point of largest allowed interval
                angle = self.get_best_allowed_angle(allowed_ranges_list)
                # modifying the position so that it safely walks
                p[ind, :2] = pt[:2] + np.array([np.cos(angle), np.sin(angle)])*self.random_walk*self.dt
        return p

    def BC_count_in_domain(self, step):
        """ Checking wether particles are inside the grid (city) """
        mask_in_x = (self.pop.positions[:, 0] > self.geo.lim_x[0]) & (self.pop.positions[:, 0] < self.geo.lim_x[1])
        mask_in_y = (self.pop.positions[:, 1] > self.geo.lim_y[0]) & (self.pop.positions[:, 1] < self.geo.lim_y[1])
        mask_inside = mask_in_x & mask_in_y
        count_inside = np.sum(mask_inside)
        particles_outside = np.arange(self.Nparticles)[~mask_inside]
        return count_inside, particles_outside

    def walk_dist(self, pold, ext_bdry_config):
        """ Updates the position of the particles using a Euler-Maruyama integrator """
        mask_unfrozen = self.pop.movement != MVT_FRZN
        p = np.copy(pold)
        v_random = self.vrand_vicsek()
        v_determ = np.zeros([self.Nparticles, 3])
        dp = self.dt*v_determ + self.dt_sqrt*v_random
        p[mask_unfrozen, :] = pold[mask_unfrozen, :] + dp[mask_unfrozen, :]    # Calculate new positions
        if ext_bdry_config == -1:
            [p, dp] = self.BC_external_hard(p, pold, dp)                       # Verify that the newly calculated posittion are inside the grid and move the particles that are outside back inside it
        else:
            [p, dp] = self.BC_external_periodic(p, pold, dp)                   # Checks if any of the new position of every particle has exited the grid and applies periodic boundary conditions
        mask_get_out = np.full(p.shape[0], False)
        [p, dp, mask_get_out] = self.BC_internal(p, pold, dp, mask_get_out)
        if np.isin(True, mask_get_out):
            p = self.enter_corridor(p, pold, mask_get_out)
        if self.show_warn_msgs and self.simulating_services:
            incorrectly_positioned = self.is_incorrectly_positioned(p)
            if np.isin(True, incorrectly_positioned):
                self.event_log("{:} {:} {:}".format(self.clk.step, self.pop.pid[incorrectly_positioned], p[incorrectly_positioned]), MSG_WRNG)
                self.event_log("Positions violating boundary conditions (please report to developers)", MSG_WRNG)
        return p

    def vrand_vicsek(self):
        dp = np.zeros([self.Nparticles, 3])                                    # Initializes variable to store the distance all the particles will walk
        xi = self.randgencom.uniform(-pi, pi, size=self.Nparticles)            # Calculate the angle between the x and y axis of the direction the particles will walk
        dp[:, 0] = self.random_walk*np.cos(xi)                                 # Calculate the distance walked by every particle in the x axis
        dp[:, 1] = self.random_walk*np.sin(xi)                                 # Calculate the distance walked by every particle in the y axis
        return dp
