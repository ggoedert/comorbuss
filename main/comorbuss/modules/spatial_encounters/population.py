import collections
from operator import ne
import random
import numpy as np
import networkx as nx
from numpy.random import default_rng
from .disease import disease
from .aux_classes import circularlist
from . import settings as S
from .net_generators import SERVICE_NET_GENS, home_net, enviroment_net
from .tools import normalize
from numba import njit
from scipy.spatial.distance import pdist as scipy_pairdistance
from scipy.spatial.distance import squareform

        self.home_address = np.copy(homes[1])
        self.positions = np.copy(self.home_address)

        # Set initial positions from input to constructor
        self.srvc_grid_pos = -1*np.ones([self.Nparticles, 2], dtype=int)

        self.sd_fraction = parameters['sd_fraction']
        self.sd_choice = self.gen_adhesion_mask(self.sd_fraction)
        self.sd_min_distance = parameters['sd_distance']
        
            self.positions_ts = np.zeros((Nsteps, self.Nparticles, 3))
            self.positions_ts[0, :, :] = self.positions
            self.positions_ts = self.positions
        #community
        self.pop.positions_ts[self.clk.step, :, :] = self.pop.positions


        # Selects a social distancing mode
        if parameters['social_distancing'] and not parameters['simulate_meanfield']:
            if int(parameters['sd_mode']) == 1:
                self.apply_social_distancing = self.apply_social_distancing_1
            elif int(parameters['sd_mode']) == 2:
                self.apply_social_distancing = self.apply_social_distancing_1
            elif int(parameters['sd_mode']) == 3:
                self.apply_social_distancing = self.apply_social_distancing_1
            else:
                self.apply_social_distancing = lambda: None
        else:
            self.apply_social_distancing = lambda: None

        if parameters['use_networks']:
            self.gen_encounters_and_infections = self.gen_encounters_and_infections_net
        else:
            self.gen_encounters_and_infections = self.gen_encounters_and_infections_dist

    def gen_mask_encounters(self, positions, radii, nparticles, social_distancing=lambda: None):
        # Computes the distances between particles on this mask
        distances = self.compute_distances_decomposed(positions)
        # Apply social distancing function
        social_distancing()
        # Find a mask of the particles that are close enough to another particle
        mask_encounters = (distances <= radii)
        # Set the diagonal entries of the previous mask to False (a particle can not infect itself)
        mask_encounters[np.diag_indices(nparticles)] = False
        return mask_encounters


    def mark_for_new_position(self, to_change, placement, activity, movement):
        """Mark particles for changes in position

        Args:
            to_change (np.ndarray): Mask or list of pids for particles to be changed
            placement (int): New placement
            activity (int): New activity
            movement (int): New movement
        """
        self.placement[to_change] = placement
        self.activity[to_change] = activity
        self.movement[to_change] = movement
    
    def compute_distances_decomposed(self, positions):
        return squareform(scipy_pairdistance(positions, metric='euclidean'))

    def compute_distances(self):
        """ Calculate a matrix whose entry (i,j) is the distance between i-th and j-th
        particle at the current time (symmetric matrix)"""
        self.distances = squareform(scipy_pairdistance(self.positions,
                                                       metric='euclidean'))

    def apply_social_distancing_1(self):
        """ Applies social distancing by making individuals distance exactly the minimum
        distance between each other"""
        mask_needs_modification = (self.distances < self.sd_min_distance) & self.sd_choice
        mask_needs_modification[np.diag_indices(self.Nparticles)] = False
        self.distances[mask_needs_modification] = self.sd_min_distance

    def apply_social_distancing_2(self):
        """ Applies social distancing by making individuals distance some value between
        the minimum and the actual distance between each other"""
        mask_needs_modification = (self.distances < self.sd_min_distance) & self.sd_choice
        mask_needs_modification[np.diag_indices(self.Nparticles)] = False
        self.distances[mask_needs_modification] = \
            self.rng.uniform(self.distances[mask_needs_modification],
                                    self.sd_min_distance)

    def apply_social_distancing_3(self):
        """ Symmetrically applies social distancing by making individuals distance some
        value between the minimum and the actual distance between each other"""
        mask_needs_modification = (self.distances < self.sd_min_distance) & self.sd_choice
        mask_needs_modification[np.diag_indices(self.Nparticles)] = False
        self.distances[mask_needs_modification] = \
            self.rng.uniform(self.distances[mask_needs_modification],
                                    self.sd_min_distance)
        self.distances = (self.distances + (self.distances.transpose()))/2.0


    def gen_encounters_and_infections_dist(self, attributes):
        """Generates encounters masks for each context (services, homes, etc) using distance mechanics
        """
        self.mask_new_infected[:] = False
        counts_accumulator = np.zeros(4)
        # Generates mask for particles at the environment
        mask_alive = self.states != S.STATE_D
        mask_particles = ((self.placement == PLC_ENV)) & mask_alive
        nparticles = np.count_nonzero(mask_particles)
        if nparticles > 1:
            mask_encounters = self.gen_mask_encounters(self.positions[mask_particles],
                                                    self.disease.inf_radii, nparticles,
                                                    self.apply_social_distancing)
            counts_accumulator += self.process_encounters(nparticles, mask_particles, mask_encounters, PLC_ENV, self.disease.env_inf_prob_weight, attributes, self.disease.mask_new_infections)

        # Generates masks for particles at home
        for home in self.homes.keys():
            nparticles = self.homes[home]['nparticles']
            if nparticles > 1:
                mask_particles = (self.placement == PLC_HOME) & mask_alive & self.homes[home]['mask']
                nparticles = np.count_nonzero(mask_particles)
                if nparticles > 1:
                    nparticles = np.count_nonzero(mask_particles)
                    mask_encounters = self.gen_mask_encounters(self.positions[mask_particles],
                                                                self.disease.inf_radii, nparticles,
                                                                self.apply_social_distancing)
                    counts_accumulator += self.process_encounters(nparticles, mask_particles, mask_encounters, PLC_HOME, self.disease.home_inf_prob_weight, attributes, self.disease.mask_new_infections)


        # Generates masks for particles in services
        for srvc in self.comm.srv.all_services:
            if np.any(self.placement == srvc.placement):
                for i in range(srvc.number):
                    for j in srvc.rooms:
                        mask_particles = ((self.placement == srvc.placement) &
                                                (srvc.instance == i) &
                                                (srvc.room == j) &
                                                mask_alive)
                        nparticles = np.count_nonzero(mask_particles)
                        if nparticles > 1:
                            mask_encounters = self.gen_mask_encounters(self.srvc_grid_pos[mask_particles],
                                                                self.disease.grid_inf_radii, nparticles)
                            counts_accumulator += self.process_encounters(nparticles, mask_particles, mask_encounters, srvc.id, srvc.inf_prob_weight, attributes, self.disease.mask_new_infections)

        return counts_accumulator
