
    def address_from_limits(self, limits):
        """Calculates the center point for a address from its limits.

        Args:
            limits (list): List with the limits for the service [x_i, x_f, y_i, y_f]

        Returns:
            np.array: Center point address for the service [x, y, z]
        """
        address = np.zeros((limits.shape[0], 3))
        address[:, 0] = np.abs(limits[:, 0] + limits[:, 1])/2.
        address[:, 1] = np.abs(limits[:, 2] + limits[:, 3])/2.
        return address

    def init_services(self, comm):
        """Initializes services for a given community

        Args:
            comm (comunity): Community for services initialization
        """
        self.event_log = comm.event_log
        # Empty lists to store initialized services
        self.service = {}
        self.all_services = []
        self.services_visitable = []
        for srvc in self.services_parameters:
            sid = srvc['id']
            name = srvc["name"]
            srvc['centers_limits'] = comm.geo.services_limits[sid]
            srvc['centers_address'] = self.address_from_limits(srvc['centers_limits'])
            self.assign_srvc_neighborhood(srvc)
            self.assign_work(srvc, comm)
            self.assign_visitors(srvc, comm)
            self.assign_workers_family(srvc, comm)
            self.assign_service_address(srvc, comm)
            self.service[name] = service(srvc, comm)
            self.all_services.append(self.service[name])
            self.services_visitable.append(self.service[name])


    # Generates a list with service address for all particles
    def assign_service_address(self, service_param, comm):
        service_param['service_address'] = np.copy(comm.pop.home_address)
        service_param['grid_address'] = -1*np.ones((comm.Nparticles, 2))
        grids = comm.geo.gen_service_grids(service_param)
        service_param['grids'] = grids
        for i in range(service_param['number']):
            mask_chosen = service_param['visitors'] == i
            grid_x = comm.pop.rng.choice(grids[i].shape[0], np.sum(mask_chosen))
            grid_y = comm.pop.rng.choice(grids[i].shape[1], np.sum(mask_chosen))
            service_param['service_address'][mask_chosen, :2] = grids[i][grid_x, grid_y]
            service_param['grid_address'][mask_chosen, :] = np.column_stack((grid_x, grid_y))

        # Overwrites service_address for workers with work address
        grids = comm.geo.gen_service_grids(service_param)
        for i in range(service_param['number']):
            workers = service_param['workers'][service_param['workers_instance'] == i]
            mask_workers = np.isin(comm.pop.pid, workers)
            grid_x = comm.pop.rng.choice(grids[i].shape[0], len(workers))
            grid_y = comm.pop.rng.choice(grids[i].shape[1], len(workers))
            service_param['service_address'][mask_workers, :2] = grids[i][grid_x, grid_y, :]
            service_param['grid_address'][mask_workers, :] = np.column_stack((grid_x, grid_y))
        for i in range(len(service_param['workers'])):
            service_param['service_address'][service_param['workers'][i], 2] = service_param['workers_room'][i]

class service:
        self.rooms = np.array(np.unique(service_parameters['service_address'][:, -1]), dtype=int)

        # Distance mechanics attributes
        self.previous_movement = MVT_FREE*np.ones(comm.Nparticles, dtype=int)

        self.centersAddress = service_parameters['centers_address']
        self.limits = service_parameters['centers_limits']
        self.service_address = service_parameters['service_address']
        self.grids = service_parameters['grids']
        self.grid_address = service_parameters['grid_address']
        self.visitors_fp = service_parameters['visitors_fixed_point']
        self.workers_fp = service_parameters['workers_fixed_point']
        if service_parameters.get("move_inside", False):
            self.mvt_inside = MVT_FREE
        else:
            self.mvt_inside = MVT_FRZN
        self.controlledAccess = service_parameters.get("controlled_access", True)


    def return_particles(self, mask_return):
        activity = self.previous_activity[mask_return]
        placement = self.previous_placement[mask_return]
        placement[(activity != S.ACT_QRNT)] = PLC_HOME # particles not in quarantine send home
        movement = self.previous_movement[mask_return]
        self.pop.mark_for_new_position(mask_return, placement, activity, movement)

    def get_particles(self, mask_get, activity, movement):
        self.previous_placement[mask_get] = self.pop.placement[mask_get]
        self.previous_activity[mask_get] = self.pop.activity[mask_get]
        self.previous_movement[mask_get] = self.pop.movement[mask_get]
        self.pop.mark_for_new_position(mask_get, self.placement, activity, movement)


    def get_grid_positon(self, mask):
        service_ids = self.instance[mask]
        fixed_gposition = self.grid_address[mask]
        gposition = np.copy(fixed_gposition)
        if not self.visitors_fp:
            for i in range(self.number):
                mask_this_i = service_ids == i
                grid_x = self.randgenserv.choice(self.grids[i].shape[0], np.sum(mask_this_i))
                grid_y = self.randgenserv.choice(self.grids[i].shape[1], np.sum(mask_this_i))
                gposition[mask_this_i] = np.column_stack((grid_x, grid_y))
        mask_workers = (self.mask_is_worker & mask)[mask]
        workers_gposition = fixed_gposition[mask_workers]
        if not self.workers_fp:
            for i in range(self.number):
                mask_this_i = service_ids[mask_workers] == i
                grid_x = self.randgenserv.choice(self.grids[i].shape[0], np.sum(mask_this_i))
                grid_y = self.randgenserv.choice(self.grids[i].shape[1], np.sum(mask_this_i))
                workers_gposition[mask_this_i] = np.column_stack((grid_x, grid_y))
        gposition[mask_workers] = workers_gposition
        return gposition


    def get_service_address(self, mask, gposition):
        service_ids = self.instance[mask]
        address = np.copy(self.service_address[mask])
        for i, grid in enumerate(self.grids):
            mask_this_i = service_ids == i
            grid_x = gposition[mask_this_i, 0]
            grid_y = gposition[mask_this_i, 1]
            address[mask_this_i, :2] = grid[grid_x, grid_y]
        return address
