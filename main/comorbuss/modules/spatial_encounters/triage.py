    param['dx'] = float_get(parameters, 'resolution_x', resolution_x['default'], event_log)
    param['dy'] = float_get(parameters, 'resolution_y', resolution_y['default'], event_log)
    param['lim_x'] = [0., float_get(parameters, 'size_x', size_x['default'], event_log)]
    param['lim_y'] = [0., float_get(parameters, 'size_y', size_y['default'], event_log)]

    unit_area = (param['dx'] + param['dy']) * (param['dx'] + param['dy'])*0.25
    param['unitArea'] = unit_area
    param['simulate_meanfield'] = bool_get(parameters, 'simulate_meanfield',
                                                  simulate_meanfield['default'], event_log)

    param['use_networks'] = bool_get(parameters, 'use_networks',
                                                        use_networks['default'], event_log)

    param["services_move_inside"] = bool_get(parameters, 'services_move_inside',
                                                services_move_inside['default'], event_log)
    param['deterministic_placement'] = bool_get(parameters, 'deterministic_placement',
                                             deterministic_placement['default'], event_log)
    param['random_walk'] = get_value(parameters, "random_walk", random_walk['default'])
    param['city_area'] = float_get(parameters, 'city_area', city_area['default'], event_log)
    norm_area = (param['city_area'] / city_pop) * Nparticles
    norm_L = np.sqrt(norm_area)
    param['norm_L'] = norm_L
    param['inf_radii'] = inf_radius / (1000. * norm_L)
    param['allowApartmentBuildings'] = bool_get(parameters, 'allow_apartment_buildings',
                                            allow_apartment_buildings['default'], event_log)

    param['sd_mode'] = int_get(parameters, 'social_distancing_mode',
                                               social_distancing_mode['default'], event_log)
    param['sd_fraction'] = float_get(parameters, 'social_distancing_fraction',
                                           social_distancing_fraction['default'], event_log)
    param['sd_distance'] = float_get(parameters, 'social_distancing_min_distance',
                                       social_distancing_min_distance['default'], event_log)
    param['sd_distance'] /= (1000. * norm_L)

    param['servs_min_dist_to_bdry'] = param['random_walk'][0]*dt
    
    
    param['norm_area'] = norm_area
    param['norm_L'] = norm_L

def srvc_area_parse(srvc, unit_area, norm_L, event_log):
    srvc["actual_area"] = srvc["area"]
    srvc["area"] /= norm_L*norm_L
    srvc["area"] = np.max([unit_area, srvc["area"]])

def srvc_number_parse(srvc, nservices, norm_N, event_log):
    if srvc["number"] == -1:
        if srvc["name"] in nservices:
            srvc["number"] = np.array(nservices[srvc["name"]])
        else:
            srvc["number"] = np.array(1)