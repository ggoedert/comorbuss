from . import settings as S
from .tools import e_dist
import numpy as np
from numpy.random import default_rng

##### Lower level class: Geography
class geography:
    """This class characterizes the domain, its limitis, units and the spacial characterizetion within"""

    def __init__(self, parameters, services_parameters, comm):
        """Geography class initalization"""
        self.comm = comm
        self.event_log = comm.event_log
        self.randgengeo = default_rng(parameters["random_seed"])
        if "net_n_homes" in parameters:
            self.homesNumber = parameters["net_n_homes"]
        else:
            self.homesNumber = parameters[
                "homesNumber"
            ]  # Normalized number of houses    in the simulated city to be consistent with the number of particles simulated
        allow_apartment_buildings = parameters[
            "allowApartmentBuildings"
        ]  # Allowance of apartment buildings or not
        self.particles_per_home = np.zeros(self.homesNumber)

        number_of_services = len(services_parameters)
        if parameters["use_networks"]:
            # Empty geography to use with networks
            self.services = [
                np.zeros((services_parameters[k]["number"], 3))
                for k in range(number_of_services)
            ]
            self.services_ids = [
                np.zeros((services_parameters[k]["number"], 1), dtype=int)
                for k in range(number_of_services)
            ]
            self.services_limits = [
                np.zeros((services_parameters[k]["number"], 4))
                for k in range(number_of_services)
            ]
            self.services_grids = [[] for _ in range(number_of_services)]
            self.dservices = np.ones(len(services_parameters))
            self.homes = np.zeros((self.homesNumber, 3))
            self.home_limits = np.zeros((self.homesNumber, 4))
            self.centers = np.zeros((1, 2))
        else:
            # Grid size and limits in the x and y axis
            self.dx = parameters["dx"]
            self.lim_x = parameters["lim_x"]
            self.dy = parameters["dy"]
            self.lim_y = parameters["lim_y"]

            self.servicesArea = np.zeros(number_of_services)
            for service in services_parameters:
                self.servicesArea[service["id"]] = service["area"]
            self.dservices = np.maximum(
                np.sqrt(self.servicesArea / (self.dx * self.dy)), 1
            ).astype(int)
            self.services_grids = [[] for _ in range(number_of_services)]

            # x, y coordinates of the subdivisions of the grid
            x_array = np.arange(self.lim_x[0], self.lim_x[1] + self.dx, self.dx)
            y_array = np.arange(self.lim_y[0], self.lim_y[1] + self.dy, self.dy)
            self.size_x = len(x_array)
            self.size_y = len(y_array)
            domain_grid = np.meshgrid(x_array, y_array)
            # Coordinates of the vertex of the Grid
            self.domain_grid = domain_grid

            centers_grid = np.meshgrid(
                x_array[:-1] + 0.5 * self.dx, y_array[:-1] + 0.5 * self.dy
            )
            centers = np.concatenate(
                (
                    centers_grid[0].reshape((centers_grid[0].size, 1)),
                    centers_grid[1].reshape((centers_grid[1].size, 1)),
                ),
                axis=1,
            )
            self.centers_grid = (
                centers_grid  # Coordinates of the centers of the cells in the grid
            )
            self.centers = (
                centers  # Indexing of the centers' coordinates by the cell index
            )

            self.Ncenters = len(centers)  # Number of centers
            self.centers_available = np.arange(
                self.Ncenters
            )  # Centers available for assignment of function (homes, markets, hospitals, ...)
            self.centers_id = np.copy(self.centers_available)  # Indexes to Centers

            dbx = int(parameters["servs_min_dist_to_bdry"] / self.dx) + 1
            dby = int(parameters["servs_min_dist_to_bdry"] / self.dy) + 1

            self.services_available = [[] for _ in range(number_of_services)]
            for i in range(number_of_services):
                mask_services_available = (
                    (centers[:, 0] > self.lim_x[0] - (dbx - 0.5) * self.dx)
                    & (
                        centers[:, 0]
                        < self.lim_x[1] - (self.dservices[i] + dbx + 0.5) * self.dx
                    )
                    & (
                        centers[:, 1]
                        > self.lim_y[0] + (self.dservices[i] + dby - 0.5) * self.dy
                    )
                    & (centers[:, 1] < self.lim_y[1] + (dbx - 0.5) * self.dy)
                )
                self.services_available[i] = self.centers_available[
                    mask_services_available
                ]

            # used as auxiliary data holder
            self.sort_idx = np.copy(self.centers_available)

            # Create lists of empty lists to be replaced with assigned values
            self.services = [[] for _ in range(number_of_services)]
            self.services_ids = [[] for _ in range(number_of_services)]
            self.services_limits = [[] for _ in range(number_of_services)]
            for service in sorted(
                services_parameters, reverse=True, key=lambda service: service["area"]
            ):
                if service["reserved_urban_area"] == 0:
                    if not parameters["deterministic_placement"]:
                        [
                            self.services[service["id"]],
                            self.services_ids[service["id"]],
                            self.services_limits[service["id"]],
                        ] = self.pick_services(
                            self.services_available[service["id"]],
                            self.dservices[service["id"]],
                            service["number"],
                            lambda *args: None,
                            self.random_map_pick,
                            self.eval_cids_picked,
                            self.del_cid_picks,
                            lambda *args: None,
                        )  # Selects services_parameters[0] (number of each service) centers to be that service, saving their locations, indexes to access them and limits
                    else:
                        [
                            self.services[service["id"]],
                            self.services_ids[service["id"]],
                            self.services_limits[service["id"]],
                        ] = self.pick_services(
                            self.services_available[service["id"]],
                            self.dservices[service["id"]],
                            service["number"],
                            self.deterministic_map_start,
                            self.deterministic_map_pick,
                            self.eval_cids_picked,
                            self.deterministic_map_loop,
                            self.del_cid_picks,
                        )  # Selects services_parameters[0] (number of each service) centers to be that service, saving their locations, indexes to access them and limits
                else:
                    [
                        self.services[service["id"]],
                        self.services_ids[service["id"]],
                        self.services_limits[service["id"]],
                    ] = self.pick_centers(
                        service["number"],
                        allow_apartment_buildings,
                        service["reserved_urban_area"],
                    )  # Selects neworksNumber centers to be non essential work places, saving their locations, indexation and geographic limits
                    self.neWorkindex = service["id"]
            [self.homes, self.home_to_center_id, self.home_limits] = self.pick_centers(
                self.homesNumber, allow_apartment_buildings
            )  # Selects homesNumber centers to be homes, saving their locations, indexation and geographic limits

    def __repr__(self):
        return "<COMORBUSS geography>"

    def pick_centers(
        self, Npicks, allow_apartment_buildings=False, avail_centers_percent=1.0
    ):
        """Selects available cell centers from the domain to be assigned as
        homes or services. It allows the cell center to represent buildings with
        floors, but it does not allow the building to have an area larger than
        one cell.

        Args:
            Npicks (int): number of centers to be assigned as homes or services.
            allow_apartment_buildings (bool): whether buidings with floors are allowed or not.
            avail_centers_percent (float): a percentage of the remaining cell centers that this function can occupy with homes or centers.

        Returns:
            [np.array, np.array, np.array]: picked_centers, cid_picks, center_limits
        """
        if (not allow_apartment_buildings) & (Npicks > len(self.centers_available)):
            self.event_log(
                "Error distributing map centers, please increase the resolution in x and y, or decrease the number of particles, or allow the existence of apartment buildings",
                MSG_ERROR,
            )
            self.comm.initOk = False
            return [], [], []
        Ncenters = int(
            np.minimum(Npicks, avail_centers_percent * self.centers_available.shape[0])
        )
        # get ids to centers address (here center is an apartment building)
        cids = self.randgengeo.choice(self.centers_available, Ncenters, replace=False)
        mask_not_picked = np.isin(
            self.centers_available, cids, invert=True
        )  # Find a mask of the centers that were not choosen out of all previously available centers
        self.centers_available = self.centers_available[
            mask_not_picked
        ]  # Mark the choosen centers as not available
        # distribute the ids of the buildings randomly according to the requested number of addresses
        cid_picks = []
        if allow_apartment_buildings:
            cid_picks = self.randgengeo.choice(cids, Npicks, replace=True)
        else:
            cid_picks = cids
        [un, uninv] = np.unique(cid_picks, return_inverse=True)
        # distribute floors to adreesses
        maxfloors = np.zeros(un.shape[0])
        unind = np.arange(un.shape[0], dtype=int)
        cid_floors = np.zeros(Npicks, dtype=int)
        for ind, indinv in enumerate(uninv):
            cid_floors[ind] = maxfloors[unind[indinv]]
            maxfloors[unind[indinv]] += 1
        # build information about all addresses and return it
        picked_centers = np.column_stack((self.centers[cid_picks, :], cid_floors))
        center_limits = np.zeros(
            [Npicks, 4]
        )  # Variable to store the boundary regions of the centers
        center_limits[:, 0] = picked_centers[:, 0] - 0.5 * self.dx
        center_limits[:, 1] = picked_centers[:, 0] + 0.5 * self.dx
        center_limits[:, 2] = picked_centers[:, 1] - 0.5 * self.dy
        center_limits[:, 3] = picked_centers[:, 1] + 0.5 * self.dy
        return picked_centers, cid_picks, center_limits

    def deterministic_map_start(self, services_available, dservices, nservices):
        """[summary]

        Args:
            services_available ([type]): [description]
            dservices ([type]): [description]
            nservices ([type]): [description]
        """
        centerpoint = [
            (self.lim_x[1] - self.lim_x[0]) * 0.5 + self.lim_x[0],
            (self.lim_y[1] - self.lim_y[0]) * 0.5 + self.lim_y[0],
        ]
        self.sort_idx = np.argsort(
            e_dist(self.centers[services_available], centerpoint)
        )

    def deterministic_map_pick(self, services_available, dservices, nservices):
        """[summary]

        Args:
            services_available ([type]): [description]
            dservices ([type]): [description]
            nservices ([type]): [description]

        Returns:
            [type]: [description]
        """
        return services_available[self.sort_idx[0]]

    def deterministic_map_loop(
        self,
        services_available,
        dservices,
        nservices,
        cids_picked,
        centers_are_available,
    ):
        """[summary]

        Args:
            services_available ([type]): [description]
            dservices ([type]): [description]
            nservices ([type]): [description]
            cids_picked ([type]): [description]
            centers_are_available ([type]): [description]
        """
        if centers_are_available:
            mask_not_picked = np.isin(
                services_available[self.sort_idx], cids_picked, invert=True
            )
            self.sort_idx = self.sort_idx[mask_not_picked]
        else:
            self.sort_idx = self.sort_idx[1:]

    def del_cid_picks(self, services_available, dservices, nservices, cid_picks, tries):
        """[summary]

        Args:
            services_available ([type]): [description]
            dservices ([type]): [description]
            nservices ([type]): [description]
            cid_picks ([type]): [description]
            tries ([type]): [description]
        """
        cid_picks_seq = cid_picks.reshape(-1)
        for service_available in self.services_available:
            mask_not_picked = np.isin(service_available, cid_picks_seq, invert=True)
            service_available = service_available[mask_not_picked]
        mask_not_picked = np.isin(self.centers_available, cid_picks_seq, invert=True)
        self.centers_available = self.centers_available[mask_not_picked]

    def eval_cids_picked(
        self, services_available, dservices, nservices, cids_picked, cid_picks
    ):
        """[summary]

        Args:
            services_available ([type]): [description]
            dservices ([type]): [description]
            nservices ([type]): [description]
            cids_picked ([type]): [description]
            cid_picks ([type]): [description]

        Returns:
            [type]: [description]
        """
        evaluation = ~np.isin(True, np.isin(cids_picked, cid_picks))
        for ids in self.services_ids:
            evaluation = evaluation & ~np.isin(True, np.isin(cids_picked, ids))
        return evaluation

    def random_map_pick(self, services_available, dservices, nservices):
        """[summary]

        Args:
            services_available ([type]): [description]
            dservices ([type]): [description]
            nservices ([type]): [description]

        Returns:
            [type]: [description]
        """
        return self.randgengeo.choice(services_available, 1, replace=False)

    def pick_services(
        self,
        services_available,
        dservices,
        nservices,
        f_start,
        f_pick,
        f_eval_cids_picked,
        f_loop,
        f_end,
    ):
        """Select an available cell center from the domain, to assign it a role (e.g. home, market, hospital etc.)

        Args:
            services_available ([type]): [description]
            dservices ([type]): [description]
            nservices ([type]): [description]
            f_start ([type]): [description]
            f_pick ([type]): [description]
            f_eval_cids_picked ([type]): [description]
            f_loop ([type]): [description]
            f_end ([type]): [description]

        Returns:
            [type]: [description]
        """
        f_start(services_available, dservices, nservices)
        cid_picks_index = 0
        cid_picks = np.zeros((nservices, dservices * dservices), dtype=int)
        for tries in range(500):  # To do: better choose this value
            if cid_picks_index >= nservices | (len(services_available) < nservices):
                break
            cid_pick = f_pick(services_available, dservices, nservices)
            pick_seq = np.arange(cid_pick, cid_pick + dservices)
            cids_picked = np.zeros(dservices * dservices, dtype=int)
            for i in range(dservices):
                cids_picked[i * dservices : (i + 1) * dservices] = pick_seq - i * (
                    self.size_y - 1
                )
            centers_are_available = f_eval_cids_picked(
                services_available, dservices, nservices, cids_picked, cid_picks
            )
            if centers_are_available:
                cid_picks[cid_picks_index, :] = cids_picked
                cid_picks_index = cid_picks_index + 1
            f_loop(
                services_available,
                dservices,
                nservices,
                cids_picked,
                centers_are_available,
            )
        if tries == 500:
            self.event_log(
                "Could not make a geography with all the services requested", MSG_ERROR
            )
            self.comm.initOk = False
        f_end(services_available, dservices, nservices, cid_picks, tries)
        picked_centers = np.column_stack(
            (self.centers[cid_picks[:, 0], :], np.zeros(cid_picks.shape[0]))
        )
        center_limits = np.zeros([len(picked_centers), 4])
        center_limits[:, 0] = picked_centers[:, 0] - 0.5 * self.dx
        center_limits[:, 1] = picked_centers[:, 0] + (dservices - 0.5) * self.dx
        center_limits[:, 2] = picked_centers[:, 1] - (dservices - 0.5) * self.dy
        center_limits[:, 3] = picked_centers[:, 1] + 0.5 * self.dy
        picked_centers = np.column_stack(
            (
                self.centers[
                    cid_picks[:, np.floor(dservices * dservices / 2).astype(int)], :
                ],
                np.zeros(cid_picks.shape[0]),
            )
        )
        return picked_centers, cid_picks, center_limits

    def gen_service_grids(self, service):
        if self.services_grids[service["id"]] == []:
            limits = service["centers_limits"]
            width = service.get("grid_width", 1)
            grids = []
            for l in limits:
                dx = (l[1] - l[0]) / (width + 1)
                dy = (l[3] - l[2]) / (width + 1)
                grid_address = np.zeros((width, width, 2))
                for i in range(width):
                    for j in range(width):
                        grid_address[i, j] = [
                            np.min(l[:2]) + dx * (i + 1),
                            np.min(l[-2:]) + dy * (j + 1),
                        ]
                grids.append(grid_address)
            self.services_grids[service["id"]] = grids
            return grids
        else:
            return self.services_grids[service["id"]]
