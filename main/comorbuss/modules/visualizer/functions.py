from networkx.generators import line
import numpy as np
from ... import settings as S
from ...tools import recursive_copy
import os

os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = "True"
import pygame


def hex_to_int_color(hex_color):
    r = int(hex_color[1:3], 16) / 255
    g = int(hex_color[3:5], 16) / 255
    b = int(hex_color[5:7], 16) / 255
    return (r, g, b)


def interpolate_colors(c1, c2, mix):
    if type(c1) == str:
        if c1[0] == "#":
            c1 = hex_to_int_color(c1)
    if type(c2) == str:
        if c2[0] == "#":
            c2 = hex_to_int_color(c2)
    c1 = np.array(c1)
    c2 = np.array(c2)
    return tuple((1 - mix) * c1 + mix * c2)


def plot_array(
    screen,
    array,
    min_color=(1, 0.2, 0.3),
    max_color=(0.2, 0.4, 1.0),
    intermediary_colors=[],
    keep_proportion=False,
):
    array = np.array(array)
    if len(array) == 0:
        return
    if len(array.shape) > 1:
        raise ValueError(
            "Array has {} axis, only one allowed.".format(len(array.shape))
        )
    i_max = np.max(array)
    i_min = np.min(array)

    width, height = screen.get_size()
    area = width * height
    i = 0
    while True:
        item_area = area / array.shape[0] - i
        if keep_proportion:
            i_height = int(np.sqrt(height * item_area / width))
            i_width = int(np.sqrt(width * item_area / height))
        else:
            i_height = int(np.sqrt(item_area))
            i_width = int(np.sqrt(item_area))
        ncols = int(width / i_width)
        nrows = int(height / i_height)
        if ncols * nrows < len(array):
            i += 1
        else:
            break

    intermediary_colors = list(intermediary_colors)
    has_more_colors = len(intermediary_colors) > 0
    if has_more_colors:
        intermediary_colors.sort(key=lambda x: x[0])
        if (
            len(intermediary_colors) == 1
            and intermediary_colors[0][0] == i_min
            and intermediary_colors[0][0] == i_max
        ):
            min_color = intermediary_colors[0][1]
            max_color = intermediary_colors[0][1]
            intermediary_colors.pop(0)
    has_more_colors = len(intermediary_colors) > 0
    if has_more_colors:
        intermediary_colors.sort(key=lambda x: x[0])
        if intermediary_colors[0][0] == i_min:
            min_color = intermediary_colors[0][1]
            intermediary_colors.pop(0)
    has_more_colors = len(intermediary_colors) > 0
    if has_more_colors:
        if intermediary_colors[-1][0] == i_max:
            max_color = intermediary_colors[-1][1]
            intermediary_colors.pop(-1)
    has_more_colors = len(intermediary_colors) > 0

    for row in range(nrows):
        for col in range(ncols):
            i = ncols * row + col
            if i >= len(array):
                break
            if has_more_colors:
                prev_c = min_color
                prev_min = i_min
                got_end_color = False
                for v, c in intermediary_colors:
                    if v > array[i]:
                        got_end_color = True
                        break
                    prev_c = c
                    prev_min = v
                if not got_end_color:
                    c = max_color
                    v = i_max
                color = [
                    v * 255
                    for v in interpolate_colors(
                        prev_c, c, (array[i] - prev_min) / (v - prev_min)
                    )
                ]
            else:
                if i_min == i_max:
                    color = [
                        v * 255 for v in interpolate_colors(min_color, max_color, 1)
                    ]
                else:
                    color = [
                        v * 255
                        for v in interpolate_colors(
                            min_color, max_color, (array[i] - i_min) / (i_max - i_min)
                        )
                    ]

            if i_width > 5 and i_height > 5:
                pygame.draw.rect(
                    screen,
                    (0, 0, 0),
                    (col * i_width, row * i_height, i_width, i_height),
                )
                pygame.draw.rect(
                    screen,
                    color,
                    (col * i_width + 1, row * i_height + 1, i_width - 2, i_height - 2),
                )
            else:
                pygame.draw.rect(
                    screen, color, (col * i_width, row * i_height, i_width, i_height)
                )


def get_placements(comm):
    other_plcs = [
        (S.PLC_CEMT, "Deceased"),
        (S.PLC_ENV, "Environment"),
        (S.PLC_HOME, "Home"),
    ]
    services_plc = [(srvc.id, srvc.name) for srvc in comm.srv.all_services]
    return other_plcs + services_plc


def get_quarantines_lines(comm):
    try:
        lines = {}
        for qrnt in reversed(comm.quarantines):
            lines[
                qrnt.name
            ] = "self.comm.quarantines[{}].quarantined[:self.clk.step]*100/self.comm.Nparticles".format(
                qrnt.id
            )
        return lines
    except AttributeError:
        return {"no quarantines": "np.zeros(self.clk.step)"}
