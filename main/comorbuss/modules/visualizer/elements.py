from comorbuss.settings import COLORS_LIST
from matplotlib.pyplot import ylabel
from .functions import *
from ... import settings as S
import os
import tempfile
import numpy as np
import seaborn as sns

os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = "True"
import pygame

COLORS_BOOLEAN = [(0, (1, 1, 1)), (1, "#001177")]
LEGENDS_BOOLEAN = [(0, "False"), (1, "True")]

COLORS_RANDOM = [("random", (1, 1, 1)), ("random", (1, 1, 1))]


class VisualizerElement:
    """Parent class for every type of visualizer element."""

    def __init__(self, **param):
        """Stores given parameters in param attribute for future uses."""
        self.param = recursive_copy(param)
        self.expression = "[]"

    def internal_init(self, screen, comm):
        """Initialization routine, is called during the visualizer module initialization.

        Args:
            screen (pygame.surface): Available surface for the element.
            comm (comorbuss.community): Simulation's community.
        """
        self.screen = screen
        self.comm = comm
        self.pop = comm.pop
        self.clk = comm.clk
        self.event_log = comm.event_log
        self.width, self.height = screen.get_size()

    def __repr__(self) -> str:
        return "{}({})".format(type(self).__name__, self.param)

    def eval(self):
        """Evaluates expression stored in expression attribute.

        Returns:
            Any: Result.
        """
        if hasattr(self.expression, "__call__"):
            return self.expression(self.comm)
        else:
            return eval(self.expression)

    def update(self):
        """Updates the element"""
        pass

    def cleanup(self):
        """Cleanup the element."""
        pass


class Split(VisualizerElement):
    """Splits available space between multiple elements.

    Args:
        *elements (VisualizerElement): Elements to show.
        vertical (bool): Splits vertical space instead of horizontal space.
    """

    def __init__(self, *elements, **params):
        self.elements = elements
        self.vertical = params.get("vertical", False)
        if "label" in params:
            self.label = params["label"]
        params["elements"] = elements
        self.param = params

    def internal_init(self, screen, comm):
        super().internal_init(screen, comm)

        # Check if this split has a title,if yes apply it and define subscreen for child elements
        if hasattr(self, "label"):
            self.subscreen = self.screen.subsurface(
                (2, 16, self.width - 2, self.height - 16)
            )
            labels = pygame.font.SysFont("Liberation Mono", 12)
            text = labels.render(format(self.label), True, (255, 255, 255))
            self.screen.blit(text, (2, 2))
        else:
            self.subscreen = self.screen

        # Get number of child elements and available space for each
        nelements = len(self.elements)
        width, height = self.subscreen.get_size()
        if self.vertical:
            element_size = int(height / nelements)
        else:
            element_size = int(width / nelements)
        # Initialize child elements with its respective subscreen
        for i in range(nelements):
            if self.vertical:
                screen = self.subscreen.subsurface(
                    (0, i * element_size, width, element_size)
                )
            else:
                screen = self.subscreen.subsurface(
                    (i * element_size, 0, element_size, height)
                )
            self.elements[i].internal_init(screen, self.comm)

    def update(self):
        # Updates each child element
        for element in self.elements:
            element.update()

    def cleanup(self):
        # Cleanup each child element
        for element in self.elements:
            element.cleanup()


class Plot(VisualizerElement):
    """Parent class for all matplotlib plots.

    Args:
        **kwargs: Each extra keyword argument passed will be called as an
            pyplot method during the plot. Example Plot(title="States plot.")
            will result in pyplot.title("States plot.") been called during each
            plot. Tuples will be interpreted as each item an argument, and dicts
            will be used as keyword arguments.
    """

    def internal_init(self, screen, comm):
        super().internal_init(screen, comm)
        self.tmp_dir = tempfile.TemporaryDirectory()
        self.dir = self.tmp_dir.name
        self.figsize = (self.width / 100, self.height / 100)

        import matplotlib.pyplot as plt

        self.plt = plt

    def get_fig(self):
        """Initializes and return a pytplot.figure object.

        Returns:
            pyplot.figure: Figure object.
        """
        return self.plt.figure(figsize=self.figsize, dpi=100)

    def show_fig(self):
        """Uses all available parameters to call pyplot methods,
        stores the plot in a temporary image and shows it in
        the visualizer.
        """
        fig_path = os.path.join(self.dir, "fig.png")
        # self.plt.tight_layout(pad=0.5)
        for attr, param in self.param.items():
            try:
                function = getattr(self.plt, attr)
                if type(param) in [list, tuple]:
                    function(*param)
                elif type(param) is dict:
                    function(**param)
                else:
                    function(param)
            except (AttributeError, TypeError) as _:
                pass
        self.plt.savefig(fig_path)
        self.plt.close()
        img = pygame.image.load(fig_path)
        self.screen.blit(img, (0, 0))

    def cleanup(self):
        self.tmp_dir.cleanup()


class HistogramPlot(Plot):
    """Shows a histogram plot using matplotlib.

    Args:
        expression (str or callable): An expression to be evaluated to get plot data or
            an function that accepts the community as argument and return values.
    """

    def __init__(self, **param):
        super().__init__(**param)
        self.expression = param["expression"]
        self.hist_params = self.param.get("hist_parameters", {})

    def update(self):
        self.get_fig()
        array = self.eval()
        self.plt.hist(array, **self.hist_params)
        self.show_fig()


class LinesPlot(Plot):
    """Shows an lines plot using matplotlib.

    Args:
        lines (list or dict): List or dict of expressions to be evaluated to get plot data or
            an function that accepts the community as argument and return values. Dictionaries
            indexes will be used as labels in the plot.
        x (str or callable):  An expression to be evaluated to get x values or
            an function that accepts the community as argument and return x values. X values
            must have same shape of each line.
    """

    def __init__(self, **param):
        super().__init__(**param)
        self.lines = param["lines"]
        self.has_x = "x" in self.param
        self.has_colors = "colors" in self.param
        if self.has_colors:
            self.colors = param["colors"]
        if "skip" in self.param:
            self.skip = param["skip"]
        else:
            self.skip = []

    def internal_init(self, screen, comm):
        super().internal_init(screen, comm)

        # Check of lines is callable
        if callable(self.lines):
            self.lines = self.lines(self.comm)
        # Stores lines and labels as lists
        if type(self.lines) is dict:
            self.lines_labels = [l for l in self.lines]
            self.lines = [v for v in self.lines.values()]
        else:
            self.lines_labels = self.lines

        # If has no colors and not many lines apply default comorbuss color pallet
        if not self.has_colors and len(self.lines) <= len(S.COLORS_LIST):
            self.colors = S.COLORS_LIST
            self.has_colors = True

    def update(self):
        self.get_fig()
        if self.has_x:
            self.expression = self.param["x"]
            x = self.eval()
        for i in range(len(self.lines)):
            if self.lines_labels[i] not in self.skip:
                self.expression = self.lines[i]
                array = self.eval()
                if self.has_x:
                    args = (x, array)
                else:
                    args = array
                if self.has_colors:
                    kwargs = dict(label=self.lines_labels[i], color=self.colors[i])
                else:
                    kwargs = dict(label=self.lines_labels[i])
                self.plt.plot(*args, **kwargs)
        self.plt.legend()
        self.show_fig()


class AgentsVisualizer(VisualizerElement):
    """Shows a grid with color coded values for each agent.

    Args:
        expression (str or callable): An expression to be evaluated to get agents data or
            an function that accepts the community as argument and return values.
        colors (list): An list of tuples in the form [(value1, color1), ..., (valuen, colorn)].
            Values in between defined colors the color will be interpolated between closest values.
            'min' and 'max' keywords can be used instead of absolute values to set the color of the
            minimum and maximum value respectively. User can pass ('random', seed) to generate random
            colors to each unique value, where seed is an integer value to a seed a random number generator.
        legends (list): An list of tuples in the form [(value1, label), ..., (valuen, labeln)]. With
            labels to be used in the legend. If ['unique'] is passed each unique value will be printed
            in the legend. It can be passed as an functions that accepts the community as argument and
            returns the label value.
        label (str): Label for the visualization, if not given will use the expression as label.
    """

    def __init__(self, **param):
        super().__init__(**param)

        self.expression = param["expression"]
        self.colors = param.get("colors", [("min", (1, 1, 1)), ("max", "#001177")])
        self.title = param.get("label", self.expression)

        self.min_color = self.colors[0][1]
        self.max_color = self.colors[-1][1]
        if self.colors[0][0] == "min":
            self.colors.pop(0)
        if self.colors[-1][0] == "max":
            self.colors.pop(-1)
        self.random_colors = False
        if len(self.colors) > 0:
            if self.colors[0][0] == "random":
                self.random_colors = True
                seed = self.colors[0][1]
                self.colors = []
                self.rng = np.random.default_rng(seed)

    def internal_init(self, screen, comm):
        super().internal_init(screen, comm)

        self.labels = pygame.font.SysFont("Liberation Mono", 12)

        self.init_legends(self.param)

        self.subscreen = self.screen.subsurface(
            (2, 16, self.width - self.legend_width - 2, self.height - 16)
        )
        text = self.labels.render(format(self.title), True, (255, 255, 255))
        self.screen.blit(text, (2, 2))

    def init_legends(self, param):
        self.has_legends = "legends" in param
        if self.has_legends:
            if callable(param["legends"]):
                self.legends = param["legends"](self.comm)
            else:
                self.legends = list(param["legends"])
            self.legend_width = param.get("legends_width", 150)
            self.unique_legends = False
            if len(self.legends) > 0:
                if self.legends[0] == "unique":
                    self.unique_legends = True
                    self.legends = []
            self.legend_screen = self.screen.subsurface(
                (
                    self.width - self.legend_width,
                    16,
                    self.legend_width,
                    self.height - 16,
                )
            )
            self.update_legends(self.legends)
        else:
            self.legend_width = 0

    def update_legends(self, legends):
        """Updates the legends as needed.

        Args:
            legends (list): List of legends.
        """
        self.legend_screen.fill((0, 0, 0))
        legends.sort(key=lambda x: x[0])
        nlegend = len(legends)
        box_height = 16 * nlegend
        if box_height > self.legend_screen.get_height():
            nlegend = int(self.legend_screen.get_height() / 16)
            legends = legends[:nlegend]
            box_height = 16 * nlegend
        legend_array = np.array([v for v, _ in legends])
        legend_boxes = self.legend_screen.subsurface((2, 0, 16, box_height))
        plot_array(
            legend_boxes,
            legend_array,
            min_color=self.min_color,
            max_color=self.max_color,
            intermediary_colors=self.colors,
        )
        for i in range(nlegend):
            text = self.labels.render(legends[i][1], True, (255, 255, 255))
            self.legend_screen.blit(text, (20, i * 16 + 2))

    def update(self):
        array = self.eval()
        if self.random_colors:
            unique_values = np.unique(array)
            for value in unique_values:
                if value not in [x for x, _ in self.colors]:
                    random_color = self.rng.uniform(0, 1, size=3)
                    self.colors.append((value, random_color))
        if self.has_legends:
            if self.unique_legends:
                self.update_legends([(v, str(v)) for v in np.unique(array)])
        plot_array(
            self.subscreen,
            array,
            min_color=self.min_color,
            max_color=self.max_color,
            intermediary_colors=self.colors,
        )


class AgentsConfigs(AgentsVisualizer):
    """Parent class for pre-configured agents visualizations.

    Args:
        mask (str, optional): An expression to be used as an mask to select agents.
        label (str, optional): An label to show in the visualization.
    """

    def __init__(self, mask=":", label=None, **kwargs):
        kwargs.update(
            dict(
                expression=self.EXPRESSION.format(mask),
                colors=self.COLORS,
                legends=self.LEGENDS,
                label=label or self.LABEL,
            )
        )
        super().__init__(**kwargs)


class AgentsStates(AgentsConfigs):
    """Shows states of agents in the simulation."""

    EXPRESSION = "self.pop.states[{}]"
    LABEL = "States"
    COLORS = [
        (S.STATE_S, S.COLORS_STATES[0]),
        (S.STATE_E, S.COLORS_STATES[1]),
        (S.STATE_I, S.COLORS_STATES[2]),
        (S.STATE_R, S.COLORS_STATES[3]),
        (S.STATE_D, S.COLORS_STATES[4]),
    ]
    LEGENDS = [
        (S.STATE_S, "Susceptible"),
        (S.STATE_E, "Exposed"),
        (S.STATE_I, "Infectious"),
        (S.STATE_R, "Recovered"),
        (S.STATE_D, "Deceased"),
    ]


class AgentsSymptoms(AgentsConfigs):
    """Shows symptoms of agents in the simulation."""

    EXPRESSION = "self.pop.symptoms[{}]"
    LABEL = "Symptoms"
    COLORS = [
        (S.SYMPT_YES, S.COLORS_SYMPT[0]),
        (S.SYMPT_SEVERE, S.COLORS_SYMPT[1]),
        (S.SYMPT_NO, S.COLORS_SYMPT[2]),
        (S.SYMPT_NYET, S.COLORS_SYMPT[3]),
        (S.NO_INFEC, S.COLORS_SYMPT[4]),
        (S.SYMPT_YES * 10, S.COLORS_SYMPT_DEEP[0]),
        (S.SYMPT_SEVERE * 10, S.COLORS_SYMPT_DEEP[1]),
        (S.SYMPT_NO * 10, S.COLORS_SYMPT_DEEP[2]),
    ]
    LEGENDS = [
        (S.SYMPT_YES, "light"),
        (S.SYMPT_SEVERE, "severe"),
        (S.SYMPT_NO, "assymptomatic"),
        (S.SYMPT_NYET, "incubating"),
        (S.NO_INFEC, "not ionfected"),
        (S.SYMPT_YES * 10, "light recovered"),
        (S.SYMPT_SEVERE * 10, "severe recovered"),
        (S.SYMPT_NO * 10, "assympt recovered"),
    ]


class AgentsDiagnostics(AgentsConfigs):
    """Shows diagnostics of agents in the simulation."""

    EXPRESSION = "self.pop.diag_states[{}]"
    LABEL = "Diagnostics"
    COLORS = [
        (S.DIAG_NYET, S.COLORS["grey"]),
        (S.DIAG_NO, S.COLORS["green"]),
        (S.DIAG_YES, S.COLORS["red"]),
    ]
    LEGENDS = [
        (S.DIAG_NYET, "Not tested"),
        (S.DIAG_NO, "Negative"),
        (S.DIAG_YES, "Positive"),
    ]


colors_paired = sns.color_palette("Paired")


class AgentsPlacement(AgentsConfigs):
    """Shows placements of agents in the simulation."""

    EXPRESSION = "self.pop.placement[{}]"
    LABEL = "Placement"
    COLORS = [(i - 3, colors_paired[i]) for i in range(len(colors_paired))]
    LEGENDS = staticmethod(get_placements)


class LinesPlotsConfigs(LinesPlot):
    """Parent class for pre-configured line plots visualizations.

    Args:
        skip (list, optional): List of curves to skip.
    """

    def __init__(self, skip=[], **param):
        param.update(
            dict(
                lines=self.LINES,
                skip=skip,
            )
        )
        opt_attrs = ["TITLE", "X", "XLABEL", "YLABEL", "COLORS"]
        for attr in opt_attrs:
            if hasattr(self, attr):
                param.update({attr.lower(): getattr(self, attr)})
        super().__init__(**param)


class StatesPlot(LinesPlotsConfigs):
    """Shows a plot of the percentage of particles in each state during the simulation."""

    LINES = {
        "susceptible": "self.pop.susceptible[:self.clk.step]*100/self.comm.Nparticles",
        "exposed": "self.pop.exposed[:self.clk.step]*100/self.comm.Nparticles",
        "infectious": "self.pop.infectious[:self.clk.step]*100/self.comm.Nparticles",
        "recovered": "self.pop.recovered[:self.clk.step]*100/self.comm.Nparticles",
        "deceased": "self.pop.deceased[:self.clk.step]*100/self.comm.Nparticles",
    }
    X = "np.arange(self.clk.step)/24*self.comm.dt"
    TITLE = "States"
    XLABEL = "Day"
    YLABEL = "% of population"
    COLORS = S.COLORS_STATES


class SymptomaticStatesPlot(LinesPlotsConfigs):
    """Shows a plot of the percentage of particles in each symptomatic state in time during the simulation."""

    LINES = {
        "mild symp": "self.pop.symptomatic[:self.clk.step]*100/self.comm.Nparticles",
        "severe symp": "self.pop.severe_symptomatic[:self.clk.step]*100/self.comm.Nparticles",
        "asymptomatic": "self.pop.asymptomatic[:self.clk.step]*100/self.comm.Nparticles",
        "pre-symptomatic": "self.pop.pre_symptomatic[:self.clk.step]*100/self.comm.Nparticles",
        "incubating": "self.pop.exposed[:self.clk.step]*100/self.comm.Nparticles",
    }
    X = "np.arange(self.clk.step)/24*self.comm.dt"
    TITLE = "Symptoms"
    XLABEL = "Day"
    YLABEL = "% of population"
    COLORS = S.COLORS_SYMPT


class EncountersPlot(LinesPlotsConfigs):
    """Shows a plot of the number of encounters during the simulation."""

    LINES = {
        "all encounters": "self.pop.encounters[:self.clk.step]",
        "infectious -> any": "self.pop.encounters_infectious[:self.clk.step]",
        "infectious -> susceptible": "self.pop.possible_infections[:self.clk.step]",
    }
    X = "np.arange(self.clk.step)/24*self.comm.dt"
    TITLE = "Encounters"
    XLABEL = "Day"
    YLABEL = "% of population"
    COLORS = (S.COLORS["red"], S.COLORS["green"], S.COLORS["blue"])


class QuarantinesPlot(LinesPlotsConfigs):
    """Shows a plot of the percentage of particles in quarantine during the simulation."""

    LINES = staticmethod(get_quarantines_lines)
    X = "np.arange(self.clk.step)/24*self.comm.dt"
    TITLE = "Quarantines"
    XLABEL = "Day"
    YLABEL = "% of population"


class DiagnosticsPlot(LinesPlotsConfigs):
    """Shows a plot of the percentage of particles in quarantine during the simulation."""

    LINES = {
        "diagnosed": "self.pop.diagnosed[:self.clk.step]*100/self.comm.Nparticles",
        "diagnosed cumulative": "self.pop.diagnosed_cum[:self.clk.step]*100/self.comm.Nparticles",
    }
    X = "np.arange(self.clk.step)/24*self.comm.dt"
    TITLE = "Diagnostics"
    XLABEL = "Day"
    YLABEL = "% of population"
