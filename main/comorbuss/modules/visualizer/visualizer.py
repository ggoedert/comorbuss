from ...aux_classes import empty_module
from ... import settings, tools
from .functions import *
import numpy as np
import seaborn as sns
import os


__name__ = "runtime_visualizer"


DEFAULTS = {}


class module(empty_module):
    def __init__(self, param, comm):
        os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = "True"
        global pygame
        import pygame

        super(module, self).__init__(param, comm, name=__name__)
        self.size = param.get("size", (500, 500))
        self.elements = param["elements"]
        self.paused = False

        self.by_function = param.get("plot_by_function", False)
        self.print_last_function_name = param.get("print_last_function_name", False)

        self.save_dir = param.get("save_directory", "")
        self.save = self.save_dir != ""
        if self.save:
            tools.check_dir(self.save_dir)

    def run_after_init(self):
        # Initialize pygame
        pygame.init()
        pygame.font.init()
        self.window = pygame.display.set_mode(self.size)
        self.window.set_alpha(None)

        nelements = len(self.elements)
        element_height = int(self.size[1] / nelements)
        for i in range(nelements):
            screen = self.window.subsurface(
                (0, i * element_height, self.size[0], element_height)
            )
            self.elements[i].internal_init(screen, self.comm)
        if self.by_function:
            for p, f, tod in list(self.comm.to_execute):
                self.comm.add_to_execution(
                    self.update, priority=p + 0.000001, time_of_the_day=tod
                )
                if self.print_last_function_name:
                    self.comm.add_to_execution(
                        self.print_last_name, priority=p + 0.000002, time_of_the_day=tod
                    )
        else:
            self.comm.add_to_execution(self.update, priority=999)

    def print_last_name(self):
        print("Last function: {}".format(self.last_function_name))

    def update(self):
        if self.by_function & self.print_last_function_name:
            self.last_function_name = self.comm.last_function.__name__
        for element in self.elements:
            element.update()
        title = "Day: {}-{}, Hour: {}, Step:{}".format(
            self.clk.today, self.clk.day_of_the_week[:3], self.clk.tod, self.clk.step
        )
        pygame.display.set_caption(title)
        pygame.display.flip()
        if self.save:
            pygame.image.save(
                self.window,
                self.save_dir
                + "/{}_{}.png".format(
                    self.clk.step,
                    title.replace(" ", "_").replace(",", "").replace(":", "_"),
                ),
            )
        one_step = False
        while True:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        self.paused = not self.paused
                    if event.key == pygame.K_RIGHT:
                        one_step = True
            if self.paused:
                pygame.display.set_caption(title + " PAUSED")
            if not self.paused or one_step:
                break
            pygame.time.wait(100)

    def log_results(self):
        for element in self.elements:
            element.cleanup()
        pygame.quit()
