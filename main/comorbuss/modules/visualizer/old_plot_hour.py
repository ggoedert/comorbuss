
    ##### Method for debug and easier visualition of data
    def plot_hour(self, step, **kwargs):
        states_filter = kwargs.get('states', [])                        # Optional parameter: Plot only particles in specified states
        placements_filter = kwargs.get('placements', [])
        pid = kwargs.get('pid', np.array([]))                                  # Optional paramenter: Plot only one particle with pid
        filename = kwargs.get('filename', "")

        area_unitario = 250000                                                 # Calculate intection radio for plotting
        area_infec = np.pi*(self.pop.disease.inf_radii**2)
        ratio_areas = area_infec/(np.pi*(0.5**2))
        circle_area = area_unitario*ratio_areas

        title = "Day: {:} Hour: {:}".format(int(step/24), step%24)
        states = self.pop.states_ts[step]                                     # Get states at time step
        positions = self.pop.positions_ts[step, :, :2]
        placement = self.pop.placement_ts[step]
        Nparticles = self.Nparticles
        if len(pid) > 0:
            positions = positions[pid]
            states = states[pid]
            Nparticles = np.shape(pid)[0]
            if(Nparticles > 10):
                title += " Pid: Many"
            else:
                title += " Pid: {:}".format(pid)
        fig = plt.figure(figsize=(7, 7))
        if self.networks:
            # Prepare axis and labels
            placements = {'Home':PLC_HOME, 'Environment':PLC_ENV, "Cemetery": PLC_CEMT}
            states_names = {
                "Susceptible": STATE_S,
                "Exposed": STATE_E,
                "Infectious": STATE_I,
                "Recovered": STATE_R,
                "Deceased": STATE_D
            }
            if placements_filter == []:
                x = np.arange((len(placements)+len(self.srv.all_services)))
            else:
                x = np.arange(len(placements_filter))
            names = []
            for name, plc in placements.items():
                if placements_filter == [] or plc in placements_filter:
                    names.append(name)
            for srvc in self.srv.all_services:
                if placements_filter == [] or srvc.placement in placements_filter:
                    names.append(srvc.name)
            # Collect data and draw bars
            cumm = np.zeros(x.shape)
            for state_name, state in states_names.items():
                if states_filter == [] or state in states_filter:
                    mask_state = states == state
                    data_workers = []
                    data_visitors = []
                    for _, plc in placements.items():
                        if placements_filter == [] or plc in placements_filter:
                            data_workers.append(0)
                            data_visitors.append(np.sum(placement[mask_state] == plc))
                    for srvc in self.srv.all_services:
                        if placements_filter == [] or srvc.placement in placements_filter:
                            data_workers.append(np.sum(placement[mask_state & srvc.mask_workers] == srvc.placement))
                            data_visitors.append(np.sum(placement[mask_state & ~srvc.mask_workers] == srvc.placement))
                    data_workers = np.array(data_workers)
                    data_visitors = np.array(data_visitors)
                    plt.bar(x, data_workers, bottom=cumm, width=0.5, color=COLORS_STATES_DEEP[state], label="{} workers".format(state_name))
                    cumm += data_workers
                    plt.bar(x, data_visitors, bottom=cumm, width=0.5, color=COLORS_STATES[state], label=state_name)
                    cumm += data_visitors
            cumm = np.zeros(x.shape)
            pos_delta = 0.3
            # Collect data and draw numbers
            for _, state in states_names.items():
                if states_filter == [] or state in states_filter:
                    mask_state = states == state
                    data_workers = []
                    data_visitors = []
                    for _, plc in placements.items():
                        if placements_filter == [] or plc in placements_filter:
                            data_workers.append(0)
                            data_visitors.append(np.sum(placement[mask_state] == plc))
                    for srvc in self.srv.all_services:
                        if placements_filter == [] or srvc.placement in placements_filter:
                            data_workers.append(np.sum(placement[mask_state & srvc.mask_workers] == srvc.placement))
                            data_visitors.append(np.sum(placement[mask_state & ~srvc.mask_workers] == srvc.placement))
                    data_workers = np.array(data_workers)
                    data_visitors = np.array(data_visitors)
                    if pos_delta > 0:
                        ha = "left"
                        ha2 = "right"
                    else:
                        ha = "right"
                        ha2 = "left"
                    for i in range(len(x)):
                        if data_workers[i] > 0:
                            plt.text(x[i]+pos_delta, data_workers[i]+cumm[i], str(int(data_workers[i])), color=COLORS_STATES_DEEP[state], va='top', ha=ha)
                    cumm += data_workers
                    for i in range(len(x)):
                        if data_visitors[i] > 0:
                            plt.text(x[i]-pos_delta, data_visitors[i]+cumm[i], str(int(data_visitors[i])), color=COLORS_STATES[state], va='top', ha=ha2)
                    pos_delta = - pos_delta
                    cumm += data_visitors
            # Set plot proprieties
            plt.xticks(x, names, rotation=-10, horizontalalignment='left')
            plt.xlim(-0.5, len(x)-0.5)
            ax = fig.gca()
            ylim = ax.get_ylim()
            plt.ylim(ylim[0], ylim[1]*1.02)
            plt.legend()
            plt.tight_layout(pad=0.5)
        else:
            #Gather data
            state_tile = np.concatenate((states.reshape(Nparticles, 1), \
                            states.reshape(Nparticles, 1)), axis=1)                          # Duplicates the states array to the same shape of positions
            s_pos = np.ma.masked_where(state_tile != STATE_S, positions[:, :])           # Mask susceptible particles
            i_pos = np.ma.masked_where(state_tile != STATE_I, positions[:, :])           # Mask infected particles
            r_pos = np.ma.masked_where(state_tile != STATE_R, positions[:, :])           # Mask recovered particles
            d_pos = np.ma.masked_where(state_tile != STATE_D, positions[:, :])           # Mask recovered particles

            ax = fig.add_axes([0, 0, 1, 1])
            ax.set_xlim(0, 1)
            ax.set_ylim(0, 1)
            # Plot particles
            if (STATE_S in states_filter or not states_filter):                    # Plot Susceptible only if selected or no filter passed
                ax.scatter(s_pos[ :, 0], s_pos[ :, 1], s=3, color='blue')
            if (STATE_I in states_filter or not states_filter):                    # Plot Infected only if selected or no filter passed
                ax.scatter(i_pos[ :, 0], i_pos[ :, 1], s=3, color='red')
                ax.scatter(i_pos[ :, 0], i_pos[ :, 1], s=circle_area, color='red', facecolors='none')
            if (STATE_R in states_filter or not states_filter):                    # Plot Recovered only if selected or no filter passed
                ax.scatter(r_pos[ :, 0], r_pos[ :, 1], s=3, color='green')
            if (STATE_D in states_filter or not states_filter):                    # Plot Recovered only if selected or no filter passed
                ax.scatter(d_pos[ :, 0], d_pos[ :, 1], s=3, color='gray')
            # Plot services limits
            color_i = 0
            for srvc in self.srv.all_services:
                for i in range(len(srvc.limits)):
                    center = srvc.limits[i]
                    plt.plot([center[0], center[1], center[1], center[0], center[0]], \
                            [center[2], center[2], center[3], center[3], center[2]], \
                            color=COLORS_STATES_DEEP[color_i])
                    plt.figtext(center[0], center[3], srvc.name[0]+str(i+1), color=COLORS_STATES_DEEP[color_i])
                color_i = (color_i + 1)%len(COLORS_STATES_DEEP)
        plt.title(title)
        if filename == "":
            plt.show()
        else:
            plt.savefig(filename)


    def plot_net(self, step, home_id=None, placement=None, state=None, srvc=None,
                 srvc_instance=None, srvc_room=None, size=(5, 4)):
        if srvc != None:
            if type(srvc) == str:
                srvc = self.srv[srvc]
            is_srvc = True
        else:
            is_srvc = False
        G = nx.from_dict_of_lists(self.pop.get_tracing(step))
        pids = self.get_particles_pids(step, home_id=home_id, placement=placement, srvc=srvc,
                                       srvc_instance=srvc_instance, srvc_room=srvc_room)
        title = "Day: {:} Hour: {:}".format(int(step/24), step%24)
        colors = []
        edges = []
        nodes = []
        not_in_net = []
        for i in pids:
            try:
                for n in G.neighbors(i):
                    if int(n) in pids:
                        edges.append((i, n))
                nodes.append(i)
            except:
                not_in_net.append(i)
        if len(nodes) < self.pop.Nparticles:
            if len(nodes) > 10:
                title += " Pids: Many"
            else:
                title += " Pids: {:}".format(nodes)
        if not_in_net != []:
            print("{} particles not in tracing.".format(not_in_net))
        g = nx.from_edgelist(edges)
        for node in g:
            if is_srvc:
                guest = False
                try:
                    if node in srvc.guests_ids_ts[step]:
                        colors.append(COLORS['purple'])
                        guest = True
                except:
                    pass
                if node in srvc.workers_ids:
                    colors.append(COLORS['red'])
                elif not guest:
                    colors.append(COLORS['blue'])
        plt.figure(figsize=size)
        plt.title(title)
        if colors == []:
            nx.draw(g, nodes=nodes, with_labels=True, font_color="white", font_size=6, node_size=200)
        else:
            nx.draw(g, nodes=nodes, with_labels=True, font_color="white", font_size=6, node_size=200, node_color=colors)
        plt.show()
        return G