from ...settings import COLORS_LONG_LIST, COLORS
import numpy as np


def plot_adverse_effects(self, seeds=None, **plot_args):
    """Plots the percentage of naive particles (never infected) in time.

    !!! Important
        This plot uses the do_plot method, see [plot_args](#plot_args) to see all available arguments.

    Args:
        seeds (list, optional): List of seeds to plot, if None will plot all seeds available.
        **plot_args (kwargs): Arguments for the do_plot method (see [plot_args](#plot_args)).
    """
    # Prepare Data
    data = self.get_data(["adverse_effects_counts", "adverse_effects_names"], seeds)
    seeds = data["seeds"]
    counts = np.array(data["adverse_effects_counts"])
    names = data["adverse_effects_names"][0]
    Nparticles = self.parameters["Nparticles"]
    data = {name: 100 * counts[:, i, :] / Nparticles for i, name in enumerate(names)}
    Nsteps = self.parameters["Nsteps"]
    dt = self.parameters["dt"]
    t = dt * np.arange(Nsteps) / 24

    # Plot graphs
    def_plot_args = {
        "xlabel": "Days",
        "ylabel": "% with effect",
    }
    plot_args = self.args_parse(plot_args, def_plot_args)
    self.do_plot(t, dt, data, COLORS_LONG_LIST, seeds, **plot_args)


def plot_vaccinated_day(self, seeds=None, **plot_args):
    """Plots the percentage of particles vaccinated at a certain time.

    !!! Important
        This plot uses the do_plot method, see [plot_args](#plot_args) to see all available arguments.

    Args:
        seeds (list, optional): List of seeds to plot, if None will plot all seeds available.
        **plot_args (kwargs): Arguments for the do_plot method (see [plot_args](#plot_args)).
    """
    # Prepare Data
    data = self.get_data(["vaccinated_counts", "vaccines_names"], seeds)
    seeds = data["seeds"]
    vaccinated = np.array(data["vaccinated_counts"])
    names = data["vaccines_names"][0]
    Nparticles = self.parameters["Nparticles"]
    data = {
        name: vaccinated[:, :, i] * 100 / Nparticles
        for i, name in enumerate(["All"] + names)
    }
    Ndays = self.parameters["Ndays"]
    dt = self.parameters["dt"]
    t = np.arange(Ndays)

    # Plot graphs
    def_plot_args = {
        "xlabel": "Days",
        "ylabel": "% vaccinated particles",
    }
    plot_args = self.args_parse(plot_args, def_plot_args)
    self.do_plot(t, dt, data, COLORS_LONG_LIST, seeds, **plot_args)
