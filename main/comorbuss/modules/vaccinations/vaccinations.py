import numpy as np
from ... import settings as S
from ...tools import (
    filter_parse,
    function,
    efficacy_calc,
    init_distribution,
    recursive_copy,
)
from ...aux_classes import empty_module, harray, list_container, RandomDistibution
from numpy.random import default_rng
from . import functions
from ...triage.triage_functions import array_get, get_days_array

__name__ = "vaccinations"


VACC_NYET = -1
"""Marker for particles not vaccinated"""

FILTER_DIAG_NO = ("diag_states", "!=", S.DIAG_YES)
FILTER_SENIORS = ("ages", "isin", list(range(12, 22)))
FILTER_ADULTS = ("ages", "isin", list(range(5, 22)))
FILTER_CHILDRENS = ("ages", "isin", list(range(0, 5)))
FILTER_MARKET_WORKERS = ("workplace_id", "==", ("service", "Markets"))
FILTER_HOSPITALS_WORKERS = ("workplace_id", "==", ("service", "Hospitals"))

SENIORS = {
    "name": "Senior citizens",
    "filter": (FILTER_SENIORS, "&", FILTER_DIAG_NO),
}
"""Apply the default number of doses on senior citizens
    (age groups 12 to 21, 60+ years old)."""

ADULTS = {
    "name": "Adult citizens",
    "filter": (FILTER_ADULTS, "&", FILTER_DIAG_NO),
}
"""Apply the default number of doses on adults citizens
    (age groups 5 to 21, 20+ years old)."""

CHILDRENS = {
    "name": "Child citizens",
    "filter": (FILTER_CHILDRENS, "&", FILTER_DIAG_NO),
}
"""Apply the default number of doses on child citizens
    (age groups 0 to 4, 0-19 years old)."""

DIAG_NO = {
    "name": "Not diagnosed",
    "filter": FILTER_DIAG_NO,
}
"""Apply the default number of doses on citizens that where not diagnosed."""

MAKETS_WORKERS = {
    "name": "Markets workers",
    "filter": (FILTER_MARKET_WORKERS, "&", FILTER_DIAG_NO),
}
"""Apply the default number of doses on markets workers."""

HOSPITALS_WORKERS = {
    "name": "Hospitals workers",
    "filter": (FILTER_HOSPITALS_WORKERS, "&", FILTER_DIAG_NO),
}
"""Apply the default number of doses on hospitals workers."""

DEFAULTS = {
    "name": "Vaccination",
    "start_day": 0,
    "stop_day": -1,
    "effectiveness": {"inf_susceptibility": np.array(30 * [0] + [1])},
    "effectiveness_parameters": {},
    "adverse_effects": {},
    "filter": FILTER_DIAG_NO,
    "filter_parameters": {},
    "priority_function": functions.random_priority,
    "doses_per_day": 1000,
    "keep_last_value": True,
    "only_not_vaccinated": True,
    "vaccinate_at_start": False,
}

DEFAULTS_TYPES = {
    "name": str,
    "start_day": int,
    "stop_day": int,
    "effectiveness": dict,
    "filter": [tuple, function],
    "filter_parameters": dict,
    "priority_function": function,
    "effectiveness_parameters": dict,
    "adverse_effects": dict,
    "doses_per_day": int,
    "keep_last_value": bool,
    "only_not_vaccinated": bool,
    "vaccinate_at_start": bool,
}


def add_vaccination(vacc_param, comm, id=None):
    if hasattr(comm, __name__):
        return getattr(comm, __name__).add_vaccination(vacc_param, id)
    else:
        vacc_param = triage([vacc_param], comm.parameters, comm.clk, comm.event_log)
        comm.init_module(__name__, vacc_param)
        return getattr(comm, __name__)[0]


def effectiveness_parse(effectiveness, vacc_name, comm):
    parsed_effect = {}
    for key, value in effectiveness.items():
        if not hasattr(comm.pop.disease, key):
            comm.event_log(
                "Disease object has no attribute {}, {} vaccine will skip it.".format(
                    key, vacc_name
                ),
                S.MSG_ERROR,
            )
        if type(getattr(comm.pop.disease, key)) is not np.ndarray:
            comm.event_log(
                "{} disease attribute is not an np.ndarray and can not be used on {} vaccine.".format(
                    key, vacc_name
                ),
                S.MSG_ERROR,
            )
        try:
            parsed_effect[key] = np.array(value, dtype=float)
        except TypeError:
            try:
                parsed_effect[key] = function(value)
            except TypeError:
                comm.event_log(
                    "Effectiveness for disease.{} can not be converted to np.array and is not callable, {} vaccine will skip it.".format(
                        key, vacc_name
                    ),
                    S.MSG_ERROR,
                )
    if len(parsed_effect) == 0:
        comm.event_log(
            "Effectiveness for {} vaccine is empty, it will not run.".format(vacc_name)
        )
        return parsed_effect, False
    else:
        return parsed_effect, True


def parse_adverse_effects(effec_params, vacc_name, clk, event_log):
    new_effecs = {}
    for effec, param in effec_params.items():
        param = recursive_copy(param)
        new_param = {}
        new_param["probability"] = array_get(
            param,
            "probability",
            0,
            None,
            None,
            event_log,
            shape=np.array(S.ALL_AGES).shape,
            dtype=(float, RandomDistibution),
            allow_single_value=True,
            limits=(0.0, 1.0),
            print_name="probability for {} adverse effect for {}"
            " vaccination".format(effec, vacc_name),
        )
        if "delay" in param:
            new_param["delay"] = get_days_array(
                param,
                "delay",
                0,
                {"clk": clk},
                None,
                event_log,
                shape=np.array(S.ALL_AGES).shape,
                dtype=(float, RandomDistibution),
                allow_single_value=True,
                limits=(0.0,),
                print_name="delay for {} adverse effect for {}"
                " vaccination".format(effec, vacc_name),
            )
        new_effecs[effec] = new_param
        if len(param) > 0:
            event_log(
                "{} parameters where passed for {} adverse effect for {} vaccination"
                " and where not used.".format(param.keys(), effec, vacc_name),
                S.MSG_WRNG,
            )
    return new_effecs


def triage(vaccinations, sim_parameters, clk, event_log):
    """Parses vaccinations parameters.

    Args:
        vaccinations (list):  List of vaccinations parameters.
        sim_parameters (dict): Parsed simulation parameters.
        clk (clock): Clock object.
        event_log (function): Event_log function.

    Returns:
        list: List of parsed vaccinations parameters.
    """
    from ...triage.triage_functions import parse_from_defaults, merge_list_of_dicts

    vaccinations = merge_list_of_dicts(vaccinations, "vaccinations", event_log)

    for vacc in vaccinations:
        parse_from_defaults(vacc, "vaccination", DEFAULTS, DEFAULTS_TYPES, event_log)

        vacc["doses_per_day"] = int(
            np.max([vacc["doses_per_day"] / sim_parameters["norm_N"], 1])
        )
        vacc["adverse_effects"] = parse_adverse_effects(
            vacc["adverse_effects"], vacc["name"], clk, event_log
        )

    return vaccinations


class module(empty_module, list_container):
    """Vaccination module, initialize and store all vaccination objects.

    Attibutes:
        vaccinations (list): List of vacination objects
    """

    def __init__(self, vacc_params, comm):
        """Initializes vaccination module.

        Args:
            vacc_params (list): List of parsed vaccinations parameters.
            comm (comorbuss.community): Community object
        """
        super(module, self).__init__(vacc_params, comm, name=__name__)

        self.pop.add_attribute(
            "particles_coefficients",
            {},
            store=comm.parameters["store_immunity_coefficients"],
        )

        # Initialize vaccinations
        self.vaccinations = []
        for i, vacc_param in enumerate(vacc_params):
            self.init_vacc(vacc_param, i + 1)

        for vaccination in self.vaccinations:
            setattr(self, vaccination.name, vaccination)

        # Check if module has valid vaccination configuration.
        self.loaded = False
        self.check_loaded()
        self.list = self.vaccinations

    def check_loaded(self):
        if not self.loaded:
            # Check if module has valid vaccination configuration.
            self.loaded = len(self.vaccinations) > 0

            if self.loaded:
                self.scenario_id = "Vac"
                # Add vaccination related population parameters
                self.pop.add_attribute(
                    "time_vaccinated",
                    harray(-1 * np.ones(self.pop.Nparticles), buffer_limit=10),
                    store=True,
                )
                self.pop.add_attribute(
                    "immunity_state",
                    harray(VACC_NYET * np.ones(self.pop.Nparticles, dtype=int)),
                    store=True,
                )
                self.pop.add_attribute(
                    "vaccinated_counts",
                    np.zeros((self.comm.Ndays, len(self.vaccinations) + 1)),
                    store=True,
                )
                self.pop.add_attribute(
                    "vaccines_names",
                    [vacc.name for vacc in self.vaccinations],
                    store=True,
                )

                # Add vaccination module to execution list
                self.comm.add_to_execution(self.run, priority=27, time_of_the_day=0)
            else:
                self.event_log(
                    "Vaccination module was loaded but no valid vaccination parameters where given.",
                    S.MSG_WRNG,
                )
                self.run = lambda *_: None
                self.log_results = lambda *_: None
        else:
            self.pop.vaccinated_counts = np.zeros(
                (self.comm.Ndays, len(self.vaccinations) + 1)
            )
            self.pop.vaccines_names = [vacc.name for vacc in self.vaccinations]

    def init_vacc(self, vacc_param, i):
        """Initialize vaccination policies.

        Args:
            vacc_param (dict): Parameters for the module.
            i (community): Vaccination id.

        Returns:
            List: List of the vaccination objects.
        """
        vacc_param["effectiveness"], eff_ok = effectiveness_parse(
            vacc_param["effectiveness"], vacc_param["name"], self.comm
        )
        vacc_param["filter"], fok = filter_parse(
            vacc_param["filter"],
            self.comm,
            allowed_filters=["tracing", "service", "module", "workers"],
            allow_function=True,
        )
        vacc_param["id"] = i
        if eff_ok and fok:
            vacc = vaccination(vacc_param, self.comm, self)
            self.vaccinations.append(vacc)
            return vacc, eff_ok and fok
        else:
            self.comm.event_log(
                "Can't activate {} vaccination protocol, there is an invalid filter or effectiveness.".format(
                    vacc_param["name"]
                ),
                S.MSG_WRNG,
            )

    def add_vaccination(self, vacc_param, id=None):
        vacc_param = triage(
            [vacc_param], self.comm.parameters, self.clk, self.event_log
        )
        i = id or len(self.vaccinations) + 1
        vacc, ok = self.init_vacc(vacc_param[0], i)
        if ok:
            self.check_loaded()
            return vacc
        else:
            return None

    def __iter__(self):
        return self.vaccinations.__iter__()

    def run(self, at_start=False):
        """Runs the vaccination campaigns set on self.vaccinations."""
        for vacc in self.vaccinations:
            vacc(at_start=at_start)

        self.count_vaccinated()
        self.update_disease_coeficients()

    def update_disease_coeficients(self):
        """Update coefficients for each affected disease attribute."""
        for attr in self.pop.particles_coefficients:
            self.pop.disease.set_coefficient(
                attr,
                self.pop.particles_coefficients[attr][self.clk.today, :],
                "vaccinations",
            )

    def count_adverse_effects(self):
        """Updates the counts of particles with each adverse effect."""
        self.pop.adverse_effects_counts[:, self.clk.step] = np.count_nonzero(
            self.pop.adverse_effects != S.NO_INFEC, axis=1
        )

    def count_vaccinated(self):
        """Updates the counts of particles vaccinated each day."""
        for vacc in self.vaccinations:
            i = vacc.id if not vacc.name == "Natural immunity" else -1
            self.pop.vaccinated_counts[self.clk.today, i] += np.count_nonzero(
                (self.pop.immunity_state == vacc.id)
                & (self.pop.time_vaccinated == self.clk.step)
            )
        self.pop.vaccinated_counts[self.clk.today, 0] += np.count_nonzero(
            self.pop.time_vaccinated == self.clk.step
        )

    def run_after_init(self):
        self.run(at_start=True)

    def log_results(self):
        """Aggregate vaccinations related results at the end of the simulation."""
        nvac = np.sum(self.pop.time_vaccinated >= 0)
        self.event_log("Total number vaccinated: {:}".format(nvac), S.MSG_RSLT)
        calculate_efficacy = nvac < self.pop.Nparticles
        if calculate_efficacy:
            self.comm.add_to_store("efficacy", "comm.vaccinations.efficacy", False)
            self.efficacy = []
        else:
            self.event_log(
                "Can't calculate efficacy of vaccinations, there is no unvaccinated particles.",
                S.MSG_RSLT,
            )
        for vacc in self.vaccinations:
            nvac = np.sum(vacc.mask_vaccinated)
            self.event_log('"{}" vaccination: {:}'.format(vacc.name, nvac), S.MSG_RSLT)
            if calculate_efficacy:
                efficacy, limits = vacc.calculate_efficacy()
                self.efficacy.append((efficacy, limits))
                self.event_log(
                    '"{}" efficacy: {:.1f}%'.format(vacc.name, 100 * efficacy),
                    S.MSG_RSLT,
                )


class vaccination:
    """This class stores the attributes for a vaccination policy and the
        methods for its progression.

    Attributes:
        name (str): Name of the vaccination.
        id (int): Id of the vaccination (same as immunity_state).
        filter (str): Parsed filter to select particles to vaccinate.
        start_day (int): Day to start vaccination.
        time_to_immunity (float): Time from the vaccine application to immunity.
        doses_per_day (int): Number of doses to be applied per day.
    """

    def __init__(self, vacc_param, comm, vacc_module):
        """Initialize a quarantine policy.

        Args:
            vacc_param (dict): Parameters related to the vaccination.
            comm (community): The community object.
            randgen (Generator): Random number generator used in vaccinations.
        """
        # Parent classes pointers
        self.comm = comm
        self.pop = comm.pop
        self.clk = comm.clk
        self.vacc_module = vacc_module
        self.event_log = comm.event_log

        # Random number generator
        self.rng = default_rng(comm.random_seed)

        # Vaccination identification
        self.name = vacc_param["name"]
        self.id = vacc_param["id"]

        # Dates to start and stop applying doses
        self.start_day = vacc_param["start_day"]
        self.stop_day = vacc_param["stop_day"]
        if self.stop_day < 0:
            self.stop_day = S.inf
        self.doses_per_day = vacc_param["doses_per_day"]
        # Filter to select particles
        self.filter = vacc_param["filter"]
        self.filter_parameters = vacc_param["filter_parameters"]
        # Priority function to select particles to apply doses
        self.priority = vacc_param["priority_function"]
        # Select if this vaccine only applies to not vaccinated or not
        self.only_nyet = vacc_param["only_not_vaccinated"]
        self.vaccinate_at_start = vacc_param["vaccinate_at_start"]

        # Initialize disease coefficients
        self.effectiveness = vacc_param["effectiveness"]
        self.effectiveness_parameters = vacc_param["effectiveness_parameters"]
        # Initialize coefficients for all affected disease attribute
        for attr in self.effectiveness:
            try:
                len(self.effectiveness[attr])
            except TypeError:
                self.effectiveness[attr] = [self.effectiveness[attr]]
            if not attr in self.pop.particles_coefficients:
                # Create a coefficient for this disease attribute and this vaccination
                self.pop.disease.add_coefficient(
                    attr, np.ones(self.pop.Nparticles), "vaccinations"
                )
                # Create a local array to store coefficients curves
                self.pop.particles_coefficients[attr] = np.ones(
                    (self.comm.Ndays, self.pop.Nparticles)
                )
        # Select value to be used after the end of the curve
        self.keep_last = vacc_param["keep_last_value"]

        self.mask_vaccinated = np.zeros(self.pop.Nparticles, dtype=bool)
        self.time_vaccinated = np.zeros(self.pop.Nparticles, dtype=int)

        self.init_effects(vacc_param["adverse_effects"])

    def __repr__(self):
        return "<{} vaccination>".format(self.name)

    def __call__(self, *args, **kwargs):
        """Runs methods related to this vaccination."""
        self.vaccinate(*args, **kwargs)

    def calculate_efficacy(self):
        # Count cases
        mask_not_vacc = self.pop.immunity_state == VACC_NYET
        mask_vacc = self.pop.immunity_state == self.id
        mask_challenged = np.sum(self.pop.challenges, axis=0) > 0
        mask_infected_vacc = self.pop.time_exposed > self.pop.time_vaccinated
        mask_infected_not_vacc = self.pop.time_exposed >= 0
        cases_vacc = np.sum(
            (self.pop.time_exposed - self.pop.time_vaccinated)[
                mask_vacc & mask_infected_vacc
            ]
        )
        cases_not_vacc = np.sum(
            self.pop.time_exposed[mask_not_vacc & mask_infected_not_vacc]
        )
        not_cases_vacc = np.sum(
            self.comm.Nsteps
            - self.pop.time_vaccinated[
                mask_vacc & ~mask_infected_vacc & mask_challenged
            ]
        )
        not_cases_not_vacc = (
            np.count_nonzero(mask_not_vacc & ~mask_infected_not_vacc & mask_challenged)
            * self.comm.Nsteps
        )

        # Calculate efficacy
        return efficacy_calc(
            cases_vacc,
            not_cases_vacc,
            cases_not_vacc,
            not_cases_not_vacc,
            return_limits=True,
        )

    def eval_filter(self):
        """Evaluate the vaccine filter."""
        try:
            if callable(self.filter):
                return self.filter(self, self.comm, self.rng)
            else:
                return eval(self.filter)
        except:
            import traceback, sys

            print("Previous traceback:")
            for line in traceback.format_exception(*sys.exc_info()):
                print(line)
            raise RuntimeError(
                "Error evaluating filter for {} vaccination, parsed filter is: {}".format(
                    self.name.lower(), self.filter
                )
            )

    def init_effects(self, effects):
        for name, params in effects.items():
            self.add_effect(name, params)

    def add_effect(self, name, params):
        """Register and creates masks related to an adverse effect.

        Args:
            name (str): Name of the adverse effect.
        """
        # Create general population attributes related to adverse effects
        if not hasattr(self.pop, "adverse_effects"):
            self.pop.add_attribute(
                "adverse_effects",
                S.NO_INFEC * np.ones((1, self.pop.Nparticles), dtype=int),
            )
            self.pop.add_attribute(
                "adverse_effects_counts",
                np.zeros((1, self.comm.Nsteps), dtype=int),
                store=True,
            )
            self.pop.add_attribute(
                "adverse_effects_names",
                [name],
                store=True,
            )
            self.comm.add_to_execution(
                self.vacc_module.count_adverse_effects, priority=101
            )
        # Updates general population attributes related to adverse effects
        else:
            self.pop.adverse_effects = np.pad(
                self.pop.adverse_effects, ((0, 1), (0, 0)), constant_values=S.NO_INFEC
            )
            self.pop.adverse_effects_counts = np.pad(
                self.pop.adverse_effects_counts, ((0, 1), (0, 0))
            )
            self.pop.adverse_effects_names.append(name)
        # Create internal attributes related to adverse effects
        if not hasattr(self, "effects"):
            self.effects = {name: params}
            self.effects_delay = np.zeros((1, self.pop.Nparticles), dtype=int)
            self.effects_names = [name]
            self.mask_effects = np.zeros((1, self.pop.Nparticles), dtype=bool)
            self.comm.add_to_execution(self.adverse_effects, priority=27.1)
        # Updates internal attributes related to adverse effects
        else:
            self.effects[name] = params
            self.effects_delay = np.pad(
                self.effects_delay, ((0, 1), (0, 0)), constant_values=0
            )
            self.effects_names.append(name)
            self.mask_effects = np.pad(self.mask_effects, ((0, 1), (0, 0)))
        self.update_effect(name)

    def update_effect(self, effect_name, mask=()):
        i = self.get_effect_index(effect_name)
        # Update mask for getting effect
        probability = init_distribution(
            self.effects[effect_name]["probability"],
            "probability for {} adverse effect for {} vaccination".format(
                effect_name, self.name
            ),
            S.DIST_CONST,
            self.pop,
            self.rng,
            self.event_log,
        )
        prob = self.rng.uniform(0.0, 1.0, size=self.pop.Nparticles)
        self.mask_effects[i][mask] = (prob < probability)[mask]
        # Update effect delay
        if "delay" in self.effects[effect_name]:
            self.effects_delay[i][mask] = init_distribution(
                self.effects[effect_name]["delay"],
                "delay for {} adverse effect for {} vaccination".format(
                    effect_name, self.name
                ),
                S.DIST_EXP,
                self.pop,
                self.rng,
                self.event_log,
            )[mask]

    def apply_effect(self, effect_name, mask):
        """Applies an adverse effect to a mask of particles.

        Args:
            effect_name (str): Name of the adverse effect.
            mask (np.ndarray): Boolean mask of particles to apply effect.
        """
        i = self.get_effect_index(effect_name, pop_index=True)
        mask_no_effect = self.pop.adverse_effects[i] == S.NO_INFEC
        self.pop.adverse_effects[i, mask & mask_no_effect] = self.id
        self.pop.update_inf_tree_attributes(mask, "adverse_effect", effect_name)
        self.pop.update_inf_tree_attributes(mask, "time_adverse_effect", self.clk.time)
        self.pop.update_inf_tree_attributes(mask, "adverse_effect_vaccine_id", self.id)

    def get_effect_index(self, effect_name, pop_index=False):
        if pop_index:
            names = self.pop.adverse_effects_names
        else:
            names = self.effects_names
        for i, name in enumerate(names):
            if name == effect_name:
                return i
        raise RuntimeError("Tried to get a not registered immunity adverse effect.")

    def adverse_effects(self):
        for i, effect in enumerate(self.effects_names):
            mask_to_effect = (
                self.mask_vaccinated
                & self.mask_effects[i]
                & self.clk.at_time_array(self.time_vaccinated + self.effects_delay[i])
            )
            self.apply_effect(effect, mask_to_effect)

    def select_to_vaccinate(self):
        """Select particles to vaccinate on one day using the filter.

        Args:
            np.ndarray: Mask of particles to be vaccinated.
        """
        # Select particles not vaccinated that mach the given filter
        if self.only_nyet:
            mask_not_vac = self.pop.immunity_state == VACC_NYET
            mask_available = mask_not_vac & self.eval_filter()
        else:
            mask_available = self.eval_filter()
        available = self.pop.pid[mask_available]
        # From those particles select the ones that will be vaccinated
        doses = int(np.min([len(available), self.doses_per_day]))
        chosen_to_vac = self.priority(available, self.rng, self, self.pop, self.comm)[
            :doses
        ]
        # Return masks
        mask_to_vac = np.isin(self.pop.pid, chosen_to_vac)
        return mask_to_vac

    def update_times(self, mask_to_vac):
        """Update times when particles where vaccinated.

        Args:
            mask_to_vac (np.ndarray): Mask of particles to be vaccinated.
        """
        # Update times
        self.pop.time_vaccinated[mask_to_vac] = self.clk.time
        self.pop.update_inf_tree_attributes(
            mask_to_vac, "time_vaccinated", self.clk.time
        )
        self.pop.update_inf_tree_attributes(mask_to_vac, "vaccine_id", self.id)
        self.pop.update_inf_tree_attributes(mask_to_vac, "vaccine_name", self.name)
        # Update immunity_state
        self.pop.immunity_state[mask_to_vac] = self.id
        # Update internal control
        self.mask_vaccinated[mask_to_vac] = True
        self.time_vaccinated[mask_to_vac] = self.clk.time

    def generate_immunity_curves(self, mask_to_vac):
        """Generate coefficients curves for all affected disease attributes.

        Args:
            mask_to_vac (np.ndarray): Mask of particles to be vaccinated.
        """
        # Generate coefficients curves for all affected disease attributes
        for pid in self.pop.pid[mask_to_vac]:
            for attr in self.effectiveness:
                # Generate or load new curve
                if type(self.effectiveness[attr]) is list:
                    if callable(self.effectiveness[attr][0]):
                        self.effectiveness[attr] = self.effectiveness[attr][0]
                if callable(self.effectiveness[attr]):
                    particle_curve = 1 - self.effectiveness[attr](
                        pid, self, self.comm, self.rng
                    )
                else:
                    particle_curve = 1 - np.copy(self.effectiveness[attr])
                # Check if particle already has an immunity curve
                if self.pop.immunity_state[pid] != VACC_NYET:
                    step_vacc = self.clk.time_to_steps(self.pop.time_vaccinated[pid])
                    old_particle_curve = self.pop.particles_coefficients[attr][
                        step_vacc:, pid
                    ]
                    new_curves = np.ones(
                        (2, np.max([len(particle_curve), len(old_particle_curve)]))
                    )
                    new_curves[0, : len(particle_curve)] = particle_curve
                    if self.keep_last:
                        new_curves[0, len(particle_curve) :] = particle_curve[-1]
                    new_curves[1, : len(old_particle_curve)] = old_particle_curve
                    # Merge new and old curve
                    particle_curve = np.min(new_curves, axis=0)
                # Stores new coefficient curves from today
                today = self.clk.today
                len_curve = len(particle_curve)
                last_day = np.min([today + len_curve, self.comm.Ndays])
                n_days = last_day - today
                self.pop.particles_coefficients[attr][
                    today:last_day, pid
                ] = particle_curve[:n_days]
                # Select value for after the end of the curve
                if n_days == len_curve:
                    if self.keep_last:
                        self.pop.particles_coefficients[attr][
                            (self.clk.today + len(particle_curve)) :, pid
                        ] = particle_curve[-1]
                    else:
                        self.pop.particles_coefficients[attr][
                            (self.clk.today + len(particle_curve)) :, pid
                        ] = 1.0

    def apply_vaccine(self, mask_to_vac):
        """Applies vaccine to selected particles.

        Args:
            mask_to_vac (np.ndarray): Mask of particles to be vaccinated.
        """
        self.generate_immunity_curves(mask_to_vac)
        self.update_times(mask_to_vac)

    def get_vaccinated(self, return_days_count=False):
        """Get vaccinated particles.

        Args:
            return_days_count (bool, optional): Return an array with the days since the particle where vaccinated. Defaults to False.

        Returns:
            np.array: Vaccinated pids.
        """
        mask_vaccinated = (self.pop.time_vaccinated >= 0) & (
            self.pop.immunity_state == self.id
        )
        if return_days_count:
            return (
                self.pop.pid[mask_vaccinated],
                self.clk.days_since(
                    self.pop.time_vaccinated[mask_vaccinated], truncate=True
                ),
            )
        else:
            return self.pop.pid[mask_vaccinated]

    def vaccinate(self, at_start=False):
        """Vaccinate particles."""
        if self.clk.today >= self.start_day or (at_start and self.vaccinate_at_start):
            if self.clk.today < self.stop_day or (at_start and self.vaccinate_at_start):
                # Select particles and vaccinate
                mask_to_vac = self.select_to_vaccinate()
                self.generate_immunity_curves(mask_to_vac)
                self.update_times(mask_to_vac)
