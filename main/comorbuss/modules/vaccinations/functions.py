def random_priority(x, rng, *_):
    return rng.permutation(x)


def priority_ages(pids, rng, vacc, pop, comm):
    ordered_pids = []
    for age in np.sort(np.unique(pop.ages[pids]))[::-1]:
        mask_age = np.isin(pop.pid, pids) & (pop.ages == age)
        ordered_pids += [pid for pid in rng.permutation(pop.pid[mask_age])]
    return np.array(ordered_pids)


def linear_decay(pid, vacc, comm, rng):
    from ...aux_classes import RandomDistibution
    import numpy as np

    par = vacc.effectiveness_parameters
    v0 = par["initial_value"]
    if isinstance(v0, RandomDistibution):
        v0 = v0.sample(rng=rng)
    a = par["decay_factor"]
    if isinstance(a, RandomDistibution):
        a = a.sample(rng=rng)
    shift = par.get("shift", 0)
    if isinstance(shift, RandomDistibution):
        shift = int(shift.sample(rng=rng))
    a = np.abs(a)
    effectiveness = [v0]
    while effectiveness[-1] >= 0:
        effectiveness.append(effectiveness[-1] - a)
    effectiveness[-1] = 0
    return np.array(effectiveness)[shift:]


def random_filter(vacc, comm, rng):
    import numpy as np

    par = vacc.filter_parameters
    mask_allow = np.ones(comm.pop.Nparticles, dtype=bool)

    # Remove particles vaccinated with vaccines not in allow_retake
    allow_retake = par["allow_retake"] + ["Natural immunity"]
    for vac in comm.vaccinations:
        if vac.name not in allow_retake:
            mask_allow[vac.mask_vaccinated] = False

    if "number" in par:
        pids = comm.pop.pid[mask_allow]
        selected = rng.choice(pids, size=par["number"], replace=True)
        return np.isin(comm.pop.pid, selected)
    else:
        return mask_allow
