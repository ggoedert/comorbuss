import os

# Load all modules in this folder
modules_path = os.path.split(__file__)[0]
__all__ = [folder for folder in os.listdir(modules_path) if folder[:2] != "__"]

# Sequence to initialize modules
MODULES_INIT_SEQ = [
    "diagnostics",
    "transport",
    "vaccinations",
    "vaccination_trial",
    "reinfection",
    "school_monitoring",
    "visualizer",
    "print_day",
]

# Add modules not in MODULES_INIT_SEQ to it
for module in __all__:
    if module not in MODULES_INIT_SEQ:
        MODULES_INIT_SEQ.append(module)
