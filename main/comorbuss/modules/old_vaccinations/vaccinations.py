import numpy as np
from ... import settings as S
from ...tools import filter_parse
from ...aux_classes import empty_module, harray

__name__ = "vaccinations"

VACC_NYET = -1
"""Marker for particles not vaccinated"""


def triage(vaccinations, sim_parameters, clk, event_log):
    """Parses vaccinations parameters.

    Args:
        vaccinations (list):  List of vaccinations parameters.
        sim_parameters (dict): Parsed simulation parameters.
        clk (clock): Clock object.
        event_log (function): Event_log function.

    Returns:
        list: List of parsed vaccinations parameters.
    """
    from ...triage.triage_functions import parse_from_defaults, merge_list_of_dicts

    vaccinations = merge_list_of_dicts(vaccinations, "vaccinations", event_log)

    for vacc in vaccinations:
        parse_from_defaults(vacc, "vaccination", DEFAULTS, DEFAULTS_TYPES, event_log)

        vacc["time_to_immunity"] = clk.days_to_time(vacc["days_to_immunity"])
        del vacc["days_to_immunity"]
        if (
            np.isscalar(vacc["time_to_immunity"])
            or len(vacc["time_to_immunity"].shape) == 0
        ):
            vacc["time_to_immunity"] = np.array([vacc["time_to_immunity"]])

        vacc["doses_per_day"] = int(
            np.max([vacc["doses_per_day"] / sim_parameters["norm_N"], 1])
        )

        if np.isscalar(vacc["effectiveness"]) or len(vacc["effectiveness"].shape) == 0:
            vacc["effectiveness"] = np.array([vacc["effectiveness"]])
        for i in range(len(vacc["effectiveness"])):
            vacc["effectiveness"][i] = np.min([vacc["effectiveness"][i], 1])

        if len(vacc["effectiveness"]) != len(vacc["time_to_immunity"]):
            event_log(
                "The effectiveness and time_to_immunity for {} vaccination have different shapes.".format(
                    vacc["name"]
                ),
                S.MSG_WRNG,
            )
            length = int(
                np.min([len(vacc["effectiveness"]), len(vacc["time_to_immunity"])])
            )
            vacc["time_to_immunity"] = vacc["time_to_immunity"][:length]
            vacc["effectiveness"] = vacc["effectiveness"][:length]
    return vaccinations


class module(empty_module):
    """Vaccination module, initialize and store all vaccination objects.

    Attibutes:
        vaccinations (list): List of vacination objects
    """

    def __init__(self, vacc_params, comm):
        """Initializes vaccination module.

        Args:
            vacc_params (list): List of parsed vaccinations parameters.
            comm (comorbuss.community): Community object
        """
        super(module, self).__init__(vacc_params, comm, name=__name__)

        # Check if module hass valid vaccination configuration.
        self.loaded = len(vacc_params) > 0
        if self.loaded:
            self.scenario_id = "Vac"
            # Add vaccination related population parameters
            self.pop.add_attribute(
                "time_vaccinated", harray(-1 * np.ones(self.pop.Nparticles), store=True)
            )
            self.pop.add_attribute(
                "immunity_state",
                harray(VACC_NYET * np.ones(self.pop.Nparticles, dtype=int)),
                store=True,
            )

            # Initialize vaccinations
            self.vaccinations = self.init_vaccinations(vacc_params, comm)
            for vaccination in self.vaccinations:
                setattr(self, vaccination.name, vaccination)

            # Add vaccination module to execution list
            self.comm.add_to_execution(
                self.run, priority=27, time_of_the_day=self.comm.free_hours[0]
            )
        else:
            self.event_log(
                "Vaccination module was loaded but no valid vaccination parameters where given.",
                S.MSG_WRNG,
            )
            self.run = lambda *_: None
            self.log_results = lambda *_: None

    def init_vaccinations(self, vacc_params, comm):
        """Initialize vaccination policies.

        Args:
            vacc_params (dict): Parameters for the module.
            comm (community): The community object.

        Returns:
            List: List of the vaccination objects.
        """
        randgenvacc = self.rng
        vaccinations = []
        for i, vacc_param in enumerate(vacc_params):
            vacc_param["filter"], fok = filter_parse(
                vacc_param["filter"],
                comm,
                allowed_filters=["tracing", "service", "module", "workers"],
            )
            vacc_param["id"] = i + 1
            if fok:
                vaccinations.append(vaccination(vacc_param, comm, randgenvacc))
            else:
                comm.event_log(
                    "Can't activate {} vaccination protocol, there is an invalid filter.".format(
                        vacc_param["name"]
                    ),
                    S.MSG_WRNG,
                )
        return vaccinations

    def __iter__(self):
        return self.vaccinations.__iter__()

    def run(self):
        """Runs the vaccination campaigns set on self.vaccinations."""
        for vacc in self.vaccinations:
            vacc()

    def log_results(self):
        """Aggregate vaccinations related results at the end of the simulation."""
        nvac = np.sum(self.pop.time_vaccinated > 0)
        self.event_log("Total number vaccinated: {:}".format(nvac), S.MSG_RSLT)
        mask_immune = np.zeros(self.pop.Nparticles, dtype=bool)
        for vacc in self.vaccinations:
            nvac = np.sum(self.pop.immunity_state == vacc.id)
            self.event_log("{} vaccination: {:}".format(vacc.name, nvac), S.MSG_RSLT)
            mask_immune |= vacc.immunity_state > 0
        nimmune = np.count_nonzero(mask_immune)
        self.event_log("Number immune on the last day: {:}".format(nimmune), S.MSG_RSLT)


class vaccination:
    """This class stores the attributes for a vaccination policy and the
        methods for its progression.

    Attributes:
        name (str): Name of the vaccination.
        id (int): Id of the vaccination (same as immunity_state).
        filter (str): Parsed filter to select particles to vaccinate.
        start_day (int): Day to start vaccination.
        time_to_immunity (float): Time from the vaccine application to immunity.
        doses_per_day (int): Number of doses to be applied per day.
    """

    def __init__(self, vacc_param, comm, randgen):
        """Initialize a quarantine policy.

        Args:
            vacc_param (dict): Parameters related to the vaccination.
            comm (community): The community object.
            randgen (Generator): Random number generator used in vaccinations.
        """
        self.comm = comm
        self.pop = comm.pop
        self.clk = comm.clk
        self.randgenvacc = randgen
        self.event_log = comm.event_log
        self.name = vacc_param["name"]
        self.id = vacc_param["id"]
        self.start_day = vacc_param["start_day"]
        self.stop_day = vacc_param["stop_day"]
        if self.stop_day < 0:
            self.stop_day = S.inf
        self.time_to_immunity = vacc_param["time_to_immunity"]
        self.effectiveness = vacc_param["effectiveness"]
        # effec is used on imperfect vaccines with step effectivenes
        self.effec = np.copy(self.effectiveness)
        self.effec[0] = 1 - self.effectiveness[0]
        if len(self.effectiveness) > 0:
            for i in range(1, len(self.effec)):
                self.effec[i] = (1 - self.effectiveness[i]) / (
                    1 - self.effectiveness[i - 1]
                )
        self.filter = vacc_param["filter"]
        self.doses_per_day = vacc_param["doses_per_day"]
        # immunity_state is used to store in which step of effectiveness each particle is
        self.immunity_state = -1 * np.ones(self.comm.pop.Nparticles, dtype=int)
        vaccines = {
            VACCINE_0_1: self.vaccinate_0_1,
            VACCINE_IMPERFECT: self.vaccinate_imperfect,
        }
        try:
            self.vaccinate = vaccines[vacc_param["type"]]
        except:
            raise RuntimeError("{} invalid type of vaccine.".format(vacc_param["type"]))
        if vacc_param["type"] == VACCINE_0_1 and len(self.effectiveness) > 1:
            self.event_log(
                "Vaccine 0 1 only accepts one value for effectiveness.", S.MSG_WRNG
            )

        if vacc_param["immunize_at_init"]:
            self.time_to_immunity = [0]
            self.effec = [1 - self.effectiveness[-1]]
            self.doses_per_day = self.comm.Nparticles
            self.vaccinate()

    def __repr__(self):
        return "<{} vaccination>".format(self.name)

    def __call__(self):
        """Runs methods related to this vaccination."""
        self.vaccinate()

    def eval_filter(self):
        try:
            return eval(self.filter)
        except:
            import traceback, sys

            print("Previous traceback:")
            for line in traceback.format_exception(*sys.exc_info()):
                print(line)
            raise RuntimeError(
                "Error evaluating filter for {} vaccination, parsed filter is: {}".format(
                    self.name.lower(), self.filter
                )
            )

    def select_to_immunize(self, time_to_immunity_index=0):
        """Select particles to immunize on VACCINE_IMPERFECT for each step.

        Args:
            time_to_immunity_index (int, optional): Step of effectiveness to select. Defaults to 0.

        Returns:
            np.array: Mask of particles to immunize.
        """
        time_to_immunity = self.time_to_immunity[time_to_immunity_index]
        return (
            (self.pop.time_vaccinated >= 0)
            & (self.clk.time >= self.pop.time_vaccinated + time_to_immunity)
            & (self.immunity_state < time_to_immunity_index)
            & (self.pop.immunity_state == self.id)
        )

    def select_to_vaccinate(self):
        """Select particles to vaccinate on one day using the filter.

        Returns:
            np.array, np.array: mask_to_vac, mask_to_immunize
        """
        # Select particles not vaccinated that mach the given filter
        mask_not_vac = self.pop.immunity_state == VACC_NYET
        mask_available = mask_not_vac & self.eval_filter()
        available = self.pop.pid[mask_available]
        # From those particles select the ones that will be vaccinated
        doses = int(np.min([len(available), self.doses_per_day]))
        chosen_to_vac = self.randgenvacc.choice(available, doses, replace=False)
        # chosen_to_immunize is only used on VACCINE_0_1
        chosen_to_immunize = self.randgenvacc.choice(
            chosen_to_vac, int(doses * self.effectiveness[0]), replace=False
        )
        # Return masks
        mask_to_vac = np.isin(self.pop.pid, chosen_to_vac)
        mask_to_immunize = np.isin(self.pop.pid, chosen_to_immunize)
        return mask_to_vac, mask_to_immunize

    def update_times(self, mask_to_vac):
        """Update times when particles where vaccinated.

        Args:
            mask_to_vac (np.ndarray): Mask with vaccinated particles.
        """
        self.pop.time_vaccinated[mask_to_vac] = self.clk.time
        self.pop.update_inf_tree_attributes(
            mask_to_vac, "time_vaccinated", self.clk.time
        )
        self.pop.update_inf_tree_attributes(mask_to_vac, "vaccine_id", self.id)
        self.pop.immunity_state[mask_to_vac] = self.id

    def vaccinate_imperfect(self):
        """Vaccinate particles for one day with `VACCINE_IMPERFECT`."""
        if self.clk.today >= self.start_day:
            if self.clk.today < self.stop_day:
                # Select particles and vaccinate
                mask_to_vac, _ = self.select_to_vaccinate()
                self.update_times(mask_to_vac)

            # Immunize particles already vaccinated
            for i in range(len(self.effec)):  # Iterate on different effectiveness
                mask_immune = self.select_to_immunize(i)
                self.immunity_state[mask_immune] = i
                self.pop.disease.inf_susceptibility[mask_immune] = (
                    self.pop.disease.inf_susceptibility[mask_immune] * self.effec[i]
                )
                self.pop.update_inf_tree_attributes(
                    mask_immune,
                    "time_immune_{}".format(i),
                    self.clk.time + self.time_to_immunity[0],
                )

    def vaccinate_0_1(self):
        """Vaccinate particles for one day with `VACCINE_0_1`."""
        if self.clk.today >= self.start_day:
            if self.clk.today < self.stop_day:
                # Select particles and vaccinate
                mask_to_vac, mask_to_immunize = self.select_to_vaccinate()
                self.immunity_state[mask_to_vac & ~mask_to_immunize] = 0
                self.update_times(mask_to_vac)

            # Immunize particles already vaccinated
            mask_immune = self.select_to_immunize()
            self.immunity_state[mask_immune] = 0
            self.pop.disease.inf_susceptibility[mask_immune] = 0
            self.pop.update_inf_tree_attributes(
                mask_immune, "time_immune", self.clk.time + self.time_to_immunity[0]
            )


VACCINE_0_1 = 0
VACCINE_IMPERFECT = 1

FILTER_DIAG_NO = ("diag_states", "!=", S.DIAG_YES)
FILTER_SENIORS = ("ages", "isin", list(range(12, 22)))
FILTER_ADULTS = ("ages", "isin", list(range(5, 22)))
FILTER_CHILDRENS = ("ages", "isin", list(range(0, 5)))
FILTER_MARKET_WORKERS = ("workplace_id", "==", ("service", "Markets"))
FILTER_HOSPITALS_WORKERS = ("workplace_id", "==", ("service", "Hospitals"))

SENIORS = {
    "name": "Senior citizens",
    "filter": (FILTER_SENIORS, "&", FILTER_DIAG_NO),
}
"""Apply the default number of doses on senior citizens
    (age groups 12 to 21, 60+ years old)."""

ADULTS = {
    "name": "Adult citizens",
    "filter": (FILTER_ADULTS, "&", FILTER_DIAG_NO),
}
"""Apply the default number of doses on adults citizens
    (age groups 5 to 21, 20+ years old)."""

CHILDRENS = {
    "name": "Child citizens",
    "filter": (FILTER_CHILDRENS, "&", FILTER_DIAG_NO),
}
"""Apply the default number of doses on child citizens
    (age groups 0 to 4, 0-19 years old)."""

DIAG_NO = {
    "name": "Not diagnosed",
    "filter": FILTER_DIAG_NO,
}
"""Apply the default number of doses on citizens that where not diagnosed."""

MAKETS_WORKERS = {
    "name": "Markets workers",
    "filter": (FILTER_MARKET_WORKERS, "&", FILTER_DIAG_NO),
}
"""Apply the default number of doses on markets workers."""

HOSPITALS_WORKERS = {
    "name": "Hospitals workers",
    "filter": (FILTER_HOSPITALS_WORKERS, "&", FILTER_DIAG_NO),
}
"""Apply the default number of doses on hospitals workers."""

DEFAULTS = {
    "name": "Vaccination",
    "start_day": 0,
    "stop_day": -1,
    "days_to_immunity": 30,
    "effectiveness": 1.0,
    "filter": FILTER_DIAG_NO,
    "doses_per_day": 1000,
    "type": VACCINE_0_1,
    "immunize_at_init": False,
}

DEFAULTS_TYPES = {
    "name": str,
    "start_day": int,
    "stop_day": int,
    "days_to_immunity": (np.array, {"dtype": float}),
    "effectiveness": (np.array, {"dtype": float}),
    "filter": tuple,
    "doses_per_day": int,
    "type": int,
    "immunize_at_init": bool,
}
