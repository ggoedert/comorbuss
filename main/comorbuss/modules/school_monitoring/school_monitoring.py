from ... import settings as S
import numpy as np
from ...aux_classes import empty_module, circularlist
from ..diagnostics import add_diagnostic

__name__ = "school_monitoring"
description = "A module to control school monitoring"

DEFAULTS = {}

DEFAULT_DIAG = {
    "name": "School Monitoring",
    "sensitivity": 0.86,
    "specificity": 0.90,
    "allow_retest": "yes",
    "retest_delay": 14.0,
    "filter_particles": ("self.comm.school_monitoring.to_be_tested"),
}


def triage(param, t_param, clk, event_log):
    return param


class school:
    """Stores a school service and monitoring related methods and data."""

    def __init__(
        self, i, srvc, comm, rng, monitoring, students_wid, teachers_wid, param
    ):
        """Initializes a school object

        Args:
            i (int): Id of the object.
            srvc (comorbuss.service): Service object.
            comm (comorbuss.community): Community object.
            rng (Generator): Random number generator.
            students_wid (list): List with the ids of the students.
            param (dict): Schools monitoring parameters dictionary.
        """
        self.id = i
        self.name = srvc.name
        self.comm = comm
        self.pop = comm.pop
        self.clk = comm.clk
        self.event_log = comm.event_log
        self.srvc = srvc
        self.monitoring = monitoring
        self.rng = rng
        self.students_wid = students_wid
        self.teachers_wid = teachers_wid

        self.suspend_time = param.get("suspend_days", 14) * 24
        self.classroom_close_days = param.get("classroom_close_days", 14) * 24
        self.school_close_time = param.get("school_close_days", 7) * 24
        self.number_of_cases_to_close = param.get("cases_to_close", 2)
        self.classroom_cases_to_close = param.get("classroom_cases_to_close", 1)
        self.window_time_to_look_for = param.get("cases_window", 7) * 24
        self.teachers_suspend_time_window = (
            param.get("teachers_suspend_time_window", 7) * 24
        )

        self.even_week_A = [0, 1, 0, 1, 0, 1, 0]
        self.odd_week_A = [0, 0, 1, 0, 1, 0, 0]
        self.even_week_B = [0, 0, 1, 0, 1, 0, 0]
        self.odd_week_B = [0, 1, 0, 1, 0, 1, 0]

        self.only_active = param.get("only_active_workers", True)

        self.mask_students_local = np.isin(self.srvc.workers_type, self.students_wid)
        self.mask_students = np.isin(
            self.pop.pid, self.srvc.workers_ids[self.mask_students_local]
        )
        self.mask_A_group = np.zeros(len(self.srvc.workers_ids), dtype=bool)
        self.mask_B_group = np.zeros(len(self.srvc.workers_ids), dtype=bool)
        self.mask_empty_group = np.zeros(len(self.srvc.workers_ids), dtype=bool)
        for instance in range(self.srvc.number):
            for room in self.srvc.rooms:
                mask_this_room = (
                    (self.srvc.workers_instance == instance)
                    & (self.srvc.workers_room == room)
                    & self.mask_students_local
                )
                n_A_group = int(np.count_nonzero(mask_this_room) / 2)
                if n_A_group > 0:
                    A_group = self.rng.choice(
                        self.srvc.workers_ids[mask_this_room], n_A_group, replace=False
                    )
                    self.mask_A_group |= np.isin(self.srvc.workers_ids, A_group)
                self.mask_B_group |= mask_this_room & ~self.mask_A_group
        self.mask_both_groups = self.mask_A_group | self.mask_B_group
        self.mask_suspended = np.zeros(len(self.srvc.workers_ids), dtype=bool)
        self.mask_teachers = np.isin(self.srvc.workers_type, self.teachers_wid)
        self.mask_count = np.zeros(len(self.srvc.workers_ids), dtype=bool)
        self.time_return = -1 * np.ones(len(self.srvc.workers_ids))
        self.already_diagnosed = np.zeros(len(self.srvc.workers_ids), dtype=bool)

        self.classrooms = np.unique(self.srvc.workers_room[self.mask_both_groups])
        self.time_instance_reopen = -1 * np.ones((self.srvc.number))
        self.instance_closed = np.zeros((self.srvc.number), dtype=bool)
        self.time_room_reopen = -1 * np.ones(
            (self.srvc.number, int(np.max(self.srvc.rooms)) + 1)
        )
        self.room_closed = np.zeros(
            (self.srvc.number, int(np.max(self.srvc.rooms)) + 1), dtype=bool
        )

        self.teachers_classroom_history = {
            teacher_id: circularlist(
                self.teachers_suspend_time_window,
                -1 * np.ones(self.teachers_suspend_time_window),
            )
            for teacher_id in self.srvc.workers_ids[self.mask_teachers]
        }

        self.groups = np.zeros(len(self.srvc.workers_ids))
        self.groups[self.mask_A_group] = 1
        self.groups[self.mask_B_group] = 2
        self.comm.add_to_store(
            "groups_school_{}".format(self.id),
            "comm.{}.schools[{}].groups".format(__name__, self.id),
        )

        self.families = dict()
        self.mask_families = np.zeros(self.pop.Nparticles, dtype=bool)
        self.family_to_worker = {}
        for pid in self.srvc.workers_ids:
            pid_home = self.pop.home_id[pid]
            mask_home = self.pop.home_id == pid_home
            mask_home[pid] = False  # Remove student from list
            for fpid in self.pop.pid[mask_home]:
                if fpid in self.family_to_worker:
                    self.family_to_worker[fpid].append(pid)
                else:
                    self.family_to_worker[fpid] = [pid]
            self.mask_families |= mask_home
            self.families[pid] = self.pop.pid[mask_home]
        family_reporting_frac = param.get("family_reporting_frac", 1.0)
        probab = self.rng.uniform(size=self.pop.Nparticles)
        # Mask of the family members that will report if they develop symptoms,
        # particle is removed from mask after the report to avoid double reports
        mask_report_frac = probab <= family_reporting_frac
        self.mask_family_report = mask_report_frac & self.mask_families
        self.mask_students_report = mask_report_frac & np.isin(
            self.pop.pid, self.srvc.workers_ids[self.mask_both_groups]
        )
        # # Family members of teachers and staff will always report
        # if param.get("staff_family_always_report", True):
        #     for pid in self.srvc.workers_ids[~self.mask_both_groups]:
        #         self.mask_family_report[self.families[pid]] = True
        self.family_cases_count = 0
        self.family_reports_count = 0

        self.teachers_room = -1 * np.ones(self.mask_teachers.shape)
        self.teachers_room[self.mask_teachers] = self.srvc.workers_room[
            self.mask_teachers
        ]
        self.classroom_teachers = {}
        self.classroom_all_teachers = {}
        for teacher, teacher_wid in zip(
            self.monitoring.teachers_param.keys(), self.teachers_wid
        ):
            teacher_param = self.monitoring.teachers_param[teacher]
            self.init_teachers(teacher_param, teacher_wid, teacher)
        self.comm.add_to_execution(self.select_teachers, priority=80)

        self.output_data = {
            "instances_count": np.array([]),
            "rooms_count": np.array([[]]),
            "classrooms_count": np.array([]),
            "workers_count": np.array([]),
            "students_count": np.array([]),
            "instances_total": np.array([]),
            "rooms_total": np.array([[]]),
            "classrooms_total": np.array([]),
            "workers_total": np.array([]),
            "students_total": np.array([]),
        }

    def init_teachers(self, teacher_param, teacher_wid, teacher_name):
        # Search for students for this teacher
        students_name = teacher_param["students"]
        found_students = False
        for students, students_wid in zip(
            self.monitoring.students_name, self.students_wid
        ):
            if students == students_name:
                found_students = True
                break
        if found_students:
            for instance in self.srvc.instances:
                mask_students = (self.srvc.workers_type == students_wid) & (
                    self.srvc.workers_instance == instance
                )
                mask_teachers = (self.srvc.workers_type == teacher_wid) & (
                    self.srvc.workers_instance == instance
                )
                students_rooms = np.unique(self.srvc.workers_room[mask_students])
                if len(students_rooms) == 0:
                    continue
                avalable_teachers = self.srvc.workers_ids[mask_teachers]
                if len(students_rooms) > len(avalable_teachers):
                    self.event_log(
                        "There is not enough {} teachers for {} students in school {}".format(
                            teacher_name, students_name, self.srvc.name
                        ),
                        S.MSG_WRNG,
                    )
                ramdomize = teacher_param.get("randomize", False)
                for room in students_rooms:
                    self.classroom_all_teachers[(instance, room)] = avalable_teachers
                if ramdomize:
                    for room in students_rooms:
                        self.classroom_teachers[(instance, room)] = []
                    classrooms_per_teacher = teacher_param["classrooms_per_teacher"]
                    for teacher in avalable_teachers:
                        for _ in range(classrooms_per_teacher):
                            max_classroom = np.max(
                                [
                                    len(self.classroom_teachers[(instance, room)])
                                    for room in students_rooms
                                ]
                            )
                            classrooms_weights = np.zeros(len(students_rooms))
                            for i, room in enumerate(students_rooms):
                                if (
                                    len(self.classroom_teachers[(instance, room)])
                                    == max_classroom
                                    or teacher
                                    in self.classroom_teachers[(instance, room)]
                                ):
                                    classrooms_weights[i] = 0
                                else:
                                    classrooms_weights[i] = 1
                            if np.sum(classrooms_weights) > 0:
                                classrooms_weights = np.array(
                                    classrooms_weights
                                ) / np.sum(classrooms_weights)
                                teacher_room = self.rng.choice(
                                    students_rooms, p=classrooms_weights
                                )
                            else:
                                teacher_room = self.rng.choice(students_rooms)
                            self.classroom_teachers[(instance, teacher_room)].append(
                                teacher
                            )
                else:
                    for room in students_rooms:
                        if len(avalable_teachers) == 0:
                            break
                        teacher = self.rng.choice(avalable_teachers)
                        avalable_teachers = avalable_teachers[
                            avalable_teachers != teacher
                        ]
                        self.classroom_teachers[(instance, room)] = [teacher]
                for room in students_rooms:
                    if (instance, room) not in self.classroom_teachers:
                        self.classroom_teachers[(instance, room)] = []
        else:
            self.event_log(
                "Couldn't find students {} for teachers {} in school {}".format(
                    students_name, teacher_name, self.srvc.name
                ),
                S.MSG_WRNG,
            )

    def select_teachers(self):
        # Get teachers back to the teachers room
        self.srvc.room[self.srvc.workers_ids[self.mask_teachers]] = self.teachers_room[
            self.mask_teachers
        ]
        self.srvc.workers_room[self.mask_teachers] = self.teachers_room[
            self.mask_teachers
        ]
        for instance in self.srvc.instances:
            # Get rooms with students and select a teacher for it
            selected_teachers = []
            rooms_without_teacher = []
            mask_students_in_school = (
                self.mask_students_local
                & (self.pop.placement[self.srvc.workers_ids] == self.srvc.id)
                & (self.srvc.workers_instance == instance)
            )
            students_rooms = np.unique(self.srvc.workers_room[mask_students_in_school])
            self.rng.shuffle(students_rooms)
            for room in students_rooms:
                # Try to randomly select a teacher from classroom_teachers
                available_teachers = [
                    teacher
                    for teacher in self.classroom_teachers[(instance, room)]
                    if teacher not in selected_teachers
                    and self.pop.placement[teacher] == self.srvc.id
                ]
                if len(available_teachers) > 0:
                    teacher = self.randomly_assign_teacher(available_teachers, room)
                    selected_teachers.append(teacher)
                    continue
                rooms_without_teacher.append(room)
            # Randomly select teachers from classroom_all_teachers to rooms without teachers
            for room in rooms_without_teacher:
                available_teachers = [
                    teacher
                    for teacher in self.classroom_all_teachers[(instance, room)]
                    if teacher not in selected_teachers
                    and self.pop.placement[teacher] == self.srvc.id
                ]
                if len(available_teachers) > 0:
                    teacher = self.randomly_assign_teacher(available_teachers, room)
                    selected_teachers.append(teacher)
                    continue
                # Couldn't get a teacher for this room print warning
                self.event_log(
                    "Could not select a teacher for room {} of instance {} of {} school.".format(
                        room, instance, self.srvc.name
                    ),
                    S.MSG_WRNG,
                )

        # Update teachers classroom history
        for teacher in self.teachers_classroom_history.keys():
            if self.pop.placement[teacher] == self.srvc.id:
                self.teachers_classroom_history[teacher].append(
                    "{},{},{}".format(
                        int(
                            self.srvc.workers_instance[self.srvc.workers_ids == teacher]
                        ),
                        int(self.srvc.workers_room[self.srvc.workers_ids == teacher]),
                        self.get_current_group(),
                    )
                )
            else:
                self.teachers_classroom_history[teacher].append("")

    def randomly_assign_teacher(self, available_teachers, room):
        teacher = self.rng.choice(available_teachers)
        self.srvc.room[teacher] = room
        self.srvc.workers_room[self.srvc.workers_ids == teacher] = room
        return teacher

    def diagnosed_count(self):
        return np.count_nonzero(self.already_diagnosed)

    def suspend_by_mask(self, mask_suspend, suspend_time=None):
        """Suspend workers particles in a mask, mask should be in the workers_ids shape.

        Args:
            mask_suspend (np.ndarray): Mask to suspend
        """
        suspend_time = suspend_time or self.suspend_time

        # Mark particles as suspended and the time they where suspended
        self.mask_suspended[mask_suspend] = True
        mask_suspended = self.time_return > (self.clk.time + suspend_time)
        self.time_return[mask_suspend & ~mask_suspended] = self.clk.time + suspend_time
        self.srvc.workers_active[mask_suspend] = False
        mask_suspend = np.isin(self.pop.pid, self.srvc.workers_ids[mask_suspend])
        self.pop.update_inf_tree_attributes(
            mask_suspend, "time_suspended", self.clk.time
        )

    def suspend_by_pids(self, pids, suspend_time=None):
        """Suspend particles in a list of pids.

        Args:
            pids (list): List of pids to suspend.
        """
        suspend_time = suspend_time or self.suspend_time

        mask_suspend = np.isin(self.srvc.workers_ids, pids)
        self.suspend_by_mask(mask_suspend, suspend_time=suspend_time)

    def close_room(
        self, instance, room, A_group, suspend_time=None, instance_closing=False
    ):
        """Suspend all particles in a group of a room and mark room as closed.

        Args:
            instance (int): Instance of the room.
            room (int): Room number.
            A_group (bool): True for A group, False for B group.
        """
        suspend_time = suspend_time or self.classroom_close_days

        if self.time_room_reopen[instance, room] <= self.clk.time + suspend_time:
            mask_this_room = (
                (self.srvc.workers_instance == instance)
                & (self.srvc.workers_room == room)
                & self.mask_students_local
            )
            if self.monitoring.divide_groups:
                mask_this_room &= (self.mask_A_group & A_group) | (
                    self.mask_B_group & (not A_group)
                )
            self.time_room_reopen[instance, room] = self.clk.time + suspend_time
            self.room_closed[instance, room] = True
            if not instance_closing:
                self.suspend_by_mask(mask_this_room, suspend_time=suspend_time)
                if A_group:
                    group = "A"
                else:
                    group = "B"
                self.event_log(
                    "Closing classroom {}-{}, instance {} of {} service due to testing "
                    "policy.".format(room, group, instance, self.srvc.name),
                    S.MSG_EVENT,
                )

    def close_instance(self, instance, suspend_time=None):
        """Closes an instance of the school and suspend all students.

        Args:
            instance (int): Instance to be closed.
        """
        suspend_time = suspend_time or self.school_close_time

        mask_this_instance = self.srvc.workers_instance == instance
        self.time_instance_reopen[instance] = self.clk.time + suspend_time
        self.instance_closed[instance] = True
        self.srvc.close_dont_reassign_instances(instance)
        for room in self.classrooms:
            self.close_room(
                instance, room, True, instance_closing=True, suspend_time=suspend_time
            )
            self.close_room(
                instance, room, False, instance_closing=True, suspend_time=suspend_time
            )
        self.suspend_by_mask(mask_this_instance, suspend_time=suspend_time)
        self.event_log(
            "Closing instance {} of {} service due to testing "
            "policy.".format(instance, self.srvc.name),
            S.MSG_EVENT,
        )

    def reopen_all(self):
        """Check instances/rooms that need to be reopened, and students/staff to be
        unsuspended.
        """
        # Unsuspend students/staff
        mask_unsuspend = (self.clk.time >= self.time_return) & self.mask_suspended
        self.mask_suspended[mask_unsuspend] = False
        self.time_return[mask_unsuspend] = -1
        self.srvc.workers_active[mask_unsuspend] = True
        # Checks for instances/rooms to be reopened
        for instance in self.srvc.instances:
            reopen_instance = False
            if (
                self.clk.time >= self.time_instance_reopen[instance]
            ) & self.instance_closed[instance]:
                self.srvc.reopen_instances(instance)
                self.time_instance_reopen[instance] = -1
                self.instance_closed[instance] = False
                reopen_instance = True
                self.event_log(
                    "Reopening {} instance of {} service due to testing "
                    "policy".format(instance, self.srvc.name),
                    S.MSG_EVENT,
                )
            for room in self.srvc.rooms:
                if (
                    self.clk.time >= self.time_room_reopen[instance, room]
                ) & self.room_closed[instance, room]:
                    self.time_room_reopen[instance, room] = -1
                    self.room_closed[instance, room] = False
                    if not reopen_instance:
                        self.event_log(
                            "Reopening classroom {}, instance {} of {} service due to testing "
                            "policy.".format(room, instance, self.srvc.name),
                            S.MSG_EVENT,
                        )

    def _is_A_group(self, dow=None):
        dow = dow or self.clk.dow
        return bool(
            ((self.clk.week % 2) == 0 and self.even_week_A[dow])
            or ((self.clk.week % 2) != 0 and self.odd_week_A[dow])
        )

    def _is_B_group(self, dow=None):
        dow = dow or self.clk.dow
        return bool(
            ((self.clk.week % 2) == 0 and self.even_week_B[dow])
            or ((self.clk.week % 2) != 0 and self.odd_week_B[dow])
        )

    def get_current_mask(self, dow=None):
        """Gets the mask of the current students group, in the workers context.

        Args:
            dow (int, optional): Selects a different day of the week. Defaults to None.

        Returns:
            np.array: Current students group mask.
        """
        dow = dow or self.clk.dow
        if not self.monitoring.divide_groups:
            return self.mask_A_group | self.mask_B_group
        elif self._is_A_group(dow):
            return self.mask_A_group
        elif self._is_B_group(dow):
            return self.mask_B_group
        else:
            return self.mask_empty_group

    def get_current_group(self, dow=None):
        dow = dow or self.clk.dow
        if not self.monitoring.divide_groups:
            return "N"
        elif self._is_A_group(dow):
            return "A"
        elif self._is_B_group(dow):
            return "B"
        else:
            return "N"

    def get_other_mask(self, dow=None):
        """Gets the mask of the other students group, in the workers context.

        Args:
            dow (int, optional): Selects a different day of the week. Defaults to None.

        Returns:
            np.array: Other students group mask.
        """
        dow = dow or self.clk.dow
        if not self.monitoring.divide_groups:
            return self.mask_suspended
        elif (self.clk.week % 2 == 0 and self.even_week_A[dow]) or (
            self.clk.week % 2 != 0 and self.odd_week_A[dow]
        ):
            return self.mask_B_group | self.mask_suspended
        elif (self.clk.week % 2 == 0 and self.even_week_B[dow]) or (
            self.clk.week % 2 != 0 and self.odd_week_B[dow]
        ):
            return self.mask_A_group | self.mask_suspended
        else:
            return self.mask_both_groups

    def test_pids(self, pids):
        self.monitoring.to_be_tested[:] = False
        self.monitoring.to_be_tested[pids] = True
        self.monitoring.diag()
        self.monitoring.to_be_tested[:] = False

    def get_staff_mask(self, pop_context=True):
        """Gets the mask of the other students group, in the workers context.

        Args:
            pop_context (bool, optional): Selects the population context. Defaults to True.

        Returns:
            [type]: [description]
        """
        mask = ~(self.mask_A_group | self.mask_B_group)
        if pop_context:
            mask = np.isin(self.pop.pid, self.srvc.workers_ids[mask])
        return mask

    def check_teachers_room(self, instance, room):
        group = self.get_current_group()
        to_test = np.array(
            [
                teacher
                for teacher in self.teachers_classroom_history.keys()
                if "{},{},{}".format(int(instance), int(room), group)
                in self.teachers_classroom_history[teacher]
            ],
            dtype=int,
        )
        if len(to_test) > 0:
            self.test_pids(to_test)
            suspend_ids = to_test[self.pop.diag_states[to_test] == S.DIAG_YES]
            if len(suspend_ids) > 0:
                self.comm.event_log(
                    "Suspending teachers ids {}, due to contact with {} instance {} room {} group {}.".format(
                        suspend_ids, self.srvc.name, instance, room, group
                    ),
                    S.MSG_EVENT,
                )
                self.suspend_by_pids(suspend_ids)

    def check_room_and_group(self, instance, room, group, close=True):
        """Checks a group of a room for diagnostics.

        Args:
            instance (int): Instance of the room.
            room (int): Room number.
            group (np.ndarray): Mask for the group.

        Returns:
            list: List of the diagnosed.
        """
        mask_group = (
            (self.srvc.workers_instance == instance)
            & (self.srvc.workers_room == room)
            & self.mask_students_local
            & group
            & ~self.already_diagnosed
        )
        group_ids = self.srvc.workers_ids[mask_group]
        mask_diag = self.pop.diag_states[group_ids] == S.DIAG_YES
        mask_sympt_no_diag = (
            (self.pop.symptoms[group_ids] == S.SYMPT_YES)
            | (self.pop.symptoms[group_ids] == S.SYMPT_SEVERE)
        ) & ~mask_diag
        nsympt = np.count_nonzero(mask_sympt_no_diag)
        sympt_ids = []
        if nsympt > 0:
            sympt_ids = group_ids[mask_sympt_no_diag]
            self.suspend_by_pids(sympt_ids)
            mask_sympt = np.isin(self.srvc.workers_ids, sympt_ids)
            self.already_diagnosed[mask_sympt] = True
        mask_diag = self.pop.diag_states[group_ids] == S.DIAG_YES
        ndiag_yes = np.count_nonzero(mask_diag)
        if ndiag_yes > 0:
            # Check if room is not already suspended
            room_closed = (
                self.room_closed[instance, room] or self.instance_closed[instance]
            )
            diag_ids = group_ids[mask_diag]
            mask_diag = np.isin(self.srvc.workers_ids, diag_ids)
            if close and not room_closed:
                mask_group_pop = np.isin(self.pop.pid, group_ids)
                mask_diag_pop = (
                    self.clk.time_since(self.pop.time_diagnosed)
                    < self.window_time_to_look_for
                ) & (self.pop.diag_states == S.DIAG_YES)
                ndiag = np.sum(mask_group_pop & mask_diag_pop)
                if ndiag >= self.classroom_cases_to_close:
                    A_group = self._is_A_group()
                    self.close_room(instance, room, A_group)
                    self.check_teachers_room(instance, room)
                    self.mask_count |= mask_diag
            else:  # If yes suspend only diagnosed students
                self.suspend_by_pids(diag_ids)
            self.already_diagnosed[mask_diag] = True
            return list(diag_ids) + list(sympt_ids), room_closed
        else:
            return list(sympt_ids), False

    def check_diagnostics(self, close=True, check_families=False):
        """Checks for diagnostics on the school.

        Args:
            close (bool, optional): If False will not close rooms/instances, used
                for initialization. Defaults to True.
            check_families (bool, optional): Also check families. Defaults to False.

        Returns:
            list: List of the diagnosed.
        """
        diagnosed = []
        # Checks of diagnosed family members
        if check_families:
            mask_diag_family = (
                self.pop.diag_states == S.DIAG_YES
            ) & self.mask_family_report
            ndiag_yes = np.count_nonzero(mask_diag_family)
            # Check if there where family members diagnosed
            if ndiag_yes > 0:
                family_diag_ids = self.pop.pid[mask_diag_family]
                # Get ids of the related students/workers and suspend them
                suspend_ids = []
                for fpid in family_diag_ids:
                    for pid in self.family_to_worker[fpid]:
                        suspend_ids.append(pid)
                self.event_log(
                    "{} workers/students suspended due to family members symptomatic in {} service.".format(
                        suspend_ids, self.srvc.name
                    ),
                    S.MSG_PRGS,
                )
                self.suspend_by_pids(suspend_ids)
                # Test selected ids
                self.test_pids(suspend_ids)
                # Remove family members from mask_family_report for control and count reported cases
                self.mask_family_report[mask_diag_family] = False
                self.family_reports_count += ndiag_yes
            # Count all family cases
            ndiag_all = np.count_nonzero(
                (self.pop.diag_states == S.DIAG_YES) & self.mask_families
            )
            if ndiag_all > 0:
                self.family_cases_count = ndiag_all
                # print("{} family members diagnosed on {}, {} reported.".format(self.family_cases_count, self.srvc.name, self.family_reports_count))
        # Check for diagnosed students in each room for each group
        for instance in self.srvc.instances:
            for room in self.srvc.rooms:
                pids, room_closed = self.check_room_and_group(
                    instance, room, self.mask_A_group, close=close
                )
                diagnosed += [
                    (
                        pid,
                        self.srvc.name,
                        self.srvc.id,
                        instance,
                        room,
                        "A",
                        room_closed,
                        self.clk.time,
                    )
                    for pid in pids
                ]
                pids, room_closed = self.check_room_and_group(
                    instance, room, self.mask_B_group, close=close
                )
                diagnosed += [
                    (
                        pid,
                        self.srvc.name,
                        self.srvc.id,
                        instance,
                        room,
                        "B",
                        room_closed,
                        self.clk.time,
                    )
                    for pid in pids
                ]
        # Check for diagnosed workers
        mask_diag_workers = (
            self.pop.diag_states[self.srvc.workers_ids] == S.DIAG_YES
        ) & ~self.already_diagnosed
        ndiag_yes = np.count_nonzero(mask_diag_workers)
        if ndiag_yes > 0:
            diag_ids = self.srvc.workers_ids[mask_diag_workers]
            self.suspend_by_pids(diag_ids)
            mask_diag = np.isin(self.srvc.workers_ids, diag_ids)
            self.already_diagnosed |= mask_diag
            for pid in diag_ids:
                wid = np.where(self.srvc.workers_ids == pid)[0][0]
                instance = self.srvc.workers_instance[wid]
                room = self.srvc.workers_room[wid]
                instance_closed = self.instance_closed[instance]
                if not instance_closed and close:
                    self.mask_count[wid] = True
                diagnosed.append(
                    (
                        pid,
                        self.srvc.name,
                        self.srvc.id,
                        instance,
                        room,
                        "W",
                        instance_closed,
                        self.clk.time,
                    )
                )
        # Check if distances should close
        if close:
            for instance in self.srvc.instances:
                if not self.srvc.instance_is_closed[instance]:
                    workers_mask = self.srvc.workers_instance == instance
                    if self.only_active:
                        workers_mask &= self.mask_count
                    pids_instance = self.srvc.workers_ids[workers_mask]
                    mask_instance = np.isin(self.pop.pid, pids_instance)
                    mask_diag = (
                        self.clk.time_since(self.pop.time_diagnosed)
                        < self.window_time_to_look_for
                    ) & (self.pop.diag_states == S.DIAG_YES)
                    rooms = self.pop.workplace_room[mask_diag & mask_instance]
                    ndiag = len(np.unique(rooms))
                    if ndiag >= self.number_of_cases_to_close:
                        self.close_instance(instance)
        return diagnosed

    def sum_statistics(self):
        count_open = {
            "instances": [0, 0],
            "rooms": [0, 0],
            "classrooms": [0, 0],
            "workers": [0, 0],
            "students": [0, 0],
        }
        for instance in self.srvc.instances:
            mask_instance = self.srvc.in_instance(instance)
            # Count instances
            count_open["instances"][1] += 1
            if np.count_nonzero(mask_instance) > 0:
                count_open["instances"][0] += 1
            for room in self.srvc.rooms:
                # Count rooms
                mask_room = self.srvc.in_room(instance, room)
                count_open["rooms"][1] += 1
                if np.count_nonzero(mask_room) > 0:
                    count_open["rooms"][0] += 1

                # Count workers that should be in room and that are in room
                n_workers_of_this_room = np.count_nonzero(
                    (self.srvc.workers_instance == instance)
                    & (self.srvc.workers_room == room)
                    & ~self.get_other_mask()
                )
                n_worker_in_this_room = np.count_nonzero(
                    mask_room & self.srvc.mask_is_worker
                )
                if n_workers_of_this_room > 0:
                    count_open["workers"][1] += n_workers_of_this_room
                if n_worker_in_this_room > 0:
                    count_open["workers"][0] += n_worker_in_this_room

                # Count students that should be in room and that are in room
                n_students_of_this_room = np.count_nonzero(
                    (self.srvc.workers_instance == instance)
                    & (self.srvc.workers_room == room)
                    & self.mask_students_local
                    & self.get_current_mask()
                )
                n_students_in_this_room = np.count_nonzero(
                    mask_room & self.mask_students
                )
                if n_students_of_this_room > 0:
                    count_open["classrooms"][1] += 1
                    count_open["students"][1] += n_students_of_this_room
                if n_students_in_this_room > 0:
                    count_open["classrooms"][0] += 1
                    count_open["students"][0] += n_students_in_this_room
        for key in count_open:
            self.output_data[key + "_count"] = np.append(
                self.output_data[key + "_count"], count_open[key][0]
            )
            self.output_data[key + "_total"] = np.append(
                self.output_data[key + "_total"], count_open[key][1]
            )


class module(empty_module):
    def __init__(self, param, comm):
        super(module, self).__init__(param, comm, name=__name__)

        self.total_tests = 0
        self.to_be_tested = np.zeros(self.pop.Nparticles, dtype=bool)
        self.diagnosed = []

        self.suspend_time = 24 * 14

        self.random_teachers = param.get("randomize_teachers", False)

        self.closed = param.get("closed_schools", [])

        self.students_name = param.get("students", ["Students"])
        self.students_name = (
            [self.students_name]
            if type(self.students_name) is str
            else self.students_name
        )
        self.teachers_param = param.get("teachers", {})
        self.teachers_name = list(self.teachers_param.keys())
        self.schools = self.init_schools(param)
        self.mask_monitored_pids = np.zeros(self.pop.Nparticles, dtype=bool)
        self.n_workers = 0
        self.n_students = 0
        self.mask_students = np.zeros(self.pop.Nparticles, dtype=bool)
        open_time = 24
        for school in self.schools:
            open_time = np.min([np.min(school.srvc.workers_shift_start), open_time])
            self.n_workers += len(school.srvc.workers_ids)
            self.n_students += np.count_nonzero(school.mask_both_groups)
            self.mask_monitored_pids |= np.isin(self.pop.pid, school.srvc.workers_ids)
            self.mask_students |= school.mask_students

        self.output_data = {
            "diagnosed_count": np.array([]),
            "susceptible_count": np.array([]),
            "exposed_count": np.array([]),
            "infectious_count": np.array([]),
            "recovered_count": np.array([]),
            "deceased_count": np.array([]),
        }
        for school in self.schools:
            self.output_data[school.name] = school.output_data
        self.comm.add_to_store(
            "{}".format(__name__), "comm.{}.output_data".format(__name__)
        )

        self.pop.add_attribute(
            "school_related_test", np.zeros(self.pop.Nparticles, dtype=bool)
        )
        for school in self.schools:
            staff = school.srvc.workers_ids[~school.mask_both_groups]
            self.pop.school_related_test[staff] = True

        priority = 27
        self.divide_groups = True
        if "scenario" in param:
            if param["scenario"] == "b0":
                self.comm.add_to_execution(
                    self.scenario_b0,
                    priority=priority,
                    time_of_the_day=self.comm.free_hours[0],
                )
                self.divide_groups = False
                self.event_log("Initialized school monitoring scenario b0.", S.MSG_PRGS)
            elif param["scenario"] == "b1":
                self.comm.add_to_execution(
                    self.scenario_b1_b2,
                    priority=priority,
                    time_of_the_day=self.comm.free_hours[0],
                )
                self.divide_groups = False
                self.event_log("Initialized school monitoring scenario b1.", S.MSG_PRGS)
            elif param["scenario"] == "b2":
                self.comm.add_to_execution(
                    self.scenario_b1_b2,
                    priority=priority,
                    time_of_the_day=self.comm.free_hours[0],
                )
                self.event_log("Initialized school monitoring scenario b2.", S.MSG_PRGS)
            elif param["scenario"] == "m1":
                self.comm.add_to_execution(
                    self.scenario_m1_m2,
                    priority=priority,
                    time_of_the_day=self.comm.free_hours[0],
                )
                self.event_log("Initialized school monitoring scenario m1.", S.MSG_PRGS)
                self.family_reporting_frac = param.get("family_reporting_frac", 1.0)
                self.divide_groups = False
                for school in self.schools:
                    self.pop.school_related_test |= school.mask_family_report
                    self.pop.school_related_test |= school.mask_students_report
            elif param["scenario"] == "m2":
                self.comm.add_to_execution(
                    self.scenario_m1_m2,
                    priority=priority,
                    time_of_the_day=self.comm.free_hours[0],
                )
                self.event_log("Initialized school monitoring scenario m2.", S.MSG_PRGS)
                self.family_reporting_frac = param.get("family_reporting_frac", 1.0)
                for school in self.schools:
                    self.pop.school_related_test |= school.mask_family_report
                    self.pop.school_related_test |= school.mask_students_report
            else:
                self.comm.add_to_execution(
                    self.scenario_original,
                    priority=priority,
                    time_of_the_day=self.comm.free_hours[0],
                )
                self.event_log(
                    "Initialized school monitoring scenario reference.", S.MSG_PRGS
                )
        else:
            self.comm.add_to_execution(
                self.scenario_original,
                priority=priority,
                time_of_the_day=self.comm.free_hours[0],
            )
        self.comm.add_to_execution(
            self.sum_statistics, priority=101, time_of_the_day=open_time
        )

        self.pre_run_diag = param.get("pre_run_diagnostics", True)
        diag_param = param.get("diagnostic", DEFAULT_DIAG)
        self.diag = add_diagnostic(
            diag_param, "school monitoring diagnostic", self.comm, DEFAULT_DIAG
        )

    def run_after_init(self):
        if self.pre_run_diag:
            self.comm.diagnostics(check_time=False)
            self.check_diagnostics(close=False, check_time=False)

    def init_schools(self, param):
        schools_list = param["schools"]
        schools = []
        i = 0
        for srvc in map(self.comm.srv.__getitem__, schools_list):
            has_students = False
            teachers_wid = []
            students_wid = []
            for wrkr_param in srvc.workers_parameters:
                if wrkr_param["name"] in self.students_name:
                    students_wid.append(wrkr_param["id"])
                    has_students = True
                if wrkr_param["name"] in self.teachers_name:
                    teachers_wid.append(wrkr_param["id"])
            if not has_students:
                self.event_log(
                    "{} service has no worker {}, will not run "
                    "testing on it.".format(school.srvc.name, self.students_name),
                    S.MSG_ERROR,
                )
                continue
            schools.append(
                school(
                    i,
                    srvc,
                    self.comm,
                    self.rng,
                    self,
                    students_wid,
                    teachers_wid,
                    param,
                )
            )
            i += 1
        return schools

    def sum_statistics(self):
        self.output_data["diagnosed_count"] = np.append(
            self.output_data["diagnosed_count"],
            np.sum([school.diagnosed_count() for school in self.schools]),
        )
        self.output_data["susceptible_count"] = np.append(
            self.output_data["susceptible_count"],
            np.count_nonzero(self.pop.states[self.mask_monitored_pids] == S.STATE_S),
        )
        self.output_data["exposed_count"] = np.append(
            self.output_data["exposed_count"],
            np.count_nonzero(self.pop.states[self.mask_monitored_pids] == S.STATE_E),
        )
        self.output_data["infectious_count"] = np.append(
            self.output_data["infectious_count"],
            np.count_nonzero(self.pop.states[self.mask_monitored_pids] == S.STATE_I),
        )
        self.output_data["recovered_count"] = np.append(
            self.output_data["recovered_count"],
            np.count_nonzero(self.pop.states[self.mask_monitored_pids] == S.STATE_R),
        )
        self.output_data["deceased_count"] = np.append(
            self.output_data["deceased_count"],
            np.count_nonzero(self.pop.states[self.mask_monitored_pids] == S.STATE_D),
        )

        if any([school.srvc.working_days[self.clk.dow] for school in self.schools]):
            for school in self.schools:
                school.sum_statistics()

    def scenario_b0(self):
        self.closed_schools(all=True)

    def scenario_b1_b2(self):
        self.closed_schools()
        self.select_group()

    def scenario_m1_m2(self):
        self.closed_schools()
        self.select_group()
        self.check_diagnostics(check_families=True)

    def scenario_0(self):
        self.closed_schools()
        self.select_group()
        self.check_diagnostics()

    # Old scenarios
    def scenario_2a(self):
        self.closed_schools()
        self.select_group()
        self.check_diagnostics()
        self.test_students_once_week()

    def scenario_1_2a(self):
        self.closed_schools()
        self.select_group()
        self.check_diagnostics(check_families=True)
        self.test_students_once_week()

    def scenario_original(self):
        self.closed_schools()
        self.select_group()
        self.check_diagnostics()
        self.test_students_once_week()

    def closed_schools(self, all=False):
        for school in self.schools:
            if school.srvc.name in self.closed or all:
                for instance in school.srvc.instances:
                    school.close_instance(instance)

    def select_group(self):
        for school in self.schools:
            if school.srvc.working_days[self.clk.dow]:
                school.reopen_all()
                school.srvc.workers_active[school.get_current_mask()] = True
                school.srvc.workers_active[school.get_other_mask()] = False

    def check_diagnostics(self, close=True, check_families=False, check_time=True):
        diagnosed = []
        for school in self.schools:
            if school.srvc.working_days[self.clk.dow] or not check_time:
                diag_ids = school.check_diagnostics(
                    close=close, check_families=check_families
                )
                diagnosed += diag_ids
        if len(diagnosed) > 0:
            self.event_log(
                "Diagnosed workers/students: {}".format(diagnosed), S.MSG_PRGS
            )
            self.diagnosed += diagnosed

    def test_students_once_week(self):
        # On Thursdays select particles to be tested on Fridays
        tests_per_room = 2
        if self.clk.dow == 5:
            mask_can_test = self.diag.can_be_tested()
            to_test = []
            for school in self.schools:
                mask_can_test_school = mask_can_test[school.srvc.workers_ids]
                for instance in school.srvc.instances:
                    if not school.instance_closed[instance]:
                        for room in school.srvc.rooms:
                            if not school.room_closed[instance, room]:
                                mask_room = (
                                    (school.srvc.workers_instance == instance)
                                    & (school.srvc.workers_room == room)
                                    & (school.srvc.workers_type == school.students_wid)
                                    & school.get_current_mask(dow=5)
                                    & mask_can_test_school
                                )
                                nparticles = np.count_nonzero(mask_room)
                                ids = school.srvc.workers_ids[mask_room]
                                ntests = int(np.min([nparticles, tests_per_room]))
                                chosen_ids = self.rng.choice(ids, ntests, replace=False)
                                to_test += list(chosen_ids)
            # Set to be tested list, run diagnostic and unset list
            self.to_be_tested = np.isin(self.pop.pid, to_test)
            self.event_log("Selected to test: {}.".format(to_test), S.MSG_PRGS)
            self.diag()
            self.to_be_tested[:] = False
