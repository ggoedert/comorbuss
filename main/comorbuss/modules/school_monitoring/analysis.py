from ... import settings as S
from ...aux_classes import empty_analysis
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


__name__ = "school_monitoring"
df_columns = [
    "Scenario",
    "School",
    "Open",
    "Closed",
    "Classroom open",
    "Classroom closed",
    "Cases",
]


class SchoolSimulationAnaysis:
    def __init__(self, df, analysis=None):
        self.df = df
        self.SimAnalysis = analysis

    @classmethod
    def from_folder(cls, folder, *args, **kwargs):
        from ...lab import SimulationAnalysis

        analysis = SimulationAnalysis.from_folder(folder, *args, **kwargs)
        df = pd.DataFrame(columns=df_columns)
        newAs = {}
        for simid, A in analysis.As.items():
            newAs[simid] = SchoolAnalysis(A)
            df = cls.gen_df(A, simid, df)
        analysis.As = newAs
        return cls(df, analysis)

    @classmethod
    def from_csv(cls, csv_file, **pd_args):
        df = pd.read_csv(csv_file, **pd_args)
        return cls()

    @staticmethod
    def gen_df(analysis, name, df, workers_type=None):
        data = analysis.get_data(["states", "ages", "home_id"], None)
        states = np.array(data["states"])
        ages = np.array(data["ages"])
        home_id = np.array(data["home_id"])
        seeds = data["seeds"]
        nseeds = len(seeds)
        nparticles = analysis.parameters["Nparticles"]

        if False:  # type(workers_type) == np.ndarray:
            srvc_data = analysis.get_srvc_data(["workers", "name"], None)
        else:
            srvc_data = analysis.get_srvc_data(
                ["workers", "name", "workers_type"], None
            )
            workers_type = [np.array(srvc["workers_type"]) for srvc in srvc_data]
        schools = [
            key
            for key in analysis.data[seeds[0]]["school_monitoring"].keys()
            if "_count" not in key
        ]

        i_count = np.zeros(nseeds)
        i_total = np.zeros(nseeds)
        c_count = np.zeros(nseeds)
        c_total = np.zeros(nseeds)
        n_cases = np.zeros(nseeds)
        teachers_count = np.zeros(nseeds)
        teachers_cases = np.zeros(nseeds)
        masks_workers = np.zeros((nseeds, nparticles), dtype=bool)
        for i, school in enumerate(schools):
            I_COUNT = ("school_monitoring", school, "instances_count")
            I_TOTAL = ("school_monitoring", school, "instances_total")
            C_COUNT = ("school_monitoring", school, "classrooms_count")
            C_TOTAL = ("school_monitoring", school, "classrooms_total")
            S_COUNT = ("school_monitoring", school, "students_count")
            S_TOTAL = ("school_monitoring", school, "rooms_total")
            data = analysis.get_data(
                [I_COUNT, I_TOTAL, S_COUNT, S_TOTAL, C_COUNT, C_TOTAL], None
            )
            for sid, sdata in enumerate(srvc_data):
                if sdata["name"][0] == school:
                    workers = sdata["workers"]
                    workers_t = workers_type[sid]
                    break

            for s in range(nseeds):
                i_open = np.sum(data[I_COUNT][s]) / np.sum(data[I_TOTAL][s])
                c_open = np.sum(data[C_COUNT][s]) / np.sum(data[C_TOTAL][s])
                cases = np.sum(
                    np.isin(
                        states[s][workers[s]],
                        [S.STATE_E, S.STATE_I, S.STATE_R, S.STATE_D],
                    )
                )

                i_count[s] += np.sum(data[I_COUNT])
                i_total[s] += np.sum(data[I_TOTAL])
                c_count[s] += np.sum(data[C_COUNT])
                c_total[s] += np.sum(data[C_TOTAL])
                n_cases[s] += cases
                teachers_ids = workers[s][(workers_t[s] == 1) | (workers_t[s] == 3)]
                teachers_count[s] += len(teachers_ids)
                teachers_cases[s] += np.sum(
                    np.isin(
                        states[s][teachers_ids],
                        [S.STATE_E, S.STATE_I, S.STATE_R, S.STATE_D],
                    )
                )
                masks_workers[s, workers[s]] = True

                row = {
                    "Scenario": name,
                    "School": school,
                    "Open": i_open,
                    "Closed": 1 - i_open,
                    "Classroom open": c_open,
                    "Classroom closed": 1 - c_open,
                    "Cases": cases,
                }
                df = df.append(row, ignore_index=True)

        for s in range(nseeds):
            i_open = i_count[s] / i_total[s]
            c_open = c_count[s] / c_total[s]

            row = {
                "Scenario": name,
                "School": "All",
                "Open": i_open,
                "Closed": 1 - i_open,
                "Classroom open": c_open,
                "Classroom closed": 1 - c_open,
                "Cases": n_cases[s],
            }
            df = df.append(row, ignore_index=True)

            mask_cases = np.isin(
                states[s], [S.STATE_E, S.STATE_I, S.STATE_R, S.STATE_D]
            )
            n_cases_comm = np.sum(mask_cases)

            row = {
                "Scenario": name,
                "School": "Community",
                "Cases": n_cases_comm,
            }
            df = df.append(row, ignore_index=True)

            adults_ages = np.arange(4, 12)
            mask_adults = np.isin(ages[s], adults_ages)
            n_adults = np.sum(mask_adults)
            cases_adults = np.sum(mask_cases & mask_adults)

            row = {
                "Scenario": name,
                "School": "Adults",
                "Cases": cases_adults / n_adults,
            }
            df = df.append(row, ignore_index=True)

            row = {
                "Scenario": name,
                "School": "Teachers",
                "Cases": teachers_cases[s] / teachers_count[s],
            }
            df = df.append(row, ignore_index=True)

            houses = np.unique(home_id[s][masks_workers[s]])
            mask_houses = np.isin(home_id[s], houses)
            cases_houses = np.sum(
                np.isin(
                    states[s, mask_houses], [S.STATE_E, S.STATE_I, S.STATE_R, S.STATE_D]
                )
            )
            count_houses = np.sum(mask_houses)
            row = {
                "Scenario": name,
                "School": "Houses",
                "Cases": cases_houses / count_houses,
            }
            df = df.append(row, ignore_index=True)
        return df

    def to_csv(self, filename="data.csv", **pd_args):
        self.df.to_csv(filename, **pd_args)

    @staticmethod
    def _show_values_on_bars(axs, unit="", value_format="{:.0f}"):
        def _show_on_single_plot(ax):
            ylim = ax.get_ylim()
            margin = (ylim[1] - ylim[0]) * 0.01
            for p in ax.patches:
                _x = p.get_x() + p.get_width() / 2
                _y = p.get_y()
                if _y >= 0:
                    _y += p.get_height() + margin
                else:
                    _y -= p.get_height() + margin
                value = value_format.format(p.get_height())
                ax.text(_x, _y, str(value) + unit, ha="center")

        if isinstance(axs, np.ndarray):
            for idx, ax in np.ndenumerate(axs):
                _show_on_single_plot(ax)
        else:
            _show_on_single_plot(axs)

    def rel_plot(
        self,
        column,
        baseline,
        title="",
        size=(5, 4),
        value_format="",
        add_baseline=False,
        filename=None,
    ):
        df = self.df
        mean = df[df.Scenario == baseline][column].mean()
        if not add_baseline:
            df = df[df.Scenario != baseline]
        df[column] = 100 * ((df[column] / mean) - 1)
        print(mean)
        plt.figure(figsize=size)
        if title != "":
            plt.title(title)
        sns.barplot(x="Scenario", y=column, data=df, orient="v", ci=None)
        plt.xticks(rotation=-45, ha="left")
        plt.ylabel("Percentage")
        ax = plt.gca()
        if value_format != "":
            self._show_values_on_bars(ax, value_format=value_format)
        plt.xlabel("Percentage above " + baseline)
        if filename != None:
            plt.savefig(filename)

    def barplot(
        self,
        x,
        y,
        size=(5, 4),
        multiplier=1,
        title="",
        school_hue=False,
        value_format="",
        ci="sd",
        xlabel=None,
        filename=None,
    ):
        data = self.df
        melt_hue = False
        if type(x) == list:
            data = pd.melt(data, id_vars=["Scenario"], value_vars=x)
            x = "value"
            melt_hue = True
        data[y] = multiplier * data[y]
        plt.figure(figsize=size)
        if title != "":
            plt.title(title)
        if melt_hue:
            sns.barplot(x=x, y=y, hue="variable", data=data, orient="v", ci=ci)
        elif school_hue:
            sns.barplot(x=x, y=y, hue="School", data=data, orient="v", ci=ci)
        else:
            sns.barplot(x=x, y=y, data=data, orient="v", ci=ci)
        ax = plt.gca()
        plt.xticks(rotation=-45, ha="left")
        if value_format != "":
            self._show_values_on_bars(ax, value_format=value_format)
        if xlabel != None:
            plt.xlabel(xlabel)
        if filename != None:
            plt.savefig(filename)


class SchoolAnalysis(empty_analysis):
    def __init__(self, analysis):
        super(SchoolAnalysis, self).__init__(analysis)

    def plot_cases(self, nparticles=0, seeds=None, **plot_args):
        # Prepare Data
        DIAG = (__name__, "diagnosed_count")
        INF = (__name__, "infectious_count")
        REC = (__name__, "recovered_count")
        DEC = (__name__, "deceased_count")
        data = self.get_data([DIAG, INF, REC, DEC], seeds)
        seeds = data["seeds"]
        perc = 1 if nparticles == 0 else 100 / nparticles
        data = {
            "detected": np.array(data[DIAG]) * perc,
            "cases": (np.array(data[INF]) + np.array(data[REC]) + np.array(data[DEC]))
            * perc,
        }
        Ndays = self.parameters["Ndays"]
        dt = self.parameters["dt"]
        t = np.arange(Ndays)

        # Plot graphs
        ylabel = "Cases" if nparticles == 0 else "Percentage"
        def_plot_args = {
            "xlabel": "Days",
            "ylabel": ylabel,
            "bands_legend_args": {"color": S.COLORS_LIST[0]},
        }
        plot_args = self.args_parse(plot_args, def_plot_args)
        self.do_plot(t, dt, data, S.COLORS_LIST, seeds, **plot_args)
