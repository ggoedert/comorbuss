from ...tools import recursive_copy
from ... import settings as S
from ..vaccinations import add_vaccination, linear_decay
from ...aux_classes import empty_module, RandomDistibution
import numpy as np
import networkx as nx

__name__ = "reinfection"


DEFAULTS = {
    "name": __name__,
    "immunity": {
        "inf_susceptibility": linear_decay,
    },
    "immunity_parameters": {
        "initial_value": 1.0,
        "decay_factor": 1 / 30,
    },
    "immunity_init_shift": 0,
}

DEFAULTS_TYPES = {
    "name": str,
    "immunity": dict,
    "immunity_parameters": dict,
    "immunity_init_shift": [int, RandomDistibution],
}


class module(empty_module):
    def __init__(self, module_param, comm):
        super(module, self).__init__(module_param, comm, name=__name__)

        self.immunity_id = 1000

        self.pop.add_attribute(
            "particle_infections", np.zeros(self.pop.Nparticles, dtype=int), store=True
        )
        self.pop.add_attribute(
            "mean_infection_count", np.zeros(self.comm.Nsteps, dtype=float), store=True
        )

        self.pop.add_attribute(
            "naive", np.zeros(self.comm.Nsteps, dtype=int), store=True
        )
        self.comm.add_to_execution(self.store_statistics, priority=100)

        natural_immunity = {
            "name": "Natural immunity",
            "effectiveness": module_param["immunity"],
            "effectiveness_parameters": module_param["immunity_parameters"],
            "filter": "np.zeros(self.pop.Nparticles, dtype=bool)",
            "start_day": 0,
            "stop_day": -1,
            "doses_per_day": 999999999999,
            "keep_last_value": True,
            "only_not_vaccinated": False,
        }
        self.immunity = add_vaccination(
            natural_immunity, self.comm, id=self.immunity_id
        )
        if self.immunity == None:
            self.comm.event_log(
                "Reinfection was not correctly initialized due to an incorrect immunity setting.",
                S.MSG_ERROR,
            )
        else:
            self.scenario_id = "Reinfec"
            self.pop.update_states = self.update_states
            if len(self.comm.vaccinations) == 1:
                self.comm.vaccinations.scenario_id = ""

        self.recovered_to_reinfectable(module_param["immunity_init_shift"])

    def store_statistics(self):
        self.pop.naive[self.clk.step] = np.count_nonzero(
            self.pop.time_exposed == S.NO_INFEC
        )

    def recovered_to_reinfectable(self, immunity_shift):
        """Set particles initialized as recovered to be reinfectable.

        Args:
            immunity_shift (int): Shift in days in the immunity curve.
        """
        # Set all recovered particles to reinfectable
        mask_recovered = self.pop.states == S.STATE_R
        if np.count_nonzero(mask_recovered) > 0:
            self.pop.states[mask_recovered] = S.STATE_S
            self.pop.symptoms[mask_recovered] = S.NO_INFEC
            self.immunity.apply_vaccine(mask_recovered)
            self.pop.disease.update_attributes(mask_recovered)

            # Transfer recovered counts to susceptible
            self.pop.susceptible[0] += self.pop.recovered[0]
            self.pop.recovered[0] = 0

            # Apply immunity shift if set
            if immunity_shift != 0:
                for pid in self.pop.pid[mask_recovered]:
                    if isinstance(immunity_shift, RandomDistibution):
                        shift = int(immunity_shift.sample(rng=self.rng))
                    else:
                        shift = immunity_shift
                    for attr in self.pop.particles_coefficients:
                        self.pop.particles_coefficients
                    new_curve = np.pad(
                        self.pop.particles_coefficients[attr][:, pid],
                        (0, shift),
                        mode="edge",
                    )
                    self.pop.particles_coefficients[attr][:, pid] = new_curve[shift:]

    def to_reinfectable(self, attributes):
        """{Symptomatic, Asymptomatic} --> Susceptible or deceased."""
        # Check if any infected particle recovered in the last time step
        mask_recover, mask_deceased = self.pop.disease.mask_to_recovered(
            self.pop.states, self.pop.symptoms
        )
        self.pop.maskNotYetSympRecovered = (
            self.pop.symptoms == S.SYMPT_NYET
        ) & mask_recover
        self.pop.states[mask_recover] = S.STATE_S
        self.pop.states[mask_deceased] = S.STATE_D
        self.pop.mark_for_new_position(mask_deceased, S.PLC_CEMT, S.ACT_deceased)
        mask_recover_n_deceased = mask_recover | mask_deceased
        self.pop.symptoms[mask_recover] = S.NO_INFEC
        self.pop.symptoms[mask_deceased] = 10 * self.pop.symptoms[mask_deceased]
        self.pop.symptoms[self.pop.maskNotYetSympRecovered] = S.NO_INFEC
        self.pop.time_recovered[mask_recover_n_deceased] = self.pop.clk.time
        for p in self.pop.pid[mask_deceased]:
            attributes[p].update({"time_deceased": self.pop.clk.time})
        for p in self.pop.pid[mask_recover]:
            attributes[p].update({"time_recovered": self.pop.clk.time})

        self.immunity.apply_vaccine(mask_recover)

        # Stores important information
        self.pop.deceased_cumulative += np.count_nonzero(mask_deceased)

    def update_states(self):
        """Update the state of the disease in every particle at the current time"""
        # Dictionary to store infection tree attributes
        attributes = dict(self.pop.empty_attributes)
        self.to_reinfectable(attributes)
        self.pop.to_infectious(attributes)
        self.pop.to_symptoms(attributes)
        prev_states = np.copy(self.pop.states)
        self.pop.to_exposed(attributes)
        mask_new_infections = (prev_states != self.pop.states) & (
            self.pop.states == S.STATE_E
        )
        self.pop.particle_infections[mask_new_infections] += 1
        self.pop.mean_infection_count[self.clk.step] = np.mean(
            self.pop.particle_infections
        )
        nx.set_node_attributes(self.pop.infection_tree, attributes)
