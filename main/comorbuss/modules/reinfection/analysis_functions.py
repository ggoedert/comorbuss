from ...settings import COLORS
import numpy as np


def plot_naive(self, seeds=None, **plot_args):
    """Plots the percentage of naive particles (never infected) in time.

    !!! Important
        This plot uses the do_plot method, see [plot_args](#plot_args) to see all available arguments.

    Args:
        seeds (list, optional): List of seeds to plot, if None will plot all seeds available.
        **plot_args (kwargs): Arguments for the do_plot method (see [plot_args](#plot_args)).
    """
    # Prepare Data
    data = self.get_data(["naive"], seeds)
    seeds = data["seeds"]
    naive = np.array(data["naive"])
    Nparticles = self.parameters["Nparticles"]
    data = {
        "naive": 100 * naive / Nparticles,
    }
    Nsteps = self.parameters["Nsteps"]
    dt = self.parameters["dt"]
    t = dt * np.arange(Nsteps) / 24

    # Plot graphs
    def_plot_args = {
        "xlabel": "Days",
        "ylabel": "% Naive",
    }
    plot_args = self.args_parse(plot_args, def_plot_args)
    self.do_plot(t, dt, data, [COLORS["blue"]], seeds, **plot_args)
