from ...tools import filter_parse, recursive_copy
from ... import settings as S
from ...aux_classes import empty_module, list_container
from numpy.random import default_rng
import numpy as np
import types


__name__ = "diagnostics"


def window_filter(start_state, start_time, end_state, end_time):
    """Generates a filter to be used in the 'filter_infected' parameter to diagnose in a time window.

    Args:
        start_state (int): State to mark the start of the window.
        start_time (float): Relative time from the transition to the start_state
        end_state (int): State to mark the end of the window.
        end_time (float): Relative time from the transition to the end_state

    Returns:
        tuple: [description]
    """
    STATES_FILTER = {
        (S.STATE_E, S.STATE_E): ("states", "==", S.STATE_E),
        (S.STATE_I, S.STATE_I): ("states", "==", S.STATE_I),
        (S.STATE_R, S.STATE_R): ("states", "==", S.STATE_R),
        (S.STATE_E, S.STATE_I): (
            ("states", "==", S.STATE_E),
            "|",
            ("states", "==", S.STATE_I),
        ),
        (S.STATE_I, S.STATE_R): (
            ("states", "==", S.STATE_I),
            "|",
            ("states", "==", S.STATE_R),
        ),
        (S.STATE_E, S.STATE_R): (
            ("states", "==", S.STATE_E),
            "|",
            ("states", "==", S.STATE_I),
            "|",
            ("states", "==", S.STATE_R),
        ),
    }
    STATES_TIMES = {
        S.STATE_E: "(self.clk.time_since(self.pop.time_exposed) {} {})",
        S.STATE_I: "(self.clk.time_since(self.pop.time_exposed + self.pop.disease.E_duration) {} {})",
        S.STATE_R: "(self.clk.time_since(self.pop.time_exposed + self.pop.disease.E_duration + self.pop.disease.I_duration) {} {})",
    }
    no_filter = "np.zeros(self.pop.Nparticles, dtype=bool)"
    if (start_state, end_state) not in STATES_FILTER:
        print(
            "start_state={} ans end_state={} is not a valid combination.".format(
                start_state, end_state
            )
        )
        return no_filter
    if (start_state == S.STATE_E and start_time <= 0) or (
        end_state == S.STATE_E and end_time <= 0
    ):
        print("A filter relative to S.STATE_E can not use negative time.")
        return no_filter
    return (
        STATES_TIMES[start_state].format(">=", start_time),
        "&",
        STATES_TIMES[end_state].format("<=", end_time),
        "&",
        STATES_FILTER[(start_state, end_state)],
    )


def sensitivity_PCR(diag, mask_test):
    days_from_symptoms = diag.clk.time_to_days(
        diag.clk.time_since(diag.pop.time_activated[mask_test])
    )
    sensitivity = np.zeros(diag.pop.Nparticles)
    sensitivity[mask_test] = 1 - days_from_symptoms * 0.0329962487
    sensitivity[sensitivity < 0] = 0
    sensitivity[sensitivity > 1] = 1
    return sensitivity


FILTER_SYMPTOMATIC = (
    ("symptoms", "==", S.SYMPT_YES),
    "|",
    ("symptoms", "==", S.SYMPT_SEVERE),
)


PCR = {
    "name": "PCR",
    "result_delay": 2.0,
    "sensitivity": 0.90,
    "specificity": 1.0,
}
"""All symptomatic particles be tested with sensitivity of 90% and a 2 days delay for results."""


SEROLOGICAL = {
    "name": "Serological",
    "sensitivity": 0.86,
    "specificity": 0.90,
    "filter_infected": (("states", "==", S.STATE_I), "|", ("states", "==", S.STATE_R)),
}
"""All symptomatic particles be tested with sensitivity of 86%."""


DEFAULTS = {
    "name": "Diagnostic",
    "filter_particles": FILTER_SYMPTOMATIC,
    "filter_infected": ("states", "==", S.STATE_I),
    "number_per_day": -1,
    "test_delay": 0.0,
    "result_delay": 0.0,
    "allow_retest": "no",
    "retest_delay": S.inf,
    "sensitivity": 0.85,
    "sensitivity_function": None,
    "specificity": 0.96,
    "start_day": 0,
    "end_day": -1,
}


DEFAULTS_TYPES = {
    "name": str,
    "filter_particles": tuple,
    "filter_infected": tuple,
    "number_per_day": int,
    "test_delay": float,
    "result_delay": float,
    "allow_retest": str,
    "retest_delay": float,
    "sensitivity": float,
    "sensitivity_function": [types.FunctionType, type(None)],
    "specificity": float,
    "start_day": int,
    "end_day": int,
}


def add_diagnostic(diag_param, name, comm, default=DEFAULTS):
    from ...triage.triage_functions import parse_from_defaults

    # Parse DEFAULTS for diagnostics
    diag_param = recursive_copy(diag_param)
    default = recursive_copy(default)
    parse_from_defaults(default, name, DEFAULTS, DEFAULTS_TYPES, comm.event_log)
    if hasattr(comm, "diagnostics"):
        return comm.diagnostics.add_diagnostics(diag_param, default)
    else:
        diag_param = triage([diag_param], {}, comm.clk, comm.event_log)
        comm.init_module("diagnostics", diag_param)
        return comm.diagnostics[0]


def diag_allow_retest_check(diag, allowed, default, event_log):
    if not diag["allow_retest"] in allowed:
        event_log(
            "Allow retest: {} for diagnostics {} is not valid, assuming default "
            "{}.".format(diag["allow_retest"], diag["name"], default),
            S.MSG_WRNG,
        )
        diag["allow_retest"] = default


def triage(diagnostics, t_parameters, clk, event_log):
    from ...triage.triage_functions import parse_from_defaults, merge_list_of_dicts

    diagnostics = merge_list_of_dicts(diagnostics, "diagnostics", event_log)

    for diag in diagnostics:
        parse_from_defaults(diag, "diagnostics", DEFAULTS, DEFAULTS_TYPES, event_log)
        diag["test_delay"] = clk.days_to_time(diag["test_delay"])
        diag["result_delay"] = clk.days_to_time(diag["result_delay"])
        diag["retest_delay"] = clk.days_to_time(diag["retest_delay"])
        diag_allow_retest_check(
            diag,
            ["yes", "no", "negative", "reinfection"],
            DEFAULTS["allow_retest"],
            event_log,
        )
    return diagnostics


class module(empty_module, list_container):
    """Diagnostics module, aggregates all diagnostic objects.

    #### Population attributes

    This module creates the following population attributes

    Attributes:
        diag_states (np.ndarray int): Last diagnostic state for every particle.
        diag_type (np.ndarray int): The id of the last diagnostics applied in each particle.
        diag_false (np.ndarray bool): Indicates if the last diagnostic for each particle is
            a false diagnostic.
        diag_waiting (np.ndarray bool): Indicates if a particle is waiting for a diagnostic (particles
            can be waiting to be tested or for the result of a test).
        last_diag_states (np.ndarray int): Copy of the diag_state at the last step.
        diagnosed (np.ndarray int): Counts of the number of diagnosed particles at each step.
        diagnosed_cum (np.ndarray int): Counts of the cumulative number of diagnosed particles at each step.
    """

    def __init__(self, diag_param, comm):
        """Initialize diagnostics policies.

        Args:
            parameters (dict): Parameters for the simulation.
            comm (community): The community object.

        Returns:
            List: List of the disease objects.
        """
        super(module, self).__init__(diag_param, comm, name=__name__)

        self.diagnostics = []
        for i, diag_param in enumerate(diag_param):
            self.init_diag(diag_param, i)

        self.pop.add_attribute(
            "diag_states", S.DIAG_NYET * np.ones(self.comm.Nparticles, dtype=int)
        )
        self.pop.add_attribute(
            "diag_type", -1 * np.ones(self.comm.Nparticles, dtype=int)
        )
        self.pop.add_attribute("diag_false", np.zeros(self.comm.Nparticles, dtype=bool))
        self.pop.add_attribute(
            "diag_waiting", -1 * np.ones(self.comm.Nparticles, dtype=int)
        )
        self.pop.add_attribute("last_diag_states", np.copy(self.pop.diag_states))
        self.pop.add_attribute("diagnosed_cumulative", 0)
        self.pop.add_attribute(
            "diagnosed", np.zeros(self.comm.Nsteps, dtype=int), store=True
        )
        self.pop.add_attribute(
            "diagnosed_cum", np.zeros(self.comm.Nsteps, dtype=int), store=True
        )
        self.pop.add_attribute(
            "reinfection_flag", np.zeros(self.comm.Nparticles, dtype=bool)
        )

        if len(self.diagnostics) > 0:
            self.comm.add_to_execution(self.diagnostic, priority=25)

        self.list = self.diagnostics

    def __call__(self, *args, **kwargs):
        return self.diagnostic(*args, **kwargs)

    def init_diag(self, diag_param, i):
        diag_param["id"] = i
        diag_param["filter_particles"], fok_p = filter_parse(
            diag_param["filter_particles"],
            self.comm,
            allowed_filters=[
                "tracing",
                "service",
                "workers",
                "vaccination",
                "module",
                "vaccination_attr",
            ],
        )
        diag_param["filter_infected"], fok_i = filter_parse(
            diag_param["filter_infected"],
            self.comm,
            allowed_filters=[
                "tracing",
                "service",
                "workers",
                "vaccination",
                "module",
                "vaccination_attr",
            ],
        )
        fok = fok_i and fok_p
        if fok:
            self.diagnostics.append(diagnostic(diag_param, self.comm))
        else:
            self.event_log(
                "Can't activate {} diagnostics protocol, there is an invalid"
                " filter.".format(diag_param["name"]),
                S.MSG_WRNG,
            )

    def add_diagnostics(self, diag_param, defaults=DEFAULTS):
        diag_param = triage([diag_param], {}, self.clk, self.event_log)
        i = len(self.diagnostics)
        self.init_diag(diag_param[0], i)
        return self.diagnostics[i]

    ##### Diagnostic implementation
    def diagnostic(self, check_time=True, diags=None):
        """Runs diagnostics policies"""
        diags = diags or self.diagnostics
        self.pop.last_diag_states = np.copy(self.pop.diag_states)
        if self.clk.between_hours(*self.comm.free_hours) or not check_time:
            for diag in diags:
                diag()

        self.pop.diagnosed[self.clk.step] = np.count_nonzero(
            (self.pop.states == S.STATE_I) & (self.pop.diag_states > 0)
        )
        self.pop.diagnosed_cum[self.clk.step] = np.count_nonzero(
            self.pop.diag_states > 0
        )
        self.pop.diagnosed_cumulative = self.pop.diagnosed_cum[self.clk.step]

    def store_time_series(self):
        self.pop.diag_states_ts[self.clk.step, :] = self.pop.diag_states

    def init_time_series(self):
        self.pop.add_attribute(
            "diag_states_ts",
            S.DIAG_NYET * np.ones((self.comm.Nsteps, self.comm.Nparticles), dtype=int),
        )
        self.comm.add_to_store("diag_states", "comm.pop.diag_states_ts", True)
        self.pop.diag_states_ts[0, :] = self.pop.diag_states


class diagnostic:
    """This class stores the attributes for a diagnostic policy and the
        methods for its progression.

    Attributes:
        name (str): Name of the diagnostic.
        id (int): Id of the diagnostic (same as diag_type).
        filter (str): Parsed filter to select particles to diagnose.
        start_day (int): Day to start this diagnostic.
        end_day (int): Day to end this diagnostic.
        sensitivity (float): Sensitivity of the diagnostic (fraction of the positives
            that it is able to correctly identify).
        specificity (float): Specificity of the diagnostic (fraction of the negatives
            that it is able to correctly identify).
        tests_per_hour (np.ndarray): Array with the number of tests to apply in each hour
            of the day.
    """

    def __init__(self, diag_param, comm):
        """Initialize diagnostics object.

        Args:
            diag_param (dict): Parameters related to the diagnostic.
            comm (community): The community object.
        """
        # Parent classes access attributes
        self.comm = comm
        self.pop = comm.pop
        self.clk = comm.clk
        self.rng = default_rng(self.comm.random_seed)
        self.event_log = comm.event_log

        # General attributes
        self.name = diag_param["name"]
        self.id = diag_param["id"]
        self.filter = diag_param["filter_particles"]
        self.filter_infected = diag_param["filter_infected"]
        self.sensitivity = diag_param["sensitivity"]
        self.specificity = diag_param["specificity"]
        self.start_day = diag_param["start_day"]
        self.end_day = diag_param["end_day"]
        if self.end_day < 0:
            self.end_day = S.inf

        # Sensitivity function
        self.has_sens_func = (
            type(diag_param["sensitivity_function"]) is types.FunctionType
        )
        if self.has_sens_func:
            self.sens_func = diag_param["sensitivity_function"]
            diag_param["sensitivity_function"] = format(self.sens_func)

        # retest attributes
        self.allow_retest = diag_param["allow_retest"] != "no"
        self.retest_yes = diag_param["allow_retest"] == "yes"
        self.retest_negative = diag_param["allow_retest"] == "negative"
        self.retest_delay = diag_param["retest_delay"]
        self.retest_reinfection = diag_param["allow_retest"] == "reinfection"

        # Delay attributes
        self.test_delay = diag_param["test_delay"]
        self.has_test_delay = self.test_delay > 0
        self.result_delay = diag_param["result_delay"]
        self.has_result_delay = self.result_delay > 0
        self.time_selected = -1 * np.ones(self.pop.Nparticles)
        self.time_tested = -1 * np.ones(self.pop.Nparticles)
        if self.has_result_delay:
            self.result_positive = np.zeros(self.pop.Nparticles, dtype=bool)
            self.result_false = np.zeros(self.pop.Nparticles, dtype=bool)

        # Number of tests attributes
        free_hours = self.clk.time_to_steps(
            self.comm.free_hours[1] - self.comm.free_hours[0]
        )
        total_tests = diag_param["number_per_day"]
        tests_per_hour = np.ceil(total_tests / free_hours)
        self.tests_per_hour = np.zeros((free_hours), dtype=int)
        for i in range(len(self.tests_per_hour)):
            if total_tests < 0:
                self.tests_per_hour[i] = np.min([total_tests, tests_per_hour])
            else:
                self.tests_per_hour[i] = -1
            total_tests -= tests_per_hour
        self.tests_counts = 0
        self.positives_counts = 0
        self.negatives_counts = 0
        self.comm.add_to_store(
            "tests_counts_{}".format(self.id),
            "comm.diagnostics[{}].tests_counts".format(self.id),
        )
        self.comm.add_to_store(
            "positives_counts_{}".format(self.id),
            "comm.diagnostics[{}].positives_counts".format(self.id),
        )

    def __repr__(self):
        return "<{} diagnostic>".format(self.name)

    def __call__(self):
        """Runs methods related to apply this diagnostic."""
        self.apply()

    def raise_filters(self, filters, name):
        import traceback, sys

        print("Previous traceback:")
        for line in traceback.format_exception(*sys.exc_info()):
            print(line)
        raise RuntimeError(
            "Error evaluating {} for {} diagnostics, parsed filter is: {}".format(
                name, self.name.lower(), filters
            )
        )

    def can_be_tested(self):
        """Returns a mask of the particles that can be tested at the moment.

        Returns:
            np.ndarray: Mask of the particles that can be tested at the moment.
        """
        mask_diagnosed = self.pop.diag_states != S.DIAG_NYET
        if self.retest_yes or self.retest_negative:
            mask_retest_delay = (
                self.clk.time_since(self.pop.time_diagnosed) >= self.retest_delay
            )
        if self.retest_yes:
            mask_diagnosed[mask_retest_delay] = False
        if self.retest_negative:
            mask_diag_no = self.pop.diag_states == S.DIAG_NO
            mask_diagnosed[mask_retest_delay & mask_diag_no] = False
        if self.retest_reinfection:
            mask_susceptible = self.pop.states == S.STATE_S
            self.pop.reinfection_flag[mask_susceptible] = False
            mask_diagnosed[~self.pop.reinfection_flag] = False
        mask_not_waiting_diag = self.pop.diag_waiting < 0
        return mask_not_waiting_diag & ~mask_diagnosed

    def apply(self):
        """Apply this diagnostic."""
        # Only enters diagnostic if between start and end day and between free hours.
        if self.clk.today >= self.start_day and self.clk.today < self.end_day:

            # Auxiliary masks
            try:
                mask_infected = eval(self.filter_infected)
            except:
                self.raise_filters(self.filter, "filter_infected")
            mask_can_test = self.can_be_tested()

            # Select particles to diagnose
            try:
                mask_testable = eval(self.filter) & mask_can_test
            except:
                self.raise_filters(self.filter, "filter_particles")
            random_pids = self.pop.pid[mask_testable]
            self.rng.shuffle(random_pids)
            number_of_tests = self.tests_per_hour[
                self.clk.time_to_steps(self.clk.tod - self.comm.free_hours[0])
            ]
            if number_of_tests >= 0:
                random_pids = random_pids[:number_of_tests]
            mask_test = np.isin(self.pop.pid, random_pids)
            self.time_selected[mask_test] = self.clk.time
            self.time_tested[mask_test] = -1  # Important to reset delays!!!

            # Check for test delay, if true replace mask_test accordingly
            if self.has_test_delay:
                # Stores selected particles for further testing
                self.pop.diag_waiting[mask_test] = self.id

                # Selects particles that already passed delay to test
                mask_test = (
                    (self.pop.diag_waiting == self.id)
                    & (self.time_tested < 0)
                    & (self.clk.time_since(self.time_selected) >= self.test_delay)
                )
                self.pop.diag_waiting[mask_test] = -1

            # Do testing
            self.time_tested[mask_test] = self.clk.time
            probab = self.rng.uniform(0.0, 1.0, size=self.comm.Nparticles)
            if self.has_sens_func:
                mask_sensitivity = probab <= self.sens_func(self, mask_test)
            else:
                mask_sensitivity = probab <= self.sensitivity
            probab = self.rng.uniform(0.0, 1.0, size=self.comm.Nparticles)
            mask_specificity = probab <= self.specificity
            mask_true_positive = mask_test & mask_infected & mask_sensitivity
            mask_false_positive = mask_test & ~mask_infected & ~mask_specificity
            mask_true_negative = mask_test & ~mask_infected & mask_specificity
            mask_false_negative = mask_test & mask_infected & ~mask_sensitivity
            self.tests_counts += np.count_nonzero(mask_test)

            # Consolidate results
            mask_positive = mask_true_positive | mask_false_positive
            mask_negative = mask_true_negative | mask_false_negative
            mask_false = mask_false_positive | mask_false_negative

            # Check for result delay, if true replace mask_test accordingly
            if self.has_result_delay:
                # Stores selected particles for further result
                self.pop.diag_waiting[mask_test] = self.id
                self.result_positive[mask_positive] = True
                self.result_positive[mask_negative] = False
                self.result_false[mask_false] = True
                self.result_false[mask_true_positive | mask_true_negative] = False

                # Selects particles that already passed delay to assign results
                mask_test = (
                    (self.pop.diag_waiting == self.id)
                    & (self.time_tested > 0)
                    & (self.clk.time_since(self.time_tested) >= self.result_delay)
                )
                mask_positive = mask_test & self.result_positive
                mask_negative = mask_test & ~self.result_positive
                mask_false = mask_test & self.result_false
                self.pop.diag_waiting[mask_test] = -1

            # Apply changes to population
            self.pop.diag_states[mask_positive] = S.DIAG_YES
            self.pop.update_inf_tree_attributes(
                mask_positive, "diagnostic_result", S.DIAG_YES
            )
            self.positives_counts += np.count_nonzero(mask_positive)
            self.pop.diag_states[mask_negative] = S.DIAG_NO
            self.pop.update_inf_tree_attributes(
                mask_positive, "diagnostic_result", S.DIAG_NO
            )
            self.negatives_counts += np.count_nonzero(mask_negative)
            self.pop.diag_type[mask_test] = self.id
            if self.retest_reinfection:
                self.pop.reinfection_flag[mask_test] = True
            self.pop.diag_false[mask_false] = True
            self.pop.update_inf_tree_attributes(mask_false, "diagnostic_false", True)
            self.pop.time_diagnosed[mask_test] = self.clk.time
            self.pop.update_inf_tree_attributes(
                mask_test, "time_diagnosed", self.clk.time
            )
            self.pop.update_inf_tree_attributes(mask_test, "diagnostic_id", self.id)
