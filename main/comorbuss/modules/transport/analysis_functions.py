from ...settings import COLORS, COLORS_LONG_LIST
import numpy as np

def plot_particles_in_pubtransp(self, seeds=None, per_ambient= False, **plot_args):
    """Plots the number of users in the public transport per day.

    !!! Important
        This plot uses the do_plot method, see [plot_args](#plot_args) to see all available arguments.

    Args:
        seeds (list, optional): List of seeds to plot, if None will plot all seeds available.
        **plot_args (kwargs): Arguments for the do_plot method (see [plot_args](#plot_args)).
    """
    # Prepare Data
    data = self.get_data(["particles_per_time_public"], seeds)
    seeds = data["seeds"]
    Nparticles = self.parameters["Nparticles"]
    particles_array = data["particles_per_time_public"]
    Nsteps = self.parameters["Nsteps"]
    Nseeds = len(seeds)
    norm_n = self.parameters["norm_N"]
    Ndays = self.parameters["Ndays"]
    dt = self.parameters["dt"]
    t = np.arange(Nsteps / 24)
    particles_by_day_dict= dict()
    def_plot_args = {
        "xlabel": "Days",
        "ylabel": " Particles in public transport",
    }
    plot_args = self.args_parse(plot_args, def_plot_args)
    if per_ambient:
        places = {S.PLC_HOME:"Home" , S.PLC_ENV:"Environment"}
        for srvc in self.parameters["services"]:
            places[srvc["id"]] = srvc["name"]
        for i in range(0, np.array(particles_array).shape[2]):
            tmp = np.array(particles_array)[:,:,i]
            tmp = np.reshape(tmp, (Nseeds , Ndays, int(Nsteps/Ndays)))
            tmp = (np.sum(tmp, axis = 2))
            particles_by_day_dict[places[i-2]] = tmp
        print(particles_by_day_dict.keys()) 
        self.do_plot(t, dt, particles_by_day_dict, COLORS_LONG_LIST, seeds=seeds, **plot_args)
    else:
        particles_by_step = np.sum(particles_array, axis=2)
        particles_by_step = np.reshape(particles_by_step, (Nseeds , Ndays, int(Nsteps/Ndays)))
        particles_by_day = (np.sum(particles_by_step, axis = 2))
        particles_by_day_dict["Particles"] = particles_by_day
        self.do_plot(t, dt, particles_by_day_dict, [COLORS["blue"]], seeds, **plot_args)