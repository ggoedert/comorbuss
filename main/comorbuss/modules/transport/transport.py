from os import stat
from ...aux_classes import empty_module
from ...net_generators import adjacency_generator
from ... import settings as S
import numpy as np

DEFAULTS = {
    "time_in_transport": -1,
    "public_transport_fraction": 0.5,
    "transport_size": 20,
    "transport_size_std": 5,
    "mean_contacts": 2,
}

# __name__ = "transport"


def triage(parameters, comm_parameters, clk, event_log):
    from ...triage.triage_functions import type_get

    param = {}
    param["time_in_transport"] = type_get(
        parameters,
        "time_in_transport",
        comm_parameters["dt"],
        None,
        None,
        event_log,
        dtype=float,
    )
    param["transport_size"] = type_get(
        parameters,
        "transport_size",
        DEFAULTS["transport_size"],
        None,
        None,
        event_log,
        dtype=float,
    )
    param["transport_size_std"] = type_get(
        parameters,
        "transport_size_std",
        DEFAULTS["transport_size_std"],
        None,
        None,
        event_log,
        dtype=float,
    )
    param["mean_contacts"] = type_get(
        parameters,
        "mean_contacts",
        DEFAULTS["mean_contacts"],
        None,
        None,
        event_log,
        dtype=float,
    )
    if param["time_in_transport"] == -1:
        param["time_in_transport"] = comm_parameters["dt"]
    param["frac_public"] = type_get(
        parameters,
        "public_transport_fraction",
        DEFAULTS["public_transport_fraction"],
        None,
        None,
        event_log,
        dtype=bool,
    )

    return param


class module(empty_module):
    def __init__(self, param, comm):
        super(module, self).__init__(param, comm, name=__name__)
        self.net_rng = np.random.default_rng(self.comm.random_seed)

        self.time_in_transport = param["time_in_transport"]
        self.frac_public = param["frac_public"]
        self.size = param["transport_size"]
        self.size_std = param["transport_size_std"]
        self.mean_contacts = param["mean_contacts"]
        self.inf_prob_weight = param["time_in_transport"] / self.comm.dt
        probab = self.rng.uniform(size=self.pop.Nparticles)
        self.mask_pubic_transport = probab <= self.frac_public
        num_services = 0
        for srvc in self.comm.srv:
            self.update_service_times(srvc)
            num_services += 1
        self.pop.mark_for_new_position = self.mark_for_new_position
        self.pop.add_attribute(
            "placement_buffer", S.PLC_NONE * np.ones(self.pop.Nparticles, dtype=int)
        )
        self.pop.add_attribute(
            "activity_buffer", S.ACT_FREE * np.ones(self.pop.Nparticles, dtype=int)
        )
        # create a matrix with shape (num_destinations, Nsteps)
        num_destinations = 2 + num_services
        self.pop.add_attribute(
            "particles_per_time_public",
            np.zeros((self.comm.Nsteps, num_destinations), dtype=int),
            store=True,
        )
        self.comm.add_to_execution(self.count_particles_in_public_transp, priority=0)
        self.comm.add_to_execution(self.move_from_transport, priority=0)
        self.pop.add_net_generator(S.PLC_TRSP, self)

    def update_service_times(self, srvc):
        srvc.working_hours[0] = srvc.working_hours[0] - self.comm.dt
        srvc.workers_shift_start = srvc.workers_shift_start - self.comm.dt

    def gen(self, mask_alive):
        mask_particles = (
            (self.pop.placement == S.PLC_TRSP) & mask_alive & self.mask_pubic_transport
        )
        nparticles = np.count_nonzero(mask_particles)
        nets = []
        if nparticles > 0:
            pids = self.pop.pid[mask_particles]
            while len(pids) > 0:
                group_size = int(self.rng.normal(self.size, self.size_std))
                while group_size < 1:
                    group_size = int(self.rng.normal(self.size, self.size_std))
                if group_size > len(pids):
                    group_size = len(pids)
                group_pids = pids[:group_size]
                pids = pids[group_size:]
                if group_size > 1:
                    mask_particles = np.isin(self.pop.pid, group_pids)
                    nets.append(
                        (
                            group_size,
                            mask_particles,
                            adjacency_generator(
                                group_size, self.mean_contacts, self.net_rng
                            ),
                        )
                    )
        return nets

    def count_particles_in_public_transp(self):
        """Count all the particles that are in public transport at the current time step

        """
        mask_pubtransp_now = (self.pop.placement == S.PLC_TRSP) & self.mask_pubic_transport
        values, counts = np.unique(self.pop.placement_buffer[mask_pubtransp_now], return_counts=True)
        values = values + 2
        if(len(values) > 0):
            self.pop.particles_per_time_public[self.clk.step,values] = counts

    def mark_for_new_position(self, to_change, placement, activity):
        """Mark particles for changes in position

        Args:
            to_change (np.ndarray): Mask or list of pids for particles to be changed
            placement (int): New placement
            activity (int): New activity
        """
        if np.count_nonzero(to_change) > 0:
            old_placement = self.pop.placement[to_change]
            if len(np.array(placement).shape) == 0:
                placement = placement * np.ones(len(old_placement))
            if len(np.array(activity).shape) == 0:
                activity = activity * np.ones(len(old_placement))
            mask_change = old_placement != placement
            mask_not_in_transport = self.pop.placement_buffer == S.PLC_NONE
            no_transport_changes = [(S.PLC_ENV, S.PLC_HOME), (S.PLC_HOME, S.PLC_ENV)]
            no_transport = np.zeros(len(old_placement), dtype=bool)
            for i in range(len(old_placement)):
                no_transport[i] = (
                    old_placement[i],
                    placement[i],
                ) in no_transport_changes
            mask_no_transport = np.zeros(self.pop.placement.shape, dtype=bool)
            mask_no_transport[to_change] = (
                no_transport | ~mask_change
            ) & mask_not_in_transport[to_change]
            mask_transport = np.zeros(self.pop.placement.shape, dtype=bool)
            mask_transport[to_change] = (
                ~no_transport & mask_not_in_transport[to_change] & mask_change
            )
            if np.count_nonzero(mask_no_transport) > 0:
                self.pop.placement[mask_no_transport] = placement[
                    mask_no_transport[to_change]
                ]
                self.pop.activity[mask_no_transport] = activity[
                    mask_no_transport[to_change]
                ]
            if np.count_nonzero(mask_transport):
                self.pop.placement[mask_transport] = S.PLC_TRSP
                self.pop.activity[mask_transport] = S.ACT_TRNP
                self.pop.placement_buffer[mask_transport] = placement[
                    mask_transport[to_change]
                ]
                self.pop.activity_buffer[mask_transport] = activity[
                    mask_transport[to_change]
                ]

    def move_from_transport(self):
        mask_to_move = self.pop.placement == S.PLC_TRSP
        self.pop.placement[mask_to_move] = self.pop.placement_buffer[mask_to_move]
        self.pop.placement_buffer[mask_to_move] = S.PLC_NONE
        self.pop.activity[mask_to_move] = self.pop.activity_buffer[mask_to_move]
