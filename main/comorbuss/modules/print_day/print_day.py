from ...aux_classes import empty_module
from ...settings import MSG_PRGS

__name__ = "print_day"

DEFAULTS = {}


class module(empty_module):
    def __init__(self, param, comm):
        super(module, self).__init__(param, comm, name=__name__)
        self.event_log = comm.event_log
        if param:
            self.comm.add_to_execution(self.print_day, priority=999, time_of_the_day=0)

    def print_day(self):
        self.event_log(
            "Simulating day: {}/{}".format(self.clk.today, self.comm.Ndays), MSG_PRGS
        )
