import networkx as nx
import numpy as np
from .aux_classes import mem_clear
from . import settings as S
from .tools import in_list_of_dicts
import math
from numba import njit
from numpy.random import default_rng
from .triage import defaults


class home_net(mem_clear):
    def __init__(self, net_par, comm):
        from .triage.defaults import home_network

        # Upper classes
        self.comm = comm
        self.pop = comm.pop
        self.clk = comm.clk
        self.parameters = net_par
        self.inf_prob_weight = net_par.get(
            "inf_prob_weight", home_network["default"]["inf_prob_weight"]
        )
        self.mean_contacts = net_par.get(
            "mean_contacts", home_network["default"]["mean_contacts"]
        )
        self.rng = default_rng(self.comm.parameters["random_seed"])
        self.homes = self.pop.homes

    def gen(self, mask_alive):
        """Generator for networks at home. Generate a network with fixed number of contacts.

        Args:
            nparticles (int): Number of nodes in the network.
            n_conections (float): Mean number of connections.
            seed (np.random): Random number generator.

        Returns:
            Graph: Generated network.
        """
        nets = []
        for home in self.homes.keys():
            mask_particles = (
                (self.pop.placement == S.PLC_HOME)
                & mask_alive
                & self.homes[home]["mask"]
            )
            nparticles = np.count_nonzero(mask_particles)
            if nparticles > 1:
                nets.append(
                    (
                        nparticles,
                        mask_particles,
                        adjacency_generator(nparticles, self.mean_contacts, self.rng),
                    )
                )
        return nets


class enviroment_net(mem_clear):
    def __init__(self, net_par, comm):
        from .triage.defaults import environment_network

        # Upper classes
        self.comm = comm
        self.pop = comm.pop
        self.clk = comm.clk
        self.rng = default_rng(self.comm.parameters["random_seed"])
        self.parameters = net_par
        self.inf_prob_weight = net_par.get(
            "inf_prob_weight", environment_network["default"]["inf_prob_weight"]
        )
        self.inf_radii = net_par.get(
            "inf_radii", environment_network["default"]["inf_radii"]
        )
        city_area = net_par.get(
            "city_area", environment_network["default"]["city_area"]
        )
        city_pop = self.comm.parameters.get("city_pop")
        norm_L = np.sqrt(self.pop.Nparticles * (city_area / city_pop))
        self.inf_radii = self.inf_radii / (1000.0 * norm_L)

    def gen(self, mask_alive):
        """Generator for networks in the environmental layer.

        Args:
            nparticles (int): Number of nodes in the network.
            mask_environment (np.ndarray): Mask of particles in the environment.
            pop (population): Population object.

        Returns:
            Graph: Generated network.
        """
        mask_environment = (self.pop.placement == S.PLC_ENV) & mask_alive
        n_particles = np.count_nonzero(mask_environment)
        # Number of contacts based on random walk and infection radius
        n_contacts = int(
            0.5
            * n_particles
            * (n_particles - 1)
            * np.pi
            * self.inf_radii
            * self.inf_radii
        )
        if n_contacts >= n_particles:
            n_contacts = n_particles - 1
        if n_contacts > 0:
            # Get a certain number of unique contact ids for being one of the particles in each contact
            unique_contact_ids = self.rng.choice(
                self.pop.pid[mask_environment], n_contacts, False
            )
            real_n_contacts = len(unique_contact_ids)
            mask_environment[unique_contact_ids] = False
            # Probabilities of contact are given according to data in Table 2 of https://doi.org/10.1016/j.socnet.2007.04.005
            waifw_normed = np.zeros((len(S.WAIFW), np.count_nonzero(mask_environment)))
            for i, waifw_row in enumerate(S.WAIFW):
                waifw_normed[i, :] = np.array(
                    norm(waifw_row[self.pop.ages[mask_environment]])
                )
            # Get particles on the other side of contacts
            contact_ids = np.zeros(real_n_contacts, dtype=int)
            for i, unique_contact_id in enumerate(unique_contact_ids):
                contact_ids[i] = self.rng.choice(
                    self.pop.pid[mask_environment],
                    size=1,
                    replace=True,
                    p=waifw_normed[self.pop.ages[unique_contact_id]],
                )
            # Make encounters mask
            [unique_ids, inverse_map] = np.unique(
                np.concatenate((unique_contact_ids, contact_ids)), return_inverse=True
            )
            n_particles_in_encounters = len(unique_ids)
            mask_in_encounters = np.full(
                (n_particles_in_encounters, n_particles_in_encounters), False
            )
            mask_in_encounters[
                inverse_map[:real_n_contacts], inverse_map[real_n_contacts:]
            ] = True
            mask_in_encounters = mask_in_encounters | mask_in_encounters.transpose()
            return [
                (
                    n_particles_in_encounters,
                    np.isin(self.pop.pid, unique_ids),
                    mask_in_encounters,
                )
            ]
        else:
            return []


#%% Services net generators
class airborne(mem_clear):
    def __init__(self, srvc, net_par, comm):
        # Upper classes
        self.comm = comm
        self.pop = comm.pop
        self.clk = comm.clk
        self.disease = comm.pop.disease
        self.srvc = srvc
        self.rng = default_rng(self.comm.parameters["random_seed"])
        self.inf_prob_weight = self.srvc.inf_prob_weight

        # Network parameters

        self.pm = self.pop.gen_value_by_age(net_par.get("mask_penetration_factor", 0.3))
        self.B = float(net_par.get("breathing_rate", 0.75))
        self.Cp = self.pop.gen_value_by_age(
            net_par.get("age_group_quanta_concentration", 72.0)
        )
        self.Lambda = float(net_par.get("outdoor_air_flow_rate", 45.0))
        self.V = float(net_par.get("room_volume", 150.0))
        self.dt = net_par.get("time_step_size", self.comm.dt)
        self.sr_multi = float(net_par.get("susceptibility_multiplication_factor", 1.0))

        # Internal parameters
        self.prob_getting_infected = np.zeros(self.comm.Nparticles)
        self.prob_not_getting_infected_accum = [
            np.zeros(self.comm.Nparticles) for _ in range(len(self.srvc.rooms) + 1)
        ]
        self.exposure_time = np.zeros(len(self.srvc.rooms) + 1)
        self.initial_room_quanta_concentration = np.zeros(len(self.srvc.rooms) + 1)
        self.room_step = np.full(len(self.srvc.rooms) + 1, self.clk.step, dtype=int)
        self.max_inf_susceptibility = np.max(
            self.comm.given_parameters.get(
                "inf_susceptibility", defaults.inf_susceptibility["default"]
            )
        )
        self.sr_all = self.disease.inf_susceptibility / self.max_inf_susceptibility

    def inf_probability_calc(self, sus_mask, inf_mask, nparticles, room):
        pm, B, Cp, Lambda, V, sr_all = (
            self.pm,
            self.B,
            self.Cp,
            self.Lambda,
            self.V,
            self.sr_all,
        )
        sr = sr_all[sus_mask]
        N = nparticles
        Ceq = np.sum(pm[inf_mask] * B * Cp[inf_mask]) / (Lambda + N * B)
        C0 = self.initial_room_quanta_concentration[room]
        tau = V / (Lambda + N * B)
        T = self.exposure_time[room]
        C = Ceq + (C0 - Ceq) * np.exp(-T / tau)
        d = (B * pm[sus_mask]) * Ceq * T + (B * pm[sus_mask]) * (C0 - Ceq) * tau * (
            1.0 - np.exp(-T / tau)
        )
        P = 1.0 - np.exp(-sr * d)
        if np.count_nonzero(sus_mask) > 0:
            p = 1.0 - (1.0 - P) / self.prob_not_getting_infected_accum[room][sus_mask]
        else:
            p = P
        return p, C

    def gen(self, mask_alive):
        """Generates networks in the airborne aerosol transmission context.

        Args:
            mask_alive (np.ndarray): Mask of the alive particles.

        Returns:
            Graph: Generated network.
        """
        nets = []
        infectious_mask = self.pop.states == S.STATE_I
        susceptible_mask = self.pop.states == S.STATE_S
        self.sr_all = (
            self.disease.inf_susceptibility
            / self.max_inf_susceptibility
            * self.sr_multi
        )
        self.prob_getting_infected[:] = 0
        for instance in range(self.srvc.number):
            for room in self.srvc.rooms:
                particles_mask = (
                    (self.pop.placement == self.srvc.placement)
                    & (self.srvc.instance == instance)
                    & (self.srvc.room == room)
                    & mask_alive
                )
                nparticles = np.count_nonzero(particles_mask)
                # Checking whether room has been opened consecutively
                if nparticles > 0:
                    if self.room_step[room] != (self.clk.step - 1):
                        self.prob_not_getting_infected_accum[room][:] = 1.0
                        self.initial_room_quanta_concentration[room] = 0.0
                        self.exposure_time[room] = 0.0
                    self.room_step[room] = self.clk.step
                # Checking if there are infectious particles
                particle_ids = self.pop.pid[particles_mask]
                inf_mask = infectious_mask & particles_mask
                ninfectious = np.count_nonzero(inf_mask)
                if ninfectious > 0:
                    self.exposure_time[room] += self.dt
                    # Checking if there are susceptible articles, in which case
                    # conections must be made
                    sus_mask = susceptible_mask & particles_mask
                    nsusceptible = np.count_nonzero(sus_mask)
                    if nsusceptible > 0:
                        # Generates connections between all particles
                        net, particle_ids = full_net(nparticles)
                        nets.append((nparticles, particles_mask, net))
                    # PS: Assuming that particles leave and enter the
                    # room uniformly. That allows for storing "room"
                    # quantities scalarly rather than vectorially
                    p, C = self.inf_probability_calc(
                        sus_mask, inf_mask, nparticles, room
                    )
                    self.prob_getting_infected[inf_mask] = 0.0
                    self.prob_getting_infected[sus_mask] = p
                    # If there were susceptible particles in the room,
                    # the probability of not getting infected must be
                    # stored for later use, and the room quanta concentration
                    # must be kept as initially. If there are no susceptible
                    # particles, then the probability of not getting infected
                    # must be reset, and room quanta concentration must be
                    # stored.
                    # PS: Ideally, initial room quanta concentration and
                    # exposure time should also be stored individually!
                    if nsusceptible == 0:
                        self.prob_not_getting_infected_accum[room][:] = 1.0
                        self.initial_room_quanta_concentration[room] = C
                    else:
                        self.prob_not_getting_infected_accum[room][sus_mask] *= 1.0 - p
                        self.prob_not_getting_infected_accum[room][~sus_mask] = 1.0

        return nets

    def mask_new_infections(self, possibly_infected, mask_particles, nparticles, *_):
        mask_new_infections = np.zeros((nparticles, nparticles), dtype=bool)
        pid = np.arange(nparticles, dtype=int)
        probab = self.rng.uniform(0.0, 1.0, size=nparticles)
        mask_sources = self.pop.states[mask_particles] == S.STATE_I
        mask_susceptible = self.pop.states[mask_particles] == S.STATE_S
        new_infections_ids = pid[
            (probab < self.prob_getting_infected[mask_particles]) & mask_susceptible
        ]
        sources_ids = self.rng.choice(
            pid[mask_sources], size=len(new_infections_ids), replace=True
        )
        mask_new_infections[sources_ids, new_infections_ids] = True
        return mask_new_infections


class markets(mem_clear):
    def __init__(self, srvc, net_par, comm):
        # Upper classes
        self.comm = comm
        self.pop = comm.pop
        self.clk = comm.clk
        self.srvc = srvc
        self.rng = default_rng(self.comm.parameters["random_seed"])
        self.inf_prob_weight = self.srvc.inf_prob_weight

        # Network parameters
        self.worker_mean_contacts = net_par["worker_mean_contacts"]
        self.visitor_mean_contacts = net_par["visitor_mean_contacts"]
        self.visitor_to_worker_mean_contacts = net_par[
            "visitor_to_worker_mean_contacts"
        ]
        self.cashier_prob = net_par["cashier_prob"]
        self.cashiers_pool_frac = net_par.get("cashiers_pool_frac", 0)

        # Extra attributes
        self.has_cashiers_type = in_list_of_dicts(
            self.srvc.workers_parameters, "Cashiers", "name"
        )
        if self.has_cashiers_type:
            cashiers_param = in_list_of_dicts(
                self.srvc.workers_parameters, "Cashiers", "name", return_dict=True
            )
            self.cashiers = self.srvc.workers_ids[
                self.srvc.workers_type == cashiers_param["id"]
            ]
        self.has_cashier_pool = self.cashier_prob < self.cashiers_pool_frac
        if self.has_cashier_pool:
            self.today_cashiers_ids = dict()

    def gen(self, mask_alive):
        """Generates networks in the markets context.

        Args:
            srvc (service): Service object.
            mask_alive (np.ndarray): Mask of the alive particles.
            seed (np.random, optional): Random number generator.

        Returns:
            Graph: Generated network.
        """
        nets = []
        for instance in range(self.srvc.number):
            expected_visitors_per_instance = (
                self.srvc.expected_visits / self.srvc.number
            )
            for room in self.srvc.rooms:
                particles_mask = (
                    (self.pop.placement == self.srvc.placement)
                    & (self.srvc.instance == instance)
                    & (self.srvc.room == room)
                    & mask_alive
                )
                nparticles = np.count_nonzero(particles_mask)
                if nparticles > 1:
                    particle_ids = self.pop.pid[particles_mask]
                    workers_mask = np.isin(particle_ids, self.srvc.workers_ids)
                    workers_ids = particle_ids[workers_mask]
                    n_workers = np.sum(workers_mask)
                    # If has Cashiers type of workers on service use them as cashiers
                    if self.has_cashiers_type:
                        cashiers_mask = np.isin(particle_ids, self.cashiers)
                        cashiers_ids = particle_ids[cashiers_mask]
                        n_cashiers = len(cashiers_ids)
                    # Else, if has pool of workers to select cashiers select from pool with cashier_prob
                    elif self.has_cashier_pool:
                        n_cashiers = int(np.ceil(self.cashier_prob * n_workers))
                        # Check if there is workers selected for this instance today
                        try:
                            cashiers_ids = self.today_cashiers_ids[
                                (instance, room, self.clk.today)
                            ]
                            cashiers_mask = np.isin(particle_ids, cashiers_ids)
                            if np.sum(cashiers_mask) < n_cashiers:
                                raise KeyError("")
                        # If not select them from pool
                        except KeyError:
                            workers_ids = particle_ids[workers_mask]
                            n_cashiers_n_pool = math.ceil(
                                self.cashiers_pool_frac * np.count_nonzero(workers_mask)
                            )
                            workers_n_pool_ids = workers_ids[:n_cashiers_n_pool]
                            cashiers_ids = self.rng.choice(
                                workers_n_pool_ids, n_cashiers, replace=False
                            )
                            self.today_cashiers_ids[
                                (instance, room, self.clk.today)
                            ] = cashiers_ids
                            cashiers_mask = np.isin(particle_ids, cashiers_ids)
                    # If none of the above select cashiers from the end of the list of workers with cashier_prob
                    else:
                        n_cashiers = int(np.ceil(self.cashier_prob * n_workers))
                        cashiers_ids = workers_ids[0:n_cashiers]
                        cashiers_mask = np.isin(particle_ids, cashiers_ids)
                    visitors_mask = ~workers_mask
                    net, particle_ids = empty_net(nparticles)
                    # generation of contacts between workers
                    workers_ids = particle_ids[workers_mask]
                    net, n_workers = gen_contacs_single_group(
                        workers_mask, net, self.worker_mean_contacts, self.rng
                    )
                    # connect visitors to visitors
                    visitors_ids = particle_ids[visitors_mask]
                    net, n_visitors = gen_contacs_single_group(
                        visitors_mask,
                        net,
                        self.visitor_mean_contacts,
                        self.rng,
                        expected_visitors_per_instance,
                    )
                    # connect visitors to workers
                    net = gen_contacs_two_groups(
                        visitors_ids,
                        workers_ids,
                        net,
                        self.visitor_to_worker_mean_contacts,
                        self.rng,
                    )
                    # every visitor particle makes a contact with a cashier
                    cashiers_ids = particle_ids[cashiers_mask]
                    if n_visitors > 0 and n_cashiers > 0:
                        random_cashiers_ids = self.rng.choice(cashiers_ids, n_visitors)
                        for cashier_id, visitor_id in zip(
                            random_cashiers_ids, visitors_ids
                        ):
                            net = add_edge(net, cashier_id, visitor_id)
                    nets.append((nparticles, particles_mask, net))
        return nets


class schools(mem_clear):
    def __init__(self, srvc, net_par, comm):
        # upper classes
        self.comm = comm
        self.pop = comm.pop
        self.clk = comm.clk
        self.srvc = srvc
        self.rng = default_rng(self.comm.parameters["random_seed"])
        self.inf_prob_weight = self.srvc.inf_prob_weight

        # network parameters
        self.break_period = net_par["break_period"]
        self.break_classrooms = net_par["break_classrooms"]
        self.break_mean_contacts = net_par["break_mean_contacts"]

    def gen(self, mask_alive):
        """Generates networks in the schools context.

        Args:
            srvc (service): Service object.
            mask_alive (np.ndarray): Mask of the alive particles.
            seed (np.random, optional): Random number generator.

        Returns:
            Graph: Generated network.
        """
        nets = []
        for instance in range(self.srvc.number):
            break_period = self.break_period
            classrooms_per_break = self.break_classrooms
            if self.clk.between_hours(
                self.srvc.working_hours[0], self.srvc.working_hours[0] + break_period
            ):
                # random generation of contacts between all particles in same school (classroom?)
                # To do: improve break period
                jRange = [
                    self.srvc.rooms[
                        i : np.minimum(i + classrooms_per_break, len(self.srvc.rooms))
                    ]
                    for i in range(0, len(self.srvc.rooms), classrooms_per_break)
                ]
                for j in jRange:
                    particles_mask = (
                        (self.pop.placement == self.srvc.placement)
                        & (self.srvc.instance == instance)
                        & (np.isin(self.srvc.room, j))
                        & mask_alive
                    )
                    nparticles = np.count_nonzero(particles_mask)
                    if nparticles > 1:
                        contacts_per_particle = self.break_mean_contacts
                        net = adjacency_generator(
                            nparticles, contacts_per_particle, self.rng
                        )
                        nets.append((nparticles, particles_mask, net))
            else:
                for room in self.srvc.rooms:
                    particles_mask = (
                        (self.pop.placement == self.srvc.placement)
                        & (self.srvc.instance == instance)
                        & (self.srvc.room == room)
                        & mask_alive
                    )
                    nparticles = np.count_nonzero(particles_mask)
                    if nparticles > 1:
                        net, _ = empty_net_nx(nparticles)
                        # students are positioned in a 2d grid, which then generates the
                        # contacts according to the neighbors
                        students_x = int(np.sqrt(nparticles))
                        students_y = students_x + 1
                        students_xy = students_x * students_y
                        net2d = nx.grid_2d_graph(students_x, students_y)
                        mapping = dict(zip(net2d, range(students_xy)))
                        net2d = nx.relabel_nodes(net2d, mapping)
                        for node in range(nparticles, students_xy):
                            net2d.remove_node(node)
                        net = nx.compose(net, net2d)
                        net.remove_edges_from(nx.selfloop_edges(net))
                        net = nx.to_numpy_array(net, dtype=bool)
                        nets.append((nparticles, particles_mask, net))
        return nets


class restaurants_1(mem_clear):
    def __init__(self, srvc, net_par, comm):
        # upper classes
        self.comm = comm
        self.pop = comm.pop
        self.clk = comm.clk
        self.srvc = srvc
        self.rng = default_rng(self.comm.parameters["random_seed"])
        self.inf_prob_weight = self.srvc.inf_prob_weight

        # network parameters
        self.waiter_prob = net_par["waiter_prob"]
        self.workers_mean_contacts = net_par["workers_mean_contacts"]

    def gen(self, mask_alive):
        """Generates networks in the restaurants context.

        Args:
            srvc (service): Service object.
            mask_alive (np.ndarray): Mask of the alive particles.
            seed (np.random, optional): Random number generator.

        Returns:
            Graph: Generated network.
        """
        nets = []
        for instance in range(self.srvc.number):
            for room in self.srvc.rooms:
                particles_mask = (
                    (self.pop.placement == self.srvc.placement)
                    & (self.srvc.instance == instance)
                    & (self.srvc.room == room)
                    & mask_alive
                )
                nparticles = np.count_nonzero(particles_mask)
                if nparticles > 1:
                    particle_ids = self.pop.pid[particles_mask]
                    workers_mask = np.isin(particle_ids, self.srvc.workers_ids)
                    visitors_mask = ~workers_mask
                    net, particle_ids = empty_net(nparticles)
                    waiter_pct = self.waiter_prob
                    n_workers = np.count_nonzero(workers_mask)
                    n_waiters = int(np.ceil((waiter_pct * n_workers)))
                    workers_ids = particle_ids[workers_mask]
                    waiters_ids = workers_ids[0:n_waiters]
                    net, _ = gen_contacs_single_group(
                        workers_mask, net, self.workers_mean_contacts, self.rng
                    )
                    n_visitors = np.count_nonzero(visitors_mask)
                    visitors_ids = particle_ids[visitors_mask]
                    random_waiter_ids = self.rng.choice(waiters_ids, n_visitors)
                    for waiter in waiters_ids:
                        visitor_by_waiter_ids = visitors_ids[
                            np.isin(random_waiter_ids, waiter)
                        ]
                        if len(visitor_by_waiter_ids) > 0:
                            table_holder_id = self.rng.choice(visitor_by_waiter_ids, 1)
                            net = add_edge(net, waiter, table_holder_id[0])
                            net_waiter = nx.cycle_graph(len(visitor_by_waiter_ids))
                            net_waiter = nx.to_numpy_array(net_waiter, dtype=bool)
                            mask_waiter = np.isin(particle_ids, visitor_by_waiter_ids)
                            net = compose_adjacency(net, net_waiter, mask_waiter)
                    nets.append((nparticles, particles_mask, net))
        return nets


class restaurants_2(mem_clear):
    def __init__(self, srvc, net_par, comm):
        # upper classes
        self.comm = comm
        self.pop = comm.pop
        self.clk = comm.clk
        self.srvc = srvc
        self.rng = default_rng(self.comm.parameters["random_seed"])
        self.inf_prob_weight = self.srvc.inf_prob_weight

        # network parameters
        self.waiter_prob = net_par["waiter_prob"]
        self.persons_per_table = net_par["persons_per_table"]
        self.workers_mean_contacts = net_par["workers_mean_contacts"]

    def gen(self, mask_alive):
        """Generates networks in the restaurants context.

        Args:
            srvc (service): Service object.
            mask_alive (np.ndarray): Mask of the alive particles.
            seed (np.random, optional): Random number generator.

        Returns:
            Graph: Generated network.
        """
        nets = []
        for instance in range(self.srvc.number):
            for room in self.srvc.rooms:
                particles_mask = (
                    (self.pop.placement == self.srvc.placement)
                    & (self.srvc.instance == instance)
                    & (self.srvc.room == room)
                    & mask_alive
                )
                nparticles = np.count_nonzero(particles_mask)
                if nparticles > 1:
                    particle_ids = self.pop.pid[particles_mask]
                    workers_mask = np.isin(particle_ids, self.srvc.workers_ids)
                    visitors_mask = ~workers_mask
                    net, particle_ids = empty_net(nparticles)
                    waiter_pct = self.waiter_prob
                    persons_per_table = self.persons_per_table
                    n_workers = np.count_nonzero(workers_mask)
                    n_waiters = int(np.ceil((waiter_pct * n_workers)))
                    workers_ids = particle_ids[workers_mask]
                    waiters_ids = workers_ids[0:n_waiters]
                    net, _ = gen_contacs_single_group(
                        workers_mask, net, self.workers_mean_contacts, self.rng
                    )
                    n_visitors = np.count_nonzero(visitors_mask)
                    if n_visitors > 0 and n_waiters > 0:
                        visitors_ids = particle_ids[visitors_mask]
                        random_waiter_ids = self.rng.choice(waiters_ids, n_visitors)
                        for waiter in waiters_ids:
                            visitor_by_waiter_ids = visitors_ids[
                                np.isin(random_waiter_ids, waiter)
                            ]
                            n_visitors_by_waiter = len(visitor_by_waiter_ids)
                            if n_visitors_by_waiter > 0:
                                for start in range(
                                    0, n_visitors_by_waiter, persons_per_table
                                ):
                                    end = np.minimum(
                                        start + persons_per_table, n_visitors_by_waiter
                                    )
                                    table_net, _ = full_net(end - start)
                                    mask_table = np.isin(
                                        particle_ids, visitor_by_waiter_ids[start:end]
                                    )
                                    net = compose_adjacency(net, table_net, mask_table)
                                for visitor_id in visitor_by_waiter_ids:
                                    net = add_edge(net, waiter, visitor_id)
                    nets.append((nparticles, particles_mask, net))
        return nets


class street_markets(mem_clear):
    def __init__(self, srvc, net_par, comm):
        # upper classes
        self.comm = comm
        self.pop = comm.pop
        self.clk = comm.clk
        self.srvc = srvc
        self.rng = default_rng(self.comm.parameters["random_seed"])
        self.inf_prob_weight = self.srvc.inf_prob_weight

        # network parameters
        self.seller_mean_contacts = net_par["seller_mean_contacts"]
        self.visitor_mean_contacts = net_par["visitor_mean_contacts"]
        self.visitor_to_seller_mean_contacts = net_par[
            "visitor_to_seller_mean_contacts"
        ]

    def gen(self, mask_alive):
        """Generates networks in the street markets context.

        Args:
            srvc (service): Service object.
            mask_alive (np.ndarray): Mask of the alive particles.
            seed (np.random, optional): Random number generator.

        Returns:
            Graph: Generated network.
        """
        nets = []
        for instance in range(self.srvc.number):
            expected_visitors_per_instance = (
                self.srvc.expected_visits / self.srvc.number
            )
            for room in self.srvc.rooms:
                particles_mask = (
                    (self.pop.placement == self.srvc.placement)
                    & (self.srvc.instance == instance)
                    & (self.srvc.room == room)
                    & mask_alive
                )
                nparticles = np.count_nonzero(particles_mask)
                if nparticles > 1:
                    particle_ids = self.pop.pid[particles_mask]
                    workers_mask = np.isin(particle_ids, self.srvc.workers_ids)
                    visitors_mask = ~workers_mask
                    net, particle_ids = empty_net(nparticles)
                    # random generation of contacts between sellers inside street market
                    sellers_id = particle_ids[workers_mask]
                    net, n_sellers = gen_contacs_single_group(
                        workers_mask, net, self.seller_mean_contacts, self.rng
                    )
                    # random generation of contacts between sellers inside street market
                    visitors_id = particle_ids[visitors_mask]
                    net, n_visitors = gen_contacs_single_group(
                        visitors_mask,
                        net,
                        self.visitor_mean_contacts,
                        self.rng,
                        expected_visitors_per_instance,
                    )
                    # every visitor particle makes a few contacts with a seller
                    if (n_visitors > 0) and (n_sellers > 0):
                        n_choice_samples = round(
                            self.rng.normal(
                                self.visitor_to_seller_mean_contacts * n_visitors, 0.25
                            )
                        )
                        if n_choice_samples > 0:
                            random_seller_ids = self.rng.choice(
                                sellers_id, n_choice_samples
                            )
                            random_visitors_ids = self.rng.choice(
                                visitors_id, n_choice_samples
                            )
                            for seller, visitor_id in zip(
                                random_seller_ids, random_visitors_ids
                            ):
                                net = add_edge(net, seller, visitor_id)
                    nets.append((nparticles, particles_mask, net))
        return nets


class hospitals(mem_clear):
    def __init__(self, srvc, net_par, comm):
        # upper classes
        self.comm = comm
        self.pop = comm.pop
        self.clk = comm.clk
        self.srvc = srvc
        self.rng = default_rng(self.comm.parameters["random_seed"])
        self.inf_prob_weight = self.srvc.inf_prob_weight

        # network parameters
        self.disease_worker_prob = net_par.get("disease_worker_prob", 0.0)
        self.worker_mean_contacts = net_par["worker_mean_contacts"]
        self.disease_worker_mean_contacts = net_par["disease_worker_mean_contacts"]
        self.disease_worker_to_worker_mean_contacts = net_par[
            "disease_worker_to_worker_mean_contacts"
        ]
        self.visitor_mean_contacts = net_par["visitor_mean_contacts"]
        self.visitor_to_worker_mean_contacts = net_par[
            "visitor_to_worker_mean_contacts"
        ]
        self.guest_to_disease_worker_mean_contacts = net_par[
            "guest_to_disease_worker_mean_contacts"
        ]
        self.expose_visitors_to_disease_workers = net_par.get(
            "expose_visitors_to_disease_workers", False
        )
        self.disease_workers_pool_frac = net_par.get("disease_workers_pool_frac", 0.0)

        self.disease_ward_weight = net_par.get("disease_ward_weight", 1.0)
        self.has_disease_ward_weight = self.disease_ward_weight != 1.0
        if self.has_disease_ward_weight:
            self.pop.disease.add_coefficient(
                "inf_susceptibility", np.ones(self.pop.Nparticles), "hosp_net"
            )

        # Extra attributes
        self.has_disease_workers = in_list_of_dicts(
            self.srvc.workers_parameters, "Disease workers", "name"
        )
        if self.has_disease_workers:
            disease_worker_param = in_list_of_dicts(
                self.srvc.workers_parameters,
                "Disease workers",
                "name",
                return_dict=True,
            )
            self.disease_workers = self.srvc.workers_ids[
                self.srvc.workers_type == disease_worker_param["id"]
            ]
        self.has_disease_workers_pool = (
            self.disease_worker_prob < self.disease_workers_pool_frac
        )
        if self.has_disease_workers_pool:
            self.today_disease_workers_ids = dict()

    def gen(self, mask_alive):
        """Generates networks in the hospitals context.

        Args:
            srvc (service): Service object.
            mask_alive (np.ndarray): Mask of the alive particles.
            seed (np.random, optional): Random number generator.

        Returns:
            Graph: Generated network.
        """
        nets = []
        if self.has_disease_ward_weight:
            dis_ward_coeff = np.ones(self.pop.Nparticles)
        for instance in range(self.srvc.number):
            expected_visitors_per_instance = (
                self.srvc.expected_visits / self.srvc.number
            )
            for room in self.srvc.rooms:
                particles_mask = (
                    (self.pop.placement == self.srvc.placement)
                    & (self.srvc.instance == instance)
                    & (self.srvc.room == room)
                    & mask_alive
                )
                nparticles = np.count_nonzero(particles_mask)
                if nparticles > 1:
                    particle_ids = self.pop.pid[particles_mask]
                    workers_mask = np.isin(particle_ids, self.srvc.workers_ids)
                    # If has Disease workers type of workers on service use them as disease workers
                    if self.has_disease_workers:
                        disease_workers_mask = np.isin(
                            particle_ids, self.disease_workers
                        )
                        normal_workers_mask = workers_mask & ~disease_workers_mask
                    # Else, if disease_worker_prob is 0 or 1 all workers are disease workers and normal workers
                    elif self.disease_worker_prob == 0 or self.disease_worker_prob == 1:
                        normal_workers_mask = workers_mask
                        disease_workers_mask = np.zeros(workers_mask.shape, dtype=bool)
                    # Else, if has pool of workers to select disease workers select from pool with disease_worker_prob
                    elif self.has_disease_workers_pool:
                        n_disease_workers = math.ceil(
                            self.disease_worker_prob * np.count_nonzero(workers_mask)
                        )
                        # Check if there is workers selected for this instance today
                        try:
                            disease_workers_ids = self.today_disease_workers_ids[
                                (instance, room, self.clk.today)
                            ]
                            disease_workers_mask = np.isin(
                                particle_ids, disease_workers_ids
                            )
                            if np.sum(disease_workers_mask) < n_disease_workers:
                                raise KeyError("")
                        # If not select them from pool
                        except KeyError:
                            workers_ids = particle_ids[workers_mask]
                            n_workers_not_n_pool = math.floor(
                                (1 - self.disease_workers_pool_frac)
                                * np.count_nonzero(workers_mask)
                            )
                            workers_n_pool_ids = workers_ids[n_workers_not_n_pool:]
                            disease_workers_ids = self.rng.choice(
                                workers_n_pool_ids, n_disease_workers, replace=False
                            )
                            self.today_disease_workers_ids[
                                (instance, room, self.clk.today)
                            ] = disease_workers_ids
                            disease_workers_mask = np.isin(
                                particle_ids, disease_workers_ids
                            )
                        normal_workers_mask = workers_mask & ~disease_workers_mask
                    # If none of the above select disease workers from the end of the list of workers with disease_worker_prob
                    else:
                        n_workers = math.floor(
                            (1 - self.disease_worker_prob)
                            * np.count_nonzero(workers_mask)
                        )
                        normal_workers_ids = particle_ids[workers_mask][:n_workers]
                        normal_workers_mask = np.isin(particle_ids, normal_workers_ids)
                        disease_workers_ids = particle_ids[workers_mask][n_workers:]
                        disease_workers_mask = np.isin(
                            particle_ids, disease_workers_ids
                        )
                    guests_mask = np.isin(particle_ids, self.srvc.guests_ids)
                    visitors_mask = ~(workers_mask | guests_mask)
                    net, particle_ids = empty_net(nparticles)
                    # connect workers to workers
                    workers_ids = particle_ids[normal_workers_mask]
                    net, _ = gen_contacs_single_group(
                        normal_workers_mask, net, self.worker_mean_contacts, self.rng
                    )
                    # Only generates encounters between disease workers and workers if there is disease workers
                    if np.sum(disease_workers_mask) > 0:
                        disease_workers_ids = particle_ids[disease_workers_mask]
                        net, _ = gen_contacs_single_group(
                            disease_workers_mask,
                            net,
                            self.disease_worker_mean_contacts,
                            self.rng,
                        )
                        net = gen_contacs_two_groups(
                            disease_workers_ids,
                            workers_ids,
                            net,
                            self.disease_worker_to_worker_mean_contacts,
                            self.rng,
                        )
                        if self.expose_visitors_to_disease_workers:
                            workers_ids = np.unique(
                                np.concatenate((workers_ids, disease_workers_ids))
                            )
                        if self.has_disease_ward_weight:
                            dis_ward_coeff[
                                np.append(
                                    disease_workers_ids, self.srvc.guests_ids
                                ).astype(int)
                            ] = self.disease_ward_weight
                    else:
                        disease_workers_ids = workers_ids
                    # connect visitors to visitors
                    visitors_ids = particle_ids[visitors_mask]
                    net, _ = gen_contacs_single_group(
                        visitors_mask,
                        net,
                        self.visitor_mean_contacts,
                        self.rng,
                        expected_visitors_per_instance,
                    )
                    # connect visitors to workers
                    net = gen_contacs_two_groups(
                        visitors_ids,
                        workers_ids,
                        net,
                        self.visitor_to_worker_mean_contacts,
                        self.rng,
                    )
                    # connect guests to disease workers
                    guests_id = particle_ids[guests_mask]
                    net = gen_contacs_two_groups(
                        guests_id,
                        disease_workers_ids,
                        net,
                        self.guest_to_disease_worker_mean_contacts,
                        self.rng,
                    )
                    # Post process and store net
                    nets.append((nparticles, particles_mask, net))
        if self.has_disease_ward_weight:
            self.pop.disease.set_coefficient(
                "inf_susceptibility", dis_ward_coeff, "hosp_net"
            )
        return nets


class isolate(mem_clear):
    def __init__(self, srvc, net_par, comm):
        # upper classes
        self.comm = comm
        self.pop = comm.pop
        self.clk = comm.clk
        self.srvc = srvc
        self.inf_prob_weight = self.srvc.inf_prob_weight

        # network parameters

    def gen(self, mask_alive):
        """Isolate all particles.

        Args:
            srvc (service): Service object.
            mask_alive (np.ndarray): Mask of the alive particles.
            seed (np.random, optional): Random number generator.

        Returns:
            Graph: Generated network.
        """
        return []


class default(mem_clear):
    def __init__(self, srvc, net_par, comm):
        # upper classes
        self.comm = comm
        self.pop = comm.pop
        self.clk = comm.clk
        self.srvc = srvc
        self.rng = np.random
        self.rng2 = default_rng(self.comm.parameters["random_seed"])
        self.inf_prob_weight = self.srvc.inf_prob_weight

        # network parameters
        self.net_par = net_par

        if "m" in net_par:
            self.gen_net = self.gen_barabasi
        elif "n_conections":
            self.gen_net = self.gen_erdos

    def gen(self, mask_alive):
        """Default generator.

        Args:
            srvc (service): Service object.
            mask_alive (np.ndarray): Mask of the alive particles.
            seed (np.random, optional): Random number generator.

        Returns:
            Graph: Generated network.
        """
        nets = []
        for instance in range(self.srvc.number):
            for room in self.srvc.rooms:
                particles_mask = (
                    (self.pop.placement == self.srvc.placement)
                    & (self.srvc.instance == instance)
                    & (self.srvc.room == room)
                    & mask_alive
                )
                nparticles = np.count_nonzero(particles_mask)
                if nparticles > 1:
                    nets.append(self.gen_net(nparticles, particles_mask))
        return nets

    def gen_barabasi(self, nparticles, particles_mask):
        m = int(np.min([nparticles - 1, self.net_par["m"]]))
        net = nx.barabasi_albert_graph(nparticles, m, seed=self.rng)
        net = nx.to_numpy_array(net, dtype=bool)
        return nparticles, particles_mask, net

    def gen_erdos(self, nparticles, particles_mask):
        net = adjacency_generator(nparticles, self.net_par["n_conections"], self.rng2)
        return nparticles, particles_mask, net


#% Auxiliary functions
def empty_net(n):
    """Generate an adjacency mask with no connections.

    Args:
        n (int): Number of nodes in the network.

    Returns:
        np.array: nxn adjacency mask.
    """
    net = np.zeros((n, n), dtype=bool)
    nodes = np.arange(n)
    return net, nodes


def empty_net_nx(n):
    net = nx.Graph()
    nodes = np.arange(n)
    net.add_nodes_from(nodes)
    return net, nodes


def full_net(n):
    net = np.ones((n, n), dtype=bool)
    np.fill_diagonal(net, False)
    nodes = np.arange(n)
    return net, nodes


def gen_contacs_single_group(
    mask_group, net, mean_contacts, randgen, expected_number=0
):
    n_particles = np.count_nonzero(mask_group)
    if n_particles > 1:
        if expected_number > 1:
            mean_contacts *= (n_particles - 1.0) / (expected_number - 1.0)
        net_group = adjacency_generator(n_particles, mean_contacts, randgen)
        return compose_adjacency(net, net_group, mask_group), n_particles
    return net, n_particles


def gen_contacs_two_groups(ids_1, ids_2, net, mean_contacts_1_to_2, randgen):
    n_particles_1 = len(ids_1)
    n_particles_2 = len(ids_2)
    if n_particles_1 > 0 and n_particles_2 > 0:
        n_choice_samples = round(
            randgen.normal(mean_contacts_1_to_2 * n_particles_1, 0.25)
        )
        if n_choice_samples > 0:
            random_ids_1 = randgen.choice(ids_1, n_choice_samples)
            random_ids_2 = randgen.choice(ids_2, n_choice_samples)
            for p1, p2 in zip(random_ids_1, random_ids_2):
                net = add_edge(net, p1, p2)
    return net


def add_edge(mask_adjacency, node1, node2):
    mask_adjacency[node1, node2] = True
    mask_adjacency[node2, node1] = True
    return mask_adjacency


def compose_adjacency(m1, m2, mask):
    j = 0
    for i in range(len(m1)):
        if mask[i]:
            m1[i, mask] = m2[j]
            j += 1
    return m1


def norm(l):
    return np.array(l) / np.sum(l)


def adjacency_generator(nodes, mean_conn, generator):
    """Generates an random adjacency matrix following the mean number of connections.

    Args:
        nodes (int): Number of nodes.
        mean_conn (float): Mean number of connections per node.
        generator (np.random.Generator): Random number generator

    Returns:
        np.array: Boolean adjacency matrix.
    """
    probab = generator.uniform(0, 1, size=(nodes, nodes))
    return _adjacency_generator(nodes, mean_conn, probab)


@njit
def _adjacency_generator(nodes, mean_conn, probab):
    if mean_conn > nodes - 1:
        mean_conn = nodes - 1
    p_edge = mean_conn / (nodes - 1)
    mask_adjacency = probab <= p_edge
    np.fill_diagonal(mask_adjacency, False)
    mask_adjacency = np.triu(mask_adjacency)
    mask_adjacency = mask_adjacency + mask_adjacency.T
    return mask_adjacency


SERVICE_NET_GENS = {
    S.NET_MARKETS: markets,
    S.NET_SCHOOLS: schools,
    S.NET_RESTAURANTS_1: restaurants_1,
    S.NET_RESTAURANTS_2: restaurants_2,
    S.NET_STREET_MARKETS: street_markets,
    S.NET_HOSPITALS: hospitals,
    S.NET_ISOLATE: isolate,
    S.NET_DEFAULT: default,
    S.NET_AIRBORNE: airborne,
}
