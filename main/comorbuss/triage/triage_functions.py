import h5dict
import numpy as np
import networkx as nx
import pandas as pd
from datetime import datetime
from numpy.random import default_rng
from .. import settings as S
from . import defaults
from ..aux_classes import clock, RandomDistibution
from .. import services as srvcs
from .. import quarantines as qrnts
from ..decisions import DEF_PAR_LCKD, DEF_PAR_SRV_CLOSE, DECISIONS_NAMES
from ..tools import normalize, recursive_copy


def demographic_normalization(value, norm_factor, min_value=1, dtype=int):
    return dtype(np.max([min_value, (value / norm_factor)]))


demo_norm = demographic_normalization


def log_invalid(value, param_name, error, default, event_log):
    event_log(
        "{:} is invalid for {:} parameter ({:}), assuming default: {:}".format(
            value, param_name, error, default
        ),
        S.MSG_WRNG,
    )


def log_out_of_limits(value, param_name, limits, assumed, event_log):
    event_log(
        "{:} is invalid for {:} parameter (out of limits {:}), assuming: {:}".format(
            value, param_name, limits, assumed
        ),
        S.MSG_WRNG,
    )


def get_value(param, name, default):
    value = param.get(name, default)
    if name in param:
        del param[name]
    return value


def type_get(
    param,
    name,
    default,
    _,
    __,
    event_log,
    dtype=None,
    limits=[],
    from_list=[],
):
    value = get_value(param, name, default)
    if dtype == None:
        dtype = np.array(default).dtype
    try:
        dtype(value)
    except TypeError:
        log_invalid(
            value,
            name,
            "couldn't convert to {:}".format(dtype.__name__),
            default,
            event_log,
        )
        value = default
    # Apply limits
    if len(limits) >= 1 and value != default:
        if limits[0] != None:
            if value < limits[0]:
                log_out_of_limits(value, name, limits, limits[1], event_log)
                value = limits[0]
        if len(limits) >= 2:
            if limits[1] != None:
                if value > limits[1]:
                    log_out_of_limits(value, name, limits, limits[1], event_log)
                    value = limits[1]
    # Test if restricted to a list
    if len(from_list) > 0:
        if value not in from_list:
            log_invalid(
                value,
                name,
                "value expected to be one of {:}".format(from_list),
                default,
                event_log,
            )
            value = default
    return value


def array_get(
    param,
    name,
    default,
    _,
    __,
    event_log,
    dtype=None,
    normalize_array=False,
    enforce_shape=False,
    shape=[],
    allow_single_value=False,
    apply_function=lambda x: x,
    limits=[],
    print_name=None,
):
    if print_name == None:
        print_name = name
    if type(default) is not np.ndarray:
        default = np.array(default)
    if dtype == None:
        dtype = [default.dtype]
    value = get_value(param, name, default)
    # Try to convert
    converted = False
    if type(dtype) not in (list, tuple):
        dtype = [dtype]
    for t in dtype:
        if not converted:
            try:
                value = np.array(value, dtype=t)
                # If type is object check if all values are instances of given class
                if value.dtype is np.dtype("O"):
                    for i in np.reshape(value, int(np.prod(value.shape))):
                        if not isinstance(i, t):
                            raise TypeError()
                converted = True
            except TypeError:
                pass
    if not converted:
        event_log(
            "{} is invalid for {} parameter (can't convert to an array of type {}),"
            " assuming default: {}".format(value, print_name, dtype, default),
            S.MSG_WRNG,
        )
        value = default
    # Check shape
    if enforce_shape:
        if shape == []:
            shape = default.shape
        if (value.shape != shape) and not allow_single_value:
            event_log(
                "{} is invalid for {} parameter (expected shape is {}), assuming"
                " default: {}".format(value, print_name, default.shape, default),
                S.MSG_WRNG,
            )
            value = default
        if allow_single_value and not (
            (value.shape == shape) or (len(value.shape) == 0)
        ):
            event_log(
                "{} is invalid for {} parameter (expected a single value or shape"
                " = {}), assuming default: {}".format(
                    value, print_name, default.shape, default
                ),
                S.MSG_WRNG,
            )
            value = default
    # Apply function and/or normalize
    if normalize_array:
        value = normalize(value)
    value = apply_function(value)
    # Apply limits
    if len(limits) >= 1:
        # Try to apply limits to numbers
        try:
            if limits[0] != None:
                value[value < limits[0]] = limits[0]
            if len(limits) >= 2:
                if limits[1] != None:
                    value[value > limits[1]] = limits[1]
        # On TypeError try to apply to objects
        except TypeError:
            for i in np.reshape(value, int(np.prod(value.shape))):
                if hasattr(i, "set_limits"):
                    i.set_limits(*limits)
    return value


def convert_date(date, name, default, event_log):
    if date != None:
        try:
            date = datetime.strptime(date, "%Y-%m-%d")
        except:
            try:
                date = datetime.fromtimestamp(int(date))
            except:
                event_log(
                    '{} is not a {} in the format "YYYY-mm-dd" or a timestamp,'
                    " assuming default {}.".format(date, name, date),
                    S.MSG_WRNG,
                )
                date = convert_date(default, "default for " + name, None, event_log)
        return date


def get_date(given_parameters, name, default, parameters, _, event_log, set_clk=False):
    date = get_value(given_parameters, name, default)
    if set_clk:
        parameters["clk"] = clock(start_date=date, dt=parameters["dt"])
    return convert_date(date, name, default, event_log)


def days_to_time(value, clk):
    try:
        if len(value) > 1:
            for i in range(len(value)):
                value[i] = days_to_time(value[i])
            return value
    except TypeError:
        pass
    try:
        value = np.array(clk.days_to_time(value))
    except TypeError:
        for i in np.reshape(value, int(np.prod(value.shape))):
            if hasattr(i, "set_factor"):
                i.set_factor(clk.days_to_time(1))
    return value


def get_days_array(given_parameters, name, default, parameters, _, event_log, **kwargs):
    clk = parameters["clk"]
    value = array_get(given_parameters, name, default, None, None, event_log, **kwargs)
    value = days_to_time(value, clk)
    return value


def prob_dist_func_get(param, name, default, dt, event_log):
    value = get_value(param, name, default)
    try:
        return float(value) * dt
    except:
        if not ("distribution" in value):
            event_log(
                "{} has to either be a number, or a dictionary in the form "
                "{'distribution': numpy_random_func_string, 'kwargs': kwargs_string}, "
                "assuming default {}.".format(param, default),
                S.MSG_WRNG,
            )
            return default * dt
        if not ("arguments" in value):
            value["arguments"] = ""
        value["multiplicative_constant"] = dt
        return value


def time_dist_func_get(param, name, default, use_virulence, clk_conversion, event_log):
    value = get_value(param, name, default)
    try:
        return clk_conversion(float(value))
    except TypeError:
        if not use_virulence:
            event_log(
                "{} has to be a number when use_virulence=False, using default value"
                " {}.".format(name, default),
                S.MSG_WRNG,
            )
            return clk_conversion(default)
        if not isinstance(value, RandomDistibution):
            event_log(
                "{} has to either be a number or a RandomDistibution "
                "object, assuming default {}.".format(param, default),
                S.MSG_WRNG,
            )
            return default
        return value


def get_Nparticles(given_parameters, name, default, parameters, _, event_log, **kwargs):
    # If already loaded an population graph use
    Nparticles = type_get(
        given_parameters, name, default, None, None, event_log, **kwargs
    )
    if "net_Nparticles" in parameters:
        Nparticles = parameters["net_Nparticles"]

    city_pop = np.sum(parameters["population_ages"])
    # Population normalization factor
    personsPerHome = parameters["persons_per_home"]
    number_of_homes = int(city_pop / personsPerHome)
    norm_homes = int(np.max([(number_of_homes / city_pop) * Nparticles, 1.0]))
    norm_N = city_pop / Nparticles

    parameters["city_pop"] = city_pop
    parameters["norm_N"] = norm_N
    parameters["homesNumber"] = norm_homes
    parameters["ageGroups"] = normalize(parameters["population_ages"])

    return Nparticles

    number_of_nbhds = len(param["neighborhoods_percentages"])


def get_duration(given_parameters, name, default, parameters, _, event_log, **kwargs):
    Ndays = type_get(given_parameters, name, default, None, None, event_log, **kwargs)
    parameters["Nhours"] = Ndays * 24
    parameters["Nsteps"] = int(parameters["Nhours"] / parameters["dt"])
    return Ndays


def get_hours(given_parameters, name, default, _, __, event_log):
    return hours_parse(
        get_value(given_parameters, name, default),
        name,
        event_log,
        default,
    )


def age_to_group(age):
    for i in range(len(S.ALL_AGES) - 1):
        if age < S.AGE_LIMITS[i + 1]:
            break
    return i


def add_to_edgelist(edgelist, node_dict, cluster_index, node, neighbors):
    for neighbor in neighbors:
        edgelist.append([node, neighbor])
        node_dict[neighbor] = cluster_index


def isolate_clusters(G):
    clusters_el = []
    node_dict = {}
    for node in G.nodes():
        if len(G[node].keys()) > 0:
            neighbors = list(G[node].keys())
            try:
                cluster_index = node_dict[node]
                add_to_edgelist(
                    clusters_el[cluster_index],
                    node_dict,
                    cluster_index,
                    node,
                    neighbors,
                )
            except KeyError:
                edgelist = []
                add_to_edgelist(edgelist, node_dict, len(clusters_el), node, neighbors)
                clusters_el.append(edgelist)

    clusters = []
    for cluster in clusters_el:
        clusters.append(nx.from_edgelist(cluster))
    return clusters


def homes_statistics(homes, ages):
    age_counts = np.zeros(len(S.ALL_AGES))
    homes_sizes = np.mean([len(home) for home in homes])
    for home in homes:
        for i in home:
            age_counts[ages[i]] += 1
    age_counts = normalize(age_counts)
    return age_counts, homes_sizes


def get_pop_net(given_parameters, name, default, parameters, _, event_log):
    # Load needed parameters
    popAges = parameters["population_ages"]
    personsPerHome = parameters["persons_per_home"]
    seed = parameters["pop_r_seed"]
    if seed == -1:
        seed = parameters["random_seed"]
    rngenerator = default_rng(seed)
    gen_pop = parameters["gen_pop"]
    buffer_file = parameters["pop_buffer_file"]
    age_attribute = parameters["pop_age_name"]

    G = get_value(given_parameters, name, default)
    pop_ages_frac = normalize(popAges)
    if type(G) is nx.Graph or (type(G) is dict and seed in G):
        if type(G) is dict:
            G = G[seed]
        if type(G) is str:
            try:
                G = nx.read_gexf(G)
            except:
                pass
        if type(G) is not nx.Graph:
            event_log(
                "Informed population network {} is not an networkx.Graph, "
                "proceeding with random generated population.".format(G),
                S.MSG_ERROR,
            )
            return
        if gen_pop:
            G_hash = nx.weisfeiler_lehman_graph_hash(G)
            loaded = False
            # Try to load population from buffer
            try:
                Nparticles = given_parameters.get(
                    "number_of_particles", defaults.number_of_particles["default"]
                )
                with h5dict.File(buffer_file, "r") as pop_buffer:
                    if "version" in pop_buffer.keys():
                        if pop_buffer["version"] in S.COMPATIBLE_HDF5:
                            list_of_homes = eval(
                                pop_buffer[format((seed, Nparticles, G_hash)) + "_lh"]
                            )
                            net_ages = eval(
                                pop_buffer[format((seed, Nparticles, G_hash)) + "_na"]
                            )
                            loaded = True
                        else:
                            event_log(
                                "Population buffer: {} is from an incompatible version, file version is {}, current version is {}.".format(
                                    buffer_file, pop_buffer["version"], S.VERSION
                                ),
                                S.MSG_WRNG,
                            )
                            raise ValueError
                    else:
                        event_log(
                            "Population buffer: {} is from an incompatible version, file version is unknown, current version is {}.".format(
                                buffer_file, S.VERSION
                            ),
                            S.MSG_WRNG,
                        )
                        raise ValueError
            except:
                if buffer_file != "":
                    event_log(
                        "Failed to load from population buffer: {}".format(buffer_file),
                        S.MSG_WRNG,
                    )
                # Extract info from graph
                graphs = isolate_clusters(G)
                net_ages = nx.get_node_attributes(G, age_attribute)
                # Convert ages to age groups
                net_ages = {
                    int(key): age_to_group(value) for key, value in net_ages.items()
                }
                # Get the last particle index if needed to add more particles
                last_particle = int(np.max(list(net_ages.keys())))
                # Generates the list of homes as a list of lists of pids
                list_of_homes = [[int(i) for i in g] for g in graphs if len(g) <= 15]
                # Counts the particles in the graph
                net_nparticles = 0
                for home in list_of_homes:
                    net_nparticles += len(home)
                if "number_of_particles" in given_parameters:
                    Nparticles = given_parameters["number_of_particles"]
                    n = 0
                    new_homes = []
                    # Shuffle the list of homes and add to population until the number of particles is reached
                    list_of_homes = np.array(list_of_homes, dtype=object)
                    while n < Nparticles:
                        rngenerator.shuffle(list_of_homes)
                        for home in list_of_homes:
                            # Check size of home to not overshoot number of particles
                            if len(home) <= Nparticles - n:
                                new_homes.append(list(home))
                                n += len(home)
                                # Get current population info to check if adjustments are needed
                                ages_frac, pph = homes_statistics(new_homes, net_ages)
                                pph_diff = (personsPerHome - pph) * Nparticles
                                pop_diff = pop_ages_frac - ages_frac
                                # If the difference in persons per home multiplied by the number of particles is greater  or equal than 1
                                # adds a particle, if between -1 and 1 replaces a particle, if lesser or equal than -1 removes a particle.

                                # Adds a particle
                                if pph_diff > -1 and n <= Nparticles:
                                    add_prob = np.copy(pop_diff)
                                    add_prob[add_prob < 0] = 0  # Remove negatives
                                    add_prob = normalize(add_prob)  # Normalize
                                    new_particle_age = rngenerator.choice(
                                        S.ALL_AGES, p=add_prob
                                    )
                                    last_particle += 1
                                    net_ages[last_particle] = new_particle_age
                                    new_homes[-1].append(last_particle)
                                    n += 1
                                # Remove a particle
                                if pph_diff < 1:
                                    rem_prob = -1 * pop_diff
                                    rem_prob[rem_prob < 0] = 0  # Remove negatives
                                    p = [rem_prob[net_ages[i]] for i in home]
                                    # Do not remove in cases where home only have needed age groups and have more than 1 particle
                                    if (sum(p) > 0) and len(home) > 1:
                                        p = normalize(p)
                                        to_be_removed = rngenerator.choice(home, p=p)
                                        new_homes[-1].remove(to_be_removed)
                                        n -= 1
                            if n == Nparticles:
                                break
                    list_of_homes = new_homes
                    try:
                        if buffer_file != "" and not loaded:
                            buffer_exists = os.path.exists(buffer_file)
                            with h5dict.File(buffer_file, "a") as pop_buffer:
                                if not buffer_exists:
                                    pop_buffer["version"] = S.VERSION
                                if pop_buffer["version"] in S.COMPATIBLE_HDF5:
                                    pop_buffer[
                                        format((seed, Nparticles, G_hash)) + "_lh"
                                    ] = format(list_of_homes)
                                    pop_buffer[
                                        format((seed, Nparticles, G_hash)) + "_na"
                                    ] = format(net_ages)
                                else:
                                    event_log(
                                        "Tried to append to incompatible version of population buffer: {}, file version is {}, current version is {}.".format(
                                            buffer_file,
                                            pop_buffer["version"],
                                            S.VERSION,
                                        ),
                                        S.MSG_WRNG,
                                    )
                    except:
                        pass  # No time to waste here retrying
                # If number of particles is not defined just load the graph
                else:
                    Nparticles = net_nparticles
            # Generate homes and ages list
            homes = np.ones(Nparticles, dtype=int) * -1
            ages = np.ones(Nparticles, dtype=int) * -1
            hid = 0
            pid = 0
            for home in list_of_homes:
                for i in home:
                    if pid >= Nparticles:
                        break
                    homes[pid] = hid
                    ages[pid] = net_ages[i]
                    pid += 1
                hid += 1
            parameters["net_Nparticles"] = Nparticles
            parameters["net_homes"] = homes
            parameters["net_ages"] = ages
            parameters["net_n_homes"] = hid
            event_log(
                "Loaded population graph, simulating {} particles.".format(Nparticles),
                S.MSG_PRGS,
            )
        # Do not generate population (used in Simulation)
        else:
            if "number_of_particles" in given_parameters:
                parameters["net_Nparticles"] = given_parameters["number_of_particles"]
            else:
                parameters["net_Nparticles"] = G.size


def decision_param_parse(decision_param, decision, default, event_log):
    param_new = {}
    for key in default:
        if key in decision_param:
            param_new[key] = decision_param[key]
            del decision_param[key]
        else:
            param_new[key] = default[key]
    if len(decision_param) > 0:
        event_log(
            "{:} parameters are not valid for {:} and where not used.".format(
                list(decision_param.keys()), DECISIONS_NAMES[decision]
            ),
            S.MSG_WRNG,
        )
    return param_new


def get_decision_param(given_parameters, name, default, parameters, _, event_log):
    decision = parameters["decision"]
    if name == "decision_par_services":
        default = DEF_PAR_SRV_CLOSE[decision]
    elif name == "decision_par_lockdown":
        default = DEF_PAR_LCKD[decision]

    decision_param = type_get(
        given_parameters, name, {}, None, None, event_log, dtype=dict
    )
    decision_param = decision_param_parse(decision_param, decision, default, event_log)
    return decision_param


def gen_ts(req_len, default, name):
    ts = np.ones((req_len,) + default.shape)
    if len(default.shape) == 0:
        ts[:] = default
    elif len(default.shape) == 1:
        ts[:, : default.shape[0]] = default
    else:
        raise ValueError(
            "{:} has more than one axis for a default value for {:} time "
            "series, this is not allowed, please send this to developers.".format(
                default, name
            )
        )
    return ts


def get_time_series(
    given_parameters, name, default, parameters, _, event_log, req_len=None
):
    if req_len == None:
        req_len = parameters["Ndays"]
    ts = get_value(given_parameters, name, [])
    default = np.array(default)
    # Time series not in dictionary, return ts contructed with defaults
    # if ts == []:
    #     return gen_ts(req_len, default, name)
    # Try to convert to np.ndarray, will fail if item is not a number
    try:
        ts = np.array(ts, dtype=float)
    except:
        event_log(
            "{:} is an incorrect time series for {:} (not a list, array, float or "
            "an item is not a number), assuming constant with default value {:}.".format(
                ts, name, default
            ),
            S.MSG_WRNG,
        )
        return gen_ts(req_len, default, name)
    # If ts has the same number of axis as default assume it is a constant value and
    # generates a time series with it
    if len(ts.shape) == len(default.shape):
        new_ts = np.copy(default)
        if len(ts.shape) == 0:
            new_ts = ts
        else:
            new_ts[: len(ts)] = ts
        return gen_ts(req_len, new_ts, name)
    # Check if it has only one axis more than default
    if len(ts.shape) == (len(default.shape) + 1):
        # If smaller than req_len pad it with default
        if len(ts) < req_len:
            new_ts = gen_ts(req_len, default, name)
            if len(ts.shape) == 1:
                new_ts[: len(ts)] = ts
            else:
                new_ts[: len(ts), : ts.shape[-1]] = ts
            ts = np.copy(new_ts)
            event_log(
                "Time series for {:} is shorter than the required {:}, completing "
                "it with default value {:}.".format(name, req_len, default),
                S.MSG_WRNG,
            )
        # If bigger cut it
        if len(ts) > req_len:
            ts = ts[:req_len]
        return ts
    else:
        event_log(
            "{:} is an incorrect time series for {:} (more axis than expected), "
            "assuming constant with default value {:}.".format(ts, name, default),
            S.MSG_WRNG,
        )
        return gen_ts(req_len, default, name)


def hours_parse(hours, service, event_log, default):
    default[0] = float(default[0])
    default[1] = float(default[1])
    if (type(hours) is list) or (type(hours) is np.ndarray):
        if len(hours) == 2:
            try:
                hours[0] = float(hours[0] % 24)
                hours[1] = float(hours[1] % 24)
                if hours[1] == 0.0:
                    hours[1] = 24.0
            except:
                event_log(
                    "{:} is an incorrect hours for {:} (an item is not a number), "
                    "assuming defaults {:}".format(hours, service, default),
                    S.MSG_WRNG,
                )
                return default
        else:
            event_log(
                "{:} is an incorrect hours for {:} (length different from 2), "
                "assuming defaults {:}".format(hours, service, default),
                S.MSG_WRNG,
            )
            return default
        return hours
    else:
        event_log(
            "{:} is an invalid hours for {:} (not a list), assuming defaults "
            "{:}".format(hours, service, default),
            S.MSG_WRNG,
        )
        return default


def get_inf0(given_parameters, name, default, parameters, rng, event_log):
    Nparticles = parameters["Nparticles"]
    inf0_perc = get_value(given_parameters, name, default)
    if (type(inf0_perc) is list) or (type(inf0_perc) is np.ndarray):
        try:
            if (type(inf0_perc[0]) is list) or (type(inf0_perc[0]) is np.ndarray):
                rand_index = rng.integers(0, len(inf0_perc))
                inf0_perc = inf0_perc[rand_index]
            for i in range(len(inf0_perc)):
                inf0_perc[i] = float(inf0_perc[i])
        except:
            event_log(
                "{:} is invalid for inf0_perc (an item is not a number). Assuming "
                "default values {:}.".format(inf0_perc, default),
                S.MSG_WRNG,
            )
            inf0_perc = default
            return inf0_perc
        if len(inf0_perc) == 4:
            if ((inf0_perc[1] + inf0_perc[2]) < 1.0 / Nparticles) or (
                (inf0_perc[1] < 1.0 / Nparticles) and inf0_perc[2] < 1.0 / Nparticles
            ):
                event_log(
                    "Percentage of susceptible and infectious too small for the "
                    "given number of particles. Increasing the percentage of "
                    "exposed to a minimum value.",
                    S.MSG_WRNG,
                )
                inf0_perc[1] = 1.1 / Nparticles
                inf0_perc[0] = 1 - np.sum(inf0_perc[1:])
            if np.sum(inf0_perc) != 1.0:
                inf0_perc = normalize(inf0_perc)
                event_log(
                    "inf0_perc must sum to exactly 1. Continuing with normalized "
                    "values {:}.".format(inf0_perc),
                    S.MSG_WRNG,
                )
        else:
            event_log(
                "{:} have an invalid length for inf0_perc, it can be passed as a "
                "float with the percentage of exposed particles or as a list with the "
                "percentage in each state [S, E, I, R]. Assuming default values "
                "{:}.".format(inf0_perc, default),
                S.MSG_WRNG,
            )
            inf0_perc = default
    elif type(inf0_perc) is float:
        if inf0_perc >= 1:
            event_log(
                "inf0_perc can not be equal o greater than 1., assuming default "
                "value {:}.".format(default[1]),
                S.MSG_WRNG,
            )
            inf0_perc = default
        elif inf0_perc <= 0:
            event_log(
                "inf0_perc can not be equal o lesser than 0., assuming default "
                "value {:}.".format(default[1]),
                S.MSG_WRNG,
            )
            inf0_perc = default
        else:
            inf0_perc = [1 - inf0_perc, inf0_perc, 0.0, 0.0]
    else:
        event_log(
            "{:} is invalid for inf0_perc, it can be passed as a float with the "
            "percentage of exposed particles or as a list with the percentage in each "
            "state [S, E, I, R]. Assuming default values {:}.".format(
                inf0_perc, default
            ),
            S.MSG_WRNG,
        )
        inf0_perc = default
    return inf0_perc


def get_inf0_symp(given_parameters, name, default, _, __, event_log):
    inf0_symp = get_value(given_parameters, name, default)
    if (type(inf0_symp) is list) or (type(inf0_symp) is np.ndarray):
        try:
            for i in range(len(inf0_symp)):
                inf0_symp[i] = float(inf0_symp[i])
        except:
            event_log(
                "{:} is invalid for inf0_perc_symp (an item is not a number). "
                "Assuming default values {:}.".format(inf0_symp, default),
                S.MSG_WRNG,
            )
            inf0_symp = default
            return inf0_symp
        if len(inf0_symp) == 3:
            if np.sum(inf0_symp) != 1.0:
                inf0_symp = normalize(inf0_symp)
                event_log(
                    "inf0_perc_symp must sum to exactly 1. Continuing with "
                    "normalized values {:}.".format(inf0_symp),
                    S.MSG_WRNG,
                )
        else:
            event_log(
                "{:} have an invalid length for inf0_perc_symp, it can be passed "
                "as a list with the percentage of infectious particle with each symptom "
                "[asymp, symp, severe]. Assuming default values {:}.".format(
                    inf0_symp, default
                ),
                S.MSG_WRNG,
            )
            inf0_symp = default
    else:
        event_log(
            "{:} is invalid format for inf0_perc_symp (not a list). Assuming "
            "default values {:}.".format(inf0_symp, default),
            S.MSG_WRNG,
        )
        inf0_symp = default
    return inf0_symp


def get_service_close_day_series(given_parameters, name, default, _, __, event_log):
    config = get_value(given_parameters, name, default)
    if config == []:
        return config
    if len(config) != 3:
        event_log(
            "{:}: either nothing or three entries must be provided [path, "
            "start_day, end_day]. Value provided: {:}. Proceeding with default config "
            "{}".format(name, config, default),
            S.MSG_WRNG,
        )
        return []
    return config


def merge_list_of_dicts(list_of_dicts, name, event_log):
    # make a copy of everything to avoid editing constants, test if dicts are realy dicts
    # and merge dicts passed in tuples
    params = []
    for d in list_of_dicts:
        invalid = False
        if type(d) is dict:
            params.append(dict(d))
        elif type(d) in [tuple, list]:
            if len(d) == 2:
                try:
                    new_d = dict(d[0])
                    extra_d = dict(d[1])
                    for key in extra_d.keys():
                        new_d[key] = extra_d[key]
                    params.append(new_d)
                except:
                    invalid = True
            else:
                invalid = True
        else:
            invalid = True
        if invalid:
            event_log(
                "{} is in an invalid format for {} parameters, parameters for a {} "
                "can be a dict or a tuple of 2 dicts, this {} will be ignored.".format(
                    d, name[:-1], name[:-1], name[:-1]
                ),
                S.MSG_WRNG,
            )

    return params


def get_list_of_dicts(parameters, name, default, event_log):
    list_of_dicts = get_value(parameters, name, default)
    return merge_list_of_dicts(list_of_dicts, name, event_log)


def parse_from_defaults(param, name, DEFAULTS, DEFAULTS_TYPES, event_log):
    # Get missing parameters from DEFAULTS and try to convert using convertion functions
    # in DEFAULTS_TYPES
    for key in DEFAULTS:
        if not key in param:
            param[key] = DEFAULTS[key]
        try:
            if not type(DEFAULTS_TYPES[key]) is tuple:
                if not type(DEFAULTS_TYPES[key]) is list:
                    # If DEFAULTS_TYPES not tuple or list simply try to convert
                    if type(param[key]) is not DEFAULTS_TYPES[key]:
                        param[key] = DEFAULTS_TYPES[key](param[key])
                else:
                    # If DEFAULTS_TYPES is list, check if type of param maches types in
                    # list, if not try to convert in order
                    if not type(param[key]) in DEFAULTS_TYPES[key]:
                        for t in DEFAULTS_TYPES[key]:
                            converted = False
                            if type(param[key]) is t:
                                converted = True
                                break
                            try:
                                param[key] = t(param[key])
                                converted = True
                                break
                            except:
                                pass
                        if not converted:
                            raise TypeError()
            else:
                # If DEFAULTS_TYPES is tuple try to covert using dict on position 1 of
                # tuple as kwargs for conversion
                param[key] = DEFAULTS_TYPES[key][0](
                    param[key], **DEFAULTS_TYPES[key][1]
                )
        except:
            # If convertion fails assumes default value and log warning
            if type(DEFAULTS_TYPES[key]) is tuple:
                DEFAULTS_TYPES[key] = DEFAULTS_TYPES[key][0]
            event_log(
                "{:} invalid {:} parameter for {:} {:} (can not convert to {:}), "
                "assuming default value {:},".format(
                    param[key],
                    key,
                    name,
                    param["name"],
                    DEFAULTS_TYPES[key].__name__,
                    DEFAULTS[key],
                ),
                S.MSG_WRNG,
            )
            param[key] = DEFAULTS[key]

    # List unused parameters and delete them from the param
    unused_par = []
    for key in param:
        if not key in DEFAULTS:
            unused_par.append(key)
    for key in unused_par:
        del param[key]
    if unused_par != []:
        event_log(
            "{:} parameters are not valid for {:} {:} and where not "
            "used.".format(unused_par, name, param["name"]),
            S.MSG_WRNG,
        )


# %% QUARANTINES TRIAGE
def get_quarantines(given_parameters, name, default, parameters, __, event_log):
    clk = parameters["clk"]
    quarantines = get_list_of_dicts(given_parameters, "quarantines", default, event_log)
    for qrnt in quarantines:
        parse_from_defaults(
            qrnt, "quarantine", qrnts.DEFAULTS, qrnts.DEFAULTS_TYPES, event_log
        )
        qrnt["delay"] = clk.days_to_time(qrnt["delay"])
    return quarantines


# %% SERVICES TRIAGE
def srvc_workers_number_parse(srvc, wrkr, ageGroups, Nparticles, norm_N, event_log):
    if wrkr["population_fraction"] > 0:
        if wrkr["population_fraction"] > 1:
            event_log(
                "Population fraction for {} for {} service is bigger than 1 ({}), "
                "assuming 1.0.".format(wrkr["name"], srvc["name"]),
                S.MSG_WRNG,
            )
            wrkr["population_fraction"] = 1.0
        nWorkers = 0
        for group in wrkr["age_groups"]:
            nWorkers = (
                nWorkers + ageGroups[group] * Nparticles * wrkr["population_fraction"]
            )
        wrkr["number"] = int(np.max([1, (nWorkers / srvc["number"])]))
    elif wrkr["number"] > 0:
        workersOnSrvcs = np.sum(srvc["real_number"]) * wrkr["number"]
        actualWorkersOnSrvc = demo_norm(workersOnSrvcs, norm_N, dtype=int)
        actualWorkersPerServ = demo_norm(actualWorkersOnSrvc, srvc["number"], dtype=int)
        wrkr["number"] = actualWorkersPerServ
    else:
        wrkr["number"] = 1


def srvc_number_parse(srvc, norm_N, event_log):
    if srvc["number"] == -1:
        if srvc["name"] in srvcs.DEFAULT_NUMBERS:
            srvc["number"] = np.array(srvcs.DEFAULT_NUMBERS[srvc["name"]])
        else:
            event_log(
                "The number of instances for service {}  was not informed, "
                "will run with only one instance".format(srvc["name"]),
                S.MSG_WRNG,
            )

    srvc["real_number"] = srvc["number"]

    srvc["number_per_nbhd"] = []
    if len(srvc["number"].shape) == 1:
        srvc["number_per_nbhd"] = np.array(srvc["number"], dtype=int)
        for i in range(len(srvc["number"])):
            if srvc["number_per_nbhd"][i] == 0:
                srvc["number_per_nbhd"][i] = 0
            else:
                srvc["number_per_nbhd"][i] = demo_norm(
                    srvc["number_per_nbhd"][i], norm_N, dtype=int
                )
        srvc["number"] = int(np.sum(srvc["number_per_nbhd"]))
    elif len(srvc["number"].shape) == 0:
        if srvc["number"] != 0:
            srvc["number"] = demo_norm(srvc["number"], norm_N, dtype=int)
    else:
        event_log(
            "{} is in an incorrect shape for number of {}, assuming the default "
            "1.".format(srvc["number"], srvc["name"]),
            S.MSG_WRNG,
        )
        srvc["number"] = 1


def srvc_workers_vis_period_parse(srvc, wrkr, event_log):
    days_frac = np.sum(srvc["days"]) / 7.0
    if srvc["hours"][1] > srvc["hours"][0]:
        wrkr["family_visitation_period"] = (
            wrkr["family_visitation_period"]
            * (srvc["hours"][1] - srvc["hours"][0])
            * days_frac
        )
    else:
        wrkr["family_visitation_period"] = (
            wrkr["family_visitation_period"]
            * (srvc["hours"][1] + 24 - srvc["hours"][0])
            * days_frac
        )


def srvc_workers_shifts_parse(srvc, wrkr, event_log):
    if type(wrkr["shifts"]) is list:
        to_remove = []
        for shift in wrkr["shifts"]:
            if (type(shift) is list) or (type(shift) is np.ndarray):
                if len(shift) == 2:
                    try:
                        shift[0] = float(shift[0] % 24)
                        shift[1] = float(shift[1] % 24)
                    except:
                        to_remove.append(shift)
                        event_log(
                            "{:} is an incorrect shift for {} for service {} (an "
                            "item is not a number), ignoring it".format(
                                shift, wrkr["name"], srvc["name"]
                            ),
                            S.MSG_WRNG,
                        )
                else:
                    to_remove.append(shift)
                    event_log(
                        "{:} is an incorrect shift for {} for service {} (length "
                        "different from 2), ignoring it".format(
                            shift, wrkr["name"], srvc["name"]
                        ),
                        S.MSG_WRNG,
                    )
            else:
                to_remove.append(shift)
                event_log(
                    "{:} is an incorrect shift for {} for service {} (not a list), "
                    "ignoring it".format(shift, wrkr["name"], srvc["name"]),
                    S.MSG_WRNG,
                )
        for i in to_remove:
            if i in wrkr["shifts"]:
                wrkr["shifts"].remove(i)
        if len(wrkr["shifts"]) == 0:
            event_log(
                "There is no shifts for for {} for service {}, there will be no {} "
                "at this service".format(wrkr["name"], srvc["name"], wrkr["name"]),
                S.MSG_WRNG,
            )
    else:
        event_log(
            "{} are invalids shifts for {} for service {} (not a list), there will "
            "be no {} at this service".format(
                srvc["shifts"], wrkr["name"], srvc["name"], wrkr["name"]
            ),
            S.MSG_WRNG,
        )
        wrkr["shifts"] = []


def srvc_rooms_parse(srvc, default_room, default_room_types, event_log):
    srvc["rooms"] = merge_tuple_of_dicts(srvc["rooms"])
    name = "room for service {}".format(srvc["name"])
    rooms = []
    for room in srvc["rooms"]:
        for key in room:
            if key in ["name", "number"]:
                continue
            key_found = False
            for wrkr in srvc["workers"]:
                if key == wrkr["name"]:
                    key_found = True
            if not key_found:
                break
        if not key_found:
            event_log(
                "Couldn't find {} worker in {} service for room {}, ignoring this "
                "room.".format(key, srvc["name"], room),
                S.MSG_WRNG,
            )
            continue
        parse_from_defaults(room, name, default_room, default_room_types, event_log)
        rooms.append(room)
    srvc["rooms"] = rooms


def srvc_hours_parse(srvc, event_log, default):
    srvc["hours"] = hours_parse(srvc["hours"], srvc["name"], event_log, default)


def srvc_vis_period_parse(srvc, event_log):
    days_frac = np.sum(srvc["days"]) / 7.0
    if srvc["hours"][1] > srvc["hours"][0]:
        srvc["visitation_period"] = (
            srvc["visitation_period"]
            * (srvc["hours"][1] - srvc["hours"][0])
            * days_frac
        )
    else:
        srvc["visitation_period"] = (
            srvc["visitation_period"]
            * (srvc["hours"][1] + 24 - srvc["hours"][0])
            * days_frac
        )


def srvc_decision_parse(srvc, gen_decision, gen_decision_par, event_log):
    if srvc["decision"] == -1:
        srvc["decision"] = gen_decision
    elif not srvc["decision"] in DEF_PAR_SRV_CLOSE:
        event_log(
            "{:} is an invalid value for decision for service {:}, assuming "
            "general: {:}.".format(
                srvc["decision"], srvc["name"], DECISIONS_NAMES[gen_decision]
            ),
            S.MSG_WRNG,
        )
        srvc["decision"] = gen_decision
    if srvc["decision"] == gen_decision:
        default_par = gen_decision_par
    else:
        default_par = DEF_PAR_SRV_CLOSE[srvc["decision"]]
    srvc["decision_par"] = decision_param_parse(
        srvc["decision_par"], srvc["decision"], default_par, event_log
    )


def load_service_close_day_series(
    filename, day_start, day_end, srvc_name, default, event_log
):
    loaded = False
    try:
        data = pd.read_csv(filename)
        loaded = True
    except:
        event_log(
            "Service Close Day Series: coudn't open service close day series "
            "database from file: {:}. Proceeding with default service close day series "
            "data".format(filename),
            S.MSG_WRNG,
        )
    if loaded:
        mask_days_in_data = data["Time (days)"].isin(np.arange(day_start, day_end))
        if np.count_nonzero(mask_days_in_data) != day_end - day_start:
            event_log(
                "Service Close Day Series: one of the requested days is either "
                "repeated or not in the database. Proceeding with default services close "
                "day series data",
                S.MSG_WRNG,
            )
            return default
        try:
            day_series = data[srvc_name][mask_days_in_data].values
            return day_series
        except:
            event_log(
                "Service Close Day Series: the following service was not present "
                "in your day series file: {:}".format(srvc_name),
                S.MSG_WRNG,
            )
    return default


def srvc_day_series_parse(srvc, param, event_log):
    if param["use_day_series"]:
        if "day_series" in srvc:
            default = srvc["day_series"]
        else:
            default = -np.ones(param["Ndays"])
        if param["service_close_day_series_config"] == []:
            srvc["day_series"] = default
        else:
            filename = param["service_close_day_series_config"][0]
            day_start = param["service_close_day_series_config"][1]
            day_end = param["service_close_day_series_config"][2]
            srvc["day_series"] = load_service_close_day_series(
                filename, day_start, day_end, srvc["name"], default, event_log
            )
    else:
        srvc["day_series"] = -np.ones(param["Ndays"])


def get_services(given_parameters, name, default, parameters, _, event_log):
    # load needed parameters
    norm_N = parameters["norm_N"]
    Nparticles = parameters["Nparticles"]
    ageGroups = parameters["ageGroups"]
    decision_srvc = parameters["decision"]
    decision_srvc_par = parameters["decision_par_services"]

    services = get_list_of_dicts(given_parameters, "services", default, event_log)
    for i, srvc in enumerate(services):
        parse_from_defaults(
            srvc, "service", srvcs.DEFAULTS, srvcs.DEFAULTS_TYPES, event_log
        )

        # Run parse functions
        srvc["id"] = i
        srvc_number_parse(srvc, norm_N, event_log)
        srvc_hours_parse(srvc, event_log, srvcs.DEFAULTS["hours"])
        srvc_vis_period_parse(srvc, event_log)
        srvc_decision_parse(srvc, decision_srvc, decision_srvc_par, event_log)
        srvc_day_series_parse(srvc, parameters, event_log)

        default_room = {"number": 0, "name": "room"}
        default_room_types = {"number": int, "name": str}
        # parse workers parameters
        name = "workers for service {}".format(srvc["name"])
        srvc["workers"] = merge_tuple_of_dicts(srvc["workers"])
        for j, wrkr in enumerate(srvc["workers"]):
            parse_from_defaults(
                wrkr,
                name,
                srvcs.WORKERS_DEFAULTS,
                srvcs.WORKERS_DEFAULTS_TYPES,
                event_log,
            )

            default_room[wrkr["name"]] = 0
            default_room_types[wrkr["name"]] = int
            wrkr["id"] = j
            srvc_workers_number_parse(
                srvc, wrkr, ageGroups, Nparticles, norm_N, event_log
            )
            srvc_workers_shifts_parse(srvc, wrkr, event_log)
            srvc_workers_vis_period_parse(srvc, wrkr, event_log)

        srvc_rooms_parse(srvc, default_room, default_room_types, event_log)
    return services


def merge_tuple_of_dicts(l):
    new_l = []
    for item in l:
        if type(item) is tuple:
            new_dict = dict(item[0])
            for key in item[1]:
                new_dict[key] = item[1][key]
        else:
            new_dict = dict(item)
        new_l.append(new_dict)
    return new_l


def get_module_params(parameters, defaults, defauts_types, name, event_log):
    parameters = recursive_copy(parameters)
    parameters = merge_list_of_dicts([parameters], name, event_log)[0]
    parse_from_defaults(parameters, __name__, defaults, defauts_types, event_log)
    return parameters
