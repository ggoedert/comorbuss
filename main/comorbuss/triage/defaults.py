##############################################################################
# List of all parameters it's default values description and limits.
#
# Parameters should be listed in the flowing format:
#
# name_of_parameter = {
#     'default': 0.1,
#     'short_description': "An one phrase description of the parameter.",
#     'limits': (0., 1.),
#     'human_name': "Short human readable name"
# }
# """A long description of parameter, accepts latex in the form $10 \\times 100$.
#     Default: 0.2."""
#
# Only 'default' is required, other items are optional, in variables where there
# is only one limit None should be placed in the other limit, eg:
# number_of_schools = {
#     'defaults': 170,
#     'limits': (0, None)
# }
# This parameter will be required to be positive.
##############################################################################
import numpy as np
from ..services import DEFAULT_SERVICES
from ..decisions import BY_DIAGNOSTICS
from ..decisions import DECISIONS_NAMES
from . import triage_functions as functions
from .. import settings as S
from ..aux_classes import RandomDistibution

########################### WARNING!!!!! #####################################
# The order of the parameters in this file is important, some parameters
# depends on it to work properly, only change it if you know what are you
# doing.
##############################################################################

# random_seed NEEDS to be before skip is defined!!!!!
random_seed = {
    "default": 1,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": int},
}
"""An integer that sets the randomization of the simulation. Repeating the
    same number and the same dictionary gives same results. Default: 1."""

# List variables to skip on triage as they are not parameters
skip = []
skip = dir()

####### Default values for all parameters used in the community class ########
population_ages = {
    "default": np.array(
        [
            13005,
            13828,
            16170,
            17023,
            19253,
            20522,
            19068,
            17048,
            15903,
            15765,
            14347,
            11152,
            8672,
            6480,
            5465,
            3917,
            2426,
            1328,
            480,
            88,
            12,
        ],
        dtype=int,
    ),
    "triage_function": functions.array_get,
    "triage_parameters": {"enforce_shape": True, "dtype": int},
}
"""A list of integers for the number of particles in each age group. The age
    groups are currently separated in intervals of five years from 0 to 100
    years, and another age group for 100 years or more. The total number of persons
    in the city is given from the sum of all of these values. The indices for these
    age groups are given in ALL_AGES, and their labels are in AGE_DEF. Default:
    demographic data from São Carlos. Default: [13005, 13828, 16170, 17023,
    19253, 20522, 19068, 17048, 15903, 15765, 14347, 11152, 8672, 6480, 5465, 3917,
    2426, 1328, 480, 88, 12]."""

age_assignment_order = {
    "default": np.array(
        [0, 1, 2, 3, 20, 19, 18, 17, 16, 15, 14, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
        dtype=int,
    ),
    "triage_function": functions.array_get,
    "triage_parameters": {"enforce_shape": True, "dtype": int},
    "store_name": "ageAssignment",
}
"""Order to assign ages to population, if a population graph is loaded this will be ignored."""

persons_per_home = {
    "default": 3.0,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": float},
}
"""A float for the average number of persons that live in a single home.
    Default: 3."""

population_graph_random_seed = {
    "default": -1,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": int},
    "store_name": "pop_r_seed",
}
"""An iteger to set the random seed for the population graph generator. (For use
    with `population_graph`) Default: random_seed."""

population_graph_age_attribute = {
    "default": "age",
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": str},
    "store_name": "pop_age_name",
}
"""An string containing the name of the nodes attribute that stores the age of
    the population in the population graph. (For use with `population_graph`)
    Default: "age"."""

population_graph_buffer_file = {
    "default": "",
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": str},
    "store_name": "pop_buffer_file",
}
"""An opitional string containing the path for a file to store and load generated
    populations. (For use with `population_graph`)."""

population_graph = {
    "default": None,
    "triage_function": functions.get_pop_net,
}
"""An opitional graph containing homes networks and ages of population."""

# Number of particles MUST BE loaded after population_graph, persons_per_home and population_ages
number_of_particles = {
    "default": 5000,
    "triage_function": functions.get_Nparticles,
    "triage_parameters": {"dtype": int},
    "store_name": "Nparticles",
}
"""An integer for the number of particles that are simulated in the city. This
    number does not have to be equal to the number of persons in the city, but it
    has to be large enough for the randomness stabilize the results. Default:
    1000."""

resolution_t = {
    "default": 1.0,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": float},
    "store_name": "dt",
}
"""A float for the time step used in the simulation. Default: 1.0"""

# number_of_days MUST BE loaded after resolution_t
number_of_days = {
    "default": 60,
    "triage_function": functions.get_duration,
    "triage_parameters": {"dtype": int},
    "store_name": "Ndays",
}
"""An integer for the number of days simulated in the real city. With -1 will
    use stop_filter to stop simulation.
    Default: 60."""

start_date = {
    "default": "2020-02-01",
    "triage_function": functions.get_date,
    "triage_parameters": {"set_clk": True},
}
"""A start date for the simulation, can be passed as an string on the format:
    \"YYYY-mm-dd\" or as an unix timestamp. Default: \"2020-02-01\""""

stop_filter = {
    "default": ("False"),
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": tuple},
}
"""An tuple filter to stop simulation. Default: ("False")."""

decision = {
    "default": BY_DIAGNOSTICS,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": int, "from_list": list(DECISIONS_NAMES.keys())},
}
"""Decision process used to close/reopen services and start/stop lockdowns.
    Default: decisions.BY_DIAGNOSTICS.

!!! sealso
    See [Decision Process](../../interventions/decisions/)"""

# decision_par_services MUST BE loaded after decision
decision_par_services = {
    "default": {},
    "triage_function": functions.get_decision_param,
}
"""Decision parameters used to close services, this can be overwritten on an
    individual service with the parameter 'decision_par' for the service.

!!! sealso
    See [Decision Process](../../interventions/decisions/)"""

# decision_par_lockdown MUST BE loaded after decision
decision_par_lockdown = {
    "default": {},
    "triage_function": functions.get_decision_param,
}
"""Decision parameters used to start/end lockdowns.

!!! sealso
    See [Decision Process](../../interventions/decisions/)"""

services_close = {
    "default": False,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": bool},
}
"""A boolean value that turns on or off the conditional closure of services
    during the simulation. The type of decision that closes the services, as well
    as its parameters, must be specified. Default: False."""

# services_enabled = {
#     "default": True,
#     "triage_function": functions.type_get,
#     "triage_parameters": {"dtype": bool},
# }
# """A boolean value that turns on or off essential services in the simulation.
#     Default: True."""

lockdown = {
    "default": False,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": bool},
}
"""A boolean value that turns on or off if a lockdown measure in the community.
    Similar to the services close, a decision must be selected, as well as the
    parameters that specify this decision. The lockdown works by altering social
    isolation's proportion of particles staying at home when it is running.
    Default value False."""

use_day_series = {
    "default": False,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": bool},
}
"""A boolean value that turns on whether services must be closed (or lockdown
    started) according to a day series array provided by the user. In this case
    the services_closing_value and services_reopening_value are overriden and
    the user must configure the service_close_day_series array. Default value:
    False."""

social_isolation = {
    "default": False,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": bool},
}
"""A boolean value that specifies if a certain portion of the population tends
    to stay at home during work day. Social isolation must be configured through
    other parameters. Default: False."""

print_events = {
    "default": True,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": bool},
}
"""A boolean value that selects whether or not to print events during the
    simulation. Default: True."""

log_progress = {
    "default": True,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": bool},
}
"""A boolean value that selects whether or not to log/print the progress during the
    simulation. Default: True."""

store_time_series = {
    "default": False,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": bool},
}
"""Store time series of main particles attributes. Disable this to reduce memory usage,
    some outputs will not be available. Default: False."""

reduce_workers = {
    "default": False,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": bool},
}
"""Reduce the number of workers during a simulation."""

# reduce_workers_series MUST BE loaded after number_of_days
reduce_workers_series = {
    "default": np.full(20, 1.0),
    "triage_function": functions.get_time_series,
}
"""Day series to reduce the number of workers"""

reduce_visitors = {
    "default": False,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": bool},
}
"""Reduce the number of visitors during a simulation."""

# reduce_visitors_series MUST BE loaded after number_of_days
reduce_visitors_series = {
    "default": np.full(20, 1.0),
    "triage_function": functions.get_time_series,
}
"""Day series to reduce the number of visitors"""

check_encounters_simmetry = {
    "default": False,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": bool},
}
"""Toggles a check for simmetry on ecounters matrices."""

limit_encounters = {
    "default": False,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": bool},
}
"""A boolean value that turns on or off the maximum number of encounters inside
    a service. Default: False."""

limit_encounters_number = {
    "default": 10,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": int},
    "store_name": "max_encounters",
}
"""An integer value that limits the number of contacts in a single time step
    inside services. Default: 10."""

free_hours = {
    "default": [7, 22],
    "triage_function": functions.get_hours,
}
"""A two-sized list with two integers delimiting the start and end of a
    productive day. For example, if [7, 22] is provided, then all particles not
    working at a service remain home from `22:00` to `24:00` and from `00:00` to
    `07:00`, everyday. For any other time in the range of the limits of the list
    provided, particles are free to act as in their daily routine. Default value:
    [7., 22.]."""

city_name = {
    "default": "default",
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": str},
}
"""A string for the name of the city. This value is used for output purposes
    only. Default: "default"."""

neighborhoods_percentages = {
    "default": [1.0],
    "triage_function": functions.array_get,
    "triage_parameters": {"normalize_array": True, "dtype": float},
}
"""A list with the percentage of the population in each neighborhood.
    Default: [0]."""

inf_probability = {
    "default": 0.016,
    "triage_function": functions.array_get,
    "triage_parameters": {
        "enforce_shape": True,
        "shape": np.array(S.ALL_AGES).shape,
        "dtype": (float, RandomDistibution),
        "allow_single_value": True,
        "limits": (0.0, 1.0),
    },
}
"""A float for the probability of an infectious particle infect a susceptible
    one if they have had a contact of one hour. This probability is internally
    corrected according to the value provided by resolution_t. Default: 0.016."""

inf_susceptibility = {
    "default": 1.0,
    "triage_function": functions.array_get,
    "triage_parameters": {
        "enforce_shape": True,
        "shape": np.array(S.ALL_AGES).shape,
        "dtype": (float, RandomDistibution),
        "allow_single_value": True,
        "limits": (0.0,),
    },
}
"""Suscetibility of particles."""

inf_incubation = {
    "default": 4.6,
    "triage_function": functions.get_days_array,
    "triage_parameters": {
        "enforce_shape": True,
        "shape": np.array(S.ALL_AGES).shape,
        "dtype": (float, RandomDistibution),
        "allow_single_value": True,
        "limits": (0.0,),
    },
}
"""A float for the mean time, in days, that an exposed particle takes to become
    infectious.  Default: 3.2."""

inf_duration = {
    "default": 8.0,
    "triage_function": functions.get_days_array,
    "triage_parameters": {
        "enforce_shape": True,
        "shape": np.array(S.ALL_AGES).shape,
        "dtype": (float, RandomDistibution),
        "allow_single_value": True,
        "limits": (0.0,),
    },
}
"""A float for the mean time, in days, that an infectious particle takes to
    recover. Default: 5.0."""

inf_severe_duration = {
    "default": 14.0,
    "triage_function": functions.get_days_array,
    "triage_parameters": {
        "enforce_shape": True,
        "shape": np.array(S.ALL_AGES).shape,
        "dtype": (float, RandomDistibution),
        "allow_single_value": True,
        "limits": (0.0,),
    },
}
"""A float for the mean time, in days, that an severe symptomatic particle takes to
    recover. Default: 0.0 (inf_duration will be used)."""

inf_activation_time = {
    "default": 2.0,
    "triage_function": functions.get_days_array,
    "triage_parameters": {
        "enforce_shape": True,
        "shape": np.array(S.ALL_AGES).shape,
        "dtype": (float, RandomDistibution),
        "allow_single_value": True,
        "limits": (0.0,),
    },
}
"""A float for the mean time, in days, that an infectious particle take to
    become symptomatic.  Default: 2.0."""

inf_prob_sympt = {
    "default": 0.4,
    "triage_function": functions.array_get,
    "triage_parameters": {
        "enforce_shape": True,
        "shape": np.array(S.ALL_AGES).shape,
        "dtype": (float, RandomDistibution),
        "allow_single_value": True,
        "limits": (0.0, 1.0),
    },
}
"""A float for the probability of an infectious particle ever becoming symptomatic.
    Default: 0.4."""

inf_severe_sympt_prob = {
    "default": 0.08,
    "triage_function": functions.array_get,
    "triage_parameters": {
        "enforce_shape": True,
        "shape": np.array(S.ALL_AGES).shape,
        "dtype": (float, RandomDistibution),
        "allow_single_value": True,
        "limits": (0.0, 1.0),
    },
    "short_description": "Probability of an infected particles to develop severe symptoms.",
    "human_name": "Severe symptoms probability",
}
"""A float for the probability of an infectious particle develop
    severe symptoms. Shoud be less than inf_prob_sympt. Default: 0.08."""

inf_severe_death_prob = {
    "default": 0.012,
    "triage_function": functions.array_get,
    "triage_parameters": {
        "enforce_shape": True,
        "shape": np.array(S.ALL_AGES).shape,
        "dtype": (float, RandomDistibution),
        "allow_single_value": True,
        "limits": (0.0, 1.0),
    },
}
"""A float for the probability of an infected particle to die instead of recover,
    only severe symptomatic particles. Shoud be less than inf_severe_sympt_prob.
    A list can be passed instead of a value, in which
    case the entries of the list are applied to each individual age group.
    Default: 0."""

# inf0_perc MUST BE loaded after number_of_particles
inf0_perc = {
    "default": [0.99, 0.01, 0.0, 0.0],
    "triage_function": functions.get_inf0,
}
"""A float for the fraction of the population that is exposed to the disease
    at the start of the simulation. Alternatively, this value can receive a list of
    fraction values for the population in each of the infection states, in the
    following order: fraction of susceptible, fraction of exposed, fraction
    of infectious and fraction of recovered particles. The sum of fractions must
    sum up to $1$, otherwise they are normalized to do so by respecting the proportions
    in the list's entries. Default: [0.99, 0.01, 0.0, 0.0]."""

inf0_perc_symp = {
    "default": [
        (1.0 - inf_prob_sympt["default"]),
        (inf_prob_sympt["default"] * (1 - inf_severe_sympt_prob["default"])),
        (inf_prob_sympt["default"] * inf_severe_sympt_prob["default"]),
    ],
    "triage_function": functions.get_inf0_symp,
}
"""A list with fractions for the infectious particles in the following three
    symptomatic states: asymptomatic, soft symptomatic and severe symptomatic,
    respectively. The value in this keyword is used to initiate the symptomatic
    states of the infectious particles determined by the inf0_perc keyword's value.
    The sum of fractions must sum up to $1$, otherwise they are normalized to do
    so by respecting the proportions in the list's entries. Default value for this
    parameter will follow the probabilities of developing symptoms (see
    inf_prob_sympt and inf_severe_sympt_prob keywords). [DERIVED PARAMETER]"""

homes_inf0_frac = {
    "default": -1,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": float, "limits": (0.0, 1.0)},
}
"""If this is set will intilialize infections giving prioriry to infect particles in
    the same house, this parameter sets the mean fraction of particles that are
    initialized infected (this is not exclusive to the state infected, if inf0_perc
    is set with the percentages in each state the proportion of Exposed, Infectious
    and Recovered will be used to initialize particles) in the same house.
"""

homes_inf0_dict = {
    "default": {},
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": dict},
}
"""If this is set will intilialize infections using this home infections dictionary.
"""

home_network = {
    "default": {
        "inf_prob_weight": 1.0,
        "mean_contacts": 2,
    },
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": dict},
}
"""Args:
    inf_prob_weight (float): A float value that multiplies the infection probability for particles
        at home. This value is useful for controlling the definition of contacts
        in homes. Default: 1.
    mean_contacts (float): A float for the mean number of contacts that a particle has inside home.
        This parameter is only used when use_networks is True. Default: 2."""

environment_network = {
    "default": {
        "city_area": 79.971,
        "inf_prob_weight": 1.0,
        "inf_radii": 2,
    },
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": dict},
}
"""Args:
    inf_prob_weight (float): A float value that multiplies the infection probability for particles
        at the environment layer, calculated by taking into account the available
        urban area city_area and the infection radius inf_radii. This value is
        useful for controlling the definition of contacts in the environment
        layer. Default: 1.
    city_area (flost: A float for the real urban area of the city. It must be given a value in
        $km^2$. Default: 79.971.
    inf_radii (float): A float for the infection radius, in meters. When a susceptible and an
        infectious particle distance lesser than this measure from each other, then an
        infection may occur. Default: 2."""

tracing_percent = {
    "default": 0.5,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": float},
}
"""A float for the fraction of the population in the community that has
    tacing capability. Default: 0.0."""

isol_stay_prob = {
    "default": 0.6,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": float},
}
"""A float for the probability $p$ that particles already in social isolation
    in a day remain in social isolation in the next day. The probability that
    particles not in social isolation follow social isolation is therefore $1-p$.
    Notice that even if p=0, the particles in the first age group are still
    isolated at home. In fact, it may happen that the actual fraction of the
    population being isolated is larger than the one provided because of this fact.
    However, if p<0, then no social isolation measure is applied.
    Quarantined particles are still isolated at the respective quarantine places.
    Default: 0.6."""

lockdown_adhere_percent = {
    "default": 0.7,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": float},
}
"""A float value for the fraction of the population adhering to the lockdown
    measure. During lockdown, the fraction of the population effectively being
    isolated home is given by the maximum value between lockdown_adhere_percent and
    isol_pct_time_series keyword's value for that day. Default: 0.7."""

lockdown_decision_offset = {
    "default": 3,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": float},
}
"""A float for a time period in days. This number is used to delay the decision
    on whether to start or end the lockdown measure. Default: 3."""

service_close_day_series_config = {
    "default": [],
    "triage_function": functions.get_service_close_day_series,
}
"""A list which is either empty or has three values, being the first a file
    path and the remaining two positive integers. The path is used to open a
    csv file filled with columns for services and rows with day series values
    (see use_day_series). The rows to be used in the simulation must be
    provided by using the two integers mentioned. A day series value equals to
    0 closes the service, while a day series value equals to 1 opens the service.
    If the day series value is negative, the default service close decision
    takes place (see service_close and decision). The first row must have the
    service name for that column. Default: []"""

isol_pct_time_series = {
    "default": 0.3,
    "triage_function": functions.get_time_series,
}
"""An array of floats for the day-dependent fraction of population that
    follows social isolation. Default: np.full(number_of_days, 0.3)."""

lockdown_day_series = {
    "default": -1,
    "triage_function": functions.get_time_series,
}
"""An array of values for whether must start or end at a given day. The array
    must have as many rows as the number of days in the simulation, and only
    one column. An entry equals to 0 means that the lockdown is not happening,
    while it is active for another entry value. Default: [-1, .., -1]"""

# inf0_perc MUST BE loaded after number_of_particles and decision_par_services
services = {
    "default": DEFAULT_SERVICES,
    "triage_function": functions.get_services,
}
""" List with the ordered dictionaries of parameters for the creation of each service. See
    below avaliable parameters to set in each dictionary.

### General services parameters

Args:
    name (string): A human readble name for the service.
    number (int): An integer for the total number of instances for the service
        in the real city. This number is internally normalized in terms of the
        demographic data. Default: 1
    hours (list): An array of two float values for the opening and closing time
        of the service. For example to open a service at 7 a.m. and close 7 p.m.,
        one shoud set [7, 19]. Default: [7, 16]
    days (list): An array of 7 booleans representing if the service works on that
        day of the week. Default: [0, 1, 1, 1, 1, 1, 1]
    age_groups (list): An array of integers corresponding to the age group indices
        for the age of visitors in for the service. The possible indices for these
        age groups are given in [ALL_AGES](../settings/#comorbuss.settings.ALL_AGES),
        and their labels in [AGE_DEF](../settings/#comorbuss.settings.AGE_DEF).
        Default: ALL_AGES
    visitation_period (float): The mean time $t_{srvc}$ that a particle that visits a
        service takes to do it again, in days. The value is 7, for example, corresponds
        to particles visiting the service once every week. This value is used to generate a
        probability that a particle will visit the service in a given time step. Because
        the service are opened, say in $t_{opened}$ hours of the day (set by hours,
        such probability is $p=1/(t_{srvc}*24*t_{opened}/\\Delta t)$, where $\\Delta t$
        is the time step provided by the resolution_t parameter. Default: 7.0
    isolation_visit_frac (float): Fraction that detrermines by how much visitation for particles
        in isolation shoud be reduced. Default: 0.5
    workers (list): List of dictionaries of parameters to select workers (see Workers paramerers).
        Default: [WORKERS_DEFAULTS]
    rooms (list): List of dictionaries of parameters to create rooms (see Rooms paramerers).
        Default: []
    close (bool): This service closes with social interventions (see
        [Services Closing](../../interventions/services-closing/)). Default: True
    decision (int): The decision process used to close this service, if not informed will use
        the global [`decision`](../../reference/all-parameters/#comorbuss.defaults.decision) parameter.
        Default: -1
    decision_par (int): A dict with the parameters for the [decision process](../..//interventions/decisions/)
        for this service, if not informed will use the global `decision_par_services_closing`.
        Default: {}
    inf_prob_weight (float): A float value that multiplies the infection probability
        for contacts that happened at the service.

### Workers parameters

Inside the `workers` parameters the user can set one or more dictionaries with parameters
    used to select workers of different types for the service.
Args:
    number (int): Number of this type of worker per instance of this service. Default: -1
    population_fraction (float): Total fraction of the population in age_group to be
        assigned to work in this service, to be distribuited through all instances (if this
        parameter is greater than zero the value set in `number` will be overritten). Default: -1
    age_groups (list): An array of integers corresponding to the age group indices
        for the age of workers of this type for this service. The possible indices for these
        age groups are given in [ALL_AGES](../settings/#comorbuss.settings.ALL_AGES),
        and their labels in [AGE_DEF](../settings/#comorbuss.settings.AGE_DEF).
        Default: np.arange(4, 12)
    shifts (list): A list of lists of two values. Those two values refer to the
        start and end time of working shifts of the service. For example, for two
        shifts of 6 hours the first starting at 8, one would set `[[8, 14], [14, 20]]`.
        Default: [[7, 16]]
    family_visitaion_period (float): Visitation period for the family of workers to fisit
        the workers. Default: 30
    family_age_groups (list): An array of integers corresponding to the age group indices
        for the mamily members that are allowed to visit the service. Default: []

### Rooms parameters

Inside the `rooms` parameters user shoud pass a list with templates for comorbuss to cretate rooms
    in each dictionaty the user can set `name` with a human readble name for the room, `number` with
    an opitional integer number of this room to be created in each instance of this room (if not set comorbuss
    will create rooms of this type until there is no more workers to create this room), and the names of the
    workers types to be assined to this room with and integer number of each type of workers. Example:

    classroom = {
        'name': 'Classroom',
        'Students': 20,
        'Teachers': 1,
    }
    teachers_room = {
        'name': 'Teachers room',
        'number': 1,
        'Teachers': 10
    }
    service['rooms'] = [classroom, teachers_room]

### Network encounters parameters

Args:
    net_type (int): Type of network generato to be utilized in this service, see available
        generators below. Default: NET_DEFAULT
    net_par (dict): Parameters for the generator. Default: {"m": 3}

#### NET_DEFAULT

Default network genereator is NetworkX's
    [`barabasi_albert_graph`](https://networkx.github.io/documentation/networkx-1.9/reference/generated/networkx.generators.random_graphs.barabasi_albert_graph.html)
    genrerator.

#### NET_MARKETS

Args:
    mean_contacts (float): The mean number of contacts that people have in a market.
        This value is used in the construction of networks for markets. Default: 5
    cashier_prob (float): A float for the percentage of workers in a market that
        are considered cashiers. This value is used in the construction of networks
        for markets. Default: 1.0

#### NET_SCHOOLS

Args:
    break_classrooms (int): An integer for the number of classrooms
        in a school that take breaks at same time. This value is
        used in the contruction of networks for schools. Default: 3
    break_period (float): A float for the mean time, in hours, that
        breaks take in schools. It is important to choose resolution_t
        at least as small as this value. This value is used in the
        contruction of networks for schools. Default: 0.5
    break_mean_contacts (float): A float for the mean number of contacts
        that students have during breaks at schools. This value is used
        in the contruction of networks for schools. Default: 5

#### NET_RESTAURANTS_1 and NET_RESTAURANTS_2

Args:
    waiter_prob (float): The percentage of workers in restaurants that
        are waiters. This value is used in the construction of networks
        for restaurants. Default: 1.0
    workers_mean_contacts (float): The mean number of contacts that
        workers have on restaurants. This value is used in the construction
        of networks for restaurants. Default: 3
    persons_per_table (int): The average number of persons that occupy
        a table in a restaurant. This value can be negative, in which case
        tables will be constructed according to the number of available waiters.
        This value is used in the construction of networks for restaurants.
        Default: 4

#### NET_STREET_MARKETS

Args:
    seller_mean_contacts (float): The mean number of contacts
        between people selling in street markets. It is important to
        notice that this value is applied to every hour the street market
        is opened. Default: 1.8
    visitor_mean_contacts (float): The mean number of contacts
        between people buying in street markets. Default: 15.0
    seller_visitor_mean_contacts (float): The mean number of
        contacts between people selling and people buying in street markets.
        In other words, the mean number of different places people buy when
        they go to street markets. Default: 3.0

### Distance encounters parameters

Args:
    grid_width (int): Width of the internal grid, in number of points, used
        to place particles inside the service. Default: 5
    area (float): Area of each instance of service in the real city.
        Default: 0
    move_inside (bool): Allow particles to walk inside services. Default: False
    controlled_access (bool): If False will allow particles to ramdomly
        walk in to service's limit. Default: True
    reserved_urban_area (float): Reserves a fraction of the urban area
        for this service. Default: 0.0
"""

quarantines = {
    "default": [],
    "triage_function": functions.get_quarantines,
}
""" List with the ordered dictionaries of parameters for quarantine policies to be
    used during the simulation. Default: [quarantines.HOSPITALIATION]

!!! important "Quarantine priority"
    If a particle meet the requirements for more than one quarantine comorbuss will
        give priority to the quarantine that is first in the `quarantines` list.

### Predefined quarantines

#### quarantines.HOSPITALIZATION

All symptomatic severe particles will imediatly be quarantined on Hospitals and stay
    quarantined until the particle recovers.

``` python
HOSPITALIZATION = {
    "name": "Hospitalization",
    "filter_in": (
        ("symptoms", "==", SYMPT_SEVERE), "&",
        ("states", "==", STATE_I)
        ),
    "filter_out": (
        ("states", "==", STATE_R), "|",
        ("states", "==", STATE_D)
        ),
    "placement": "Hospitals",
}
```

#### quarantines.SYMPTOMS

Symptomatic particles will stay home until they recover.

``` python
SYMPTOMS = {
    "name": "Symptoms",
    "filter_in": (
        (
            ("symptoms", "==", SYMPT_YES), "|",
            ("symptoms", "==", SYMPT_SEVERE),
        ), "&",
        ("states", "==", STATE_I)
        ),
    "filter_out": (
            ("states", "==", STATE_R), "|",
            ("states", "==", STATE_D)
            ),
    "placement": PLC_HOME,
}
```

#### quarantines.DIAGNOSTICS

Diagnosed particles will quarantine at home 1 day after the diagnostic and stay quarantined
    for 14 days.

``` python
DIAGNOSTICS = {
    "name": "Diagnostics",
    "delay": 1.0,
    "filter_in": (
        ("diag_states", "==", DIAG_YES), "&",
        ("states", "==", STATE_I)
        ),
    "filter_out": ("days", 14),
    "placement": PLC_HOME,
}
```

!!! seealso "See also"
    [Diagnostics](../diagnostics)

#### quarantines.TRACING

Particles with tracing capabilities (see [tracing_percent](../../reference/all-parameters/#comorbuss.defaults.tracing_percent))
    that had contact with diagnosed particles also with tracing capabilities in the last 5 days
    will be quarantined at home 1 day after they are identified and stay quarantined for 14 days.

``` python
TRACING_DIAGNOSED_FILTER = (
        ("last_diag_states", "!=", "diag_states"), "&",
        ("diag_states", "==", DIAG_YES)
        )

TRACING = {
    "name": "Tracing",
    "delay": 1.0,
    "filter_in": ("tracing", TRACING_DIAGNOSED_FILTER, 5),
    "filter_out": ("days", 14),
    "placement": PLC_HOME,
}
```

!!! seealso "See also"
    [Tracing](../tracing/)

### Custom quarantine parameters

Args:
    name (string): Name of the quarantine policy.
    delay (float): Delay bettwen the particle is selected to quarantine and the start of the
        quarantine in days. Default: 0.
    filter_in (tuple): A sequence of nested tuples of strings and values to be evaluated to select
        particles to enter quarantine, strings can be population attributes (see
        [particles states attributes](../../reference/population/#particles-states-attributes)),
        comparative operators, binary operators or makers (see markers bellow). Examples:

        * `("symptoms", "==", SYMPT_YES)` equivalent as `population.symptoms == SYMPT_YES`
        * `((("symptoms", "==", SYMPT_YES), "|", ("symptoms", "==", SYMPT_SEVERE)), "&",
            ("states", "==", STATE_I))` is equivalent as `((population.symptoms == SYMPT_YES) |
            (population.symptoms == SYMPT_SEVERE)) & (population.states == STATE_I)`

        Available markers for filter_in:

        * `("tracing", trace_filters, number_of_days)` will select all particles that had tracable contact
            with particles selected by trace_filters in the last number_of_days days.
        * `("service", service_name)` will be replaced with the id of the service with service_name.
        * `("workers", service_name, workers_name)` will be replaced with the id of the workers with
            workers_name type on service with service_name.
        * `("diagnostic", diagnostics_name)` will be replaced with the id of the diagnostic with diagnostics_name.

        **ATTENTION:** Do NOT use python's normal boolean operators (`and`, `or`, `not`, etc), all operations
            are applied on numpy boolean arrays and the apropriate operators to use are binary operators
            (`&`, `|`, `~`, etc).

        Default: `((("symptoms", "==", SYMPT_YES), "|", ("symptoms", "==", SYMPT_SEVERE),), "&",
            ("states", "==", STATE_I))`
    filter_out (tuple): Filters to select particles to end quarantine. See filter_in.

        Available markers for filter_out:

        * `("days", number_of_days)` Quarantine will end after number_of_days.

        Default: `(("states", "==", STATE_R), "|", ("states", "==", STATE_D))`
    placement (string or int): Placement marker or name of the servive for the place where the particle
        shoud quarantine. Default: PLC_HOME
    confine_particles (bool): If true particle will not work or visit services. Default: True
    allow_requarantine (bool): If true allow partices to be quarantined more than one time in this
        quarantine. Default: False
"""

diagnostics = {"default": []}
""" List with the ordered dictionaries of parameters for diagnostics policies to be
    used during the simulation. Default: [diagnostics.SEROLOGICAL]

### Predefined diagnostics

#### diagnostics.SEROLOGICAL

All symptomatic particles be tested with sensitivity of 86%.

``` python
SEROLOGICAL = {
    "name": "Serological",
    "sensitivity": 0.86,
    "specificity": 0.90,
}
```

#### diagnostics.PCR

All symptomatic particles be tested with sensitivity of 90% and a 2 days delay for
    results.

``` python
PCR = {
    "name": "PCR",
    "result_delay": 2.0,
    "sensitivity": 0.90,
    "specificity": 1.0,
}
```

### Custom diagnostic parameters

Arguments:
    name (string): Name of the diagnostic.
    filter_particles (tuple): A sequence of nested tuples of strings and values to be evaluated to select
        particles to apply diagnostic, strings can be population attributes (see
        [particles states attributes](../../reference/population/#particles-states-attributes)),
        comparative operators, binary operators or makers (see markers bellow). Examples:

        * `("symptoms", "==", SYMPT_YES)` equivalent as `population.symptoms == SYMPT_YES`
        * `((("symptoms", "==", SYMPT_YES), "|", ("symptoms", "==", SYMPT_SEVERE)), "&",
            ("states", "==", STATE_I))` is equivalent as `((population.symptoms == SYMPT_YES) |
            (population.symptoms == SYMPT_SEVERE)) & (population.states == STATE_I)`

        Available markers for filter_particles:

        * `("tracing", trace_filters, number_of_days)` will select all particles that had tracable contact
            with particles selected by trace_filters in the last number_of_days days.
        * `("service", service_name)` will be replaced with the id of the service with service_name.
        * `("workers", service_name, workers_name)` will be replaced with the id of the workers with
            workers_name type on service with service_name.

        **ATTENTION:** Do NOT use python's normal boolean operators (`and`, `or`, `not`, etc), all operations
            are applied on numpy boolean arrays and the apropriate operators to use are binary operators
            (`&`, `|`, `~`, etc).

        Default: `("symptoms", "==", SYMPT_YES), "|", ("symptoms", "==", SYMPT_SEVERE)`
    number_per_day (int): Number of tests to be applied in the selected population, particles selected with
        filter_particles will be selected ramdomly to test if there isn't enough tests. If set to -1 will
        test all particles selected with filter_particles. Default: -1
    sensitifity (float): Sensitivity value for the test in the [0., 1.] interval. Default: 0.85
    specificity (float): Specificity value for the test in the [0., 1.] interval. Default: 0.96
    test_delay (float): Time in days between a particle is selected to be tested and the test is made.
        Default: 0.0
    result_delay (float): Time in days between a particle is tested and the result is available. Default: 0.0
    allow_retest (str): Allow particles to retest the test, available settings:

        * "yes": All particles can retest.
        * "no": retests are not allowed.
        * "negative": Particles with negative results can retest.

        Default: "no"
    retest_delay (float): Time since last result for a particle to be alloed retest. Default: 2.0
    start_day (int): Days from the start of the simulation to start using this diagnostic. Default: 0
    end_day (int): Days from the start of the simulation to stop using this diagnostic. If set to
        -1 this diagnostic will be used up to the end of the simulation. Default: -1
"""

store_immunity_coefficients = {
    "default": False,
    "triage_function": functions.type_get,
    "triage_parameters": {"dtype": bool},
}
"""If set to true and there is a vaccination acctive will store time series for immunity coefficients."""


vaccinations = {"default": []}
""" List with the ordered dictionaries of parameters for vaccination policies to be
    used during the simulation. Default: []

### Predefined vaccinations

!!! seealso
    Predefined vaccinations uses [predefined filters](#predefined-filters).

#### vaccination.SENIORS

Apply the deafult number of doses on senior citzens (age groups 12 to 21, 60+ years old).

``` python
SENIORS = {
    "name": "Senior citizens",
    "filter": (FILTER_SENIORS, "&", FILTER_DIAG_NO),
}
```

#### vaccination.ADULTS

Apply the deafult number of doses on adults citzens (age groups 5 to 21, 20+ years old).

``` python
ADULTS = {
    "name": "Adult citizens",
    "filter": (FILTER_ADULTS, "&", FILTER_DIAG_NO),
}
```

#### vaccination.CHILDRENS

Apply the deafult number of doses on child citzens (age groups 0 to 4, 0-19 years old).

``` python
CHILDRENS = {
    "name": "Child citizens",
    "filter": (FILTER_CHILDRENS, "&", FILTER_DIAG_NO),
}
```

#### vaccination.DIAG_NO

Apply the deafult number of doses on citzens that where not diagnosed.

``` python
DIAG_NO = {
    "name": "Not dignosed",
    "filter": FILTER_DIAG_NO,
}
```

#### vaccination.MAKETS_WORKERS

Apply the deafult number of doses on markets workers.

``` python
MAKETS_WORKERS = {
    "name": "Markets workers",
    "filter": (FILTER_MARKET_WORKERS, "&", FILTER_DIAG_NO),
}
```

#### vaccination.HOSPITALS_WORKERS

Apply the deafult number of doses on hospitals workers.

``` python
HOSPITALS_WORKERS = {
    "name": "Hospitals workers",
    "filter": (FILTER_HOSPITALS_WORKERS, "&", FILTER_DIAG_NO),
}
```

### Custom vaccinations parameters

Args:
    name (string): Name of the vaccination policy.
    start_day (int): Day from the start of the simulation to start the vaccination policy.
        Default: 0.
    days_to_immunity (float): Number of days for vaccinated particles to became immune.
        Default: 30.0.
    effectiveness (float): Fraction of vaccianted particles to became immune.
        Default: 1.0.
    filter (tuple): A sequence of nested tuples of strings and values to be evaluated to select
        particles to vaccinate, strings can be population attributes (see
        [particles states attributes](../../reference/population/#particles-states-attributes)),
        comparative operators, binary operators or makers (see markers bellow). Examples:

        * `("diag_states", "!=", DIAG_YES)` equivalent as `population.diag_states != DIAG_YES`,
            and will select all particles with a dianostins states diferent than `DIAG_YES`
            that is a positive disgnostic.
        * `((("symptoms", "==", SYMPT_YES), "|", ("symptoms", "==", SYMPT_SEVERE)), "&",
            ("diag_states", "!=", DIAG_YES))` is equivalent as `((population.symptoms == SYMPT_YES)
            | (population.symptoms == SYMPT_SEVERE)) & (population.diag_states != DIAG_YES)`, and
            will select all sympomatic particles (severe or mild) that has a a dianostins
            states diferent than `DIAG_YES`.
        * `(("diag_states", "!=", DIAG_YES), "&", ("ages", "isin", list(range(12, 22))))` is
            equivalent as `((population.diag_states != DIAG_YES) & (np.isin(population.ages, list(range(12, 22)))))`,
            and will select  all particles with a dianostins states diferent than `DIAG_YES` in
            age groups from 12 to 21 (60+ years old).
        * `("workplace_id", "==", ("service", "Markets"))`  is equivalent as `(population.workplace_id == services["Markets"].id)`,
            and will select all Markets workers.
        * `(("workplace_id", "==", ("service", "SCHOOLS")), "&", ("worker_type", "==",
            ("workers", "Schools", "Teachers")))`  is equivalent as `(population.workplace_id == services["Schools"].id)
            & (population.worker_type == services["Schools"].workers_parameters["Teachers"].id)`,
            and will select all school's teachers.

        Available markers for filter_in:

        * `("service", service_name)` will load the id of the service with the name `service_name`.
        * `("workers", service_name, workers_name)` will load the id of the worker type with the
            name `workers_name` of the service with the name `service_name`.
        * `("tracing", trace_filters, number_of_days)` will select all particles that had tracable
            contact with particles selected by trace_filters in the last number_of_days days.

        Default: `FILTER_DIAG_NO`

        **ATTENTION:** Do NOT use python's normal boolean operators (`and`, `or`, `not`, etc), all operations
            are applied on numpy boolean arrays and the apropriate operators to use are binary operators
            (`&`, `|`, `~`, etc).
    doses_per_day (int): Number of available doses per day for this policy (this number will be normalized
        with the number of particles). Default: 1000.

### Predefined filters

#### vaccination.FILTER_DIAG_NO

Selects citzens that where not diagnosed.

``` python
FILTER_DIAG_NO = ("diag_states", "!=", DIAG_YES)
```

#### vaccination.FILTER_SENIORS

Selects senior citzens (age groups 12 to 21, 60+ years old).

 ``` python
FILTER_SENIORS = ("ages", "isin", list(range(12, 22)))
```

#### vaccination.FILTER_ADULTS

Selects adults citzens (age groups 5 to 21, 20+ years old).

``` python
FILTER_ADULTS = ("ages", "isin", list(range(5, 22)))
```

#### vaccination.FILTER_CHILDRENS

Selects child citzens (age groups 0 to 4, 0-19 years old).

``` python
FILTER_CHILDRENS = ("ages", "isin", list(range(0, 5)))
```

#### vaccination.FILTER_MARKET_WORKERS

Selects markets workers.

``` python
FILTER_MARKET_WORKERS = ("workplace_id", "==", ("service", "Markets"))
```

#### vaccination.FILTER_HOSPITALS_WORKERS

Selects hospitals workers.

``` python
FILTER_HOSPITALS_WORKERS = ("workplace_id", "==", ("service", "Hospitals"))
```

"""
