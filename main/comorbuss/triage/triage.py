import importlib
import numpy as np
from numpy.random import default_rng
from .. import settings as S
from . import defaults
from ..modules import MODULES_INIT_SEQ
from .. import modules
from ..tools import recursive_copy
from .triage_functions import type_get, get_value, get_module_params


def triage_parameters(parameters, comm=None, gen_pop=True):
    given_parameters = recursive_copy(parameters)
    parameters = dict()
    ########################################################################################
    ################################ Parameters input ######################################
    ########################################################################################

    if comm:
        event_log = comm.event_log
    else:
        event_log = lambda *_: None

    r_seed = type_get(
        given_parameters,
        "random_seed",
        defaults.random_seed["default"],
        parameters,
        None,
        event_log,
        dtype=int,
    )
    parameters["random_seed"] = r_seed
    np.random.seed(r_seed)
    rng = default_rng(r_seed)
    parameters["gen_pop"] = gen_pop

    # Add modules to skip list
    skip = defaults.skip
    skip += list(dir(modules))
    # Iterate through all parameters in defaults.py
    for p_name in vars(defaults):
        if p_name not in skip:
            default = getattr(defaults, p_name)
            if "store_name" in default:
                store_name = default["store_name"]
            else:
                store_name = p_name
            if "triage_parameters" in default:
                triage_parameters = default["triage_parameters"]
            else:
                triage_parameters = {}
            parameters[store_name] = default["triage_function"](
                given_parameters,
                p_name,
                default["default"],
                parameters,
                rng,
                event_log,
                **triage_parameters,
            )

    remaining_param = list(given_parameters.keys())
    parameters["modules"] = []
    loaded_modules = []
    for module in MODULES_INIT_SEQ:
        if module in remaining_param:
            name = module
            module = importlib.import_module("comorbuss.modules.{}".format(module))
            module_param = get_value(given_parameters, name, module.DEFAULTS)
            # Check if the module has it's own triage function
            if hasattr(module, "triage"):
                module_param = module.triage(
                    module_param,
                    parameters,
                    parameters["clk"],
                    event_log,
                )
            # If not check if it has default types and triage with it
            elif hasattr(module, "DEFAULTS_TYPES"):
                module_param = get_module_params(
                    module_param,
                    module.DEFAULTS,
                    module.DEFAULTS_TYPES,
                    name,
                    event_log,
                )
            loaded_modules.append(name)
            parameters["modules"].append((module, module_param))
            event_log("Loaded {} module.".format(module.__name__), S.MSG_PRGS)

    remaining_param = list(given_parameters.keys())
    if len(remaining_param):
        event_log(
            "{:} parameters are not valid and where not used.".format(remaining_param),
            S.MSG_WRNG,
        )
    scenario_id = "S"
    if parameters["services_close"]:
        scenario_id += "C"
    if parameters["social_isolation"]:
        scenario_id += "Si"
    if (len(parameters["quarantines"]) >= 2) or (
        len(parameters["quarantines"]) == 1
        and parameters["quarantines"][0]["name"].lower() != "hospitalization"
    ):
        scenario_id += "Q"
        for q in parameters["quarantines"]:
            if q["name"].lower() == "tracing":
                scenario_id += "T"
                break
    if parameters["lockdown"]:
        scenario_id += "L"
    if scenario_id == "":
        scenario_id = "noMech"

    parameters["scenario_id"] = scenario_id

    return parameters, parameters["clk"], rng
