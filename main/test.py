## THIS SCRIPT IS OUT OF DATE AND PROBABLY WILL NOT RUN

import os, time, datetime
import numpy as np
from comorbuss import community, decisions
from comorbuss.settings import *
from comorbuss import services, quarantines, vaccinations
from comorbuss import tools, Analysis
import networkx as nx

fixed_parameters = {
    "random_seed": 12,
    "number_of_particles": 1000,
    "number_of_days": 60,
    "log_progress": False,
}

store_data = True
out_direc = os.path.join(OUT_DIR, "test")

test_configurations = [
    {
        "name": "Meanfield",
        "parameters": {
            "simulate_meanfield": True,
            },
        "run_after": [
            "comm.plot_hour(5, filename=case+\"_hour_5.pdf\")",
            "comm.plot_hour(15, filename=case+\"_hour_15.pdf\")",
            "analysis = Analysis.from_comm(comm)",
            "analysis.plot_SEIR(filename=case+\".pdf\", events=True)",
            "analysis.close()",
        ],
        "comments": "Particles shoud move every hour, a low number of infections is expected."
    },
    {
        "name": "Distance mechanics",
        "parameters": {
            "use_networks": False,
            },
        "run_after": [
            "comm.plot_hour(5, filename=case+\"_hour_5.pdf\")",
            "comm.plot_hour(8, filename=case+\"_hour_8.pdf\")",
            "comm.plot_hour(15, filename=case+\"_hour_15.pdf\")",
            "analysis = Analysis.from_comm(comm)",
            "analysis.plot_SEIR(filename=case+\".pdf\", events=True)",
            "analysis.close()",
        ],
        "comments": "Particles be at home on the night, on services on the day."
    },
    {
        "name": "Quarantine OFF",
        "parameters": {
            "diagnostics_positive_prob": 0.5,
            },
        "run_after": [
            "analysis = Analysis.from_comm(comm)",
            "analysis.plot_SEIR(filename=case+\".pdf\", events=True)",
            "analysis.plot_quarantine(filename=case+\"_quarantine.pdf\")",
            "analysis.close()",
        ],
        "comments": "To be compared with Quarantine ON. Particles shoud be only quarantined at hospitals."
    },
    {
        "name": "Quarantine ON",
        "parameters": {
            "quarantines": [
                quarantines.HOSPITALIZATION,
                quarantines.DIAGNOSTICS,
                quarantines.SYMPTOMS
                ],
            "diagnostics_positive_prob": 0.5,
            },
        "run_after": [
            "analysis = Analysis.from_comm(comm)",
            "analysis.plot_SEIR(filename=case+\".pdf\", events=True)",
            "analysis.plot_quarantine(filename=case+\"_quarantine.pdf\")",
            "analysis.close()",
        ],
        "comments": "The number of infections shoud decrease from Quarantine OFF. Quarantined particles at home and hospital."
    },
    {
        "name": "Quarantine TRACING",
        "parameters": {
            "quarantines": [
                quarantines.HOSPITALIZATION,
                (quarantines.DIAGNOSTICS, {"filter_out": quarantines.OUT_CURED}),
                quarantines.SYMPTOMS,
                quarantines.TRACING
                ],
            "diagnostics_positive_prob": 0.5,
            "tracing_percent": 0.5,
            },
        "run_after": [
            "analysis = Analysis.from_comm(comm)",
            "analysis.plot_SEIR(filename=case+\".pdf\", events=True)",
            "analysis.plot_quarantine(filename=case+\"_quarantine.pdf\")",
            "analysis.close()",
        ],
        "comments": "The number of infections shoud further decrease. Quarantined particles at home, hospital and tracing."
    },
    {
        "name": "Social Isolation OFF",
        "parameters": {
            "social_isolation": False,
            "services": [services.MARKETS, services.HOSPITALS],
            },
        "run_after": [
            "analysis = Analysis.from_comm(comm)",
            "analysis.plot_SEIR(filename=case+\".pdf\", events=True)",
            "analysis.plot_isolation_percentage(filename=case+\"_isol.pdf\")",
            "analysis.close()",
        ],
        "comments": "To be compared with Social Isolation ON."
    },
    {
        "name": "Social Isolation loaded from file",
        "parameters": {
            "social_isolation": True,
            "isol_pct_time_series": np.loadtxt(os.path.join(CONFIGS_DIREC, 'isolation_percent_time_series_example.txt')),
            "services": [services.MARKETS, services.HOSPITALS],
            },
        "run_after": [
            "analysis = Analysis.from_comm(comm)",
            "analysis.plot_SEIR(filename=case+\".pdf\", events=True)",
            "analysis.plot_isolation_percentage(filename=case+\"_isol.pdf\")",
            "analysis.close()",
        ],
        "comments": "The number of infections shoud decrease from Social Isolation OFF. Isolation curve shoud be loaded."
    },
    {
        "name": "Social Isolation with lockdown",
        "parameters": {
            "social_isolation": True,
            "lockdown": True,
            "decision_par_lockdown": {
                    "start_frac": 0.01,
                    "stop_frac": 0.005,
                },
            "isol_pct_time_series": np.loadtxt(os.path.join(CONFIGS_DIREC, 'isolation_percent_time_series_example.txt')),
            "services": [services.MARKETS, services.HOSPITALS],
            },
        "run_after": [
            "analysis = Analysis.from_comm(comm)",
            "analysis.plot_SEIR(filename=case+\".pdf\", events=True)",
            "analysis.plot_isolation_percentage(filename=case+\"_isol.pdf\")",
            "analysis.close()",
        ],
        "comments": "The number of infections shoud shoud further decrease. Isolation curve shoud be flat on lockdown days."
    },
    {
        "name": "Services don't close",
        "parameters": {
            "services_close": False,
            "services": services.DEFAULT_SERVICES+[services.GENERIC],
            },
        "run_after": [
            "analysis = Analysis.from_comm(comm)",
            "analysis.plot_SEIR(filename=case+\".pdf\", events=True)",
            "analysis.close()",
        ],
        "comments": "To be compared with services close."
    },
    {
        "name": "Services close",
        "parameters": {
            "services_close": True,
            "services": services.DEFAULT_SERVICES+[services.GENERIC],
            },
        "run_after": [
            "analysis = Analysis.from_comm(comm)",
            "analysis.plot_SEIR(filename=case+\".pdf\", events=True)",
            "analysis.close()",
        ],
        "comments": "Infections shoud decrease, closing and reopening events shoud be noted."
    },
    {
        "name": "Social Isolation + Lockdowns + Closing of services",
        "parameters": {
            "services_close": True,
            "social_isolation": True,
            "lockdown": True,
            "services": services.DEFAULT_SERVICES+[services.GENERIC],
            "isol_pct_time_series": np.loadtxt(os.path.join(CONFIGS_DIREC, 'isolation_percent_time_series_example.txt')),
            },
        "run_after": [
            "analysis = Analysis.from_comm(comm)",
            "analysis.plot_SEIR(filename=case+\".pdf\", events=True)",
            "analysis.close()",
        ],
        "comments": "Infections shoud further decrease, without enviromental layer it might increase."
    },
    {
        "name": "vaccinations OFF",
        "parameters": {
            "vaccinations": [],
            },
        "run_after": [
            "analysis = Analysis.from_comm(comm)",
            "analysis.plot_SEIR(filename=case+\".pdf\", events=True)",
            "analysis.close()",
        ],
        "comments": "To be compared with vaccinations ON."
    },
    {
        "name": "vaccinations ON",
        "parameters": {
            "vaccinations": [
                (vaccinations.DIAG_NO, {"doses_per_day": 10000}),
                (vaccinations.SENIORS, {"doses_per_day": 5000}),
                (vaccinations.HOSPITALS_WORKERS, {"doses_per_day": 100})
                ]
            },
        "run_after": [
            "analysis = Analysis.from_comm(comm)",
            "analysis.plot_SEIR(filename=case+\".pdf\", events=True)",
            "analysis.close()",
        ],
        "comments": "Infections shoud decrease."
    },
]

##### Test configurations ends here!!!!
def run_list(l):
    for line in l:
        exec(line)

def mkdir(direc):
    if os.path.isdir(direc) == False:
            os.makedirs(direc)

mkdir(out_direc)
date = datetime.datetime.fromtimestamp(int(time.time())).strftime("%Y-%m-%d_%H:%M:%S")
out_direc = os.path.join(out_direc, date)
mkdir(out_direc)

for conf in test_configurations:
    print("Running {}:".format(conf["name"]))
    print("")
    parameters = dict(fixed_parameters)
    out = os.path.join(out_direc, conf["name"].replace(" ", "_"))
    mkdir(out)
    case = os.path.join(out, "plot")
    for par in conf["parameters"]:
        parameters[par] = conf["parameters"][par]
    if "run_before" in conf:
        run_list(conf["run_before"])
    comm = community(**parameters)
    comm.simulate()
    if "run_after" in conf:
        run_list(conf["run_after"])
    if store_data:
        f = os.path.join(out, "{}.hdf5".format(conf["name"].replace(" ", "_")))
        tools.save_hdf5([comm], f)
    if "comments" in conf:
        print(conf["comments"])
    print("")