# COMORBUSS

[![License: AGPL v3](https://img.shields.io/badge/License-AGPLv3-green.svg?style=flat-square)](https://www.gnu.org/licenses/agpl-3.0.en.html)  [![Release](https://img.shields.io/badge/dynamic/json?color=blueviolet&label=Release&query=%24%5B0%5D.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F18979538%2Frepository%2Ftags&style=flat-square)](https://gitlab.com/ggoedert/comorbuss/)

COMORBUSS is a detailed agent-based stochastic simulator for disease propagation driven by social iterations, providing clear evaluation of the effect of each community component on viral spread.

## Documentation

COMORBUSS documentation can be found at [docs.comorbuss.org](https://docs.comorbuss.org/).

## Dependencies

### For simulation and output

Basic dependencies for running simulations:

* Python (tested with version 3.12.4)
* NumPy (version 2.0 or newer)
* Numba
* Matplotlib
* Seaborn
* h5dict
* Pandas
* SciPy
* portion
* NetworkX
* tqdm
* setuptools (for installation as a module)

## Instalation

To install COMORBUSS as a module install dependencies as shown in the [documentation](http://docs.comorbuss.org/stable/quick-guide/#for-simulation-and-output), than clone this repository, navigate to the main directory and run the `setup.py` script:

    git clone https://gitlab.com/ggoedert/comorbuss.git
    cd comorbuss/main
    python setup.py install

## COMORBUS team

### Current Development Team

- Prof. Guilherme Goedert (FGV - EMAp)
- Juliano Genari (FGV - EMAp)

### Collaborators

- Prof. Claudio Struchiner (FGV - EMAp)
- Prof. Lara Coelho (INI - Fiocruz)
- Prof. Tiago Pereira (ICMC - USP São Carlos)
- Prof. Krerley Oliveira (UFAL)
- Prof. Sérgio Lira (UFAL)

### Former Members

- Ismael Ledoino (LNCC - Laboratório Nacional de Computação Científica)
- Rafael Terra (LNCC - Laboratório Nacional de Computação Científica)
- Lucas Resende (IMPA - Instituto de Matemática Pura e Aplicada)
- Edmilson Roque (ICMC - USP São Carlos)

## Acknowledgments

This project was partly financed by Instituto Serrapilheira and CNPq (grant 403679/2020-6) as part of the [ModCOVID19](http://www.cemeai.icmc.usp.br/ModCovid19/) project.